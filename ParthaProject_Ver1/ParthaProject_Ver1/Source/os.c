/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name:OS schedule                                                                                                         */
/* Author:Karthik                                                                                                                     */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file os.c
*   @brief This file contains API's which are required to schedule the OS tasks for 1ms,5ms,10ms,20ms,50ms,100ms Tasks.
*
*   @details OS.
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | KARTHIK
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 01-07-2021
*/

/**@{*/
/********************************************************************************************************************************/
/* Include Section                                                                                                              */
/********************************************************************************************************************************/

#include "os.h"
#include "FreeRTOS.h"
#include "task.h"
#include "../ParthaProj.X/cmu.h"
#include "../ParthaProj.X/bms.h"
#include "../ParthaProj.X/soc.h"


/********************************************************************************************************************************/
/* Variables Section - GLOBAL                                                                                                   */
/********************************************************************************************************************************/

/** @fn       void Task_1ms(void)
*   @brief    OS task for every 1ms
*
*
*   @details  Executes task for every 1ms time slice.
*
*/ 
void Task_1ms(void)
{
	const int xDelay=pdMS_TO_TICKS(1);
	for(;;)
	{
        BMS_Trigger();
		vTaskDelay(xDelay);
	}
}


/** @fn       void Task_5ms(void)
*   @brief    OS task for every 5ms
*
*
*   @details  Executes task for every 5ms time slice.
*
*/ 
void Task_5ms(void)
{
	const int xDelay=pdMS_TO_TICKS(5);
	for(;;)
	{
		vTaskDelay(xDelay);
	}
}


/** @fn       void Task_10ms(void)
*   @brief    OS task for every 10ms
*
*
*   @details  Executes task for every 10ms time slice.
*
*/ 
void Task_10ms(void)
{
	const int xDelay=pdMS_TO_TICKS(10);
	for(;;)
	{
		vTaskDelay(xDelay);
	}
}

/** @fn       void Task_50ms(void)
*   @brief    Os task for every 50ms
*
*
*   @details  Executes task for every 50ms time slice.
*
*/ 
void Task_50ms(void)
{
	const int xDelay=pdMS_TO_TICKS(50);
	for(;;)
	{
		vTaskDelay(xDelay);
	}
}

/** @fn       void Task_100ms(void)
*   @brief    OS task for every 100ms
*
*
*   @details  Executes task for every 100ms time slice.
*
*/
void Task_100ms(void)
{
	const int xDelay=pdMS_TO_TICKS(100);
	for(;;)
	{      
        BAL_Trigger();
        CONT_Trigger();
        CMU_Trigger();
		vTaskDelay(xDelay);
	}
}

/** @fn       void Task_1000ms(void)
*   @brief    OS task for every 1000ms
*
*
*   @details  Executes task for every 1000ms time slice.
*
*/
void Task_1000ms(void)
{
	const int xDelay=pdMS_TO_TICKS(1000);
	for(;;)
	{
        CAN_Stack_Trigger();
        SOC_CalculateKalmanFilter();
        Soh_Calculate();
		vTaskDelay(xDelay);
	}
}

/** @fn       void Os_Schedule()
*   @brief    Create OS TASKS for different time slices(i.e. 5ms, 10ms, 50ms,100ms)
*             and start the schedular.  
*   @details  Executes task for every 50ms time slice.
*
*/ 
void Os_Schedule()
{
    /* Task create for 1ms */
    xTaskCreate((void *)Task_1ms,( const char * const )"100mstask",100,(void*)0,1,0);
          
    /* Task create for 10ms */
    xTaskCreate((void *)Task_10ms,( const char * const )"10mstask",100,(void*)0,1,0);
     
    /* Task create for 100ms */
    xTaskCreate((void *)Task_100ms,( const char * const )"100mstask",100,(void*)0,1,0);
      
    /* Task create for 1000ms */
    xTaskCreate((void *)Task_1000ms,( const char * const )"100mstask",100,(void*)0,1,0);
    
    /*Start the schedular*/
    vTaskStartScheduler();
    
    while(1);
    
    
}