/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name: OS                                                                                                         */
/* Author:KARTHIK                                                                                                                     */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file os.h
*   @brief This file contains structure declaration,variable.
*
*   @details OS Driver.
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | KARTHIK
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 01-07-2021
*/


/********************************************************************************************************************************/
/* Function declaration                                                                                                  */
/********************************************************************************************************************************/

void Os_Schedule();