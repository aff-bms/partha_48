/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name:CMU(BQ79656-Q1)                                                                                                          */
/* Author:Pavithran                                                                                                                     */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file CMU.c
*   @brief This file contains API's which are required to communicate with CMU via UART.
*
*   @details CMU Driver.
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Pavithran
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 22-06-2021
*/

/** @defgroup CMU
*
* The  Texas  Instruments  BQ79656  device  is  a highly  integrated,  high-accuracy  
* battery  monitor and  protector  for  16-series  Li-ion battery  packs. The device 
* includes a high-accuracy monitoring system, a highly configurable protection subsystem, 
* and support for autonomous controlled cell balancing and a host communication peripheral 
* supporting UART/SPI interface.
*
*/

/**@{*/
/********************************************************************************************************************************/
/* Include Section                                                                                                              */
/********************************************************************************************************************************/
#include "cmu.h"
#include "mcc_generated_files/pin_manager.h"
#include "uart_app.h"
#include "database.h"
#include "crc16.h"

/********************************************************************************************************************************/
/* Define Section - LOCAL                                                                                                       */
/********************************************************************************************************************************/
/**
 * Saves the last state and the last substate
 */
#define CMU_SAVELASTSTATES()    cmu_st_state.laststate = cmu_st_state.state; \
                                cmu_st_state.u8_lastsubstate = cmu_st_state.u8_substate

#define CMU_STATE_RETRY                         (10)
#define NO_OF_FAULT_REGISTERS                   (39)
#define NO_OF_MEASUREMENT_DATA_REGISTERS        (32)
#define T_HLD_WAKE_TIME                         (36)
#define MAX_WRITE_VALUE                         (127)

/********************************************************************************************************************************/
/* Variables Section - GLOBAL                                                                                                   */
/********************************************************************************************************************************/
/*cmu state machine*/
static st_CMU_STATE cmu_st_state;
/*cmu config structure*/
st_bcc_drv_config cmu_st_config;
/*cmu battery data*/
static st_pdc_bms_pack cmu_st_batPack[TOTALBOARDS];
/*battery min and max data*/
static DATA_BLOCK_MINMAX_s cmu_st_bms_minmax;
/*bms cell voltages*/
static DATA_BLOCK_CELLVOLTAGE_s cmu_st_bms_cellvolt;

/* Initial configuration of cmu. */
static const st_cmu_init_reg cmu_st_init_config[CMU_INIT_CONFIG_REG_CNT] = 
{
    {ADC_CTRL1,ADC_CTRL1_CONFIG_VALUE},  /*ADC configuration*/
    {OV_THRESH, CMU_CONFIG_OV_TH_VALUE},/*over voltage configuration*/
    {UV_THRESH, CMU_CONFIG_UV_TH_VALUE},/*under voltage configuration*/
    {OTUT_THRESH, CMU_CONFIG_OTUT_TH_VALUE},/*over under temperature configuration*/
    {OVUV_CTRL, CMU_OVUV_CTRL_CONFIG_VALUE},/*over under voltage control sconfiguration*/
    {OTUT_CTRL, CMU_OTUT_CTRL_CONFIG_VALUE}/*over under temperature control configuration*/
};

/*cmu boards,frame buffers*/
u8 cmu_u8_total_boards=TOTALBOARDS;
u8 cmu_u8_currentBoard;
u8 cmu_u8_currentCell;
u8 cmu_u8_bReturn;
u8 cmu_u8_bRes;
u8 cmu_u8_bBuf[8];
u8 cmu_u8_pFrame[64];
u8 cmu_u8_balancing_state[16];


/********************************************************************************************************************************/
/* Function Implementations Section - GLOBAL                                                                                    */
/********************************************************************************************************************************/
/** @fn       CMU_initRegisters(bcc_drv_config_t* const drvConfig)
*   @brief    CMU Initialization 
*
*
*   @details  CMU Initialization before communicating with CMU
*
*   @pre      system initialization
*   @post     
*   @param    drvConfig cmu configuration structure
*   @return   initialization of cmu status either success/failure
*/
e_bcc_status CMU_Init_Registers(st_bcc_drv_config* const drvConfig)
{
    u8 u8_cid=0,u8_i=0;
    e_bcc_status status;
    
    if(drvConfig==NULL)
    {
        status=CMU_STATUS_FAILURE;
    }
    else
    {
        for (u8_cid = 0; u8_cid < drvConfig->u8_devicesCnt; u8_cid++)
        {
            for (u8_i = 0; u8_i < CMU_INIT_CONFIG_REG_CNT; u8_i++)
            {
                status = CMU_Reg_Write(u8_cid,cmu_st_init_config[u8_i].u16_address, cmu_st_init_config[u8_i].u16_value,1, FRMWRT_SGL_W);
                if (status != CMU_STATUS_SUCCESS)
                {
                    break;
                }
            }
            if (status != CMU_STATUS_SUCCESS)
            {
                break;
            }
        }
    }

    return status;
}

/** @fn       e_bcc_status CMU_Init(bcc_drv_config_t* const drvConfig)
*   @brief    CMU global variable Initialization 
*
*
*   @details  CMU global variable Initialization before communicating with CMU
*
*   @pre      system initialization
*   @post     
*   @param    void
*   @return   void
*/
void CMU_App_Init(void)
{
    /*cmu state machine config*/
    cmu_st_state.u16_timer               = 0,
    cmu_st_state.statereq                = CMU_STATEMACH_INIT_REQUEST,
    cmu_st_state.state                   = CMU_STATEMACH_UNINITIALIZED,
    cmu_st_state.u8_substate             = 0,
    cmu_st_state.laststate               = CMU_STATEMACH_UNINITIALIZED,
    cmu_st_state.u8_lastsubstate         = 0,
    cmu_st_state.u8_ErrRetryCounter      = 0,
    cmu_st_state.u8_ErrRequestCounter    = 0,
    cmu_st_state.u8_balance_control_done = 0,

   /*cmu config structure*/
    cmu_st_config.u8_drvInstance =1,
    cmu_st_config.u8_devicesCnt=cmu_u8_total_boards,

    cmu_u8_currentBoard = 0;
    cmu_u8_currentCell = 0;
    cmu_u8_bReturn = 0;
    cmu_u8_bRes = 0;
    //cmu_u8_bBuf[8];
    //cmu_u8_pFrame[64];
    //cmu_u8_balancing_state[16];
}

/** @fn       e_bcc_status CMU_Init(bcc_drv_config_t* const drvConfig)
*   @brief    CMU Initialization 
*
*
*   @details  CMU Initialization before communicating with CMU
*
*   @pre      system initialization
*   @post     
*   @param    drvConfig cmu configuration structure
*   @return   initialization of cmu status either success/failure
*/
e_bcc_status CMU_Init(void)
{
    /*for initializing all global variables*/
    CMU_App_Init();
    
    /*wake up cmu incase if it is in sleep mode*/
    CMU_WakeUp();
    
    /*cmu software reset*/
    CMU_Soft_Reset();
    
    /*ENABLE AUTO ADDRESSING MODE*/
    CMU_Reg_Write(0, CONTROL1, 0X01, 1, FRMWRT_ALL_W);

    /*SET ADDRESSES FOR EVERY BOARD*/
    for(cmu_u8_currentBoard=0; cmu_u8_currentBoard<cmu_u8_total_boards; cmu_u8_currentBoard++)
    {
        CMU_Reg_Write(0, DIR0_ADDR, cmu_u8_currentBoard, 1, FRMWRT_SGL_W);
    }
    
    if(cmu_u8_total_boards==1)
    {
        /*if there's only 1 board, it's the base and top of stack, so change it to those*/
        CMU_Reg_Write(0, COMM_CTRL, 0x01, 1, FRMWRT_SGL_W);
    }
    
    /*CMU register init*/
    CMU_Init_Registers(&cmu_st_config);
    
    /*Cell balance configuration*/
    CMU_Cell_Balance_Configuration();
    
    /*RESET ANY COMM FAULT CONDITIONS FROM STARTUP*/
    CMU_Reg_Write(0, FAULT_RST2, 0x03, 1, FRMWRT_ALL_W);
    
    return CMU_STATUS_SUCCESS;
}

/** @fn       e_bcc_status CMU_Soft_Reset(void)
*   @brief    cmu soft reset 
*
*
*   @details   cmu soft reset  
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    void
*   @return   status either success/failure
*/
e_bcc_status CMU_Soft_Reset(void)
{
    /*Reset device*/
    return CMU_Reg_Write(0, CONTROL1, 0x02, 1, FRMWRT_SGL_W);
}

/** @fn       e_bcc_status CMU_Goto_Sleep(void)
*   @brief    put cmu in sleep mode  
*
*
*   @details   put cmu in sleep mode 
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    void
*   @return   status either success/failure
*/
e_bcc_status CMU_Goto_Sleep(void)
{
    /*goto sleep*/
    return CMU_Reg_Write(0, CONTROL1, 0x04, 1, FRMWRT_SGL_W);
}

/** @fn       e_bcc_status CMU_Goto_Shutdown(void)
*   @brief    put cmu in shutdown mode
*
*
*   @details  put cmu in shutdown mode
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    void
*   @return   status either success/failure
*/
e_bcc_status CMU_Goto_Shutdown(void)
{
    /*Reset device*/
    return CMU_Reg_Write(0, CONTROL1, 0x80, 1, FRMWRT_SGL_W);
}

/** @fn       e_bcc_status CMU_Cell_Balance_Configuration(void)
*   @brief    cell balancing configuration 
*
*
*   @details  cell balancing configuration  
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    void
*   @return   status either success/failure
*/
e_bcc_status CMU_Cell_Balance_Configuration(void)
{
	e_bcc_status error = CMU_STATUS_SUCCESS;
	
    /*SET DUTY CYCLE TO 10 s (default)*/
    error=CMU_Reg_Write(0, BAL_CTRL1, 0x01, 1, FRMWRT_ALL_W);   

    /*OPTIONAL: SET VCBDONE THRESH TO 3V, AND OVUV_GO*/
    error|=CMU_Reg_Write(0, VCB_DONE_THRESH, 0x08, 1, FRMWRT_ALL_W);   
	
	/*round-robin and OVUV_GO*/ 
    error|=CMU_Reg_Write(0, OVUV_CTRL, 0x05, 1, FRMWRT_ALL_W);          

    /*MANUAL CELL BALANCING (default)*/
    error|=CMU_Reg_Write(0, BAL_CTRL2, 0x00, 1, FRMWRT_ALL_W); 
    
    return error;  
}

/** @fn       void CMU_WakeUp(void)
*   @brief    wake up CMU 
*
*
*   @details  wake up CMU from sleep/shutdown mode
*
*   @pre      uart rx pin initialization
*   @post     
*   @param    void
*   @return   void
*/
void CMU_WakeUp(void)
{
    /*make UART_RX_Pin High*/
    IO_RE0_SetHigh();
    
    /*make UART_RX_Pin Low*/
    IO_RE0_SetLow();
    
    /*wait for tHLD_WAKE time(2.5ms))*/
    MCU_WaitMs(3);
    
    /*make UART_RX_Pin High*/
    IO_RE0_SetHigh();
}

/** @fn       void CMU_HW_Rst(void)
*   @brief    reset CMU 
*
*
*   @details  reset CMU
*
*   @pre      uart rx pin initialization
*   @post     
*   @param    void
*   @return   void
*/
void CMU_HW_Rst(void)
{
    /*make UART_RX_Pin High*/
    IO_RE0_SetHigh();
    
    /*make UART_RX_Pin Low*/
    IO_RE0_SetLow();
    
    /*wait for tHLD_WAKE time(36ms))*/
    MCU_WaitMs(T_HLD_WAKE_TIME);
    
    /*make UART_RX_Pin High*/
    IO_RE0_SetHigh();
}

/** @fn       s32 WriteFrame(u8 u8_bID, u16 u16_wAddr, u8 * pData, u8 u8_bLen, u8 u8_bWriteType)
*   @brief    Generate command frame 
*
*
*   @details  Generate command frame 
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    bID device id
*   @param    u16_wAddr register address
*   @param    pData data to write to register
*   @param    u8_bLen no of register to write
*   @param    u8_bWriteType write type
*   @return   status either success/failure
*/
s32 CMU_WriteFrame(u8 u8_bID, u16 u16_wAddr, u8 *pData, u8 u8_bLen, u8 u8_bWriteType) 
{
	s32 s32_bPktLen = 0;
	u8 *pBuf = cmu_u8_pFrame;
    u16 u16_wCRC=0;
    
    if(pData==NULL)
    {
        return CMU_STATUS_FAILURE;
    }
    
	memset(cmu_u8_pFrame, MAX_WRITE_VALUE, sizeof(cmu_u8_pFrame));
	*pBuf++ = FRM_START_BYTE | (u8_bWriteType) | ((u8_bWriteType & 0x10) ? u8_bLen - 0x01 : 0x00); /*Only include u8_bLen if it is a write; Writes are 0x90, 0xB0, 0xD0*/
	if (u8_bWriteType == FRMWRT_SGL_R || u8_bWriteType == FRMWRT_SGL_W)
	{
		*pBuf++ = (u8_bID & MASK_1ST_BYTE);
	}
	*pBuf++ = (u16_wAddr & MASK_2ND_BYTE) >> SHIFT_1BYTE;
	*pBuf++ = u16_wAddr & MASK_1ST_BYTE;

	while (u8_bLen--)
		*pBuf++ = *pData++;

	s32_bPktLen = pBuf - cmu_u8_pFrame;

	u16_wCRC = CRC16(cmu_u8_pFrame, s32_bPktLen);
	*pBuf++ = u16_wCRC & MASK_1ST_BYTE;
	*pBuf++ = (u16_wCRC & MASK_2ND_BYTE) >> SHIFT_1BYTE;
	s32_bPktLen += 2;

	UART_Send_Buffer(1,cmu_u8_pFrame, s32_bPktLen);

	return s32_bPktLen;
}

/** @fn       e_bcc_status CMU_Reg_Write(u8 u8_bID, u8 u16_wAddr, uint64_t u64_dwData, u8 u8_bLen, u8 u8_bWriteType)
*   @brief    Generate command frame 
*
*
*   @details  Generate command frame 
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    bID device id
*   @param    u16_wAddr register address
*   @param    pData data to write to register
*   @param    u8_bLen no of register to write
*   @param    u8_bWriteType write type
*   @return   status either success/failure
*/
e_bcc_status CMU_Reg_Write(u8 u8_bID, u16 u16_wAddr, u64 u64_dwData, u8 u8_bLen, u8 u8_bWriteType)
{
    /* device address, register start address, data uin8_ts, data length, write type (single, broadcast, stack)*/
	cmu_u8_bRes = 0;
	memset(cmu_u8_bBuf,0,sizeof(cmu_u8_bBuf));
	switch (u8_bLen) 
    {
	case 1:
		cmu_u8_bBuf[0] = u64_dwData & MASK_1ST_BYTE;
		cmu_u8_bRes = CMU_WriteFrame(u8_bID, u16_wAddr, cmu_u8_bBuf, 1, u8_bWriteType);
		break;
	case 2:
		cmu_u8_bBuf[0] = (u64_dwData & MASK_2ND_BYTE) >> SHIFT_1BYTE;
		cmu_u8_bBuf[1] = u64_dwData & MASK_1ST_BYTE;
		cmu_u8_bRes = CMU_WriteFrame(u8_bID, u16_wAddr, cmu_u8_bBuf, 2, u8_bWriteType);
		break;
	case 3:
		cmu_u8_bBuf[0] = (u64_dwData & MASK_3RD_BYTE) >> SHIFT_2BYTE;
		cmu_u8_bBuf[1] = (u64_dwData & MASK_2ND_BYTE) >> SHIFT_1BYTE;
		cmu_u8_bBuf[2] = u64_dwData & MASK_1ST_BYTE;
		cmu_u8_bRes = CMU_WriteFrame(u8_bID, u16_wAddr, cmu_u8_bBuf, 3, u8_bWriteType);
		break;
	case 4:
		cmu_u8_bBuf[0] = (u64_dwData & MASK_4TH_BYTE) >> SHIFT_3BYTE;
		cmu_u8_bBuf[1] = (u64_dwData & MASK_3RD_BYTE) >> SHIFT_2BYTE;
		cmu_u8_bBuf[2] = (u64_dwData & MASK_2ND_BYTE) >> SHIFT_1BYTE;
		cmu_u8_bBuf[3] = u64_dwData & MASK_1ST_BYTE;
		cmu_u8_bRes = CMU_WriteFrame(u8_bID, u16_wAddr, cmu_u8_bBuf, 4, u8_bWriteType);
		break;
	case 5:
		cmu_u8_bBuf[0] = (u64_dwData & MASK_5TH_BYTE) >> SHIFT_4BYTE;
		cmu_u8_bBuf[1] = (u64_dwData & MASK_4TH_BYTE) >> SHIFT_3BYTE;
		cmu_u8_bBuf[2] = (u64_dwData & MASK_3RD_BYTE) >> SHIFT_2BYTE;
		cmu_u8_bBuf[3] = (u64_dwData & MASK_2ND_BYTE) >> SHIFT_1BYTE;
		cmu_u8_bBuf[4] = u64_dwData & MASK_1ST_BYTE;
		cmu_u8_bRes = CMU_WriteFrame(u8_bID, u16_wAddr, cmu_u8_bBuf, 5, u8_bWriteType);
		break;
	case 6:
		cmu_u8_bBuf[0] = (u64_dwData & MASK_6TH_BYTE) >> SHIFT_5BYTE;
		cmu_u8_bBuf[1] = (u64_dwData & MASK_5TH_BYTE) >> SHIFT_4BYTE;
		cmu_u8_bBuf[2] = (u64_dwData & MASK_4TH_BYTE) >> SHIFT_3BYTE;
		cmu_u8_bBuf[3] = (u64_dwData & MASK_3RD_BYTE) >> SHIFT_2BYTE;
		cmu_u8_bBuf[4] = (u64_dwData & MASK_2ND_BYTE) >> SHIFT_1BYTE;
		cmu_u8_bBuf[5] = u64_dwData & MASK_1ST_BYTE;
		cmu_u8_bRes = CMU_WriteFrame(u8_bID, u16_wAddr, cmu_u8_bBuf, 6, u8_bWriteType);
		break;
	case 7:
		cmu_u8_bBuf[0] = (u64_dwData & MASK_7TH_BYTE) >> SHIFT_6BYTE;
		cmu_u8_bBuf[1] = (u64_dwData & MASK_6TH_BYTE) >> SHIFT_5BYTE;
		cmu_u8_bBuf[2] = (u64_dwData & MASK_5TH_BYTE) >> SHIFT_4BYTE;
		cmu_u8_bBuf[3] = (u64_dwData & MASK_4TH_BYTE) >> SHIFT_3BYTE;
		cmu_u8_bBuf[4] = (u64_dwData & MASK_3RD_BYTE) >> SHIFT_2BYTE;
		cmu_u8_bBuf[5] = (u64_dwData & MASK_2ND_BYTE) >> SHIFT_1BYTE;
		cmu_u8_bBuf[6] = u64_dwData & MASK_1ST_BYTE;
		cmu_u8_bRes = CMU_WriteFrame(u8_bID, u16_wAddr, cmu_u8_bBuf, 7, u8_bWriteType);
		break;
	case 8:
		cmu_u8_bBuf[0] = (u64_dwData & MASK_8TH_BYTE) >> SHIFT_7BYTE;
		cmu_u8_bBuf[1] = (u64_dwData & MASK_7TH_BYTE) >> SHIFT_6BYTE;
		cmu_u8_bBuf[2] = (u64_dwData & MASK_6TH_BYTE) >> SHIFT_5BYTE;
		cmu_u8_bBuf[3] = (u64_dwData & MASK_5TH_BYTE) >> SHIFT_4BYTE;
		cmu_u8_bBuf[4] = (u64_dwData & MASK_4TH_BYTE) >> SHIFT_3BYTE;
		cmu_u8_bBuf[5] = (u64_dwData & MASK_3RD_BYTE) >> SHIFT_2BYTE;
		cmu_u8_bBuf[6] = (u64_dwData & MASK_2ND_BYTE) >> SHIFT_1BYTE;
		cmu_u8_bBuf[7] = u64_dwData & MASK_1ST_BYTE;
		cmu_u8_bRes = CMU_WriteFrame(u8_bID, u16_wAddr, cmu_u8_bBuf, 8, u8_bWriteType);
		break;
        
	default:
		break;
	}
	return cmu_u8_bRes;
}

/** @fn       int ReadFrameReq(u8 u8_bID, u16 u16_wAddr, u8 u8_bByteToReturn, u8 u8_bWriteType)
*   @brief    Generate read frame request 
*
*
*   @details  Generate read frame request  
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    bID device id
*   @param    u16_wAddr register address
*   @param    u8_bByteToReturn data to read from registerS
*   @param    u8_bWriteType write type
*   @return   status either success/failure
*/
s32 CMU_ReadFrameReq(u8 u8_bID, u16 u16_wAddr, u8 u8_bByteToReturn, u8 u8_bWriteType) 
{
	cmu_u8_bReturn = u8_bByteToReturn - 1;

	if (cmu_u8_bReturn > MAX_WRITE_VALUE)
		return 0;

	return CMU_WriteFrame(u8_bID, u16_wAddr, &cmu_u8_bReturn, 1, u8_bWriteType);
}

/** @fn       e_bcc_status CMU_Reg_Read(u8 u8_bID, u8 u16_wAddr, u8 * pData, u8 u8_bLen, u32 dwTimeOut,u8 u8_bWriteType)
*   @brief    to read data from cmu register 
*
*
*   @details  to read data from cmu register  
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    bID device id
*   @param    u16_wAddr register address
*   @param    pData data to write to register
*   @param    u8_bLen no of register to write
*   @param    dwTimeOut write tieout value
*   @param    u8_bWriteType write type
*   @return   status either success/failure
*/
e_bcc_status CMU_Reg_Read(u8 u8_bID, u16 u16_wAddr, u8 *pData, u8 u8_bLen,u8 u8_bWriteType)
{
	 e_bcc_status status=CMU_STATUS_SUCCESS;
	 
    /* device address, register start address, uin8_t frame pointer to store data, data length, read type (single, broadcast, stack)*/
    cmu_u8_bRes = 0;
    
    if(pData==NULL)
    {
        status=CMU_STATUS_FAILURE;
    }
    else
    {
        if (u8_bWriteType == FRMWRT_SGL_R) 
        {
            cmu_u8_bRes=CMU_ReadFrameReq(u8_bID, u16_wAddr, u8_bLen, u8_bWriteType);
            memset(pData, 0, sizeof(pData));
            UART_Receive_Buffer(1,pData, u8_bLen + 6);
            cmu_u8_bRes = u8_bLen + 6;
        } 
        else if (u8_bWriteType == FRMWRT_STK_R) 
        {
            cmu_u8_bRes = CMU_ReadFrameReq(u8_bID, u16_wAddr, u8_bLen, u8_bWriteType);
            memset(pData, 0, sizeof(pData));
            UART_Receive_Buffer(1,pData, u8_bLen + 6);
            cmu_u8_bRes = (u8_bLen + 6) * (cmu_u8_total_boards - 1);
        } 
        else if (u8_bWriteType == FRMWRT_ALL_R) 
        {
            cmu_u8_bRes = CMU_ReadFrameReq(u8_bID, u16_wAddr, u8_bLen, u8_bWriteType);
            memset(pData, 0, sizeof(pData));
            UART_Receive_Buffer(1,pData, u8_bLen + 6);
            cmu_u8_bRes = (u8_bLen + 6) * cmu_u8_total_boards;
        } 
        else 
        {
            cmu_u8_bRes = 0;
        }
    }
    
     return status;
}

/** @fn       e_bcc_status CMU_Meas_StartConversion(u8 u8_bID,u8 u8_ADC_Mode_Value)
*   @brief    to start conversion status
*
*
*   @details  to start conversion status 
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    bID device id
*   @param    u8_ADC_Mode_Value adc mode value
*   @return   status either success/failure
*/
e_bcc_status CMU_Meas_StartConversion(u8 u8_bID,u8 u8_ADC_Mode_Value)
{
   e_bcc_status status;
   
   status= CMU_Reg_Write(u8_bID, ADC_CTRL1, u8_ADC_Mode_Value, 1, FRMWRT_ALL_W);

   return status;
}

/** @fn       e_bcc_status CMU_Meas_IsConverting (u8 u8_bID,bool *completed)
*   @brief    to read conversion status
*
*
*   @details  to read conversion status 
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    bID device id
*   @param    completed pointer to source buffer
*   @return   status either success/failure
*/
e_bcc_status CMU_Meas_IsConverting (u8 u8_bID,u8 *u8_completed)
{
   e_bcc_status status;
   
    if(u8_completed==NULL)
    {
        status= CMU_STATUS_FAILURE;
    }
    else
    {
       status= CMU_Reg_Read(u8_bID, ADC_STAT1, u8_completed, 1, FRMWRT_SGL_R); 
    }
   
   return status;
}


/** @fn       e_bcc_status CMU_Meas_GetRawValues(u8 u8_bID, u16 measurements[])
*   @brief    to read measurement values
*
*
*   @details  to read measurement values  
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    bID device id
*   @param    measurements pointer to source buffer
*   @return   status either success/failure
*/
e_bcc_status CMU_Meas_GetRawValues(u8 u8_bID, u8 u8_measurements[])
{
   e_bcc_status status;
   
   u8 u8_i=0;
   
    if(u8_measurements==NULL)
    {
        status=CMU_STATUS_FAILURE;
    }
    else
    {
        for(u8_i=0;u8_i<cmu_st_config.u8_devicesCnt;u8_i++)
        {    
             status= CMU_Reg_Read(u8_i, VCELL16_HI, u8_measurements+(u8_i*NO_OF_MEASUREMENT_DATA_REGISTERS), NO_OF_MEASUREMENT_DATA_REGISTERS, FRMWRT_SGL_R);
        }
    }
   
   return status;
}

/** @fn       e_bcc_status MaskAllFaults(u8 u8_bID, u8 u8_bWriteType)
*   @brief    mask fault status 
*
*
*   @details  mask fault status  
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    bID device id
*   @param    u8_bWriteType write type
*   @return   status either success/failure
*/
e_bcc_status CMU_MaskAllFaults(u8 u8_bID, u8 u8_bWriteType)
{
    if(u8_bWriteType==FRMWRT_ALL_W)
    {
        CMU_Reg_Write(0, FAULT_MSK1, FAULT_RST1_CLEAR_FAULT, 2, FRMWRT_ALL_W);
    }
    else if(u8_bWriteType==FRMWRT_SGL_W)
    {
        CMU_Reg_Write(u8_bID, FAULT_MSK1, FAULT_RST1_CLEAR_FAULT, 2, FRMWRT_SGL_W);
    }
    else if(u8_bWriteType==FRMWRT_STK_W)
    {
        CMU_Reg_Write(0, FAULT_MSK1, FAULT_RST1_CLEAR_FAULT, 2, FRMWRT_STK_W);
    }
    else
    {
        //u8_bWriteType incorrect
    }
    return CMU_STATUS_SUCCESS;
}

/** @fn       e_bcc_status CMU_Get_Fault_Status(u8 u8_bID, u8 u8_bWriteType)
*   @brief    clear fault status 
*
*
*   @details  clear fault status  
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    bID device id
*   @param    u8_bWriteType write type
*   @return   status either success/failure
*/
e_bcc_status CMU_Get_Fault_Status(u8 u8_bID, u8 u8_bWriteType)
{
    cmu_u8_currentBoard = 0;
    cmu_u8_currentCell = 0;
    memset(cmu_st_batPack[0].u8_bccStatus,0,sizeof(cmu_st_batPack[0].u8_bccStatus));
    if(u8_bWriteType==FRMWRT_ALL_R)
    {
        CMU_Reg_Read(0, FAULT_SUMMARY, cmu_st_batPack[0].u8_bccStatus, NO_OF_FAULT_REGISTERS, FRMWRT_ALL_R);       
    }
    else if(u8_bWriteType==FRMWRT_SGL_R)
    {
        CMU_Reg_Read(u8_bID, FAULT_SUMMARY, cmu_st_batPack[0].u8_bccStatus, NO_OF_FAULT_REGISTERS, FRMWRT_SGL_R);
    }
    else if(u8_bWriteType==FRMWRT_STK_R)
    {
        CMU_Reg_Read(0, FAULT_SUMMARY, cmu_st_batPack[0].u8_bccStatus, NO_OF_FAULT_REGISTERS, FRMWRT_STK_R);
    }
    else
    {
        /*u8_bWriteType incorrect*/
    }
    return CMU_STATUS_SUCCESS;
}

/** @fn       e_bcc_status CMU_Fault_ClearStatus(u8 u8_bID, u8 u8_bWriteType)
*   @brief    clear fault status 
*
*
*   @details  clear fault status  
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    bID device id
*   @param    u8_bWriteType write type
*   @return   status either success/failure
*/
e_bcc_status CMU_Fault_ClearStatus(u8 u8_bID, u8 u8_bWriteType)
{
     /*BROADCAST INCLUDES EXTRA FUNCTIONALITY TO OVERWRITE THE CUST_CRC WITH THE CURRENT SETTINGS*/
    if(u8_bWriteType==FRMWRT_ALL_W)
    {
        /*READ THE CALCULATED CUSTOMER CRC VALUES*/
        CMU_Reg_Read(0, CUST_CRC_RSLT_HI, cmu_st_batPack[0].u8_bccStatus, 2, FRMWRT_ALL_R);
        /*OVERWRITE THE CRC OF EVERY BOARD IN THE STACK WITH THE CORRECT CRC*/
        for(cmu_u8_currentBoard=0; cmu_u8_currentBoard<cmu_u8_total_boards; cmu_u8_currentBoard++)
        {
            /*THE RETURN FRAME STARTS WITH THE HIGHEST BOARD FIRST, SO THIS WILL WRITE THE HIGHEST BOARD FIRST*/
            CMU_Reg_Write(cmu_u8_total_boards-cmu_u8_currentBoard-1, CUST_CRC_HI, cmu_st_batPack[cmu_u8_currentBoard].u8_bccStatus[cmu_u8_currentBoard*8+4] << 8 | cmu_st_batPack[cmu_u8_currentBoard].u8_bccStatus[cmu_u8_currentBoard*8+5], 2, FRMWRT_SGL_W);
        }
        /*NOW CLEAR EVERY FAULT*/
        CMU_Reg_Write(0, FAULT_RST1, FAULT_RST1_CLEAR_FAULT, 2, FRMWRT_ALL_W);
    }
    else if(u8_bWriteType==FRMWRT_SGL_W)
    {
        CMU_Reg_Write(u8_bID, FAULT_RST1, FAULT_RST1_CLEAR_FAULT, 2, FRMWRT_SGL_W);
    }
    else if(u8_bWriteType==FRMWRT_STK_W)
    {
        CMU_Reg_Write(0, FAULT_RST1, FAULT_RST1_CLEAR_FAULT, 2, FRMWRT_STK_W);
    }
    else
    {
        /*u8_bWriteType incorrect*/
    }
    return CMU_STATUS_SUCCESS;
}

/** @fn       e_bcc_status CMU_Enable_Balancing(u8 balancing_state[])
*   @brief    enable cell balancing 
*
*
*   @details  enable cell balancing  
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    drvConfig pointer to config structure
*   @param    balancing_state pointer to cell balancing status
*   @return   status either success/failure
*/
e_bcc_status CMU_Enable_Balancing(u8 balancing_state[])
{
    u8 u8_i;
    e_bcc_status error = CMU_STATUS_SUCCESS;
    
    if(balancing_state==NULL)
    {
        error=CMU_STATUS_FAILURE;
    }
    else
    {
        /* 1. Set all CBx drivers to ON/OFF. */
        for (u8_i = 0; u8_i < 16; u8_i++)
        {
            error = CMU_Reg_Write(0, CB_CELL16_CTRL+u8_i, (balancing_state[u8_i]<<1), 1, FRMWRT_SGL_W); 
        }

        /*enabling cell balancing*/
        error = CMU_Reg_Write(0, BAL_CTRL2, BAL_CTRL2_ENABLE_CELL_BALANCING, 1, FRMWRT_SGL_W);
    }
    
    return error;
}

/** @fn       e_bcc_status CMU_Disable_Cell_Balancing(void)
*   @brief    disable cell balancing 
*
*
*   @details  disable cell balancing  
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    void
*   @return   status either success/failure
*/
e_bcc_status CMU_Disable_Cell_Balancing(void)
{
    e_bcc_status error = CMU_STATUS_SUCCESS;

    /*disabling cell balancing*/
    error = CMU_Reg_Write(0, BAL_CTRL2, BAL_CTRL2_DISABLE_CELL_BALANCING, 1, FRMWRT_SGL_W);
   
    return error;
}

/** @fn       e_bcc_status CMU_Get_Cell_Balancing_Status(u8 bal_status[])
*   @brief    to read cell balancing status 
*
*
*   @details  to read cell balancing status  
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    bal_status pointer to source buffer
*   @return   status either success/failure
*/
e_bcc_status CMU_Get_Cell_Balancing_Status(u8 u8_bal_status[])
{
    e_bcc_status error = CMU_STATUS_SUCCESS;
    
    if(u8_bal_status==NULL)
    {
        error=CMU_STATUS_FAILURE;
    }
    else
    {
        /*Reading cell balancing status*/
        error = CMU_Reg_Read(0, CB_COMPLETE1, u8_bal_status, 2,FRMWRT_SGL_R);
    }
    
    return error;
}

/** @fn       e_bcc_status CMU_Get_Cell_Balancing_Remaining_Time(u8 bal_time[])
*   @brief    to get cell balancing time 
*
*
*   @details  to get cell balancing time  
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    bal_time pointer to src buffer
*   @return   status either success/failure
*/
e_bcc_status CMU_Get_Cell_Balancing_Remaining_Time(u8 u8_bal_time[])
{
    e_bcc_status error = CMU_STATUS_SUCCESS;
    
    if(u8_bal_time==NULL)
    {
        error=CMU_STATUS_FAILURE;
    }
    else
    {
        /*Reading cell balancing status*/
        error = CMU_Reg_Read(0, BAL_TIME, u8_bal_time, 1,FRMWRT_SGL_R);
    }
    
    return error;
}


/** @fn       CMU_STATEMACH_e CMU_GetStateRequest(void)
*   @brief    to get new state request from other modules 
*
*
*   @details  to get new state request from other modules 
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    void
*   @return   void
*/
e_CMU_STATEMACH CMU_GetStateRequest(void)
{
	e_CMU_STATEMACH retval = CMU_STATEMACH_NO_REQUEST;

    retval    = cmu_st_state.statereq;
    
    cmu_st_state.statereq=CMU_STATEMACH_NO_REQUEST;

    return retval;
}

/** @fn        void CMU_StateTransition(CMU_STATEMACH_e state, u8 substate,u16 timer_ms,u8 ret_val)
*   @brief     to change the state of the cmu 
*
*
*   @details  to change the state of the cmu 
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    state current state
*   @param    substate current substate
*   @param    timer_ms wait time for next state
*   @param    ret_val current state return value
*   @return   void
*/
void CMU_StateTransition(e_CMU_STATEMACH state, u8 substate,u16 timer_ms,u8 ret_val)
{
    CMU_SAVELASTSTATES();

    if(ret_val==CMU_STATUS_SUCCESS)
    {
        cmu_st_state.state = state;
        cmu_st_state.u8_substate = substate;
        cmu_st_state.u8_ErrRetryCounter=0;
    }
    else
    {
        cmu_st_state.u8_ErrRetryCounter++;
        if(cmu_st_state.u8_ErrRetryCounter>CMU_STATE_RETRY)
        {
            cmu_st_state.state = CMU_STATEMACH_REINIT;
            cmu_st_state.u8_substate = 0;
            cmu_st_state.u8_ErrRetryCounter=0;
        }
    }

    cmu_st_state.u16_timer = timer_ms;
}

/** @fn       static e_bcc_status CMU_ConvertRawData(bcc_drv_config_t* drvConfig, pdc_bms_pack_t *cmu_st_batPack)
*   @brief    convert raw data to voltage 
*
*
*   @details  convert raw data to voltage 
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    drvConfig pointer to cmu configuration structure
*   @param    cmu_st_batPack to store converted battery pack info like voltage,current,temperature
*   @return   status either success/failure
*/
e_bcc_status CMU_ConvertRawData(st_bcc_drv_config *drvConfig, st_pdc_bms_pack *cmu_st_batPack)
{
    u8 u8_i=0;
    
    e_bcc_status error = CMU_STATUS_SUCCESS;
    
    //float vout=0.0;
    
    if(drvConfig==NULL || cmu_st_batPack==NULL)
    {
        error=CMU_STATUS_FAILURE;
    }
    else
    {
        #ifdef PDC_CMU_ISENSE_CONNECTED
            /* Measured ISENSE in [mA]. Value of shunt resistor is used. */
            cmu_st_batPack->current =0;
        #elif CURRENT_SENSOR_CONNECTED==1
            vout=(float)(CMU_GET_VOLT(cmu_st_batPack->bccRawMeas[CMU_MSR_AN6])/1000000.0);
            lem_current=Get_Current_Sensor_Conversion_Result(vout);
            bms_tab_cur_sensor.current=cmu_st_batPack->current=(s32)(lem_current*1000);
        #else
            cmu_st_batPack->s32_current=0;
        #endif

            /* Stack voltage in [mV]. */
            //cmu_st_batPack->stackVolt =(float)(CMU_GET_VOLT(cmu_st_batPack->bccRawMeas[CMU_MSR_CELL_VOLT1 - u8_i])/1000000.0);//TODO

            /* Cells voltage in [mV]. */
            for (u8_i = 0; u8_i < 16; u8_i++)
            {
                //cmu_st_batPack->cellVolt[u8_i] = (float)(CMU_GET_VOLT(cmu_st_batPack->bccRawMeas[CMU_MSR_CELL_VOLT1 - u8_i])/1000000.0);//TODO
            }

            /* For calculation of temperature on GPIO[0-6] in analog mode with use of NTC resistor. */
            /* To measure temperature selected GPIOs have to be set to analog input AM or RM mode! */
            /* overwrite measurement when temp sensors are not connected*/
            for (u8_i = 0; u8_i < 6; u8_i++)
            {
        #ifdef PDC_CMU_NTC_CONNECTED
                error = CMU_GetNtcCelsius(drvConfig, cmu_st_batPack->bccRawMeas[CMU_MSR_AN0 - u8_i],
                                          &(cmu_st_batPack->ntcTemp[u8_i]));
                if (error != CMU_STATUS_SUCCESS)
                {
                    return error;
                }
        #else
                //vout=(float)(CMU_GET_VOLT(cmu_st_batPack->bccRawMeas[CMU_MSR_AN0 - u8_i])/1000000.0);//TODO
                //cmu_st_batPack->ntcTemp[u8_i]=(float)(Get_Temperature_Conversion_Result((double)vout));//TODO
        #endif
            }
    
    /* IC temperature measurement in degC (register MEAS_IC_TEMP). */
    //cmu_st_batPack->icTemp = CMU_GET_IC_TEMP(cmu_st_batPack->bccRawMeas[CMU_MSR_ICTEMP]);//TODO
    }

    return error;
}

/** @fn       STD_RETURN_TYPE_e CMU_CheckVoltageMinMax(void)
*   @brief    function to check Cell voltage above and below measurement range 
*
*
*   @details  function to check Cell voltage above and below measurement range 
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    void
*   @return   status either success/failure
*/
e_STD_RETURN_TYPE CMU_CheckVoltageMinMax(void) 
{
    e_STD_RETURN_TYPE retval = E_OK;
    u16 u16_mod = 0;
    u16 u16_cell = 0;

        /* Iterate over all modules */
        for (u16_mod = 0; u16_mod < BS_NR_OF_MODULES; u16_mod++) 
        {
            /* Iterate over all cells */
            for ( u16_cell = 0; u16_cell < BS_NR_OF_BAT_CELLS; u16_cell++) 
            {
                /* Cell voltage above measurement range */
                if (cmu_st_batPack[u16_mod].u16_cellVolt[u16_cell] > SPL_MAX_CELL_VOLTAGE_LIMIT_mV) 
                {
                    /* Deviation too large -> set invalid flag */
                    retval = E_NOT_OK;
                }
                /* Cell voltage below measurement range */
                if (cmu_st_batPack[u16_mod].u16_cellVolt[u16_cell] < SPL_MIN_CELL_VOLTAGE_LIMIT_mV) 
                {
                    /* Deviation too large -> set invalid flag */
                    retval = E_NOT_OK;
                }
            }
        }

    return retval;
}

/** @fn       STD_RETURN_TYPE_e CMU_CheckVoltageSpread(u16 averageCellVolt_mV)
*   @brief    function to check Cell voltage deviation from average cell voltage too large or not
*
*
*   @details  function to check Cell voltage deviation from average cell voltage too large or not 
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    averageCellVolt_mV average cell voltage
*   @return   status either success/failure
*/
e_STD_RETURN_TYPE CMU_CheckVoltageSpread(u16 averageCellVolt_mV) 
{
    e_STD_RETURN_TYPE retval = E_OK;
    s16 s16_diff = 0;
    u16 u16_mod = 0;
    u16 u16_cell = 0;

        /* Iterate over all modules */
        for (u16_mod = 0; u16_mod < BS_NR_OF_MODULES; u16_mod++) 
        {
            /* Iterate over all cells */
            for (u16_cell = 0; u16_cell < BS_NR_OF_BAT_CELLS; u16_cell++) 
            {
                /* Cell voltage deviation from average cell voltage too large */
                s16_diff = (averageCellVolt_mV - cmu_st_batPack[u16_mod].u16_cellVolt[u16_cell]);
                if (abs(s16_diff) > SPL_CELL_VOLTAGE_AVG_TOLERANCE_mV) {
                    /* Deviation too large -> set invalid flag */
                    retval = E_NOT_OK;
                }
            }
        }

    return retval;
}

/** @fn       STD_RETURN_TYPE_e CMU_CheckTempMinMax(void)
*   @brief    function to check Cell temperature above and below measurement range
*
*
*   @details  function to check Cell temperature above and below measurement range 
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    void
*   @return   status either success/failure
*/
e_STD_RETURN_TYPE CMU_CheckTempMinMax(void) 
{
    e_STD_RETURN_TYPE retval = E_OK;
    u16 u16_mod = 0;
    u8 u8_sensor=0;

        /* Iterate over all modules */
        for (u16_mod = 0; u16_mod < BS_NR_OF_MODULES; u16_mod++) 
        {
            /* Iterate over all cells */
            for (u8_sensor = 0; u8_sensor < BCC_GPIO_INPUT_CONNECTED; u8_sensor++) 
            {
                /* Cell temperature below measurement range */
                if (cmu_st_batPack[u16_mod].s16_ntcTemp[u8_sensor] < SPL_MINIMUM_TEMP_MEASUREMENT_RANGE) 
                {
                    /* Cell temperature below range -> set invalid flag */
                    /* Deviation too large -> set invalid flag */
                    retval = E_NOT_OK;
                }

                /* Cell temperature above measurement range */
                if (cmu_st_batPack[u16_mod].s16_ntcTemp[u8_sensor] > SPL_MAXIMUM_TEMP_MEASUREMENT_RANGE) 
                {
                    /* Cell temperature above range -> set invalid flag */
                    /* Deviation too large -> set invalid flag */
                    retval = E_NOT_OK;
                }
            }
        }

    return retval;
}

/** @fn       void CMU_Compute_MinMax_Voltages(void)
*   @brief    stores the measured temperatures and the measured multiplexer feedbacks in the database
*
*
*   @details  stores the measured voltage and the measured multiplexer feedbacks in the database
*
*   @pre      CMU Initialization
*   @post     
*   @param    void
*   @return   void
*/
void CMU_Compute_MinMax_Voltages(void) 
{
    u16 u16_i = 0;
    u16 u16_j = 0;
    u16 min = 0;
    u16 max = 0;
    u8 u8_module_number_min = 0;
    u8 u8_module_number_max = 0;
    u8 u8_cell_number_min = 0;
    u8 u8_cell_number_max = 0;
    u32 u32_mean = 0;
    u32 u32_sum = 0;
    e_STD_RETURN_TYPE retval_PLminmax = E_NOT_OK;
    e_STD_RETURN_TYPE retval_PLspread = E_NOT_OK;
    e_STD_RETURN_TYPE result = E_NOT_OK;

    retval_PLminmax = CMU_CheckVoltageMinMax();

    min=max=cmu_st_batPack[0].u16_cellVolt[0];

    for (u16_i=0; u16_i < BS_NR_OF_MODULES; u16_i++)
    {
        for (u16_j=0; u16_j < BS_NR_OF_BAT_CELLS_PER_MODULE; u16_j++)
        {
        	u32_sum=u32_sum+cmu_st_batPack[u16_i].u16_cellVolt[u16_j];

                if (cmu_st_batPack[u16_i].u16_cellVolt[u16_j] < min) {
                    min = cmu_st_batPack[u16_i].u16_cellVolt[u16_j];
                    u8_module_number_max = u16_i;
                    u8_cell_number_max = u16_j;
                }
                if (cmu_st_batPack[u16_i].u16_cellVolt[u16_j] > max) {
                    max = cmu_st_batPack[u16_i].u16_cellVolt[u16_j];
                    u8_module_number_max = u16_i;
                    u8_cell_number_max = u16_j;
                }
        }
      }

    u32_mean = u32_sum/BS_NR_OF_BAT_CELLS;

    retval_PLspread = CMU_CheckVoltageSpread(u32_mean);

    /* Set flag if plausibility error detected */
    if ((retval_PLminmax == E_OK) && (retval_PLspread == E_OK)) {
        result = E_OK;
    }
    //DIAG_checkEvent(result, DIAG_CH_PLAUSIBILITY_CELL_VOLTAGE, 0);

    cmu_st_bms_minmax.voltage_mean = u32_mean;
    cmu_st_bms_minmax.previous_voltage_min = cmu_st_bms_minmax.voltage_min;
    cmu_st_bms_minmax.previous_voltage_max = cmu_st_bms_minmax.voltage_max;
    cmu_st_bms_minmax.voltage_min = min;
    cmu_st_bms_minmax.voltage_module_number_min = u8_module_number_min;
    cmu_st_bms_minmax.voltage_cell_number_min = u8_cell_number_min;
    cmu_st_bms_minmax.voltage_max = max;
    cmu_st_bms_minmax.voltage_module_number_max = u8_module_number_max;
    cmu_st_bms_minmax.voltage_cell_number_max = u8_cell_number_max;
}

/** @fn       void CMU_Compute_MinMax_Temperatures(void) 
*   @brief    stores the measured temperatures and the measured multiplexer feedbacks in the database
*
*
*   @details  stores the measured temperatures and the measured multiplexer feedbacks in the database
*
*   @pre      CMU Initialization
*   @post     
*   @param    void
*   @return   void
*/
void CMU_Compute_MinMax_Temperatures(void) 
{
    u16 u16_i = 0;
    u16 u16_j = 0;
    s16 s16_min = 0;
    s16 s16_max = 0;
    s32 s16_sum = 0;
    s16 s16_mean = 0;
    u8 u8_module_number_min = 0;
    u8 u8_module_number_max = 0;
    u8 u8_sensor_number_min = 0;
    u8 u8_sensor_number_max = 0;
    e_STD_RETURN_TYPE retval_PL  = E_NOT_OK;

    /* Perform plausibility check */
    retval_PL = CMU_CheckTempMinMax();
    /* Set flag if plausibility error detected */
    //DIAG_checkEvent(retval_PL, DIAG_CH_PLAUSIBILITY_CELL_TEMP, 0);

	s16_min=s16_max=cmu_st_batPack[0].s16_ntcTemp[0];

    for (u16_i=0; u16_i < BS_NR_OF_MODULES; u16_i++)
    {
        for (u16_j=0; u16_j < BCC_GPIO_INPUT_CONNECTED; u16_j++)
        {
        	s16_sum=s16_sum+cmu_st_batPack[u16_i].s16_ntcTemp[u16_j];

                if (cmu_st_batPack[u16_i].s16_ntcTemp[u16_j] < s16_min) 
                {
                    s16_min = cmu_st_batPack[u16_i].s16_ntcTemp[u16_j];
                    u8_module_number_max = u16_i;
                    u8_sensor_number_max = u16_j;
                }
                if (cmu_st_batPack[u16_i].s16_ntcTemp[u16_j] > s16_max) 
                {
                    s16_max = cmu_st_batPack[u16_i].s16_ntcTemp[u16_j];
                    u8_module_number_max = u16_i;
                    u8_sensor_number_max = u16_j;
                }
         }
      }

    s16_mean = s16_sum/BCC_GPIO_INPUT_CONNECTED;
    cmu_st_bms_minmax.temperature_mean = s16_mean/10.0;
    cmu_st_bms_minmax.temperature_min = s16_min/10.0;
    cmu_st_bms_minmax.temperature_module_number_min = u8_module_number_min;
    cmu_st_bms_minmax.temperature_sensor_number_min = u8_sensor_number_min;
    cmu_st_bms_minmax.temperature_max = s16_max/10.0;
    cmu_st_bms_minmax.temperature_module_number_max = u8_module_number_max;
    cmu_st_bms_minmax.temperature_sensor_number_max = u8_sensor_number_max;
}

/** @fn       void CMU_Update_Die_Temperature(void)
*   @brief    stores the measured die temperatures in the database
*
*
*   @details  stores the measured die temperatures in the database
*
*   @pre      CMU Initialization
*   @post     
*   @param    void
*   @return   void
*/
void CMU_Update_Die_Temperature(void)
{
    u8 u8_i = 0;
            
	cmu_st_bms_minmax.die_temperature=0;

	for (u8_i = 0; u8_i< BS_NR_OF_MODULES; u8_i++)
	{
		if(cmu_st_batPack[u8_i].s16_icTemp>cmu_st_bms_minmax.die_temperature)
		{
			cmu_st_bms_minmax.die_temperature=cmu_st_batPack[u8_i].s16_icTemp;
		}
	}
}

/** @fn       void CMU_Process_ADC_Data(void)
*   @brief    for processing adc data  
*
*
*   @details  for processing adc data 
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    void
*   @return   void
*/                                                                 
void CMU_Process_ADC_Data(void)
{
	e_bcc_cid_t cid = CMU_CID_DEV1;

	for (cid = CMU_CID_DEV1; cid<= cmu_st_config.u8_devicesCnt; cid++)
	{
		CMU_ConvertRawData(&cmu_st_config, &cmu_st_batPack[cid-1]);
	}
    
	CMU_Compute_MinMax_Voltages();
    
	CMU_Compute_MinMax_Temperatures();
    
	CMU_Update_Die_Temperature();
    
    /*writing battery pack information to data base*/
    DB_WriteBlock(&cmu_st_bms_cellvolt,DATA_BLOCK_ID_CELLVOLTAGE);
    
	//Send_Bat_Pack_VIT_Data_To_UART();//TODO
}

/** @fn       u8 CMU_Set_StateRequest(e_CMU_STATEMACH u8_statereq)
*   @brief    function to set the state of cmu  
*
*
*   @details  function to set the state of cmu 
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    void
*   @return   void
*/ 
u8 CMU_Set_StateRequest(e_CMU_STATEMACH u8_statereq) 
{
    e_CMU_STATEMACH retVal = CMU_STATEMACH_NO_REQUEST;

    //retVal = CMU_CheckStateRequest(u8_statereq);

    //if (retVal == E_OK) 
    //{
        cmu_st_state.statereq   = u8_statereq;
    //}
    
    return retVal;
}

/** @fn       u8 CMU_Get_StateRequest(void) 
*   @brief    function to get the state of cmu  
*
*
*   @details  function to get the state of cmu 
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    void
*   @return   void
*/ 
u8 CMU_Get_Current_State(void) 
{
    e_CMU_STATEMACH retval = CMU_STATEMACH_NO_REQUEST;

    retval    = cmu_st_state.state;

    return retval;
}


/** @fn       void CMU_Trigger(void)
*   @brief    CMU State machine's 
*
*
*   @details  ths function will be  called in os task
*
*   @pre      Os initialization
*   @post     
*   @param    void
*   @return   void
*/
void CMU_Trigger(void)
{
    u8 u8_retVal = 0;
    e_CMU_STATEMACH statereq = CMU_STATEMACH_NO_REQUEST;
    
	if (cmu_st_state.u16_timer)
	{
		if (--cmu_st_state.u16_timer) 
        {
			return;    /* handle state machine only if timer has elapsed*/
		}
	}

    switch (cmu_st_state.state)
    {
        /****************************UNINITIALIZED***********************************/
        case CMU_STATEMACH_UNINITIALIZED:
            /* waiting for Initialization Request */
        	statereq=CMU_GetStateRequest();
            if (statereq == CMU_STATEMACH_INIT_REQUEST) 
            {
    			CMU_StateTransition(CMU_STATEMACH_INITIALIZATION,0,1,CMU_STATUS_SUCCESS);
            } 
            else if (statereq == CMU_STATEMACH_NO_REQUEST) 
            {
                /* no actual request pending */
            } else 
            {
                cmu_st_state.u8_ErrRequestCounter++;   /* illegal request pending */
            }
            break;

        /****************************INITIALIZATION**********************************/
        case CMU_STATEMACH_INITIALIZATION:
			//u8_retVal = CMU_Init(); /* Initialize CMU device */
			CMU_StateTransition(CMU_STATEMACH_INITIALIZED,0,1,u8_retVal=CMU_STATUS_SUCCESS);
            break;

        /****************************INITIALIZED*************************************/
        case CMU_STATEMACH_INITIALIZED:
			CMU_StateTransition(CMU_STATEMACH_IDLE,0,1,u8_retVal=CMU_STATUS_SUCCESS);
            break;

		/****************************REINIT*********************************/
		case CMU_STATEMACH_REINIT:
			CMU_StateTransition(CMU_STATEMACH_INITIALIZATION,0,1,u8_retVal=CMU_STATUS_SUCCESS);
			break;

		/****************************IDLE*************************************/
		case CMU_STATEMACH_IDLE:
			CMU_StateTransition(CMU_STATEMACH_CLEAR_FAULT,0,1,u8_retVal=CMU_STATUS_SUCCESS);
			break;

		/****************************CLEAR FAULT STATUS*************************************/
		case CMU_STATEMACH_CLEAR_FAULT:
			u8_retVal=CMU_Fault_ClearStatus(0,FRMWRT_SGL_W);
			u8_retVal=CMU_STATUS_SUCCESS;
			CMU_StateTransition(CMU_STATEMACH_STARTMEAS,0,1,u8_retVal);
			break;

		/****************************START MEASUREMNET*************************************/
		case CMU_STATEMACH_STARTMEAS:
			u8_retVal=CMU_Meas_StartConversion(0,1);
			CMU_StateTransition(CMU_STATEMACH_GET_FAULT,0,1,u8_retVal);
			break;

		/****************************GET FAULT STATUS*************************************/
		case CMU_STATEMACH_GET_FAULT:
			u8_retVal=CMU_Get_Fault_Status(0,FRMWRT_SGL_W);
			//Check_Faults_And_Log_Event();//TODO
			CMU_StateTransition(CMU_STATEMACH_CHECK_MEAS_STATUS,0,1,u8_retVal);
			break;

		/****************************START MEASUREMNET*************************************/
		case CMU_STATEMACH_CHECK_MEAS_STATUS:
			u8_retVal=CMU_Meas_IsConverting(0,&u8_retVal);
			CMU_StateTransition(CMU_STATEMACH_READVOLTAGE,0,1,u8_retVal);
			break;

		/****************************READ VOLTAGE*************************************/
		case CMU_STATEMACH_READVOLTAGE:
			u8_retVal=CMU_Meas_GetRawValues(0,cmu_st_config.u8_rxBuf);
			CMU_Process_ADC_Data();
			CMU_StateTransition(CMU_STATEMACH_BALANCECONTROL,0,1,u8_retVal);;
			break;

		/****************************BALANCE CONTROL*********************************/
		case CMU_STATEMACH_BALANCECONTROL:
			u8_retVal=CMU_Enable_Balancing(cmu_u8_balancing_state);
			CMU_StateTransition(CMU_STATEMACH_CLEAR_FAULT,0,1,u8_retVal);
			break;
            
        /****************************GOTO SHUTDOWN*********************************/
		case CMU_STATEMACH_GOTO_SHUTDOWN:
			u8_retVal=CMU_Goto_Shutdown();
			CMU_StateTransition(CMU_STATEMACH_EXIT_FROM_SLEEP_SHUTDOWN,0,1,u8_retVal);
			break;

		/****************************GOTO SLEEP*********************************/
		case CMU_STATEMACH_GOTO_SLEEP:
			u8_retVal=CMU_Goto_Sleep();
			CMU_StateTransition(CMU_STATEMACH_EXIT_FROM_SLEEP_SHUTDOWN,0,1,u8_retVal);
			break;

		/****************************WAKEUP*********************************/
		case CMU_STATEMACH_EXIT_FROM_SLEEP_SHUTDOWN:
			CMU_WakeUp();
			CMU_StateTransition(CMU_STATEMACH_REINIT,0,1,u8_retVal=CMU_STATUS_SUCCESS);
			break;

		/****************************DEFAULT**************************/
		default:
			break;
    }
}
/********************************************************************************************************************************/
/* End of file                                                                                                                  */
/********************************************************************************************************************************/
/**@}*/

