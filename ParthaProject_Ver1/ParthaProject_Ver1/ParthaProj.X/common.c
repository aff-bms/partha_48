/*
 * File:   common.c
 * Author: PAVITHRAN
 *
 * Created on 8 July, 2021, 3:41 PM
 */


#include "common.h"

e_STD_RETURN_TYPE IsTimerExpired(u32 start_timer,u32 MAX_TIMEOUT)
{
	u32 TimerValue=Timer_1ms;

	if(TimerValue >= start_timer)
	{
		if(0xFFFFFFFF - MAX_TIMEOUT >= start_timer)
		{
			if(start_timer + MAX_TIMEOUT < TimerValue)
			{
				return E_OK;
			}
			else
			{
				return E_NOT_OK;
			}
		}
		else
		{
			//start_timer + MAX_TIMEOUT can overflow
			return E_NOT_OK;
		}
	 }
	else
	{   /*timer reset case*/
		u32 TimeLapsed = (0XFFFFFFFF - start_timer)+ TimerValue;
	    if(TimeLapsed >= MAX_TIMEOUT)
	    return E_OK;
	}

 return E_NOT_OK;
}

void MCU_WaitMs(u32 delay)
{
	u32 starttimer;

	starttimer=Timer_1ms;

	while(1)
	{
		if(IsTimerExpired(starttimer,delay)==1)
		{
			break;
		}
	}
}
