/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name: event logging                                                                                                        */
/* Author:Pavithran                                                                                                                     */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file event_log.c
*   @brief This file contains API's which are required to store events in flash memory.
*
*   @details event logging .
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Pavithran
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 15-07-2021
*/

/** @defgroup event log
*
* This file contains API's which are required to store events in flash memory.
*
*/

/**@{*/
/********************************************************************************************************************************/
/* Include Section                                                                                                              */
/********************************************************************************************************************************/
#include "flash_app.h"
#include "event_log.h"
#include "database.h"
#include "diag.h"

/********************************************************************************************************************************/
/* Defines Section - GLOBAL                                                                                                   */
/********************************************************************************************************************************/
/*flash memory page size*/
#define FLASH_MEMORY_PAGE_SIZE 2048

/*event logging flash start address*/
#define EVENT_LOGGING_START_ADDRESS 0xD000

/*event logging flash end address*/
#define EVENT_LOGGING_END_ADDRESS (EVENT_LOGGING_START_ADDRESS+FLASH_MEMORY_PAGE_SIZE)

/*timeout value to store event log in flash memory every 2mins (1000ms*60s*2min)*/
#define TIME_OUT_VALUE (120000)

/********************************************************************************************************************************/
/* Variables Section - GLOBAL                                                                                                   */
/********************************************************************************************************************************/

/*variable for storing event logs main data and copy of main data*/
static st_event_log st_event_log_main,st_event_log_copy;

/*variable for storing event index*/
static u8 event_log_u8_index;
/********************************************************************************************************************************/
/* Function Implementations Section - GLOBAL                                                                                    */
/********************************************************************************************************************************/

/** @fn       void Event_Logging_Init(void)
*   @brief    function to initialize event logging
*
*   @details  function to initialize event logging
*
*   @pre      flash initialization
*   @post     
*   @param    void
*   @return   void
*/
void Event_Logging_Init(void)
{
    /*clearing main ram data*/
    memset(&st_event_log_main,0,sizeof(st_event_log_main));
    
    /*clearing copy of main ram data*/
    memset(&st_event_log_copy,0,sizeof(st_event_log_copy));
    
    /*clearing event log index*/
    event_log_u8_index=0;
}

/** @fn       void Event_Logging(u16 u16_Event_ID, u32 u32_Time_Stamp, u8 u8_Event_Status)
*   @brief    function to log an event in ram memory
*
*   @details  function to log an event in ram memory
*
*   @pre      flash initialization
*   @post     
*   @param    u16_Event_ID event id 
*   @param    u32_Time_Stamp time stamp
*   @param    u8_Event_Status event status ok/not ok
*   @return   void
*/
void Event_Logging(u16 u16_Event_ID, u8 u8_Event_Status, u32 u32_Time_Stamp)
{
    /*function to report errors in global memory*/
    DIAG_Call_Back_Function(u16_Event_ID,u8_Event_Status);
            
    /*storing event log if it is less than allocated buffer size*/
    if(event_log_u8_index<EVENT_LOG_BUFFER_SIZE)
    {
        st_event_log_main.st_event_field[event_log_u8_index].u16_event_id=(u16_Event_ID | (u8_Event_Status<<15));
        st_event_log_main.st_event_field[event_log_u8_index].u32_timestamp=u32_Time_Stamp;
        event_log_u8_index++;
        
        st_event_log_main.u16_size=(sizeof(st_event)*event_log_u8_index);
        
        /*checking whether to allow to over write copy of main ram memory*/
        if(st_event_log_copy.u8_lock==0)
        {    
            /*copying stored event logs from main ram memory to copy of main ram memory*/
            memcpy(&st_event_log_copy,&st_event_log_main,sizeof(st_event_log_main));    
        }
    }
    
    /*clearing event log index to 0 and copying stored event logs from main ram memory to copy of main ram memory*/
    if(event_log_u8_index>=EVENT_LOG_BUFFER_SIZE)
    {
        /*if already locked,copy latest data to copy of main ram data*/
        if(st_event_log_copy.u8_lock==1)
        {
            /*copying stored event logs from main ram memory to copy of main ram memory*/
            memcpy(&st_event_log_copy,&st_event_log_main,sizeof(st_event_log_main));
        }
        
        /*clearing main ram memory*/
        memset(&st_event_log_main,0,sizeof(st_event_log_main));
        
        /*clearing event log index to 0 for storing new events from starting of the main ram memory*/
        event_log_u8_index=0;
        
        /*indicating that copy of main ram data not stored in flash memory*/
        st_event_log_copy.u8_lock=1;
    }
}

/** @fn       void Event_Logging_Main_Function(void)
*   @brief    function to log an event in flash memory every 2min and it will be called in os task
*
*   @details  function to log an event in flash memory  every 2min
*
*   @pre      flash initialization
*   @post     
*   @param    void
*   @return   void
*/
void Event_Logging_Main_Function(void)
{
    /*variable for storing return value*/
    u8 u8_return_value=0; 
    
    /*variable for storing timer value*/
    u32 u32_2min_timer=0,u32_1sec_timer=0;
    
    /*variable for storing event log start address*/
    static u32 u32_flashAddress=EVENT_LOGGING_START_ADDRESS; 
    
    /*updating timer value initially for timeout*/
    u32_2min_timer=Timer_1ms;
    u32_1sec_timer=Timer_1ms;
    
#if 0
    /*test code*/
    /*u8 rx_data[720]={0};
    //u8 tx_data[10]={0};
    //st_event_log_copy.Event_st[0].u16_event_id=0x2211;
    //st_event_log_copy.Event_st[0].u32_timestamp=0x66554433;
    //memcpy(tx_data,(u8 *)&st_event_log_copy.Event_st[u8_index].u16_event_id,6);*/
#endif
    
    while(1)
    {
        /*checking 2mins expired or not*/
        if(IsTimerExpired(u32_2min_timer,TIME_OUT_VALUE))               
        { 
            /*writing available event logs in flash memory*/
            u8_return_value=Flash_Write(u32_flashAddress,(u8 *)&st_event_log_copy.st_event_field[0],st_event_log_copy.u16_size,1); 
            
            if(u8_return_value==0)
            {
                /*for testing flash content called Flash_Read*/
                //Flash_Read(u32_flashAddress,rx_data,720,1);

                /*incrementing flash address for next write and single address can store 2 bytes of data,so dividing by 2*/
                u32_flashAddress=u32_flashAddress+(st_event_log_copy.u16_size/2)+(st_event_log_copy.u16_size%2);
                
                /*clearing event logging ram data*/
                if(st_event_log_copy.u8_lock==0)
                {
                    /*clearing main ram data*/
                    memset(&st_event_log_copy,0,sizeof(st_event_log_copy));
                 
                    /*clearing event log index to 0 for storing new events from starting of the main ram memory*/
                    event_log_u8_index=0;
                }
                
                /*clearing copy of main ram data*/
                 memset(&st_event_log_copy,0,sizeof(st_event_log_copy));

                /*if address exceeds than allocated flash memory address reassign it to start address*/
                if(u32_flashAddress>EVENT_LOGGING_END_ADDRESS)
                {
                    u32_flashAddress=EVENT_LOGGING_START_ADDRESS;
                }
                
                /*updating timer value for next 2min expiry*/
                u32_2min_timer=Timer_1ms;  
            }                                       
        }
        
        /*test code for generating and storing event logs every 1 sec*/
        if(IsTimerExpired(u32_1sec_timer,1000*1)==1)
        {
            /*event id,timestamp,event status ok/not ok*/
           Event_Logging(EVENT_LOG_SOC_MAX_LIMIT,EVENT_NOK,0xa0b0c0d0);
           
           /*updating timer value for next 1 sec expiry*/
           u32_1sec_timer=Timer_1ms;
        } 
    }
}

/********************************************************************************************************************************/
/* End of file                                                                                                                  */
/********************************************************************************************************************************/
/**@}*/