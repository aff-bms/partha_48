/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name:Header for the driver for balancing                                                                                                         */
/* Author:Pavithran                                                                                                                     */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file bal_cfg.h
*   @brief This file contains Header for the configuration for the driver for balancing
*
*   @details Header for the configuration for the driver for balancing.
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Manikanta
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 22-06-2021
*/

/** @defgroup cell balancing
*
* This file contains Header for the configuration for the driver for balancing
*
*/

/**@{*/
/********************************************************************************************************************************/
/* Include Section                                                                                                              */
/********************************************************************************************************************************/

#ifndef BAL_CFG_H_
#define BAL_CFG_H_

#include "common.h"

/*================== Macros and Definitions ===============================*/

/**
 * BAL statemachine short time definition in 100*ms
 */

#define BAL_STATEMACH_SHORTTIME_100MS     1

/**
 * BAL statemachine long time definition in 100*ms
 */

#define BAL_STATEMACH_LONGTIME_100MS     50

/**
 * BAL statemachine balancing time in 100*ms
 */
#define BAL_STATEMACH_BALANCINGTIME_100MS     10

/**
 * BAL voltage threshold for balancing in mV
 */

#define BAL_THRESHOLD_MV     25

/**
 * BAL hysteresis for voltage threshold when balancing was finished in mV
 */

#define BAL_HYSTERESIS_MV     BAL_THRESHOLD_MV

/**
 * BAL lower voltage limit in MV
 */

#define BAL_LOWER_VOLTAGE_LIMIT_MV     2000

/**
 * BAL upper temperature limit in Celsius
 */

#define BAL_UPPER_TEMPERATURE_LIMIT_DEG     65.0

/**
 * If set to FALSE, SOC-history based balancing is used.
 * An open-circuit-voltage/SOC look-up table is needed.
 * If set to TRUE, voltage-based balancing is used.
 *
*/
#define BALANCING_VOLTAGE_BASED           true


/*================== Constant and Variable Definitions ====================*/

/*================== Function Prototypes ==================================*/

/*================== Function Implementations =============================*/


#endif /* BAL_CFG_H_ */
