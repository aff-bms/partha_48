/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name:Header for the driver for balancing                                                                                                         */
/* Author:Pavithran                                                                                                                     */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file bal.h
*   @brief This file contains Header for the driver for balancing
*
*   @details Header for the driver for balancing.
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Manikanta
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 22-06-2021
*/

/** @defgroup cell balancing
*
* This file contains Header for the driver for balancing
*
*/

/**@{*/
/********************************************************************************************************************************/
/* Include Section                                                                                                              */
/********************************************************************************************************************************/
#ifndef BAL_H_
#define BAL_H_

#include "cell_balancing_cfg.h"

/*================== Macros and Definitions ===============================*/

/*================== Constant and Variable Definitions ====================*/

/*================== Function Prototypes ==================================*/

/*================== Function Implementations =============================*/

/*================== Constant and Variable Definitions ====================*/

/**
 * States of the BAL state machine
 */
typedef enum {
    /* Init-Sequence */
    BAL_STATEMACH_UNINITIALIZED             = 0,    /*!<    */
    BAL_STATEMACH_INITIALIZATION            = 1,    /*!<    */
    BAL_STATEMACH_INITIALIZED               = 2,    /*!<    */
    BAL_STATEMACH_CHECK_BALANCING           = 3,    /*!<    */
    BAL_STATEMACH_BALANCE                   = 4,    /*!<    */
    BAL_STATEMACH_NOBALANCING               = 5,    /*!<    */
    BAL_STATEMACH_ALLOWBALANCING            = 6,    /*!<    */
    BAL_STATEMACH_GLOBALDISABLE             = 7,    /*!<    */
    BAL_STATEMACH_GLOBALENABLE              = 8,    /*!<    */
    BAL_STATEMACH_UNDEFINED                 = 20,   /*!< undefined state                                */
    BAL_STATEMACH_RESERVED1                 = 0x80, /*!< reserved state                                 */
    BAL_STATEMACH_ERROR                     = 0xF0, /*!< Error-State:  */
} BAL_STATEMACH_e;


/**
 * Substates of the BAL state machine
 */
typedef enum {
    BAL_ENTRY                                    = 0,    /*!< Substate entry state       */
    BAL_CHECK_IMBALANCES                         = 1,  /*!< Check if balancing has been initialized       */
    BAL_COMPUTE_IMBALANCES                       = 2,  /*!< Compute imbalances */
    BAL_ACTIVATE_BALANCING                       = 3,  /*!< Activated balancing resistors */
    BAL_CHECK_LOWEST_VOLTAGE                     = 4,  /*!< Check if lowest voltage is still  above limit */
    BAL_CHECK_CURRENT                            = 5,  /*!< Check if current is still  under limit */
} BAL_STATEMACH_SUB_e;


/**
 * State requests for the BAL statemachine
 */
typedef enum {
    BAL_STATE_INIT_REQUEST                     = BAL_STATEMACH_INITIALIZATION,           /*!<    */
    BAL_STATE_ERROR_REQUEST                    = BAL_STATEMACH_ERROR,   /*!<    */
    BAL_STATE_NOBALANCING_REQUEST              = BAL_STATEMACH_NOBALANCING,     /*!<    */
    BAL_STATE_ALLOWBALANCING_REQUEST           = BAL_STATEMACH_ALLOWBALANCING,     /*!<    */
    BAL_STATE_GLOBAL_DISABLE_REQUEST           = BAL_STATEMACH_GLOBALDISABLE,     /*!<    */
    BAL_STATE_GLOBAL_ENABLE_REQUEST            = BAL_STATEMACH_GLOBALENABLE,     /*!<    */
    BAL_STATE_NO_REQUEST                       = BAL_STATEMACH_RESERVED1,                /*!<    */
} BAL_STATE_REQUEST_e;


/**
 * Possible return values when state requests are made to the BAL statemachine
 */
typedef enum {
    BAL_OK                                 = 0,    /*!< BAL --> ok                             */
    BAL_BUSY_OK                            = 1,    /*!< BAL busy                               */
    BAL_REQUEST_PENDING                    = 2,    /*!< requested to be executed               */
    BAL_ILLEGAL_REQUEST                    = 3,    /*!< Request can not be executed            */
    BAL_INIT_ERROR                         = 7,    /*!< Error state: Source: Initialization    */
    BAL_OK_FROM_ERROR                      = 8,    /*!< Return from error --> ok               */
    BAL_ERROR                              = 20,   /*!< General error state                    */
    BAL_ALREADY_INITIALIZED                = 30,   /*!< Initialization of BAL already finished */
    BAL_ILLEGAL_TASK_TYPE                  = 99,   /*!< Illegal                                */
} BAL_RETURN_TYPE_e;


/**
 * This structure contains all the variables relevant for the BAL state machine.
 * The user can get the current state of the BAL state machine with this variable
 */
typedef struct {
    uint16_t timer;                         /*!< time in ms before the state machine processes the next state, e.g. in counts of 1ms */
    BAL_STATE_REQUEST_e statereq;           /*!< current state request made to the state machine                                     */
    BAL_STATEMACH_e state;                  /*!< state of Driver State Machine                                                       */
    BAL_STATEMACH_SUB_e substate;           /*!< current substate of the state machine                                               */
    BAL_STATEMACH_e laststate;              /*!< previous state of the state machine                                                 */
    uint8_t lastsubstate;                   /*!< previous substate of the state machine                                              */
    uint8_t triggerentry;                   /*!< counter for re-entrance protection (function running flag) */
    uint32_t ErrRequestCounter;             /*!< counts the number of illegal requests to the BAL state machine */
    e_STD_RETURN_TYPE initFinished;         /*!< E_OK if statemachine initialized, otherwise E_NOT_OK */
    uint8_t active;                         /*!< indicate if balancing active or not */
    uint32_t balancing_threshold;           /*!< effective balancing threshold */
    uint8_t balancing_allowed;              /*!< flag to disable balancing */
    uint8_t balancing_global_allowed;       /*!< flag to globally disable balancing */
} BAL_STATE_s;


/*================== Function Prototypes ==================================*/

/**
 * @brief   sets the current state request of the state variable bal_state.
 *
 * This function is used to make a state request to the state machine,e.g, start voltage measurement,
 * read result of voltage measurement, re-initialization
 * It calls BAL_CheckStateRequest() to check if the request is valid.
 * The state request is rejected if is not valid.
 * The result of the check is returned immediately, so that the requester can act in case
 * it made a non-valid state request.
 *
 * @param   statereq                state request to set
 *
 * @return  retVal                  current state request, taken from BAL_STATE_REQUEST_e
 */
extern BAL_RETURN_TYPE_e BAL_SetStateRequest(BAL_STATE_REQUEST_e statereq);

/**
 * @brief   gets the current state.
 *
 * This function is used in the functioning of the BAL state machine.
 *
 * @return  current state, taken from BAL_STATEMACH_e
 */
extern  BAL_STATEMACH_e BAL_GetState(void);

/**
 * @brief   gets the initialization state.
 *
 * This function is used for getting the balancing initialization state
 *
 * @return  E_OK if initialized, otherwise E_NOT_OK
 */
extern  e_STD_RETURN_TYPE BAL_GetInitializationState(void);

/**
 * @brief   trigger function for the BAL driver state machine.
 *
 * This function contains the sequence of events in the BAL state machine.
 * It must be called time-triggered, every 1ms.
 */
extern void BAL_Trigger(void);

extern uint8_t BAL_Activate_Balancing_Voltage(void);

#endif /* BAL_H_ */
