/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name:UART                                                                                                          */
/* Author:Pavithran                                                                                                                     */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file uart_app.h
*   @brief This file contains API's which are required to communicate with CMU via UART.
*
*   @details Uart application fucntions.
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Pavithran
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 22-06-2021
*/

/** @defgroup CMU
*
* UART Application functions
*
*/

/**@{*/
/********************************************************************************************************************************/
/* Include Section                                                                                                              */
/********************************************************************************************************************************/
#ifndef UART_APP_H
#define	UART_APP_H
#include "common.h"  

/********************************************************************************************************************************/
/* Function Prototype Section - GLOBAL                                                                                          */
/********************************************************************************************************************************/
void UART_Send_Buffer(u8 Base,u8 *txData,u8 len);

void UART_Receive_Buffer(u8 Base,u8 *rxData,u8 len);

#endif	/* XC_HEADER_TEMPLATE_H */
/********************************************************************************************************************************/
/* End of file                                                                                                                  */
/********************************************************************************************************************************/
/**@}*/

