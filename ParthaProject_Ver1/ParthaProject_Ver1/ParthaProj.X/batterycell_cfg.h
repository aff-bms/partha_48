/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name:Configuration of the battery cell                                                                              */
/* Author:Pavithran                                                                                                             */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file bms_cfg.h
*   @brief Configuration of the battery cell (e.g., minimum and maximum cell voltage)
*
*   @details Configuration of the battery cell (e.g., minimum and maximum cell voltage)
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Pavithran
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 22-06-2021
*/

/** @defgroup battery cell config
*
* This files contains basic macros of the battery cell in order to derive needed inputs
* in other parts of the software. These macros are all depended on the hardware.
*
*/

/**@{*/
/********************************************************************************************************************************/
/* Include Section                                                                                                              */
/********************************************************************************************************************************/

#ifndef BATTERYCELL_CFG_H_
#define BATTERYCELL_CFG_H_


#if BUILD_MODULE_IMPORT_CELL_DATASHEET == 1

#include "LG_INR18650MJ1.h"

#else


/**
 * @ingroup CONFIG_BATTERYCELL
 * Maximum temperature limit during discharge.
 * When maximum safety limit (MSL) is violated, error state is requested and
 * contactors will open. When recommended safety limit (RSL) or maximum
 * operating limit (MOL) is violated, the respective flag will be set.
 * \par Type:
 * int
 * \par Unit:
 * &deg;C
 * \par Default:
 * 75
*/
#define BC_TEMPMAX_DISCHARGE_MSL      55.0
#define BC_TEMPMAX_DISCHARGE_RSL      50.0
#define BC_TEMPMAX_DISCHARGE_MOL      45.0

/**
 * @ingroup CONFIG_BATTERYCELL
 * Minimum temperature limit during discharge.
 * When maximum safety limit (MSL) is violated, error state is requested and
 * contactors will open. When recommended safety limit (RSL) or maximum
 * operating limit (MOL) is violated, the respective flag will be set.
 * \par Type:
 * int
 * \par Unit:
 * &deg;C
 * \par Default:
 * -20
*/
#define BC_TEMPMIN_DISCHARGE_MSL      -20.0
#define BC_TEMPMIN_DISCHARGE_RSL      -15.0
#define BC_TEMPMIN_DISCHARGE_MOL      -10.0

/**
 * @ingroup CONFIG_BATTERYCELL
 * Maximum temperature limit during charge.
 * When maximum safety limit (MSL) is violated, error state is requested and
 * contactors will open. When recommended safety limit (RSL) or maximum
 * operating limit (MOL) is violated, the respective flag will be set.
 * \par Type:
 * int
 * \par Unit:
 * &deg;C
 * \par Default:
 * 50
*/
#define BC_TEMPMAX_CHARGE_MSL     55.0
#define BC_TEMPMAX_CHARGE_RSL     50.0
#define BC_TEMPMAX_CHARGE_MOL     45.0

/**
 * @ingroup CONFIG_BATTERYCELL
 * Minimum temperature limit during discharge.
 * When maximum safety limit (MSL) is violated, error state is requested and
 * contactors will open. When recommended safety limit (RSL) or maximum
 * operating limit (MOL) is violated, the respective flag will be set.
 * \par Type:
 * int
 * \par Unit:
 * &deg;C
 * \par Default:
 * 0
*/
#define BC_TEMPMIN_CHARGE_MSL     -20.0
#define BC_TEMPMIN_CHARGE_RSL     -15.0
#define BC_TEMPMIN_CHARGE_MOL     -10.0


#define BC_DIE_TEMPMAX_MSL     65.0
#define BC_DIE_TEMPMAX_RSL     60.0
#define BC_DIE_TEMPMAX_MOL     55.0

#define BC_DIE_TEMPMIN_MSL     -20.0
#define BC_DIE_TEMPMIN_RSL     -15.0
#define BC_DIE_TEMPMIN_MOL     -10.0

/**
 * @ingroup CONFIG_BATTERYCELL
 * Maximum cell voltage limit.
 * When maximum safety limit (MSL) is violated, error state is requested and
 * contactors will open. When recommended safety limit (RSL) or maximum
 * operating limit (MOL) is violated, the respective flag will be set.
 * \par Type:
 * int
 * \par Unit:
 * mV
 * \par Default:
 * 2800
*/
#define BC_VOLTMAX_MSL      4100
#define BC_VOLTMAX_RSL      4050
#define BC_VOLTMAX_MOL      4000

/**
 * @ingroup CONFIG_BATTERYCELL
 * nominal cell voltage according to datasheet
 * \par Type:
 * int
 * \par Unit:
 * mV
 * \par Default:
 * 3600
*/
#define BC_VOLT_NOMINAL     3600

/**
 * @ingroup CONFIG_BATTERYCELL
 * Minimum cell voltage limit.
 * When maximum safety limit (MSL) is violated, error state is requested and
 * contactors will open. When recommended safety limit (RSL) or maximum
 * operating limit (MOL) is violated, the respective flag will be set.
 * \par Type:
 * int
 * \par Unit:
 * mV
 * \par Default:
 * 2500
*/
#define BC_VOLTMIN_MSL      3000
#define BC_VOLTMIN_RSL      3100
#define BC_VOLTMIN_MOL      3150

/**
 * @ingroup CONFIG_BATTERYCELL
 * Deep-discharge cell voltage limit.
 * If this voltage limit is violated, the cell is faulty. The BMS won't allow
 * a closing of the contactors until this cell is replaced. a replacement of
 * the cell is confirmed by sending the respective CAN debug message
 * \par Type:
 * int
 * \par Unit:
 * mV
 * \par Default:
 * BC_VOLTMIN_MSL
*/
#define BC_VOLT_DEEP_DISCHARGE          BC_VOLTMIN_MSL

/**
 * @ingroup CONFIG_BATTERYCELL
 * Maximum discharge current limit.
 * When maximum safety limit (MSL) is violated, error state is requested and
 * contactors will open. When recommended safety limit (RSL) or maximum
 * operating limit (MOL) is violated, the respective flag will be set.
 * \par Type:
 * int
 * \par Unit:
 * mA
 * \par Default:
 * 180000
*/
#define BC_CURRENTMAX_DISCHARGE_MSL     500000
#define BC_CURRENTMAX_DISCHARGE_RSL     450000
#define BC_CURRENTMAX_DISCHARGE_MOL     400000

/**
 * @ingroup CONFIG_BATTERYCELL
 * Maximum charge current limit.
 * When maximum safety limit (MSL) is violated, error state is requested and
 * contactors will open. When recommended safety limit (RSL) or maximum
 * operating limit (MOL) is violated, the respective flag will be set.
 * \par Type:
 * int
 * \par Unit:
 * mA
 * \par Default:
 * 180000
*/
#define BC_CURRENTMAX_CHARGE_MSL        50000
#define BC_CURRENTMAX_CHARGE_RSL        45000
#define BC_CURRENTMAX_CHARGE_MOL        40000

/*
 * the cell capacity used for SOC calculation, in this case Ah counting
 * @type int
 * @unit mAh
 * @default 4800
 * @group
 */
#define BC_CAPACITY 4800

#if BC_VOLTMIN_MSL < BC_VOLT_DEEP_DISCHARGE
#error "Configuration error! - Maximum safety limit for under voltage can't be lower than deep-discharge limit"
#endif

/********************************************************************/
/**
 * @ingroup CONFIG_SOX
 * the cell capacity used for SOC calculation, in this case Ah counting
 * Specified once according to data sheet of cell usually.
 * \par Type:
 * float
 * \par Unit:
 * mAh
 * \par Default:
 * 20000.0
*/
#define SOX_CELL_CAPACITY               50000.0f

/**
 * @ingroup CONFIG_SOX
 * the maximum current in charge direction that the battery pack can sustain.
 * Normally set once for the specific battery cell from datasheet
 * \par Type:
 * float
 * \par Unit:
 * A
 * \par Default:
 * 120.0
*/
#define SOX_CURRENT_MAX_CONTINUOUS_CHARGE           50.00f

/**
 * @ingroup CONFIG_SOX
 * the maximum current in discharge direction that the battery pack can deliver.
 * Normally set once for the specific battery cell from datasheet
 *
 * \par Type:
 * float
 * \par Unit:
 * A
 * \par Default:
 * 120.0
*/
#define SOX_CURRENT_MAX_CONTINUOUS_DISCHARGE 50.00f

/**
 * @ingroup CONFIG_SOX
 * the current that the battery pack should be able to discharge when in LIMPHOME mode,
 * i.e., something noncritical went wrong but it should be able to drive home.
 * The value is system engineer's choice.
 *
 * \par Type:
 * float
 * \par Unit:
 * A
 * \par Range:
 * [1,40]
 * \par Default:
 * 20.0
*/
#define SOX_CURRENT_LIMP_HOME 40.00f

/**
 * @ingroup CONFIG_SOX
 * the cold temperature where the derating of maximum discharge current starts,
 * i.e., below this temperature battery pack should not deliver full discharge current
 * \par Type:
 * float
 * \par Range:
 * -40.0<x<80.0
 * \par Unit:
 * &deg;C
 * \par Default:
 * 0.0
*/
#define SOX_TEMP_LOW_CUTOFF_DISCHARGE 55.0f

/**
 * @ingroup CONFIG_SOX
 * the cold temperature where the derating of maximum discharge current is fully applied,
 * i.e., below this temperature battery pack should not deliver any current in discharge direction
 * \par Type:
 * float
 * \par Range:
 * -40.0<x<80.0
 * \par Unit:
 * &deg;C
 * \par Default:
 * -10.0
*/
#define SOX_TEMP_LOW_LIMIT_DISCHARGE -20.0f

/**
 * @ingroup CONFIG_SOX
 * the cold temperature where the derating of maximum charge current starts,
 * i.e., below this temperature battery pack should not deliver full charge current
 * \par Type:
 * float
 * \par Range:
 * -40.0<x<80.0
 * \par Unit:
 * &deg;C
 * \par Default:
 * 0.0
*/
#define SOX_TEMP_LOW_CUTOFF_CHARGE 55.0f

/**
 * @ingroup CONFIG_SOX
 * the cold temperature where the derating of maximum charge current is fully applied,
 * i.e., below this temperature battery pack should not deliver any current in charge direction
 * \par Type:
 * float
 * \par Range:
 * -40.0<x<80.0
 * \par Unit:
 * &deg;C
 * \par Default:
 * -10.0
*/
#define SOX_TEMP_LOW_LIMIT_CHARGE 55.0f

/**
 * @ingroup CONFIG_SOX
 * the hot temperature where the derating of maximum discharge current starts,
 * i.e., above this temperature battery pack should not deliver full discharge current
 * \par Type:
 * float
 * \par Range:
 * -40.0<x<80.0
 * \par Unit:
 * &deg;C
 * \par Default:
 * 45.0
*/
#define SOX_TEMP_HIGH_CUTOFF_DISCHARGE 55.0f

/**
 * @ingroup CONFIG_SOX
 * the hot temperature where the derating of maximum discharge current is fully applied,
 * i.e., above this temperature battery pack should not deliver any current in discharge direction
 * \par Type:
 * float
 * \par Range:
 * -40.0<x<80.0
 * \par Unit:
 * &deg;C
 * \par Default:
 * 55.0
*/
#define SOX_TEMP_HIGH_LIMIT_DISCHARGE 55.0f

/**
 * @ingroup CONFIG_SOX
 * the hot temperature where the derating of maximum charge current starts,
 * i.e., above this temperature battery pack should not deliver full charge current
 * \par Type:
 * float
 * \par Range:
 * -40.0<x<80.0
 * \par Unit:
 * &deg;C
 * \par Default:
 * 45.0
*/
#define SOX_TEMP_HIGH_CUTOFF_CHARGE 55.0f

/**
 * @ingroup CONFIG_SOX
 * the hot temperature where the derating of maximum charge current is fully applied,
 * i.e., above this temperature battery pack should not deliver any current in charge direction
 * \par Type:
 * float
 * \par Range:
 * -40.0<x<80.0
 * \par Unit:
 * &deg;C
 * \par Default:
 * 55.0
*/
#define SOX_TEMP_HIGH_LIMIT_CHARGE 55.0f

/**
 * @ingroup CONFIG_SOX
 * above this SOC value battery pack should not be exposed to full current in charge direction
 * \par Type:
 * int
 * \par Range:
 * 0<=x<=10000
 * \par Unit:
 * 0.01%
 * \par Default:
 * 8500
*/
#define SOX_SOC_CUTOFF_CHARGE 8500

/**
 * @ingroup CONFIG_SOX
 * above this SOC value battery pack should not be exposed to any current in charge direction
 * \par Type:
 * int
 * \par Range:
 * 0<=x<=10000
 * \par Unit:
 * 0.01%
 * \par Default:
 * 9500
*/
#define SOX_SOC_LIMIT_CHARGE 9500

/**
 * @ingroup CONFIG_SOX
 * below this SOC value battery pack should not deliver full current in discharge direction
 * \par Type:
 * int
 * \par Range:
 * 0<=x<=10000
 * \par Unit:
 * 0.01%
 * \par Default:
 * 1500
*/
#define SOX_SOC_CUTOFF_DISCHARGE 1500

/**
 * @ingroup CONFIG_SOX
 * below this SOC value battery pack should not deliver any current in discharge direction
 * \par Type:
 * int
 * \par Range:
 * 0<=x<=10000
 * \par Unit:
 * 0.01%
 * \par Default:
 * 500
*/
#define SOX_SOC_LIMIT_DISCHARGE 500

/**
 * @ingroup CONFIG_SOX
 * above this voltage value battery pack should not be exposed to full current in charge direction
 * \par Type:
 * int
 * \par Unit:
 * mV
 * \par Range:
 * [2000,2500]
 * \par Default:
 * 2400
*/
#define SOX_VOLT_CUTOFF_CHARGE 4000

/**
 * @ingroup CONFIG_SOX
 * above this voltage value battery pack should not be exposed to any current in charge direction
 * \par Type:
 * int
 * \par Unit:
 * mV
 * \par Range:
 * [2500,3000]
 * \par Default:
 * 2550
*/
#define SOX_VOLT_LIMIT_CHARGE 4100

/**
 * @ingroup CONFIG_SOX
 * below this voltage value battery pack should not deliver full current in discharge direction
 * \par Type:
 * int
 * \par Unit:
 * mV
 * \par Range:
 * [1900,2200]
 * \par Default:
 * 2000
*/
#define SOX_VOLT_CUTOFF_DISCHARGE 3100

/**
 * @ingroup CONFIG_SOX
 * below this voltage value battery pack should not deliver any current in discharge direction
 * \par Type:
 * int
 * \par Unit:
 * mV
 * \par Range:
 * [1700,1900]
 * \par Default:
 * 1750
*/
#define SOX_VOLT_LIMIT_DISCHARGE 2750

/**
 * @ingroup CONFIG_PLAUSIBILITY
 * Defines maximum accepted cell voltage as a this is the LTC operating limit
 * \par Type:
 * int
 */
#define SPL_MAX_CELL_VOLTAGE_LIMIT_mV            (4200)

/**
 * @ingroup CONFIG_PLAUSIBILITY
 * Defines maximum accepted cell voltage as a this is the LTC operating limit
 * \par Type:
 * int
 */
#define SPL_MIN_CELL_VOLTAGE_LIMIT_mV            (2700)

/**
 * @ingroup CONFIG_PLAUSIBILITY
 * Defines the accepted tolerance between the average cell voltage and
 * individual cell voltage readings
 * \par Type:
 * int
 * \par Range:
 * [0, 2000]
 * \par Default:
 * 1000
*/
#define SPL_CELL_VOLTAGE_AVG_TOLERANCE_mV        (1000)

/**
 * @ingroup CONFIG_PLAUSIBILITY
 * Defines the minimum operating range temperature of cell temp measurements
 * \par Type:
 * int
 * \par Range:
 * [-150, 0]
 * \par Default:
 * -25
*/
#define SPL_MINIMUM_TEMP_MEASUREMENT_RANGE       (-40)

/**
 * @ingroup CONFIG_PLAUSIBILITY
 * Defines the maximum operating range temperature of cell temp measurements
 * \par Type:
 * int
 * \par Range:
 * [80, 250]
 * \par Default:
 * 125
*/
#define SPL_MAXIMUM_TEMP_MEASUREMENT_RANGE       (150)

#endif

#endif /* BATTERYCELL_CFG_H_ */
