/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name: state of health                                                                                                       */
/* Author:Karthik                                                                                                                     */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file soh.c
*   @brief This file contains API's which are required to communicate with CMU via UART.
*
*   @details soh .
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Karthik
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 16-07-2021
*/

/** @defgroup C
*
* 
*
*/

/****SOH*****/
#include "math.h"
#include "common.h"


/********************************************************************************************************************************/
/* Variables Section - GLOBAL                                                                                                   */
/********************************************************************************************************************************/
u32 u32_one_time_variable =1;
f32 f32_c1 =0,f32_c2 =0,f32_c3 =0,f32_c4 =0,f32_c5 =0,f32_c6 =0;
f32 f32_C1 =0,f32_C2 =0,f32_C3 =0,f32_C4 =0,f32_C5 =0,f32_C6 =0;
f32 f32_K;
f32 f32_Y1,f32_Y3,f32_Y2;
f32 f32_X1,f32_X2,f32_X3,f32_X4;
f32 f32_Y2_R ;
f32 f32_Y3_R ;
f32 f32_Y2_C;
f32 f32_Y3_C;
u8  u8_Complex_numbers = 0;
f32 f32_Ah_data[300];
f32 f32_soc_data[300];
f32 f32_Ah_data_1[300];
f32 f32_measY_1[300];
f32 f32_measX_1[300];


/** @fn      void cube_roots(f32 l,f32 i, f32 j,f32 k)
*   @brief    find the roots of cubic equation
*
*   @details  To find the Roots of cubic equation
*
*   @pre      
*   @post     
*   @param    coeeficient of x^2
*   @param    coeeficient of x^
*   @param    constant
*   @return   return success
*/
void cube_roots(f32 f32_l,f32 f32_i, f32 f32_j,f32 f32_k)
{
    /*Local variables for calculating roots*/
    f32 f32_c1 = ((3*f32_j/f32_l) - (f32_i*f32_i/(f32_l*f32_l)))/3;
    f32 f32_c2 = ((2 * f32_i*f32_i*f32_i/(f32_l*f32_l*f32_l)) - (9*f32_i*f32_j/(f32_l*f32_l)) + (27 * f32_k/f32_l))/27;
    f32 f32_c3 = (f32_c2*f32_c2/4) + (f32_c1*f32_c1*f32_c1/27 );
    if (f32_c3 > 0)
    {
        f32 f32_c41 = -(f32_c2/2)+sqrt(f32_c3);
        f32 f32_c42 = pow(f32_c41,0.33);
        f32 f32_c43 = -(f32_c2/2)- sqrt(f32_c3);
        f32 f32_c44 ;//
        if(f32_c43 < 0)
        {
             f32_c44 = -pow(-f32_c43,0.33);
        }
        else
        {
            f32_c44 = pow(f32_c43,0.33);
        }
        f32_Y1 = f32_c42 + f32_c44 - (f32_i/(3*f32_l));


        f32_Y2_R = -(f32_c42 + f32_c44)/2 - (f32_i/(3*f32_l));
        f32_Y3_R = -(f32_c42 + f32_c44)/2 - (f32_i/(3*f32_l));
        f32_Y2_C = (f32_c42 - f32_c44)*sqrt(3)/2;
        f32_Y3_C = -(f32_c42 - f32_c44)*sqrt(3)/2;

        u8_Complex_numbers = 1;
    }
    else
    {

        f32 f32_c4 = pow(((f32_c2 * f32_c2 / 4)-f32_c3), 0.5);

        f32 f32_c5 = pow(f32_c4, 0.33);
        f32 f32_c6 = acos(-f32_c2/(2 * f32_c4));
        if(f32_c2==0)
        {
            f32_c6 =3.14/2;
        }

        f32 f32_c7 = -f32_c5;
        f32 f32_c8 = cos(f32_c6/3);
        f32 f32_c9 = sqrt(3) * sin(f32_c6/3);
        f32 f32_c10 = -f32_i/(3*f32_l);

        f32_Y1 = 2 * f32_c5 *cos(f32_c6 / 3) - (f32_i/(3*f32_l));
        f32_Y2 = f32_c7 *( f32_c8 + f32_c9) + f32_c10;
        f32_Y3 = f32_c7*(f32_c8 - f32_c9)+ f32_c10;

        if(  (f32_Y1< 0 && f32_Y2 < 0) || (f32_Y1< 0 && f32_Y3 < 0) || (f32_Y3< 0 && f32_Y2 < 0) )
        {
            u8_Complex_numbers = 2;
        }

    }

}


/** @fn      void Quartic_roots(f32 a,f32 b, f32 c, f32 d,f32 e)
*   @brief    find the roots of Quartic equation
*
*   @details  To find the Roots of Quartic equation
*
*   @pre      
*   @post     
*   @param    coeeficient of x^4
*   @param    coeeficient of x^3
*   @param    coeeficient of x^2
*   @param    coeeficient of x^1
*   @param    constant
*   @return   return success
*/
void Quartic_roots(f32 f32_a,f32 f32_b, f32 f32_c, f32 f32_d,f32 f32_e)
{
    /*Local variables for calculating cube roots*/
    f32 f32_f = f32_c- ( 3 * f32_b * f32_b / 8);
    f32 f32_g = f32_d + ( f32_b * f32_b * f32_b / 8) - ( f32_b * f32_c/2);
    f32 f32_h = f32_e - (3 * f32_b * f32_b* f32_b* f32_b /256) + (f32_b * f32_b * f32_c/16) - (f32_b * f32_d/4);
    cube_roots(1, f32_f/2, (f32_f*f32_f - 4*f32_h)/16, -f32_g*f32_g/64);

    f32 f32_p ;
    f32 f32_q ;
    f32 f32_r ;
    f32 f32_s ;

    if(u8_Complex_numbers == 1)
    {
        f32 f32_pp_imag;
        f32 f32_pp_real=sqrt((f32_Y2_R + sqrt(f32_Y2_R * f32_Y2_R + f32_Y2_C * f32_Y2_C))/2);
        if(f32_Y2_C > 0)
                f32_pp_imag=sqrt((-f32_Y2_R + sqrt(f32_Y2_R * f32_Y2_R + f32_Y2_C*f32_Y2_C))/2);
        else
                f32_pp_imag=-sqrt((-f32_Y2_R + sqrt(f32_Y2_R * f32_Y2_R + f32_Y2_C*f32_Y2_C))/2);

        f32 f32_qq_real=sqrt((f32_Y3_R + sqrt(f32_Y3_R * f32_Y3_R + f32_Y3_C * f32_Y3_C))/2);
        f32 f32_qq_imag;

        if(f32_Y2_C > 0)
                f32_qq_imag=sqrt((-f32_Y3_R + sqrt(f32_Y3_R * f32_Y3_R + f32_Y3_C*f32_Y3_C))/2);
        else
                f32_qq_imag=-sqrt((-f32_Y3_R + sqrt(f32_Y3_R * f32_Y3_R + f32_Y3_C * f32_Y3_C))/2);

        f32_r = -f32_g/(8*(f32_pp_real * f32_pp_real + f32_pp_imag * f32_pp_imag));
        if(f32_g==0)
        {
            f32_r=0;
        }
        f32_s = f32_b/(4*f32_a);

        f32_X1 = f32_pp_real + f32_qq_real + f32_r - f32_s;
        f32_X4 = -f32_pp_real - f32_qq_real + f32_r - f32_s;
        return;
    }
    if(u8_Complex_numbers == 2)
    {
        return;
    }

    if(f32_Y1 < f32_Y2 && f32_Y1 < f32_Y3)
    {
        f32_p = sqrt(f32_Y2);
        f32_q = sqrt(f32_Y3);
        f32_r = -f32_g/(8*f32_p*f32_q);
        f32_s = f32_b/(4*f32_a);
    }
    else if(f32_Y2 < f32_Y1 && f32_Y2< f32_Y3)
    {
        f32_p = sqrt(f32_Y1);
        f32_q = sqrt(f32_Y3);
        f32_r = -f32_g/(8*f32_p*f32_q);
        f32_s = f32_b/(4*f32_a);
    }
    else
    {
        f32_p = sqrt(f32_Y1);
        f32_q = sqrt(f32_Y2);
        f32_r = -f32_g/(8*f32_p*f32_q);
        if(f32_g==0)
        {
            f32_r=0;
        }
        f32_s = f32_b/(4*f32_a);
    }

    f32_X1 = f32_p + f32_q + f32_r - f32_s;
    f32_X2 = f32_p - f32_q - f32_r - f32_s;
    f32_X3 = -f32_p + f32_q - f32_r - f32_s;
    f32_X4 = -f32_p - f32_q + f32_r - f32_s;

}
/********************************************************************************************************************************/
/* Variables Section - GLOBAL                                                                                                   */
/********************************************************************************************************************************/

#if 0
f32 Ah_data_123[300] = {
-0.00078,-0.00082,-0.00085,
-0.00089,-0.00094,-0.00098,-0.00103,-0.00108,-0.00113,-0.00117,-0.00123,-0.00127,-0.00132,-0.00137,-0.00142,-0.00147,-0.00153,-0.00158,-0.00163,-0.00169,-0.00174,-0.00174,
-0.0018 ,-0.00185,-0.0019 ,-0.00194,-0.00197,-0.00201,-0.00204,-0.00207,-0.00211,-0.00214,-0.00218,-0.00221,-0.00224,-0.00228,-0.00231,-0.00235,-0.00238,-0.00241,-0.00245,
-0.00248,-0.00252,-0.00255,-0.00258,-0.00262,-0.00265,-0.00269,-0.00272,-0.00275,-0.00279,-0.00282,-0.00285,-0.00289,-0.00292,-0.00298,-0.00303,-0.00309,-0.00303,-0.00309,
-0.00315,-0.0032 ,-0.00326,-0.00331,-0.00337,-0.00342,-0.00348,-0.00352,-0.00357,-0.00362,-0.00366,-0.0037 ,-0.00375,-0.0038 ,-0.00384,-0.00389,-0.00393,-0.00393,-0.00393,
-0.00397,-0.004  ,-0.00404,-0.00408,-0.00411,-0.00415,-0.00418,-0.00422,-0.00425,-0.00429,-0.00432,-0.00435,-0.00438,-0.00441,-0.00444,-0.00447,-0.0045 ,-0.00453,-0.00456,
-0.00459,-0.00462,-0.00465,-0.00467,-0.0047 ,-0.00472,-0.00475,-0.00477,-0.0048 ,-0.00482,-0.00484,-0.00487,-0.0049 ,-0.00492,-0.00495,-0.00497,-0.00499,-0.00502,-0.00504,
-0.00507,-0.00509,-0.0051 ,-0.00511,-0.00511,-0.00511,-0.00512,-0.00512,-0.00513,-0.00513,-0.00513,-0.00514,-0.00514,-0.00514,-0.00514,-0.00514,-0.00514,-0.00514,-0.00514,
-0.00514,-0.00514,-0.00513,-0.00513,-0.00513,-0.00514,-0.00514,-0.00514,-0.00515,-0.00515,-0.00515,-0.00515,-0.00516,-0.00516,-0.00517,-0.00518,-0.00519,-0.00521,-0.00521,
-0.00522,-0.00523,-0.00525,-0.00526,-0.00527,-0.00528,-0.0053 ,-0.00531,-0.00533,-0.00535,-0.00537,-0.00538,-0.0054 ,-0.00542,-0.00544,-0.00546,-0.00547,-0.00549,-0.00551,
-0.00552,-0.00554,-0.00556,-0.00558,-0.00559,-0.00561,-0.00563,-0.00565,-0.00567,-0.00569,-0.00571,-0.00574,-0.00576,-0.00578,-0.0058 ,-0.00583,-0.00585,-0.00587,-0.00589,
-0.00591,-0.00594,-0.00596,-0.00598,-0.006  ,-0.00602,-0.00605,-0.00607,-0.00608,-0.0061 ,-0.00611,-0.00612,-0.00613,-0.00615,-0.00616,-0.00617,-0.00618,-0.00619,-0.00617,
-0.00615,-0.00613,-0.00612,-0.0061 ,-0.00608,-0.00606,-0.00604,-0.00602,-0.006  ,-0.00598,-0.00597,-0.00595,-0.00594,-0.00592,-0.00591,-0.00589,-0.00588,-0.00586,-0.00585,
-0.00583,-0.00581,-0.00578,-0.00577,-0.00575,-0.00574,-0.00572,-0.00569,-0.00568,-0.00567,-0.00566,-0.00566,-0.00565,-0.00564,-0.00563,-0.00563,-0.00562,-0.00561,-0.00564,
-0.00566,-0.0057 ,-0.00573,-0.00577,-0.00579,-0.00582,-0.00584,-0.00594,-0.00599,-0.00604,-0.00609,-0.00614,-0.00619,-0.00624,-0.00629,-0.00634,-0.0064 ,-0.00646,-0.00653,
-0.00659,-0.00665,-0.00671,-0.00677,-0.00683,-0.00689,-0.00696,-0.00701,-0.00707,-0.00712,-0.00718,-0.00723,-0.00728,-0.00733,-0.00738,
-0.00754,-0.00759,-0.00763,-0.00767,-0.00772,-0.00776,-0.00781,-0.00785,-0.00789,-0.00794,-0.00795,-0.00796,-0.00797,-0.00798,-0.00799,-0.00801
};


f32 Ah_data_11234[300]={
-0.00749,-0.00754,-0.00759,-0.00763,-0.00767,-0.00772,-0.00776,-0.00781,-0.00785,-0.00789,-0.00794,-0.00795,-0.00796,-0.00797,-0.00798,-0.00799,-0.00801,-0.00802,-0.00803,-0.00804,
-0.00805,-0.00806,-0.00807,-0.00808,-0.00809,-0.0081 ,-0.00811,-0.00813,-0.00814,-0.00815,-0.00816,-0.00817,-0.00818,-0.00819,-0.0082 ,-0.00821,-0.00822,-0.00823,-0.00824,-0.00825,
-0.00826,-0.00829,-0.00832,-0.00835,-0.00838,-0.00841,-0.00844,-0.00846,-0.0085 ,-0.00853,-0.00855,-0.00859,-0.00862,-0.00866,-0.0087 ,-0.00874,-0.00878,-0.00882,-0.00886,
-0.0089 ,-0.00893,-0.00898,-0.00903,-0.00908,-0.00913,-0.00918,-0.00923,-0.00928,-0.00933,-0.00938,-0.00942,-0.00947,-0.00952,-0.00956,-0.00961,-0.00965,-0.0097 ,-0.00974,-0.00979,
-0.00983,-0.00988,-0.00992,-0.00996,-0.01   ,-0.01004,-0.01008,-0.01012,-0.01017,-0.0102 ,-0.01025,-0.01029,-0.01033,-0.01037,-0.01041,-0.01045,-0.01049,-0.01053,-0.01058,
-0.01061,-0.01066,-0.01069,-0.01072,-0.01074,-0.01076,-0.01078,-0.01081,-0.01083,-0.01085,-0.01087,-0.01089,-0.01091,-0.01092,-0.01093,-0.01094,-0.01094,-0.01095,-0.01096,-0.01096,-0.01097,
-0.01098,-0.01098,-0.01099,-0.01099,-0.01099,-0.011  ,-0.011  ,-0.011  ,-0.011  ,-0.011  ,-0.01101,-0.01101,-0.01101,-0.01101,-0.01101,-0.01102,-0-0.01102,-0.01102,-0.01102,-0.01102,
-0.01104,-0.01106,-0.01107,-0.01108,-0.0111 ,-0.01112,-0.01113,-0.01115,-0.01116,-0.01118,-0.0112 ,-0.01122,-0.01124,-0.01127,-0.01129,-0.01132,-0.01134,-0.01137,-0.01139,
-0.01142,-0.01144,-0.01146,-0.01148,-0.0115 ,-0.01152,-0.01154,-0.01156,-0.01158,-0.0116 ,-0.01162,-0.01164,-0.01166,-0.01168,-0.0117 ,-0.01172,-0.01174,-0.01176,-0.01178,-0.01179,-0.01182,-0.01184,-0.01186,-0.01188,
-0.01191,-0.01193,-0.01196,-0.01198,-0.01201,-0.01203,-0.01206,-0.01208,-0.01211,-0.01214,-0.01217,-0.0122 ,-0.01223,-0.01226,-0.01229,-0.01232,-0.01234,-0.01238,-0.01242,
-0.01246,-0.0125 ,-0.01254,-0.01258,-0.01262,-0.01266,-0.01269,-0.01274,-0.01278,-0.01283,-0.01287,-0.01293,-0.01298,-0.01302,-0.01307,-0.01312,-0.01317,-0.01322,-0.01325,
-0.01328,-0.01331,-0.01334,-0.01337,-0.0134 ,-0.01343,-0.01346,-0.01349,-0.01352,-0.01352,-0.01352,-0.01353,-0.01353,-0.01353,-0.01353,-0.01353,-0.01353,-0.01354,-0.01354,
-0.01353,-0.01351,-0.0135 ,-0.01348,-0.01347,-0.01345,-0.01343,-0.01342,-0.0134 ,-0.01339,-0.01337,-0.01335,-0.01332,-0.0133 ,-0.01328,-0.01325,-0.01323,-0.01321,-0.01318,
-0.01316,-0.01314,-0.01313,-0.01312,-0.01311,-0.01309,-0.01308,-0.01307,-0.01306,-0.01305,-0.01304,-0.01303,-0.01302,-0.01301,-0.013  ,-0.013  ,-0.01299,-0.01298,-0.01297,
-0.01297,-0.01296,-0.01298,-0.013  ,-0.01302,-0.01304,-0.01306,-0.01308,-0.0131 ,-0-0.01315,-0.01317,-0.0132 ,-0-0.01327,
-0.0133 ,-0.01334,-0.01337,-0.01341,-0.01344,-0.01348

};

f32 Ah_data_old[300] = {
	  -0.01351,-0.01356,-0.01361,-0.01366,-0.01372, -0.01377, -0.01383, -0.01388, -0.01393, -0.01399, -0.01404, -0.01411, -0.01418, -0.01425, -0.01431, -0.01438, -0.01445, -0.01452, -0.01459, -0.01466, -0.01473, -0.01479, -0.01484, -0.0149 , -0.01496, -0.01502, -0.01507, -0.01513, -0.01519, -0.01524, -0.0153 , -0.01534, -0.01538, -0.01543, -0.01547, -0.01552, -0.01556, -0.0156 , -0.01564, -0.01568, -0.01573, -0.01576, -0.01579, -0.01582, -0.01585, -0.01588, -0.01591, -0.01594, -0.01597, -0.016  , -0.01602, -0.01606, -0.01609, -0.01612, -0.01616, -0.01619, -0.01623, -0.01626, -0.0163 , -0.01633, -0.01636, -0.01641, -0.01646, -0.01651, -0.01656, -0.01661, -0.01666, -0.01671, -0.01676, -0.01681, -0.01685, -0.0169 , -0.01695, -0.01699, -0.01704, -0.01708, -0.01713, -0.01717, -0.01722, -0.01726, -0.01731, -0.01735, -0.0174 , -0.01744, -0.01749, -0.01754, -0.01758, -0.01763, -0.01767, -0.01772, -0.01776, -0.01781, -0.01786, -0.01791, -0.01795, -0.018  , -0.01805, -0.01809, -0.01814, -0.01818, -0.01823, -0.01827, -0.01832, -0.01836, -0.0184 , -0.01844, -0.01848, -0.01853, -0.01857, -0.01861, -0.01865, -0.01869, -0.01874, -0.01879, -0.01884, -0.01888, -0.01893, -0.01898, -0.01903, -0.01907, -0.01912, -0.01918, -0.01925, -0.01931, -0.01937, -0.01944, -0.01951, -0.01957, -0.01963, -0.0197 , -0.01976, -0.01981, -0.01986, -0.01991, -0.01996, -0.02001, -0.02006, -0.02011, -0.02016, -0.02021, -0.02026, -0.0203 , -0.02035, -0.02039, -0.02044, -0.02048, -0.02053, -0.02057, -0.02062, -0.02067, -0.02071, -0.02075, -0.02079, -0.02083, -0.02087, -0.02091, -0.02095, -0.02099, -0.02103, -0.02107, -0.02111, -0.02114, -0.02116, -0.02118, -0.02121, -0.02123, -0.02126, -0.02128, -0.02131, -0.02133, -0.02136, -0.02138, -0.02141, -0.02144, -0.02147, -0.0215 , -0.02153, -0.02156, -0.02159, -0.02162, -0.02165, -0.02168, -0.0217 , -0.02173, -0.02175, -0.02177, -0.0218 , -0.02182, -0.02185, -0.02187, -0.0219 , -0.02192, -0.02195, -0.02198, -0.02201, -0.02204, -0.02207, -0.0221 , -0.02213, -0.02216, -0.02219, -0.02221, -0.02224, -0.02227, -0.0223 ,
-0.02233, -0.02236, -0.02239, -0.02242, -0.02245, -0.02248, -0.0225 , -0.02253, -0.02255, -0.02258, -0.0226 , -0.02263, -0.02265, -0.02268, -0.0227 , -0.02272, -0.02275, -0.02277, -0.02279, -0.02282, -0.02284, -0.02287, -0.02289, -0.02291, -0.02294, -0.02296, -0.02299, -0.02302, -0.02305, -0.02307, -0.02311, -0.02313, -0.02316, -0.02319, -0.02322, -0.02325, -0.02328, -0.0233 , -0.02332, -0.02335, -0.02337, -0.0234 , -0.02342, -0.02344, -0.02347, -0.02349, -0.02352, -0.02354, -0.02357, -0.02359, -0.02361, -0.02363, -0.02366, -0.02368, -0.02371, -0.02373, -0.02376, -0.02379, -0.02383, -0.02386, -0.02389, -0.02393, -0.02396, -0.024  , -0.02403, -0.02407, -0.0241 , -0.02414, -0.02417, -0.0242 , -0.02424, -0.02427, -0.0243 , -0.02434, -0.02437, -0.02441, -0.02445, -0.02448, -0.02452, -0.02456, -0.0246 , -0.02464, -0.02468, -0.02472, -0.02476, -0.0248 , -0.02484, -0.02489, -0.02493, -0.02498, -0.02503, -0.02507, -0.02511, -0.02516, -0.0252

};


f32 Ah_data[300]=
{

	0.005555, 0.01111 , 0.016665, 0.02222 , 0.027775, 0.03333 , 0.038885, 0.04444 , 0.049995, 0.05555 , 0.061105, 0.06666 , 0.072215, 0.07777 , 0.083325, 0.08888 , 0.094435, 0.09999 , 0.105545, 0.1111  , 0.116655, 0.12221 , 0.127765, 0.13332 , 0.138875, 0.14443 , 0.149985, 0.15554 , 0.161095, 0.16665 , 0.172205, 0.17776 , 0.183315, 0.18887 , 0.194425, 0.19998 , 0.205535, 0.21109 , 0.216645, 0.2222  , 0.227755, 0.23331 , 0.238865, 0.24442 , 0.249975, 0.25553 , 0.261085, 0.26664 , 0.272195, 0.27775 , 0.283305, 0.28886 , 0.294415, 0.29997 , 0.305525, 0.31108 , 0.316635, 0.32219 , 0.327745, 0.3333  , 0.338855, 0.34441 , 0.349965, 0.35552 , 0.361075, 0.36663 , 0.372185, 0.37774 , 0.383295, 0.38885 , 0.394405, 0.39996 , 0.405515, 0.41107 , 0.416625, 0.42218 , 0.427735, 0.43329 , 0.438845, 0.4444  , 0.449955, 0.45551 , 0.461065, 0.46662 , 0.472175, 0.47773 , 0.483285, 0.48884 , 0.494395, 0.49995 , 0.505505, 0.51106 , 0.516615, 0.52217 , 0.527725, 0.53328 , 0.538835, 0.54439 , 0.549945, 0.5555  , 0.561055, 0.56661 , 0.572165, 0.57772 , 0.583275, 0.58883 , 0.594385, 0.59994 , 0.605495, 0.61105 , 0.616605, 0.62216 , 0.627715, 0.63327 , 0.638825, 0.64438 , 0.649935, 0.65549 , 0.661045, 0.6666  , 0.672155, 0.67771 , 0.683265, 0.68882 , 0.694375, 0.69993 , 0.705485, 0.71104 , 0.716595, 0.72215 , 0.727705, 0.73326 , 0.738815, 0.74437 , 0.749925, 0.75548 , 0.761035, 0.76659 , 0.772145, 0.7777  , 0.783255, 0.78881 , 0.794365, 0.79992 , 0.805475, 0.81103 , 0.816585, 0.82214 , 0.827695, 0.83325 , 0.838805, 0.84436 , 0.849915, 0.85547 , 0.861025, 0.86658 , 0.872135, 0.87769 , 0.883245, 0.8888  , 0.894355, 0.89991 , 0.905465, 0.91102 , 0.916575, 0.92213 , 0.927685, 0.93324 , 0.938795, 0.94435 , 0.949905, 0.95546 , 0.961015, 0.96657 , 0.972125, 0.97768 , 0.983235, 0.98879 , 0.994345, 0.9999  , 1.005455, 1.01101 , 1.016565, 1.02212 , 1.027675, 1.03323 , 1.038785, 1.04434 , 1.049895, 1.05545 , 1.061005, 1.06656 , 1.072115, 1.07767 , 1.083225, 1.08878 , 1.094335, 1.09989 , 1.105445, 1.111   , 1.116555, 1.12211 , 1.127665, 1.13322 , 1.138775, 1.14433 , 1.149885, 1.15544 , 1.160995, 1.16655 , 1.172105, 1.17766 , 1.183215, 1.18877 , 1.194325, 1.19988 , 1.205435, 1.21099 , 1.216545, 1.2221  , 1.227655, 1.23321 , 1.238765, 1.24432 , 1.249875, 1.25543 , 1.260985, 1.26654 , 1.272095, 1.27765 , 1.283205, 1.28876 , 1.294315, 1.29987 , 1.305425, 1.31098 , 1.316535, 1.32209 , 1.327645, 1.3332  , 1.338755, 1.34431 , 1.349865, 1.35542 , 1.360975, 1.36653 , 1.372085, 1.37764 , 1.383195, 1.38875 , 1.394305, 1.39986 , 1.405415, 1.41097 , 1.416525, 1.42208 , 1.427635, 1.43319 , 1.438745, 1.4443  , 1.449855, 1.45541 , 1.460965, 1.46652 , 1.472075, 1.47763 , 1.483185, 1.48874 , 1.494295, 1.49985 , 1.505405, 1.51096 , 1.516515, 1.52207 , 1.527625, 1.53318 , 1.538735, 1.54429 , 1.549845, 1.5554  , 1.560955, 1.56651 , 1.572065, 1.57762 , 1.583175, 1.58873 , 1.594285, 1.59984 , 1.605395, 1.61095 , 1.616505, 1.62206 , 1.627615, 1.63317 , 1.638725, 1.64428 , 1.649835, 1.65539 , 1.660945, 1.6665

};

f32 Ah_dataold1[300]=
{

	1.672055,1.67761,1.683165,1.68872,1.694275,1.69983,1.705385,1.71094,1.716495,1.72205,1.727605,1.73316,1.738715,1.74427,1.749825,1.75538,1.760935,1.76649,1.772045,1.7776,1.783155,1.78871,1.794265,1.79982,1.805375,1.81093,1.816485,1.82204,1.827595,1.83315,1.838705,1.84426,1.849815,1.85537,1.860925,1.86648,1.872035,1.87759,1.883145,1.8887,1.894255,1.89981,1.905365,1.91092,1.916475,1.92203,1.927585,1.93314,1.938695,1.94425,1.949805,1.95536,1.960915,1.96647,1.972025,1.97758,1.983135,1.98869,1.994245,1.9998,2.005355,2.01091,2.016465,2.02202,2.027575,2.03313,2.038685,2.04424,2.049795,2.05535,2.060905,2.06646,2.072015,2.07757,2.083125,2.08868,2.094235,2.09979,2.105345,2.1109,2.116455,2.12201,2.127565,2.13312,2.138675,2.14423,2.149785,2.15534,2.160895,2.16645,2.172005,2.17756,2.183115,2.18867,2.194225,2.19978,2.205335,2.21089,2.216445,2.222,2.227555,2.23311,2.238665,2.24422,2.249775,2.25533,2.260885,2.26644,2.271995,2.27755,2.283105,2.28866,2.294215,2.29977,2.305325,2.31088,2.316435,2.32199,2.327545,2.3331,2.338655,2.34421,2.349765,2.35532,2.360875,2.36643,2.371985,2.37754,2.383095,2.38865,2.394205,2.39976,2.405315,2.41087,2.416425,2.42198,2.427535,2.43309,2.438645,2.4442,2.449755,2.45531,2.460865,2.46642,2.471975,2.47753,2.483085,2.48864,2.494195,2.49975,2.505305,2.51086,2.516415,2.52197,2.527525,2.53308,2.538635,2.54419,2.549745,2.5553,2.560855,2.56641,2.571965,2.57752,2.583075,2.58863,2.594185,2.59974,2.605295,2.61085,2.616405,2.62196,2.627515,2.63307,2.638625,2.64418,2.649735,2.65529,2.660845,2.6664,2.671955,2.67751,2.683065,2.68862,2.694175,2.69973,2.705285,2.71084,2.716395,2.72195,2.727505,2.73306,2.738615,2.74417,2.749725,2.75528,2.760835,2.76639,2.771945,2.7775,2.783055,2.78861,2.794165,2.79972,2.805275,2.81083,2.816385,2.82194,2.827495,2.83305,2.838605,2.84416,2.849715,2.85527,2.860825,2.86638,2.871935,2.87749,2.883045,2.8886,2.894155,2.89971,2.905265,2.91082,2.916375,2.92193,2.927485,2.93304,2.938595,2.94415,2.949705,2.95526,2.960815,2.96637,2.971925,2.97748,2.983035,2.98859,2.994145,2.9997,3.005255,3.01081,3.016365,3.02192,3.027475,3.03303,3.038585,3.04414,3.049695,3.05525,3.060805,3.06636,3.071915,3.07747,3.083025,3.08858,3.094135,3.09969,3.105245,3.1108,3.116355,3.12191,3.127465,3.13302,3.138575,3.14413,3.149685,3.15524,3.160795,3.16635,3.171905,3.17746,3.183015,3.18857,3.194125,3.19968,3.205235,3.21079,3.216345,3.2219,3.227455,3.23301,3.238565,3.24412,3.249675,3.25523,3.260785,3.26634,3.271895,3.27745,3.283005,3.28856,3.294115,3.29967,3.305225,3.31078,3.316335,3.32189,3.327445,3.333

};
#endif



/** @fn    f32 Soh_Approximate_Weighted_total_least_square(f32 f32_SigmaX,f32 f32_SigmaY,f32 f32_gamma,f32 f32_Qnom)
*   @brief    Apply Approximate weighted total least squares
*
*   @details  To find percentage of Q
*
*   @pre      
*   @post     
*   @param    SigmaX variance on x axis
*   @param    SigmaY variance on y axis
*   @param    gamma
*   @param    Qnom nominal voltage 
*   @return   return percentage of Qhat
*/
f32 Soh_Approximate_Wighted_total_least_square(/*f32 measX, f32 measY,*/f32 f32_SigmaX,f32 f32_SigmaY,f32 f32_gamma,f32 f32_Qnom)
{


	f32 f32_Qhat;
    f32 f32_SigmaQ;
    
	if(f32_Qnom != 0)
	{
	    u32_one_time_variable =0;
    	f32_K = sqrt(f32_SigmaX/f32_SigmaY);


        f32_c1 = 1/f32_SigmaY;

        f32_c2 = f32_Qnom/f32_SigmaY;

        f32_c3 = pow(f32_Qnom,2)/f32_SigmaY;

        f32_c4 = 1 / f32_SigmaX;

        f32_c5 = f32_Qnom / f32_SigmaX;

        f32_c6 = pow(f32_Qnom, 2) / f32_SigmaX;


        f32_C1 = 1/(f32_K*f32_K*f32_SigmaY);

        f32_C2 = f32_K*f32_Qnom/(f32_K*f32_K*f32_SigmaY);

        f32_C3 = f32_K*f32_K*(f32_Qnom*f32_Qnom)/(f32_K*f32_K*f32_SigmaY);

        f32_C4 = 1 / f32_SigmaX;

		f32_C5 = f32_K*f32_Qnom / f32_SigmaX;

        f32_C6 = f32_K*f32_K*pow(f32_Qnom, 2) / f32_SigmaX;

    }
    
    u32 u32_iter =0;
	for ( u32_iter =0 ;u32_iter < 300;u32_iter++)
	{
        f32_c1 = f32_gamma*f32_c1 + f32_measX_1[u32_iter]* f32_measX_1[u32_iter]/f32_SigmaY;

        f32_c2 = f32_gamma*f32_c2 +  f32_measX_1[u32_iter] * f32_measY_1[u32_iter]/f32_SigmaY;

        f32_c3 = f32_gamma*f32_c3 + f32_measY_1[u32_iter] * f32_measY_1[u32_iter]/f32_SigmaY;

        f32_c4 = f32_gamma*f32_c4 + f32_measX_1[u32_iter] * f32_measX_1[u32_iter]/f32_SigmaX;

        f32_c5 = f32_gamma*f32_c5 + f32_measX_1[u32_iter] * f32_measY_1[u32_iter]/f32_SigmaX;

        f32_c6 = f32_gamma*f32_c6 + f32_measY_1[u32_iter] * f32_measY_1[u32_iter]/f32_SigmaX;


        f32_C1 = f32_gamma*f32_C1 + f32_measX_1[u32_iter] * f32_measX_1[u32_iter]/(f32_K*f32_K*f32_SigmaY);

        f32_C2 = f32_gamma*f32_C2 + f32_K*f32_measX_1[u32_iter] * f32_measY_1[u32_iter]/(f32_K*f32_K*f32_SigmaY);

        f32_C3 = f32_gamma*f32_C3 + f32_K*f32_K*f32_measY_1[u32_iter] * f32_measY_1[u32_iter]/(f32_K*f32_K*f32_SigmaY);

        f32_C4 = f32_gamma*f32_C4 + f32_measX_1[u32_iter]*f32_measX_1[u32_iter]/f32_SigmaX;

        f32_C5 = f32_gamma*f32_C5 + f32_K*f32_measX_1[u32_iter]*f32_measY_1[u32_iter]/f32_SigmaX;

        f32_C6 = f32_gamma*f32_C6 + f32_K*f32_K*f32_measY_1[u32_iter]*f32_measY_1[u32_iter]/f32_SigmaX;

	}

	/*******/
	//WLS: method
#if 0
	f32 Q_WLS = c2/c1;
	f32 H_WLS = 2*c1;

	printf("Q_WLS---------->%f\n",Q_WLS);
	printf("H_WLS---------->%f\n",H_WLS);
#endif

	#if 1
        /*Coefficients for the polynomial equation*/
        f32 f32_a = f32_C5;

        f32 f32_b = (-f32_C1+2*f32_C4-f32_C6);

        f32 f32_c = (3*f32_C2-3*f32_C5);

        f32 f32_d = (f32_C1-2*f32_C3+f32_C6);

        f32 f32_e = -f32_C2;


        Quartic_roots(1,f32_b/f32_a,f32_c/f32_a,f32_d/f32_a,f32_e/f32_a);
        f32 f32_r = f32_X1;

        f32 f32_Jr =(1/pow((f32_r*f32_r)+1,2))*(pow(f32_r,4)*f32_C4 - 2*f32_C5*pow(f32_r,3) + (f32_C1+f32_C6)*f32_r*f32_r - 2*f32_C2*f32_r +f32_C3);

        f32 f32_J;
        f32_J=f32_Jr;
        f32  f32_Q = f32_r;



        //#Hessian matrix
        f32 f32_H = (2/pow((f32_Q*f32_Q+1),4))*(-2*f32_C5*pow(f32_Q,5) + (3*f32_C1-6*f32_C4+3*f32_C6)*pow(f32_Q,4) + (-12*f32_C2+16*f32_C5)*pow(f32_Q,3) + (-8*f32_C1+10*f32_C3+6*f32_C4-8*f32_C6)*f32_Q*f32_Q
					+ (12*f32_C2-6*f32_C5)*f32_Q + (f32_C1-2*f32_C3+f32_C6));

        f32_Qhat = f32_Q/f32_K;

        f32_SigmaQ = 2/(f32_H/f32_K*f32_K);


	//printf("Qhat---------->%f\nSigmaQ----------->%f\n",Qhat,SigmaQ);
	#endif

    return (f32_Qhat*100/40.0);

}


/** @fn    f32 soh_AWTLS(f32 f32_Q0, f32 f32_maxI, f32 f32_precisionI, f32 f32_slope,f32 f32_Qnom, f32 f32_SOC_true, f32 f32_Ah_true,f32 f32_m,f32 f32_mode,f32 f32_sigma,f32 f32_socnoise,f32 f32_gamma)
*   @brief    Apply Approximate weighted total least squares
*
*   @details  To find percentage of Q
*
*   @pre      
*   @post     
*   @param    Q0
*   @param    maxI
*   @param    precisionI
*   @param    slope
*   @param    Qnom
*   @param    SOC_true
*   @param    Ah_true
*   @param    mode
*   @param    Sigma
*   @param    gamma
*   @param    noise 
*   @return   return percentage of Qhat
*/
f32 soh_AWTLS(f32 f32_Q0, f32 f32_maxI, f32 f32_precisionI, f32 f32_slope,f32 f32_Qnom, f32 f32_SOC_true, f32 f32_Ah_true,f32 f32_m,f32 f32_mode,f32 f32_sigma,f32 f32_socnoise,f32 f32_gamma)
{

    /*Number of samples = 300*/
	f32 f32_n = 300;
	f32 f32_Q = f32_Ah_true;
	f32 f32_x = f32_SOC_true;
	f32 f32_y= f32_Q*f32_x;


	f32 f32_binsize = 2 * f32_maxI/f32_precisionI;

    /*Variance*/
	f32 f32_sy;
	f32_sy = (f32_binsize * sqrt(f32_m/12));// / 3600;
	
    /*Noise*/
	f32 f32_sx = f32_socnoise;

    /*Noise + data*/
	f32_x= f32_x + f32_sx;
	f32_y = f32_y+ f32_sy;
    u32 u32_j=0;
	for( u32_j =0; u32_j<f32_n; u32_j++)
	{
		f32_measX_1[u32_j] = f32_soc_data[u32_j] + f32_sx;
		f32_measY_1[u32_j] = f32_Ah_data_1[u32_j] * f32_soc_data[u32_j] + f32_sy;
	}

	return Soh_Approximate_Wighted_total_least_square(/*x,y,*/f32_sx*f32_sx,f32_sy*f32_sy,f32_gamma,f32_Qnom);
}

/** @fn   f32 Soh_Calculate(f32 *Cmu_Ah_data)
*   @brief    Calculate SOH
*
*   @details  To find percentage 
*
*   @pre      
*   @post     
*   @param    Pointer current data
*   @return   return percentage of Qhat
*/
f32 Soh_Calculate(/*f32 *Cmu_Ah_data*/)
{
    f32 Cmu_Ah_data[300];
    
    /*Runtime value update*/
	f32 f32_Total_Ah =40.0;// 2.9;

    /*Copying cmu current data to soh current buffer*/
	memcpy(f32_Ah_data,Cmu_Ah_data,300);
    u32 u32_i=0;

    /**Copying SOC and current data**/
	for( u32_i =0;u32_i<300 ;u32_i++)
	{
		f32_soc_data[u32_i] = 1-(f32_Ah_data[u32_i]/f32_Total_Ah);
		f32_Ah_data_1[u32_i] = f32_Total_Ah - f32_Ah_data[u32_i];
	}



	/***Total capacity of battery***/
	f32 f32_Q0 = 40;//2.9;
	

	/*Max current :  example as c/20*/
	f32 f32_maxI = 2;//0.145;//(1/20)*2.9;

	/*Precision :  10 bit : 2^10 ---> 1024*/
	f32 f32_precisionI = 1024;

	/*Slope as -0.01*/
	f32 f32_slope = -0.01;

	/*Qnom 0.99 times of Q0*/
	f32 f32_Qnom=0.99*40;//0.99*2.9;

	/*Max soc*/
	//f32 f32_xmax = 0.8;

	/*Min soc*/
	//f32 f32_xmin = -0.8;

	/*True  soc*/
	f32 f32_SOC_True = f32_soc_data[0];

	/*True Ampere hour*/
	f32 f32_Ah_True = f32_Ah_data_1[0];

	/*Number of samples for process AWTLS*/
	f32 f32_m = 300;

	/*For variance*/
	//f32 f32_thecase = 1;

	/*Mode*/
	f32 f32_mode = 0.5;

	/*Sigma */
	f32 f32_sigma = 0.6;

	/* soc noise*/
	f32 f32_socnoise = 0.01;

	/*Gamma */
	f32 f32_gamma = 0.99;

    /**SoH using AWTLS**/
	return soh_AWTLS(f32_Q0,f32_maxI,f32_precisionI,f32_slope,f32_Qnom,f32_SOC_True,f32_Ah_True, f32_m, f32_mode, f32_sigma, f32_socnoise, f32_gamma);
}



