/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name: System Configuration                                                                                                               */
/* Author: Manikanta Pantala                                                                                             */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @.h
*   @brief Description of file
*
*   @details Detail information about file
*
*   \Copyright Copyright 2021 by Affluence Info Systems Pvt Ltd..
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Manikanta Pantala.
*   $LastChangedRevision$ | 23-07-2021: 0.1 Initial version 
                          | 28-07-2021: 0.2 Incorporation of review comments by harinath
*   $LastChangedDate$     | 28-07-2021
*/

/** @ingroup system_configuration
*
*  Detail description about module
*
*/

#ifndef SYSTEM_CONFIG_H_
#define SYSTEM_CONFIG_H_

/**@{*/
/********************************************************************************************************************************/
/* Include Section                                                                                                              */
/********************************************************************************************************************************/
#include "mcc_generated_files/memory/flash.h"
#include "uart_app.h"
//#include "platform_stddefs.h"
#include "common.h"

/********************************************************************************************************************************/
/* Defines Section - GLOBAL                                                                                                     */
/********************************************************************************************************************************/
#define SUCCESS 1
#define length_of_tx_buff 100
#define length_of_rx_buff 100

//#define FLASH_STORAGE_ADDRESS 0XC000

/********************************************************************************************************************************/
/* Macro Section - GLOBAL                                                                                                       */
/********************************************************************************************************************************/
#define no_of_ReadBytes 100
#define no_of_WriteBytes 100 
#define sysconfig_ReadFlashAddress 0xf000
#define sysconfig_WriteFlashAddress 0xf000


/********************************************************************************************************************************/
/* Data Types Section - GLOBAL                                                                                                  */
/********************************************************************************************************************************/

typedef enum 
{
    SYSCONFIG_OK                                 = 0,    /*!< BAL --> ok                             */
    SYSCONFIG_BUSY_OK                            = 1,    /*!< BAL busy                               */
    SYSCONFIG_REQUEST_PENDING                    = 2,    /*!< requested to be executed               */
    SYSCONFIG_ILLEGAL_REQUEST                    = 3,    /*!< Request can not be executed            */
    SYSCONFIG_INIT_ERROR                         = 7,    /*!< Error state: Source: Initialization    */
    SYSCONFIG_OK_FROM_ERROR                      = 8,    /*!< Return from error --> ok               */
    SYSCONFIG_ERROR                              = 20,   /*!< General error state                    */
    SYSCONFIG_ALREADY_INITIALIZED                = 30,   /*!< Initialization of BAL already finished */
    SYSCONFIG_ILLEGAL_TASK_TYPE                  = 99,   /*!< Illegal                                */
} SYSCONFIG_RETURN_TYPE_e;

typedef enum 
{
    /* Init-Sequence */
    SYSCONFIG_STATEMACH_UNINITIALIZED             = 0,    /*!<    */
    SYSCONFIG_STATEMACH_INITIALIZATION            = 1,    /*!<    */
    SYSCONFIG_STATEMACH_INITIALIZED               = 2,    /*!<    */
    SYSCONFIG_STATEMACH_CHECK_CONFIGMODE          = 3,
    SYSCONFIG_STATEMACH_WRITE                     = 4,
    SYSCONFIG_STATEMACH_READ                      = 5,
    SYSCONFIG_STATEMACH_EXIT_CONFIGMODE           = 6,
    SYSCONFIG_STATEMACH_UNDEFINED                 = 20,   /*!< undefined state                                */
    SYSCONFIG_STATEMACH_RESERVED1                 = 0x80, /*!< reserved state                                 */
    SYSCONFIG_STATEMACH_ERROR                     = 0xF0, /*!< Error-State:  */
} SYSCONFIG_STATEMACH_e;


typedef enum 
{
    SYSCONFIG_STATE_UNINIT_REQUEST                   = SYSCONFIG_STATEMACH_UNINITIALIZED,
    SYSCONFIG_STATE_INIT_REQUEST                     = SYSCONFIG_STATEMACH_INITIALIZATION,           /*!<    */
    SYSCONFIG_STATE_ERROR_REQUEST                    = SYSCONFIG_STATEMACH_ERROR,
    SYSCONFIG_STATEMACH_RX_CONFIG_DATA               = SYSCONFIG_STATEMACH_WRITE,
    SYSCONFIG_STATEMACH_TX_CONFIG_DATA               = SYSCONFIG_STATEMACH_READ,
    SYSCONFIG_STATE_NO_REQUEST                       = SYSCONFIG_STATEMACH_RESERVED1,                /*!<    */
} SYSCONFIG_STATE_REQUEST_e;

typedef struct 
{
    u16 timer;                         /*!< time in ms before the state machine processes the next state, e.g. in counts of 1ms */
    SYSCONFIG_STATE_REQUEST_e statereq;           /*!< current state request made to the state machine                                     */
    SYSCONFIG_STATEMACH_e state;                  /*!< state of Driver State Machine                                                       */
    SYSCONFIG_STATEMACH_e laststate;              /*!< previous state of the state machine                                                 */
    u8 lastsubstate;                   /*!< previous substate of the state machine                                              */
    u8 triggerentry;                   /*!< counter for re-entrance protection (function running flag) */
    u32 ErrRequestCounter;             /*!< counts the number of illegal requests to the BAL state machine */
    e_STD_RETURN_TYPE initFinished;         /*!< E_OK if statemachine initialized, otherwise E_NOT_OK */
    e_STD_RETURN_TYPE write;
    e_STD_RETURN_TYPE read;
    u8 active;                         /*!< indicate if balancing active or not */
    u32 balancing_threshold;           /*!< effective balancing threshold */
    u8 balancing_allowed;              /*!< flag to disable balancing */
    u8 balancing_global_allowed;       /*!< flag to globally disable balancing */
} SYSCONFIG_STATE_s;



/********************************************************************************************************************************/
/* Function Prototype Section - GLOBAL                                                                                          */
/********************************************************************************************************************************/

void SysConfig_Init(void);
u8 SysConfig_Write(u8 *buff_source);
u8 SysConfig_Read(u8 *buff_dest);
void test_sysconfig();


#endif    /* SYSTEM_CONFIG_H_ */
/********************************************************************************************************************************/
/* End of file                                                                                                                  */
/********************************************************************************************************************************/
/**@}*/