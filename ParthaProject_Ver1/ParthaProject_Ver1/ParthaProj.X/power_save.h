/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name:power save                                                                                                          */
/* Author:Pavithran                                                                                                                     */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file power_save.h
*   @brief This file contains API's which are required for power save
*
*   @details power save Application function.
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Pavithran
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 29-06-2021
*/

/** @defgroup power save
*
* power save Application functions
*
*/

/**@{*/
/********************************************************************************************************************************/
/* Include Section                                                                                                              */
/********************************************************************************************************************************/
#ifndef POWER_SAVE_H
#define	POWER_SAVE_H
#include "common.h"  
#include "bms.h"  

/********************************************************************************************************************************/
/* Data Types Section - GLOBAL                                                                                                     */
/********************************************************************************************************************************/

/**
 * State requests for the power save statemachine
 */
typedef enum 
{
    POWER_SAVE_INIT_REQUEST=0,
    POWER_SAVE_ERROR_REQUEST,
    POWER_SAVE_NO_REQUEST,
} e_POWER_SAVE_STATE_REQUEST;

/**
 * States of the power save state machine
 */
typedef enum 
{
    /* Init-Sequence */
    POWER_SAVE_STATEMACH_UNINITIALIZED             = 0,    /*!<    */
    POWER_SAVE_STATEMACH_INITIALIZATION            = 1,    /*!<    */
    POWER_SAVE_STATEMACH_INITIALIZED               = 2,    /*!<    */
    POWER_SAVE_STATEMACH_IDLE                      = 3,    /*!<    */
    POWER_SAVE_STATEMACH_POWER_SAVE                = 4,    /*!<    */
    POWER_SAVE_STATEMACH_NORMAL                    = 5,    /*!<    */
    POWER_SAVE_STATEMACH_UNDEFINED                 = 6,   /*!< undefined state                                */
    POWER_SAVE_STATEMACH_RESERVED1                 = 0x80, /*!< reserved state                                 */
    POWER_SAVE_STATEMACH_ERROR                     = 0xF0, /*!< Error-State:  */
} e_POWER_SAVE_STATEMACH;

/**
 * States of the power save sub state machine
 */
typedef enum 
{
    /* Init-Sequence */
    POWER_SAVE_SUB_STATEMACH_NO_SUB_STATE_REQUEST  = 1,    /*!<    */
    POWER_SAVE_SUB_STATEMACH_SET_STATE_REQUEST     = 2,    /*!<    */
    POWER_SAVE_SUB_STATEMACH_GET_STATE_REQUEST     = 3,    /*!<    */
    POWER_SAVE_SUB_STATEMACH_WAITING_FOR_IRQ       = 4,    /*!<    */
    POWER_SAVE_SUB_STATEMACH_ENABLE_PERIPHERAL     = 5,    /*!<    */
    POWER_SAVE_SUB_STATEMACH_SYS_REINIT            = 6,    /*!<    */
    POWER_SAVE_SUB_STATEMACH_UNDEFINED             = 7,   /*!< undefined state                                */
    POWER_SAVE_SUB_STATEMACH_RESERVED1             = 0x80, /*!< reserved state                                 */
    POWER_SAVE_SUN_STATEMACH_ERROR                 = 0xF0, /*!< Error-State:  */
} e_POWER_SAVE_SUB_STATEMACH;

/**
 * This structure contains all the variables relevant for the CONT state machine.
 * The user can get the current state of the CONT state machine with this variable
 */
typedef struct 
{
    u16 u16_timer;                                     /*!< time in ms before the state machine processes the next state, e.g. in counts of 1ms    */
    e_POWER_SAVE_STATE_REQUEST statereq;                /*!< current state request made to the state machine                                        */
    e_POWER_SAVE_STATEMACH state;                       /*!< state of Driver State Machine                                                          */
    e_POWER_SAVE_SUB_STATEMACH substate;             /*!< current substate of the state machine                                                  */
    BMS_CURRENT_FLOW_STATE_e currentFlowState;          /*!< state of battery system                                                                */
    e_POWER_SAVE_STATEMACH laststate;                   /*!< previous state of the state machine                                                    */
    e_POWER_SAVE_SUB_STATEMACH lastsubstate;         /*!< previous substate of the state machine                                                 */
    u32 u32_ErrRequestCounter;                          /*!< counts the number of illegal requests to the state machine                         */                            /*!< general purpose counter                                                                */
} st_power_save;

/********************************************************************************************************************************/
/* Function Prototype Section - GLOBAL                                                                                          */
/********************************************************************************************************************************/
void MCU_Power_Save_Sleep(void);

void MCU_Power_Save_Idle(void);

void MCU_Power_Save_Peripheral_Module_Enable(void);

void MCU_Power_Save_Peripheral_Module_Disable(void);

void MCU_Power_Save_Main_Function(void);

void MCU_Power_Save_Doze_Mode_Enable(void);

void MCU_Power_Save_Doze_Mode_Disable(void);

#endif	/* POWER_SAVE_H */
/********************************************************************************************************************************/
/* End of file                                                                                                                  */
/********************************************************************************************************************************/
/**@}*/

