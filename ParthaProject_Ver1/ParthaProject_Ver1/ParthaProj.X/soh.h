/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name:state of health                                                                                                        */
/* Author:Karthik                                                                                                                     */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file soh.h
*   @brief This file contains structure declaration,variable.
*
*   @details variable type naming conventions.
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Karthik
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 16-07-2021
*/

/** @defgroup
*
* This file contains structure declaration,variable.
*
*/

/**@{*/
/********************************************************************************************************************************/
/* Include Section                                                                                                              */
/********************************************************************************************************************************/
#ifndef SOH_H
#define	SOH_H
#include "common.h"

void cube_roots(f32 f32_l,f32 f32_i, f32 f32_j,f32 f32_k);
void Quartic_roots(f32 f32_a,f32 f32_b, f32 f32_c, f32 f32_d,f32 f32_e);
f32 Soh_Approximate_Wighted_total_least_square(f32 f32_SigmaX,f32 f32_SigmaY,f32 f32_gamma,f32 f32_Qnom);
f32 soh_AWTLS(f32 f32_Q0, f32 f32_maxI, f32 f32_precisionI, f32 f32_slope,f32 f32_Qnom, f32 f32_SOC_true, f32 f32_Ah_true,f32 f32_m,f32 f32_mode,f32 f32_sigma,f32 f32_socnoise,f32 f32_gamma);
f32 Soh_Calculate(/*f32 *Cmu_Ah_data*/);

#endif	/* SOH_H */
/********************************************************************************************************************************/
/* End of file                                                                                                                  */
/********************************************************************************************************************************/
/**@}*/