/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name:power save                                                                                                          */
/* Author:Pavithran                                                                                                                     */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file power_save.c
*   @brief This file contains API's which are required for power save mode
*
*   @details power save mode.
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Pavithran
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 29-07-2021
*/

/** @defgroup power save mode
*
* This file contains API's which are required for power save mode
*
*/

/**@{*/
/********************************************************************************************************************************/
/* Include Section                                                                                                              */
/********************************************************************************************************************************/
#include "power_save.h"
#include "mcc_generated_files/memory/flash.h"
#include "cmu.h"

/********************************************************************************************************************************/
/* Variables Section - GLOBAL                                                                                                   */
/********************************************************************************************************************************/
/**
 * contains the state of the power save state machine
 */
static st_power_save st_power_save_state = 
{
    .u16_timer         = 0,
    .statereq          = POWER_SAVE_NO_REQUEST,
    .state             = POWER_SAVE_STATEMACH_UNINITIALIZED,
    .substate          = POWER_SAVE_SUB_STATEMACH_NO_SUB_STATE_REQUEST,
    .currentFlowState  = BMS_AT_REST,
    .laststate         = POWER_SAVE_STATEMACH_UNINITIALIZED,
    .lastsubstate      = POWER_SAVE_SUB_STATEMACH_NO_SUB_STATE_REQUEST,
    .u32_ErrRequestCounter = 0
};

/*wake up flag*/
volatile u8 power_save_wakeup_flag;

/********************************************************************************************************************************/
/* Function Implementations Section - GLOBAL                                                                                    */
/********************************************************************************************************************************/

/** @fn       void MCU_Power_Save_Sleep(void)
*   @brief    function to put mcu in sleep mode
*
*   @details  function to put mcu in sleep mode
*
*   @pre      
*   @post     
*   @param    void
*   @return   void
*/
void MCU_Power_Save_Sleep(void)
{
    MCU_Sleep();
}

/** @fn       void MCU_Power_Save_Idle(void)
*   @brief    function to put mcu in idle mode
*
*   @details  function to put mcu in idle mode
*
*   @pre      
*   @post     
*   @param    void
*   @return   void
*/
void MCU_Power_Save_Idle(void)
{
    MCU_Idle();
}

/** @fn       void MCU_Power_Save_Doze_Mode_Enable(void)
*   @brief    function to enable doze mode
*
*   @details  function to enable doze mode
*   @pre      
*   @post     
*   @param    void
*   @return   void
*/
void MCU_Power_Save_Doze_Mode_Enable(void)
{
    /*disabling doze mode*/
    CLKDIVbits.DOZEN=0;
    /*clock divider value*/
    CLKDIVbits.DOZE=7;
    /*enabling doze mode*/
    CLKDIVbits.DOZEN=1;
    CLKDIVbits.ROI=1;
}

/** @fn       void Peripheral_Module_Enable(void)
*   @brief    function to disable doze mode
*
*   @details  function to disable doze mode
*   @pre      
*   @post     
*   @param    void
*   @return   void
*/
void MCU_Power_Save_Doze_Mode_Disable(void)
{
    /*disabling doze mode*/
    CLKDIVbits.DOZEN=0;
     /*clock divider value*/
    CLKDIVbits.DOZE=0;
}

/** @fn       void Peripheral_Module_Enable(void)
*   @brief    function to enable peripheral module
*
*   @details  function to enable peripheral module
*   @pre      
*   @post     
*   @param    void
*   @return   void
*/
void MCU_Power_Save_Peripheral_Module_Enable(void)
{
    //PMD1bits.ADC1MD=0;
    //PMD1bits.C1MD=0;
    //PMD1bits.C2MD=0;
    //PMD1bits.SPI1MD=0;
    //PMD1bits.SPI2MD=0;
    PMD1bits.U1MD=0;
    //PMD1bits.U2MD=0;
    //PMD1bits.I2C1MD=0;
    //PMD1bits.PWMMD=0;
    //PMD1bits.QEIMD=0;
    //PMD1bits.T1MD=0;
    
    //PMD2=0;
    //PMD3=0;
    //PMD4=0;
    //PMD6=0;
    //PMD7=0;
    //PMD8=0;
}

/** @fn       void Peripheral_Module_Enable(void)
*   @brief    function to disable peripheral module
*
*   @details  function to disable peripheral module
*   @pre      
*   @post     
*   @param    void
*   @return   void
*/
void MCU_Power_Save_Peripheral_Module_Disable(void)
{
    //PMD1bits.ADC1MD=1;
    //PMD1bits.C1MD=1;
    //PMD1bits.C2MD=1;
    //PMD1bits.SPI1MD=1;
    //PMD1bits.SPI2MD=1;
    PMD1bits.U1MD=1;
    //PMD1bits.U2MD=1;
    //PMD1bits.I2C1MD=1;
    //PMD1bits.PWMMD=1;
    //PMD1bits.QEIMD=1;
    //PMD1bits.T1MD=1;
    
    //PMD2=0xFFFF;
    //PMD3=0XFFFF;
    //PMD4=0XFFFF;
    //PMD6=0XFFFF;
    //PMD7=0XFFFF;
    //PMD8=0XFFFF;
}

/** @fn       void MCU_Power_Save_Set_State_Request(void)
*   @brief    function to set state request to all the modules
*
*   @details  function to set state request to all the modules
*   @pre      
*   @post     
*   @param    void
*   @return   void
*/
void MCU_Power_Save_Set_State_Request(u8 u8_state_request)
{
    /*sleep mode request*/
    if(u8_state_request==1)
    {
        CMU_Set_StateRequest(CMU_STATEMACH_GOTO_SLEEP);
        //TODO:other modules set state request for sleep
    }
    else/*wake up mode request*/
    {
        CMU_Set_StateRequest(CMU_STATEMACH_EXIT_FROM_SLEEP_SHUTDOWN);
        //TODO:other modules set state request for wakeup
    }
    
    
}

/** @fn       void MCU_Power_Save_Get_Current_States(void)
*   @brief    function to get state request from all the modules
*
*   @details  function to get state request from all the modules
*   @pre      
*   @post     
*   @param    void
*   @return   void
*/
u8 MCU_Power_Save_Get_Current_State(void)
{
    u8 u8_return_value=0;
    
    if(CMU_Get_Current_State()==CMU_STATEMACH_IN_SLEEP_MODE)
    {
        u8_return_value=1;
    }
    
    //TODO:other modules get current state
   
    return u8_return_value;
}

/** @fn       u8 MCU_Power_Save_Waiting_For_WakeUp(void)
*   @brief    function to get mcu wakeup request from interrupt
*
*   @details  function to get mcu wakeup request from interrupt
*   @pre      
*   @post     
*   @param    void
*   @return   void
*/
u8 MCU_Power_Save_Waiting_For_WakeUp(void)
{
    //TODO:this flag need to be updated in gpio interrupt handler
    return power_save_wakeup_flag;
}

/** @fn        void MCU_Power_Save_StateTransition(e_POWER_SAVE_STATEMACH state, e_POWER_SAVE_SUB_STATEMACH substate,u16 u16_timer_ms)
*   @brief     to change the state of the power save mode 
*
*
*   @details  to change the state of the power save mode
*
*   @pre      system init
*   @post     
*   @param    state current state
*   @param    substate current substate
*   @param    timer_ms wait time for next state
*   @return   void
*/
void MCU_Power_Save_StateTransition(e_POWER_SAVE_STATEMACH state, e_POWER_SAVE_SUB_STATEMACH substate,u16 u16_timer_ms)
{
    /*saving last state and sub state*/
    st_power_save_state.laststate = st_power_save_state.state; 
    st_power_save_state.lastsubstate = st_power_save_state.substate;
    
    st_power_save_state.state = state;
    st_power_save_state.substate = substate;     
    st_power_save_state.u16_timer = u16_timer_ms;
}


/** @fn       void MCU_Power_Save_Main_Function(void)
*   @brief    function to enable/disable low power mode
*
*   @details  function to enable/disable low power mode
*   @pre      
*   @post     
*   @param    void
*   @return   void
*/
void MCU_Power_Save_Main_Function(void)
{
    u8 u8_ret_val=0;
    
    if (st_power_save_state.u16_timer)
    {
		if (--st_power_save_state.u16_timer) 
        {
			return;    /* handle state machine only if timer has elapsed*/
		}
    }
    
    switch(st_power_save_state.state)
    {
         /****************************UNINITIALIZED***********************************/
        case POWER_SAVE_STATEMACH_UNINITIALIZED:
            MCU_Power_Save_StateTransition(POWER_SAVE_STATEMACH_INITIALIZATION,POWER_SAVE_SUB_STATEMACH_NO_SUB_STATE_REQUEST,1);
            break;
        
        /****************************INITIALIZATION***********************************/
        case POWER_SAVE_STATEMACH_INITIALIZATION:
            MCU_Power_Save_StateTransition(POWER_SAVE_STATEMACH_INITIALIZED,POWER_SAVE_SUB_STATEMACH_NO_SUB_STATE_REQUEST,1);
            break;
            
        /****************************INITIALIZED***********************************/    
        case POWER_SAVE_STATEMACH_INITIALIZED:
            MCU_Power_Save_StateTransition(POWER_SAVE_STATEMACH_IDLE,POWER_SAVE_SUB_STATEMACH_NO_SUB_STATE_REQUEST,1);
            break;
        
        /****************************IDLE***********************************/
        case POWER_SAVE_STATEMACH_IDLE:
            /*if bms is in idle state enter into low power mode,otherwise stay in this state only*/
            st_power_save_state.currentFlowState=BMS_GetBatterySystemState();
            if(st_power_save_state.currentFlowState==BMS_AT_REST)
            {
                /*changing state to power save mode*/
                /*changing sub state to set state request*/
                MCU_Power_Save_StateTransition(POWER_SAVE_STATEMACH_POWER_SAVE,POWER_SAVE_SUB_STATEMACH_SET_STATE_REQUEST,1);
            }
            break;
        
        /****************************POWER SAVE***********************************/
        case POWER_SAVE_STATEMACH_POWER_SAVE:
            if(st_power_save_state.substate==POWER_SAVE_SUB_STATEMACH_SET_STATE_REQUEST)
            {
                /*sending sleep state request to all the external modules to stop operation*/
                MCU_Power_Save_Set_State_Request(1);
                /*changing substate to get the sleep state request response*/
                MCU_Power_Save_StateTransition(POWER_SAVE_STATEMACH_POWER_SAVE,POWER_SAVE_SUB_STATEMACH_GET_STATE_REQUEST,1);
            }
            else if(st_power_save_state.substate==POWER_SAVE_SUB_STATEMACH_GET_STATE_REQUEST)
            {
                /*getting current state of all the external modules*/
                u8_ret_val=MCU_Power_Save_Get_Current_State();
                
                /*if all external modules entered sleep mode then disabling all the peripherals*/
                if(u8_ret_val==1)
                {
                    /*putting mcu to low power mode(idle)*/
                    MCU_Power_Save_Idle();
                    /*disabling all the peripherals*/
                    MCU_Power_Save_Peripheral_Module_Disable();
                    /*changing substate to wait for mcu wake up request(any interrupt)*/
                    MCU_Power_Save_StateTransition(POWER_SAVE_STATEMACH_POWER_SAVE,POWER_SAVE_STATEMACH_POWER_SAVE,1);
                }
                else
                {
                    /*staying current state and substate*/
                    MCU_Power_Save_StateTransition(POWER_SAVE_STATEMACH_POWER_SAVE,POWER_SAVE_SUB_STATEMACH_GET_STATE_REQUEST,1);
                }
            }
            else if(st_power_save_state.substate==POWER_SAVE_SUB_STATEMACH_WAITING_FOR_IRQ)
            {
                /*waiting for wake up source*/
                u8_ret_val=MCU_Power_Save_Waiting_For_WakeUp();
                
                /*wake up source detected*/
                if(u8_ret_val==1)
                {
                    MCU_Power_Save_StateTransition(POWER_SAVE_STATEMACH_NORMAL,POWER_SAVE_SUB_STATEMACH_ENABLE_PERIPHERAL,1);
                }
                else
                {
                    /*staying current state and substate*/
                    MCU_Power_Save_StateTransition(POWER_SAVE_STATEMACH_POWER_SAVE,POWER_SAVE_SUB_STATEMACH_WAITING_FOR_IRQ,1);
                }
            }
            else
            {
                    /*invalid state request*/
                    MCU_Power_Save_StateTransition(POWER_SAVE_STATEMACH_POWER_SAVE,POWER_SAVE_SUB_STATEMACH_GET_STATE_REQUEST,1);
            }
            break;
        
        /****************************NORMAL***********************************/
        case POWER_SAVE_STATEMACH_NORMAL:
            if(st_power_save_state.substate==POWER_SAVE_SUB_STATEMACH_ENABLE_PERIPHERAL)
            {
                /*enabling all the peripheral modules*/
                MCU_Power_Save_Peripheral_Module_Enable();
                /*changing substate system reinitialization*/
                MCU_Power_Save_StateTransition(POWER_SAVE_STATEMACH_NORMAL,POWER_SAVE_SUB_STATEMACH_SYS_REINIT,1);
            }
            else if(st_power_save_state.substate==POWER_SAVE_SUB_STATEMACH_SYS_REINIT)
            {
                /*system reinitialization*/
                //TODO:wait for 1 instruction cycle
                //TODO:system reinitialization
                /*changing substate to send wake up request to all the external modules*/
                MCU_Power_Save_StateTransition(POWER_SAVE_STATEMACH_NORMAL,POWER_SAVE_SUB_STATEMACH_SET_STATE_REQUEST,1);
            }
            else if(st_power_save_state.substate==POWER_SAVE_SUB_STATEMACH_SET_STATE_REQUEST)
            {
                /*sending wake up set state request to all the external modules*/
                MCU_Power_Save_Set_State_Request(0);
                /*changing sub state to get response of the wake up set state request*/
                MCU_Power_Save_StateTransition(POWER_SAVE_STATEMACH_NORMAL,POWER_SAVE_SUB_STATEMACH_GET_STATE_REQUEST,1);
            }
            else if(st_power_save_state.substate==POWER_SAVE_SUB_STATEMACH_GET_STATE_REQUEST)
            {
                /*getting response of the wake up set state request*/
                u8_ret_val=MCU_Power_Save_Get_Current_State();
                
                /*all external modules are woke up from sleep mode*/
                if(u8_ret_val==1)
                {
                    /*changing state to idle state for checking bms state is idle or not*/
                    /*no substate for this state*/
                    MCU_Power_Save_StateTransition(POWER_SAVE_STATEMACH_IDLE,POWER_SAVE_SUB_STATEMACH_NO_SUB_STATE_REQUEST,1);
                }
                else
                {
                    /*not changing state and sub state*/
                    MCU_Power_Save_StateTransition(POWER_SAVE_STATEMACH_POWER_SAVE,POWER_SAVE_STATEMACH_POWER_SAVE,1);
                }
            }
            else
            {
                /*invalid state request*/
                MCU_Power_Save_StateTransition(POWER_SAVE_STATEMACH_NORMAL,POWER_SAVE_SUB_STATEMACH_ENABLE_PERIPHERAL,1);
            }
            break;
        
        /*default*/
        default:
            break;
    }
}

/*sleep and idle mode assembly api's*/
#if 0
    .equ    SLEEP_MODE,0x0
    .equ    IDLE_MODE,0x1
            
;void MCU_Sleep(void);
    .global         _MCU_Sleep
    .type           _MCU_Sleep, @function
_MCU_Sleep:
PWRSAV #SLEEP_MODE;
    
    return

;void MCU_Idle(void);
    .global         _MCU_Idle
    .type           _MCU_Idle, @function
_MCU_Idle:
PWRSAV #IDLE_MODE;
    
    return
#endif

/********************************************************************************************************************************/
/* End of file                                                                                                                  */
/********************************************************************************************************************************/
/**@}*/
