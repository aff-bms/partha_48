/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name: can protocol stack                                                                                                        */
/* Author:Pavithran                                                                                                                     */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file can_protocol_stack.c
*   @brief This file contains API's which are required to communicate with CMU via UART.
*
*   @details can protocol stack .
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Pavithran
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 30-06-2021
*/

/** @defgroup C
*
* 
*
*/

/**@{*/
/********************************************************************************************************************************/
/* Include Section                                                                                                              */
/********************************************************************************************************************************/
#include "common.h"
#include "can1.h"
#include "can_stack.h"
#include "can_stack_cfg.h"

/********************************************************************************************************************************/
/* Defines Section - GLOBAL                                                                                                   */
/********************************************************************************************************************************/

/********************************************************************************************************************************/
/* Variables Section - GLOBAL                                                                                                   */
/********************************************************************************************************************************/
static st_CANS_STATE st_cans_state;
CAN_MSG_OBJ st_can_txMsg;
CAN_MSG_OBJ st_can_rxMsg;
u8 can_txMsg_buffer[64]={0};

/********************************************************************************************************************************/
/* Function Implementations Section - GLOBAL                                                                                    */
/********************************************************************************************************************************/

/** @fn     u16 MATH_swapBytes_uint16_t(u16 val)
*   @brief    Swap 2 byte data
*
*   @details  Swap 2 byte data
*
*   @pre      
*   @post     

*   @param    val 2 byte value
*   @return   swapped 2 byte value
*/
u16 CAN_Stack_MATH_swapBytes_uint16_t(u16 u16_val)
{
    return (u16_val << 8) | (u16_val >> 8 );
}

/** @fn     u32 MATH_swapBytes_uint32_t(u32 val)
*   @brief    Swap 4 byte data
*
*   @details  Swap 4 byte data
*
*   @pre      
*   @post     
*   @param    val 4 byte value
*   @return   swapped 4 byte value
*/
u32 CAN_Stack_MATH_swapBytes_uint32_t(u32 u32_val)
{
    u32_val = ((u32_val << 8) & 0xFF00FF00u) | ((u32_val >> 8) & 0xFF00FFu);
    return (u32_val << 16) | (u32_val >> 16);
}

/** @fn     u64 MATH_swapBytes_uint64_t(u64 val)
*   @brief    Swap 8 byte data
*
*   @details  Swap 8 byte data
*
*   @pre      
*   @post     
*   @param    val 8 byte value
*   @return   swapped 8 byte value
*/
u64 CAN_Stack_MATH_swapBytes_uint64_t(u64 u64_val)
{
    u64_val = ((u64_val << 8) & 0xFF00FF00FF00FF00ull) | ((u64_val >> 8) & 0x00FF00FF00FF00FFull);
    u64_val = ((u64_val << 16) & 0xFFFF0000FFFF0000ull) | ((u64_val >> 16) & 0x0000FFFF0000FFFFull);
    return (u64_val << 32) | (u64_val >> 32);
}

/** @fn       static u64 CAN_StackGetBitmask(u8 bitlength)
*   @brief    generates bitfield, which masks the bits where the actual signal (defined by its bitlength) is located
*
*   @details  generates bitfield, which masks the bits where the actual signal (defined by its bitlength) is located
*
*   @pre      
*   @post     
*   @param    bitlength   length of the signal in bits
*   @return   bitmask     bitfield mask
*/
static u64 CAN_Stack_GetBitmask(u8 u8_bitlength)
{
    u64 u64_bitmask = 0x00000000;
    u8 u8_i = 0;

    for (u8_i = 0; u8_i < u8_bitlength; u8_i++)
    {
        u64_bitmask = u64_bitmask << 1;
        u64_bitmask = u64_bitmask | 0x00000001;
    }
    return u64_bitmask;
}

/** @fn      void CAN_Stack_SetSignalData(CANS_signal_s signal, u64 value, u8 *dataPtr)
*   @brief    set signal data for transmission
*
*   @details  set signal data for transmission
*
*   @pre      
*   @post     
*   @param    sigIdx signal id
*   @param    signal value data
*   @param    dataPtr CAN message data, in which the signal data is inserted
*   @return   return success
*/
 void CAN_Stack_SetSignalData(CANS_signal_s signal, u64 u64_value, u8 *pdata)
{
     u64 u64_bitmask = 0x0000000000000000;
     
    if(pdata==NULL)
    {
        return;
    }
         
    u64 *dataPtr64 = (u64 *)pdata;

    /* Swap byte order if necessary */
    if (signal.byteOrder == littleEndian)
    {
        /* No need to switch byte order as native MCU endianness is little-endian (intel) */
    }
    else if (signal.byteOrder == bigEndian)
    {
        if (signal.bit_length <= 8)
        {
            /* No need to switch byte order as signal length is smaller than one byte */
        }
        else if (signal.bit_length <= 16)
        {
            /* Swap byte order */
            u64_value = (u64)CAN_Stack_MATH_swapBytes_uint16_t((u64)u64_value);
        }
        else if (signal.bit_length <= 32)
        {
            /* Swap byte order */
            u64_value = (u64)CAN_Stack_MATH_swapBytes_uint32_t((u32)u64_value);
        }
        else if (signal.bit_length <= 64)
        {
            /* Swap byte order */
            u64_value = CAN_Stack_MATH_swapBytes_uint64_t(u64_value);
        }
    }

    /* Set can data according to configuration */
    u64_bitmask = CAN_Stack_GetBitmask(signal.bit_length);
    dataPtr64[0] &= ~(((u64)u64_bitmask) << signal.bit_position);
    dataPtr64[0] |= ((((u64)u64_value) & u64_bitmask) << signal.bit_position);
}

/** @fn     u32 CAN_Get_BMS_Data(u32 sigIdx, void * value)
*   @brief    call back function to get bms data
*
*   @details  call back function to get bms data
*
*   @pre      
*   @post     
*   @param    sigIdx signal id
*   @param    value pointer to store data
*   @return   return success
*/
 u32 CAN_Stack_Get_BMS_Data(u32 u32_sigIdx, void *value)
 {
    u64 u64_temp_data=0;
    
    if(value==NULL)
    {
        return 1;
    }

	switch(u32_sigIdx)
	{
        case CAN_SIG_TX_TEST_12:
			u64_temp_data=0xFF;
			break;
            
        case CAN_SIG_TX_TEST_11:
			u64_temp_data=0xFF;
			break;
            
        case CAN_SIG_TX_TEST_10:
			u64_temp_data=0xFF;
			break;
            
		case CAN_SIG_Slave_status_0:
			/*u64_temp_data=batPack[0].bccStatus[BCC_FS_CELL_OV];
			u64_temp_data|=(batPack[0].bccStatus[BCC_FS_CELL_UV]<<16);
			u64_temp_data|=(batPack[0].bccStatus[BCC_FS_CB_OPEN]<<32);
			u64_temp_data|=(batPack[0].bccStatus[BCC_FS_CB_SHORT]<<48);*/
			break;

		case CAN_SIG_Slave_status_1:
			/*u64_temp_data=batPack[0].bccStatus[BCC_FS_COMM];
			u64_temp_data|=(batPack[0].bccStatus[BCC_FS_FAULT1]<<16);
			u64_temp_data|=(batPack[0].bccStatus[BCC_FS_FAULT2]<<32);
			u64_temp_data|=(batPack[0].bccStatus[BCC_FS_FAULT3]<<48);*/
			break;

		case CAN_SIG_Moving_mean_current_1s:
			//u64_temp_data=batPack[0].current;
			break;

		case CAN_SIG_PackVolt_Battery:
			//u64_temp_data=batPack[0].stackVolt;
			break;

		case CAN_SIG_Module_0_cell_voltage_0:
			//u64_temp_data=batPack[0].cellVolt[0];
			break;

		case CAN_SIG_Module_0_cell_voltage_1:
			//u64_temp_data=batPack[0].cellVolt[1];
			break;

		case CAN_SIG_Module_0_cell_voltage_2:
			//u64_temp_data=batPack[0].cellVolt[2];
			break;

		case CAN_SIG_Module_0_cell_voltage_3:
			//u64_temp_data=batPack[0].cellVolt[3];
			break;

		case CAN_SIG_Module_0_cell_voltage_4:
			//u64_temp_data=batPack[0].cellVolt[4];
			break;

		case CAN_SIG_Module_0_cell_voltage_5:
			//u64_temp_data=batPack[0].cellVolt[5];
			break;

		case CAN_SIG_Module_0_cell_voltage_6:
			//u64_temp_data=batPack[0].cellVolt[6];
			break;

		case CAN_SIG_Module_0_cell_voltage_7:
			//u64_temp_data=batPack[0].cellVolt[7];
			break;

		case CAN_SIG_Module_0_cell_voltage_8:
			//u64_temp_data=batPack[0].cellVolt[8];
			break;

		case CAN_SIG_Module_0_cell_voltage_9:
			//u64_temp_data=batPack[0].cellVolt[9];
			break;

		case CAN_SIG_Module_0_cell_voltage_10:
			//u64_temp_data=batPack[0].cellVolt[10];
			break;

		case CAN_SIG_Module_0_cell_voltage_11:
			//u64_temp_data=batPack[0].cellVolt[11];
			break;

		case CAN_SIG_Module_0_cell_voltage_12:
			//u64_temp_data=batPack[0].cellVolt[12];
			break;

		case CAN_SIG_Module_0_cell_voltage_13:
			//u64_temp_data=batPack[0].cellVolt[13];
			break;

		case CAN_SIG_Module_0_cell_temp_0:
			//u64_temp_data=batPack[0].ntcTemp[0];
			break;

		case CAN_SIG_Module_0_cell_temp_1:
			//u64_temp_data=batPack[0].ntcTemp[1];
			break;

		case CAN_SIG_Module_0_cell_temp_2:
			//u64_temp_data=batPack[0].ntcTemp[2];
			break;

		case CAN_SIG_Module_0_cell_temp_3:
			//u64_temp_data=batPack[0].ntcTemp[3];
			break;

		case CAN_SIG_Module_0_cell_temp_4:
			//u64_temp_data=batPack[0].ntcTemp[4];
			break;

		case CAN_SIG_Module_0_cell_temp_5:
			//u64_temp_data=batPack[0].ntcTemp[5];
			break;

		case CAN_SIG_Module_0_cell_temp_6:
			//u64_temp_data=batPack[0].icTemp;
			break;

		case CAN_SIG_Cell_voltage_mean:
			//u64_temp_data=bms_tab_minmax.voltage_mean;
			break;

		case CAN_SIG_Cell_voltage_minimum:
			//u64_temp_data=bms_tab_minmax.voltage_min;
			break;

		case CAN_SIG_Cell_voltage_maximum:
			//u64_temp_data=bms_tab_minmax.voltage_max;
			break;

		case CAN_SIG_Module_nr_cell_volt_min:
			//u64_temp_data=bms_tab_minmax.voltage_module_number_min;
			//u64_temp_data|=(bms_tab_minmax.voltage_cell_number_min<<4);
			break;

		case CAN_SIG_Module_nr_cell_volt_max:
			//u64_temp_data=bms_tab_minmax.voltage_module_number_max;
			//u64_temp_data|=(bms_tab_minmax.voltage_cell_number_max<<4);
			break;

		case CAN_SIG_Cell_temperature_mean:
			//u64_temp_data=bms_tab_minmax.temperature_mean;
			break;

		case CAN_SIG_Cell_temperature_minimum:
			//u64_temp_data=bms_tab_minmax.temperature_min;
			break;

		case CAN_SIG_Cell_temperature_maximum:
			//u64_temp_data=bms_tab_minmax.temperature_max;
			break;

		case CAN_SIG_Module_nr_cell_temp_min:
			//u64_temp_data=bms_tab_minmax.temperature_module_number_min;
			//u64_temp_data|=(bms_tab_minmax.temperature_sensor_number_min<<4);
			break;

		case CAN_SIG_Module_nr_cell_temp_max:
			//u64_temp_data=bms_tab_minmax.temperature_module_number_max;
			//u64_temp_data|=(bms_tab_minmax.temperature_sensor_number_max<<4);
			break;

		case CAN_SIG_General_error:
			//for(uint8_t i=0;i<BS_NR_OF_BAT_CELLS;i++)
			//u64_temp_data|=(bal_balancing.balancing_state[i]<<i);
			break;

		case CAN_SIG_SOC_mean:
			//u64_temp_data=current_soc*100;
			break;

		case CAN_SIG_SOH_mean:
			//u64_temp_data=current_soh*100;
			break;
	}
	*(unsigned long long int *)value=u64_temp_data;
    
    return E_OK;
 }
 
 /** @fn      void CAN_Stack_ComposeMessage( CANS_messagesTx_e msgIdx, u8 dataptr[])
*   @brief    Composes message data from all signals associated with this msgIdx
*
*   @details  signal data is received by callback getter functions
*
*   @pre      
*   @post     
*   @param    msgID message ID
*   @param    dataptr to compose message
*   @return   void
*/
void CAN_Stack_ComposeMessage( CANS_messagesTx_e msgIdx, u8 *pdata)
{
    u32 u32_i = 0;
    u64 u64_value = 0;
    
    if(pdata==NULL)
    {
        return;
    }

    for (u32_i = 0; u32_i < cans_signals_tx_length; u32_i++)
    {
        if (cans_signals_tx[u32_i].msgIdx.Tx == msgIdx)
    	{
            if (cans_signals_tx[u32_i].callback != NULL)
            {
            	cans_signals_tx[u32_i].callback(u32_i, &u64_value);
            }
            CAN_Stack_SetSignalData(cans_signals_tx[u32_i], u64_value, pdata);
        }
    }
}

/** @fn       void CAN_Stack_GetSignalData(u64 *dst, CANS_signal_s signal, u8 *dataPtr)
*   @brief    Extracts signal data from CAN message data
*
*   @details  Extracts signal data from CAN message data
*
*   @pre      
*   @post     
*   @param    dst       pointer where the signal data should be copied to
*   @param    signal    signal identifier
*   @param    signal    CAN message data, from which signal data is extracted
*   @return   void
*/
 void CAN_Stack_GetSignalData(u64 *pdst, CANS_signal_s signal, u8 *pdata)
{
    if(pdata==NULL)
    {
        return;
    }

    u64 bitmask = 0x00000000;
    u64 *dataPtr64 = (u64 *)pdata;

    /* Get signal data */
    bitmask = CAN_Stack_GetBitmask(signal.bit_length);
    *pdst = (((*dataPtr64) >> signal.bit_position) & bitmask);

    /* Swap byte order if necessary */
    if (signal.byteOrder == littleEndian)
    {
        /* No need to switch byte order as native MCU endianness is little-endian (intel) */
    }
    else if (signal.byteOrder == bigEndian)
    {
        if (signal.bit_length <= 8)
        {
            /* No need to switch byte order as signal length is smaller than one byte */
        }
        else if (signal.bit_length <= 16)
        {
            /* Swap byte order */
            *pdst = (u64)CAN_Stack_MATH_swapBytes_uint16_t((u16)*pdst);
        }
        else if (signal.bit_length <= 32)
        {
            /* Swap byte order */
            *pdst = (u64)CAN_Stack_MATH_swapBytes_uint32_t((u32)*pdst);
        }
        else if (signal.bit_length <= 64)
        {
            *pdst = CAN_Stack_MATH_swapBytes_uint64_t(*pdst);
        }
    }
}

/** @fn       void CAN_Stack_ParseMessage(CANS_messagesRx_e msgIdx, u8 dataptr[])
*   @brief    Parses signal data from message associated with this msgIdx
*
*   @details  signal data is received by callback setter functions
*
*   @pre      
*   @post     
*   @param    msgIdx   message index for which the data should be parsed
*   @param    dataptr  pointer where the message data is stored
*   @return   void
*/
 void CAN_Stack_ParseMessage(CANS_messagesRx_e msgIdx, u8 *pdata)
{
    u32 u32_i = 0;

    u64 u64_value = 0;
        
    if(pdata==NULL)
    {
        return;
    }

    	for (u32_i = 0; u32_i < cans_signals_rx_length; u32_i++)
    	{
    		/* Iterate over CAN rx signals and find message */
            if (cans_signals_rx[u32_i].msgIdx.Rx  ==  msgIdx)
            {             
                CAN_Stack_GetSignalData(&u64_value, cans_signals_rx[u32_i], pdata);

                if (cans_signals_rx[u32_i].callback != NULL)
                {
                    cans_signals_rx[u32_i].callback(u32_i, &u64_value);
                }
            }
        }
}

/** @fn       static STD_RETURN_TYPE_e CAN_Stack_PeriodicTransmit(CAN_MSG_OBJ st_can_txMsg)
*   @brief    handles the processing of messages that are meant to be transmitted
*
*   @details  * This function looks for the repetition times and the repetition phase of
* messages that are intended to be sent periodically. If a comparison with
* an internal counter (i.e., the counter how often this function has been called)
* states that a transmit is pending, the message is composed by call of CAN_Stack_ComposeMessage
* and transfered to the buffer of the CAN module. If a callback function
* is declared in configuration, this callback is called after successful transmission.
*
*   @pre      
*   @post     
*   @param    st_can_txMsg contains data to transmit
*   @return   E_OK if a successful transfer to CAN buffer occured, E_NOT_OK otherwise
*/
static e_STD_RETURN_TYPE CAN_Stack_PeriodicTransmit(void) 
{
    u32 u32_i = 0,u32_test_timer=0;
    e_STD_RETURN_TYPE result = E_NOT_OK;

    for (u32_i = 0; u32_i < can_tx_length; u32_i++) 
    {
        if(IsTimerExpired(u32_test_timer,can_messages_tx[u32_i].repetition_time))
        {
            CAN_Stack_ComposeMessage((CANS_messagesTx_e)(u32_i), st_can_txMsg.data);
			st_can_txMsg.msgId = can_messages_tx[u32_i].ID;
            
            result=CAN1_Transmit(CAN1_TXQ,&st_can_txMsg);

			if (can_messages_tx[u32_i].cbk_func != NULL && result == E_OK)
			{
				can_messages_tx[u32_i].cbk_func(u32_i, NULL);
				result = E_OK;
			}
            
             u32_test_timer=Timer_1ms;
        }
    }
    
    return result;
}

/** @fn       static STD_RETURN_TYPE_e CAN_Stack_PeriodicReceive(CAN_MSG_OBJ *st_can_rxMsg)
*   @brief    handles the processing of received CAN messages
*
*   @details  This function gets the messages in the receive buffer
*   of the CAN module. If a message ID is
*   matching one of the IDs in the configuration of
*   CANS module, the signal processing is executed
*   by call to CAN_Stack_ParseMessage 
*
*   @pre      
*   @post     
*   @param    st_can_rxMsg source buffer pointer
*   @return   E_OK, if a message has been received and parsed, E_NOT_OK otherwise
*/
static e_STD_RETURN_TYPE CAN_Stack_PeriodicReceive(void) 
{
    e_STD_RETURN_TYPE result = E_NOT_OK;
    u32 u32_i = 0;

    while (CAN1_Receive(&st_can_rxMsg) ==  true) 
    {
        for (u32_i = 0; u32_i < can_rx_length; u32_i++) 
        {           
            if (st_can_rxMsg.msgId ==  can_RxMsgs[u32_i].ID)
			{
				CAN_Stack_ParseMessage( (CANS_messagesRx_e)u32_i, st_can_rxMsg.data);
                CAN1_Transmit(CAN1_TXQ,&st_can_rxMsg);
				result = E_OK;
			}
        }
    }

    return result;
}

/** @fn       STD_RETURN_TYPE_e CAN_Stack_DynamicTransmit(u32 msgID,CAN_MSG_OBJ st_can_txMsg)
*   @brief    handles the processing of messages that are meant to be transmitted
*
*   @details  * This function looks for the repetition times and the repetition phase of
* messages that are intended to be sent periodically. If a comparison with
* an internal counter (i.e., the counter how often this function has been called)
* states that a transmit is pending, the message is composed by call of CAN_Stack_ComposeMessage
* and transfered to the buffer of the CAN module. If a callback function
* is declared in configuration, this callback is called after successful transmission.
*
*   @pre      
*   @post     
*   @param    msgID message ID
*   @param    st_can_txMsg contains data to transmit
*   @return   E_OK if a successful transfer to CAN buffer occured, E_NOT_OK otherwise
*/
e_STD_RETURN_TYPE CAN_Stack_DynamicTransmit(u32 msgID)
{
    e_STD_RETURN_TYPE result = E_NOT_OK;

    u32 u32_i=0;

	for (u32_i = 0; u32_i < can_tx_length; u32_i++)
	{
    	if (msgID ==  can_messages_tx[u32_i].ID)
    	{
			CAN_Stack_ComposeMessage((CANS_messagesTx_e)(u32_i), st_can_txMsg.data);
			st_can_txMsg.msgId = can_messages_tx[u32_i].ID;

            result=CAN1_Transmit(CAN1_TXQ,&st_can_txMsg);

			if (can_messages_tx[u32_i].cbk_func != NULL && result == E_OK)
			{
				can_messages_tx[u32_i].cbk_func(u32_i, NULL);
				result = E_OK;
			}
    	}
	}
    
    return result;
}

/** @fn      STD_RETURN_TYPE_e CAN_Stack_DynamicReceive(u32 ID,CAN_MSG_OBJ *st_can_rxMsg)
*   @brief    This function gets the messages in the receive buffer
*
*   @details  * This function gets the messages in the receive buffer
 * of the CAN module. If a message ID is
 * matching one of the IDs in the configuration of
 * CANS module, the signal processing is executed
 * by call to CAN_Stack_ParseMessage.
*
*   @pre      
*   @post     
*   @param    ID message ID
*   @param    st_can_rxMsg contains data to receive
*   @return   E_OK, if a message has been received and parsed, E_NOT_OK otherwise
*/
e_STD_RETURN_TYPE CAN_Stack_DynamicReceive(u32 ID,CAN_MSG_OBJ *can_st_Rx_Msg_Data)
{
    e_STD_RETURN_TYPE result = E_NOT_OK;
    u32 u32_i = 0,u32_j=0;
 
    if(can_st_Rx_Msg_Data==NULL)
    {
        return result;
    }
 
    for (u32_j=0;u32_j<32;u32_j++)
    {
        CAN1_Receive(&st_can_rxMsg);
         
		for (u32_i = 0; u32_i < can_rx_length; u32_i++)
		{
			if (st_can_rxMsg.msgId ==  can_RxMsgs[u32_i].ID &&  st_can_rxMsg.msgId == ID)
			{
				CAN_Stack_ParseMessage( (CANS_messagesRx_e)u32_i, st_can_rxMsg.data);
                memcpy(can_st_Rx_Msg_Data,&st_can_rxMsg,sizeof(st_can_rxMsg));
				result = E_OK;
			}
		}
    }

    return result;
}

/** @fn       void CAN_Stack_Init(void)
*   @brief    CAN structure initialization
*
*
*   @details  CAN structure initialization 
*
*   @pre      call after system and os init
*   @post     
*   @param    void
*   @return   void
*/
void CAN_Stack_Init(void) 
{ 
    /*variable initialization*/
    st_cans_state.statereq                    = CANS_STATEMACH_INIT_REQUEST,
    st_cans_state.state                       = CANS_STATEMACH_UNINITIALIZED,
    st_cans_state.u8_ErrRetryCounter          = 0,
    st_cans_state.u8_periodic_tx_enable = 0;
    st_cans_state.u8_periodic_rx_enable = 0;
                
    st_can_txMsg.msgId=150;
    st_can_txMsg.field.idType=0;
    st_can_txMsg.field.frameType=0;
    st_can_txMsg.field.dlc=8;
    st_can_txMsg.field.formatType=0;
    st_can_txMsg.field.brs=0;
    st_can_txMsg.data=can_txMsg_buffer;  
    st_can_txMsg.data="helloooo";
}

/** @fn       void CAN_Stack_MainFunction(void)
*   @brief    CAN transmit and receive messages
*
*
*   @details  CAN main function to transmit and receive messages periodically/dynamically 
*
*   @pre      call after system and os init
*   @post     
*   @param    void
*   @return   void
*/
void CAN_Stack_MainFunction(void) 
{   
    bool status = E_NOT_OK;
     
    if(st_cans_state.u8_periodic_rx_enable==true)
    {
        (void)CAN_Stack_PeriodicReceive();
    }
    else
    {
       //status=CAN_Stack_DynamicReceive(1808,&st_can_rxMsg);
    }       
    //TODO:manage CAN timings here      
    if (st_cans_state.u8_periodic_tx_enable == true) 
    {
       (void)CAN_Stack_PeriodicTransmit();
    }
    else
    {
       (void)CAN_Stack_DynamicTransmit(1220);
    }
}

/** @fn       void CANS_Trigger(void)
*   @brief    CAN transmit and receive messages
*
*
*   @details  function to transmit and receive messages periodically/dynamically 
*
*   @pre      call after system and os init
*   @post     
*   @param    void
*   @return   void
*/
void CAN_Stack_Trigger(void)
{
    e_CANS_STATEMACH statereq = CANS_STATEMACH_NO_REQUEST;
    
    switch (st_cans_state.state)
    {
        /****************************UNINITIALIZED***********************************/
        case CANS_STATEMACH_UNINITIALIZED:
            /* waiting for Initialization Request */
        	statereq=st_cans_state.statereq;
            if (statereq == CANS_STATEMACH_INIT_REQUEST) 
            {
    			st_cans_state.state=CANS_STATEMACH_INITIALIZATION;
            } else if (statereq == CANS_STATEMACH_NO_REQUEST) 
            {
                /* no actual request pending */
            } else 
            {
                st_cans_state.u8_ErrRetryCounter++;   /* illegal request pending */
            }
            break;

        /****************************INITIALIZATION**********************************/
        case CANS_STATEMACH_INITIALIZATION:
			CAN_Stack_Init();
            st_cans_state.state=CANS_STATEMACH_INITIALIZED;
            break;

        /****************************INITIALIZED*************************************/
        case CANS_STATEMACH_INITIALIZED:
            st_cans_state.state=CANS_STATEMACH_TRANSFER;
            break;

		/****************************REINIT*********************************/
		case CANS_STATEMACH_REINIT:
			st_cans_state.state=CANS_STATEMACH_INITIALIZATION;
			break;

		/****************************IDLE*************************************/
		case CANS_STATEMACH_IDLE:
			st_cans_state.state=CANS_STATEMACH_INITIALIZATION;
			break;
            
        /****************************IDLE*************************************/
		case CANS_STATEMACH_TRANSFER:
            CAN_Stack_MainFunction();
			st_cans_state.state=CANS_STATEMACH_TRANSFER;
			break;
            
        /****************************DEFAULT**************************/
		default:
			break;
    }
}

/** @fn       void CAN_Stack_Test_Function(void)
*   @brief    CAN transmit and receive messages
*
*
*   @details  CAN main function to transmit and receive messages periodically/dynamically 
*
*   @pre      call after system init
*   @post     
*   @param    void
*   @return   void
*/
void CAN_Stack_Test_Function(void) 
{   
    bool status = E_NOT_OK;
    
    CAN_Stack_Init();
    
    while(1)
    {      
         if(st_cans_state.u8_periodic_rx_enable)
         {
             status=CAN_Stack_PeriodicReceive();
         }
         else
         {
            //status=CAN1_Receive(&st_can_rxMsg);
            //status=CAN_Stack_DynamicReceive(1808,&st_can_rxMsg);
         }       
      
        if (st_cans_state.u8_periodic_tx_enable == true) 
        {
            (void)CAN_Stack_PeriodicTransmit();
        }
        else
        {
            //if(status = E_OK)
            //{
                CAN1_Transmit(CAN1_TXQ,&st_can_txMsg);
                //CAN_Stack_DynamicTransmit(1220);
                status=E_NOT_OK;
            //}
        }
    }
}
    
/********************************************************************************************************************************/
/* End of file                                                                                                                  */
/********************************************************************************************************************************/
/**@}*/