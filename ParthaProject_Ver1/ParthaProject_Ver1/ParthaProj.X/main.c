/**
  Generated main.c file from MPLAB Code Configurator

  @Company
    Microchip Technology Inc.

  @File Name
    main.c

  @Summary
    This is the generated main.c using PIC24 / dsPIC33 / PIC32MM MCUs.

  @Description
    This source file provides main entry point for system initialization and application code development.
    Generation Information :
        Product Revision  :  PIC24 / dsPIC33 / PIC32MM MCUs - 1.170.0
        Device            :  dsPIC33CH512MP508
    The generated drivers are tested against the following:
        Compiler          :  XC16 v1.61
        MPLAB 	          :  MPLAB X v5.45
*/

/*
    (c) 2020 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

/**
  Section: Included Files
*/
#include "mcc_generated_files/system.h"
#include "os.h"
#include "can_stack.h"
#include "mcc_generated_files/watchdog.h"
#include "mcc_generated_files/pin_manager.h"
#include "event_log.h"
#include "mcc_generated_files/memory/flash.h"
#include "power_save.h"
/*
                         Main application
 */
int main(void)
{
    u32 test_timer=0;
    
    //initialize the device
    SYSTEM_Initialize();

#if 1 
    //Watch_Dog_Test_Code();
   
    printf("before while1\n");
    test_timer=Timer_1ms;
    while(1)
    {
        UART_Send_Buffer(1,"abc\n",4);
        IO_RE0_Toggle();
        if(IsTimerExpired(test_timer,3*1000))
        {   
            //WATCHDOG_TimerClear();                    
            test_timer=Timer_1ms;
            
            UART_Send_Buffer(1,"def\n",4); 
            
            if(int_flag==0)
            {
                //MCU_Power_Save_Sleep();
                MCU_Power_Save_Idle();
                //IO_RE0_Toggle();
                MCU_Power_Save_Peripheral_Module_Disable();
                int_flag=2;
            }           
            else if(int_flag==1)
            {    
                //IO_RE0_Toggle();                 
                MCU_Power_Save_Peripheral_Module_Enable(); 
                UART1_Initialize();
                int_flag=0;
            }
        }
        MCU_WaitMs(1000);              
    }
#endif 
    
#if 0
    CAN_Stack_Test_Function();
#endif
 
#if 0    
    Event_Logging_Main_Function();
#endif

#if 0     
    MCU_Power_Save_Main_Function();
#endif
     
#if 0     
    /*Create task and Start the schedular*/
    //Os_Schedule();
#endif
     
#if 0
    /*Flash test*/
    FlashDemo();
#endif
    
#if 0
    char buffer[8]={0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37};
    CRC_CalculateBufferStart(buffer,8);
    while(CRC_CalculationIsDone() == false)
    {
        CRC_Task();
    }
    result = CRC_CalculationResultGet(false,0xFFFFFFFF);
#endif
    
    return 0;
}
/**
 End of File
*/

