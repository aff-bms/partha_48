/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name: event logging                                                                                                        */
/* Author:Pavithran                                                                                                                     */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file diag.c
*   @brief This file contains API's which are required for diagnostic.
*
*   @details diagnostic .
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Pavithran
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 27-07-2021
*/

/** @defgroup event log
*
* This file contains API's which are required for diagnostic.
*
*/

/**@{*/
/********************************************************************************************************************************/
/* Include Section                                                                                                              */
/********************************************************************************************************************************/
#include "database.h"
#include "diag.h"

/********************************************************************************************************************************/
/* Variables Section - GLOBAL                                                                                                   */
/********************************************************************************************************************************/
static DATA_BLOCK_ERRORSTATE_s error_flags = { 0 };
static DATA_BLOCK_MOL_FLAG_s mol_flags = { 0 };
static DATA_BLOCK_RSL_FLAG_s rsl_flags = { 0 };
static DATA_BLOCK_MSL_FLAG_s msl_flags = { 0 };

/********************************************************************************************************************************/
/* Function Implementations Section - GLOBAL                                                                                    */
/********************************************************************************************************************************/

/** @fn       void DIAG_updateFlags(void)
*   @brief    function to update the error flag in global memory
*
*   @details  function to update the error flag in global memory
*
*   @pre      
*   @post     
*   @param    void
*   @return   void
*/
void DIAG_updateFlags(void) //todo:move it to diag.c
{
    DB_WriteBlock(&error_flags, DATA_BLOCK_ID_ERRORSTATE);
    DB_WriteBlock(&msl_flags, DATA_BLOCK_ID_MSL);
    DB_WriteBlock(&rsl_flags, DATA_BLOCK_ID_RSL);
    DB_WriteBlock(&mol_flags, DATA_BLOCK_ID_MOL);
}

/** @fn       void Event_Logging_Call_Back_Function(e_event_id ch_id, e_event_log event)
*   @brief    function to update the corresponding error flag in global memory
*
*   @details  function to update the corresponding error flag in global memory
*
*   @pre      
*   @post     
*   @param    void
*   @return   void
*/
void DIAG_Call_Back_Function(e_event_id ch_id, e_event_log event)
{
    switch(ch_id)
    {
        /*over voltage maximum safety limit reached*/
        case EVENT_LOG_CELLVOLTAGE_OVERVOLTAGE_MSL:
            if(event==EVENT_NOK)
            {
                msl_flags.over_voltage = 1;
            }
            else
            {
                msl_flags.over_voltage = 0;
            }
            break;
            
        /*over voltage recommended safety limit reached*/
        case EVENT_LOG_CELLVOLTAGE_OVERVOLTAGE_RSL:
            if(event==EVENT_NOK)
            {
                rsl_flags.over_voltage = 1;
            }
            else
            {
                rsl_flags.over_voltage = 0;
            }
            break;
            
        /*over voltage maximum operating limit reached*/    
        case EVENT_LOG_CELLVOLTAGE_OVERVOLTAGE_MOL:
            if(event==EVENT_NOK)
            {
                mol_flags.over_voltage = 1;
            }
            else
            {
                mol_flags.over_voltage = 0;
            }
            break;
        
        /*under voltage maximum safety limit reached*/
        case EVENT_LOG_CELLVOLTAGE_UNDERVOLTAGE_MSL:
            if(event==EVENT_NOK)
            {
                msl_flags.under_voltage = 1;
            }
            else
            {
                msl_flags.under_voltage = 0;
            }
            break;
            
        /*over voltage recommended safety limit reached*/    
        case EVENT_LOG_CELLVOLTAGE_UNDERVOLTAGE_RSL:
            if(event==EVENT_NOK)
            {
                rsl_flags.under_voltage = 1;
            }
            else
            {
                rsl_flags.under_voltage = 0;
            }
            break;
        
        /*over voltage maximum operating limit reached*/
        case EVENT_LOG_CELLVOLTAGE_UNDERVOLTAGE_MOL:
            if(event==EVENT_NOK)
            {
                mol_flags.under_voltage = 1;
            }
            else
            {
                mol_flags.under_voltage = 0;
            }
            break;
            
        /*charge over temperature maximum safety limit reached*/
        case EVENT_LOG_TEMP_OVERTEMPERATURE_CHARGE_MSL:
            if(event==EVENT_NOK)
            {
                msl_flags.over_temperature_charge = 1;
            }
            else
            {
                msl_flags.over_temperature_charge = 0;
            }
            break;
        
        /*charge over temperature recommended safety limit reached*/
        case EVENT_LOG_TEMP_OVERTEMPERATURE_CHARGE_RSL:
            if(event==EVENT_NOK)
            {
                rsl_flags.over_temperature_charge = 1;
            }
            else
            {
                rsl_flags.over_temperature_charge = 0;
            }
            break;
            
        /*charge over temperature maximum operating limit reached*/    
        case EVENT_LOG_TEMP_OVERTEMPERATURE_CHARGE_MOL:
            if(event==EVENT_NOK)
            {
                mol_flags.over_temperature_charge = 1;
            }
            else
            {
                mol_flags.over_temperature_charge = 0;
            }
            break;
            
        /*discharge over temperature maximum safety limit reached*/
        case EVENT_LOG_TEMP_OVERTEMPERATURE_DISCHARGE_MSL:
            if(event==EVENT_NOK)
            {
                msl_flags.over_temperature_charge = 1;
            }
            else
            {
                msl_flags.over_temperature_charge = 0;
            }
            break;
        
        /*discharge over temperature recommended safety limit reached*/
        case EVENT_LOG_TEMP_OVERTEMPERATURE_DISCHARGE_RSL:
            if(event==EVENT_NOK)
            {
                rsl_flags.over_temperature_charge = 1;
            }
            else
            {
                rsl_flags.over_temperature_charge = 0;
            }
            break;
        
        /*discharge over temperature maximum operating limit reached*/
        case EVENT_LOG_TEMP_OVERTEMPERATURE_DISCHARGE_MOL:
            if(event==EVENT_NOK)
            {
                mol_flags.over_temperature_charge = 1;
            }
            else
            {
                mol_flags.over_temperature_charge = 0;
            }
            break;
        
        /*charge under temperature maximum safety limit reached*/
        case EVENT_LOG_TEMP_UNDERTEMPERATURE_CHARGE_MSL:
            if(event==EVENT_NOK)
            {
                msl_flags.under_temperature_charge = 1;
            }
            else
            {
                msl_flags.under_temperature_charge = 0;
            }
            break;
            
        /*charge under temperature recommended safety limit reached*/    
        case EVENT_LOG_TEMP_UNDERTEMPERATURE_CHARGE_RSL:
            if(event==EVENT_NOK)
            {
                rsl_flags.under_temperature_charge = 1;
            }
            else
            {
                rsl_flags.under_temperature_charge = 0;
            }
            break;
        
        /*charge under temperature maximum operating limit reached*/
        case EVENT_LOG_TEMP_UNDERTEMPERATURE_CHARGE_MOL:
            if(event==EVENT_NOK)
            {
                mol_flags.under_temperature_charge = 1;
            }
            else
            {
                mol_flags.under_temperature_charge = 0;
            }
            break;
        
        /*discharge under temperature maximum safety limit reached*/
        case EVENT_LOG_TEMP_UNDERTEMPERATURE_DISCHARGE_MSL:
            if(event==EVENT_NOK)
            {
                msl_flags.under_temperature_discharge = 1;
            }
            else
            {
                msl_flags.under_temperature_discharge = 0;
            }
            break;
            
        /*discharge under temperature recommended safety limit reached*/    
        case EVENT_LOG_TEMP_UNDERTEMPERATURE_DISCHARGE_RSL:
            if(event==EVENT_NOK)
            {
                rsl_flags.under_temperature_discharge = 1;
            }
            else
            {
                rsl_flags.under_temperature_discharge = 0;
            }
            break;
            
        /*discharge under temperature maximum operating limit reached*/    
        case EVENT_LOG_TEMP_UNDERTEMPERATURE_DISCHARGE_MOL:
            if(event==EVENT_NOK)
            {
                mol_flags.under_temperature_discharge = 1;
            }
            else
            {
                mol_flags.under_temperature_discharge = 0;
            }
            break;
        
        /*die temperature maximum safety limit reached*/
        case EVENT_LOG_ERROR_MCU_DIE_TEMPERATURE:
            if(event==EVENT_NOK)
            {
                error_flags.mcuDieTemperature = 1;
            }
            else
            {
                error_flags.mcuDieTemperature = 0;
            }
            break;
        
        /*charge over current maximum safety limit reached*/
        case EVENT_LOG_OVERCURRENT_CHARGE_CELL_MSL:
            if(event==EVENT_NOK)
            {
                msl_flags.over_current_charge_cell = 1;
            }
            else
            {
                msl_flags.over_current_charge_cell = 0;
            }
            break;
            
        /*charge over current recommended safety limit reached*/    
        case EVENT_LOG_OVERCURRENT_CHARGE_CELL_RSL:
            if(event==EVENT_NOK)
            {
                msl_flags.over_current_charge_cell = 1;
            }
            else
            {
                msl_flags.over_current_charge_cell = 0;
            }
            break;
        
        /*charge over current maximum operating limit reached*/
        case EVENT_LOG_OVERCURRENT_CHARGE_CELL_MOL:
            if(event==EVENT_NOK)
            {
                msl_flags.over_current_charge_cell = 1;
            }
            else
            {
                msl_flags.over_current_charge_cell = 0;
            }
            break;
            
        /*discharge over current maximum safety limit reached*/    
        case EVENT_LOG_OVERCURRENT_DISCHARGE_CELL_MSL:
            if(event==EVENT_NOK)
            {
                msl_flags.over_current_discharge_cell = 1;
            }
            else
            {
                msl_flags.over_current_discharge_cell = 0;
            }
            break;
            
         /*discharge over current recommended safety limit reached*/    
        case EVENT_LOG_OVERCURRENT_DISCHARGE_CELL_RSL:
            if(event==EVENT_NOK)
            {
                msl_flags.over_current_discharge_cell = 1;
            }
            else
            {
                msl_flags.over_current_discharge_cell = 0;
            }
            break;
            
         /*discharge over current maximum operating limit reached*/    
        case EVENT_LOG_OVERCURRENT_DISCHARGE_CELL_MOL:
            if(event==EVENT_NOK)
            {
                msl_flags.over_current_discharge_cell = 1;
            }
            else
            {
                msl_flags.over_current_discharge_cell = 0;
            }
            break;
            
        default:
            break;
    }
}
/********************************************************************************************************************************/
/* End of file                                                                                                                  */
/********************************************************************************************************************************/
/**@}*/