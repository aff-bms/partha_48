/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name:Driver for the contactors                                                                                                       */
/* Author:Pavithran                                                                                                                     */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file contactor_cfg.h
*   @brief This file contains Header for the configuration for the driver for the contactors.
*
*   @details Header for the configuration for the driver for the contactors
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Pavithran
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 22-06-2021
*/

/** @defgroup contactor
*
* This file contains Header for the configuration for the driver for the contactors
*
*/

/**@{*/
/********************************************************************************************************************************/
/* Include Section                                                                                                              */
/********************************************************************************************************************************/
#ifndef CONTACTOR_CFG_H_
#define CONTACTOR_CFG_H_

#include "battery_system_cfg.h"

/**
 * @ingroup CONFIG_GENERAL
 * enables CONTACTOR module
 * \par Type:
 * select(2)
 * \par Default:
 * 0
*/
#define BUILD_MODULE_ENABLE_CONTACTOR           1


/*================== Macros and Definitions ===============================*/

/**
 * @ingroup CONFIG_CONTACTOR
 * defines the number of bad countings of opening contactors at too
 *  high current
 * \par Type:
 * int
 * \par Default:
 * 10
 * \par Range:
 * [9,11]
*/
#define CONT_NUMBER_OF_BAD_COUNTINGS 10

/**
 * @ingroup CONFIG_CONTACTOR
 * This macro describes the limiting current from the the positive to
 * negative side of the contactor at which a damaging of the
 * contactor occurs. If this limit is exceeded the contactor
 * module makes an entry in the diagnosis module indicating a
 * switching off of the contactors under a bad  condition
 *
 * \par Type:
 * float
 * \par Default:
 * 1.0
 * \par Range:
 * [1.0,2.0]
*/
#define BAD_SWITCHOFF_CURRENT_POS 100000.0f

/**
 * @ingroup CONFIG_CONTACTOR
 * This macro describes the limiting current from the the negative to
 * positive side of the contactor at which a damaging of the
 * contactor occurs. If this limit is exceeded the contactor
 * module makes an entry in the diagnosis module indicating a
 * switching off of the contactors under a bad  condition
 *
 * \par Type:
 * float
 * \par Default:
 * -1.0
 * \par Range:
 * [-2.0,-1.0]
*/
#define BAD_SWITCHOFF_CURRENT_NEG -1000000.0f

/*
 * The number of defines per contactor must be the same as the length
 *  of the array cont_contactors_cfg in contactor_cfg.c
 * Every contactor consists of 1 control pin and 1 feedback pin
 * counting together as 1 contactor.
 * E.g. if you have 1 contactor your define has to be:
 *      #define CONT_MAIN_PLUS_CONTROL      IO_PIN_CONTACTOR_0_CONTROL
 *      #define CONT_MAIN_PLUS_FEEDBACK     IO_PIN_CONTACTOR_0_FEEDBACK
 */
//TODO:pavi:pins need to be define
#define CONT_MAIN_PLUS_CONTROL                  {0}//IO_PIN_CONTACTOR_0_CONTROL
#define CONT_MAIN_PLUS_FEEDBACK                 {0}//IO_PIN_CONTACTOR_0_FEEDBACK

#define CONT_PRECHARGE_PLUS_CONTROL             {0}//IO_PIN_CONTACTOR_1_CONTROL
#define CONT_PRECHARGE_PLUS_FEEDBACK            {0}//IO_PIN_CONTACTOR_1_FEEDBACK

#define CONT_MAIN_MINUS_CONTROL                 {0}//IO_PIN_CONTACTOR_2_CONTROL
#define CONT_MAIN_MINUS_FEEDBACK                {0}//IO_PIN_CONTACTOR_2_FEEDBACK

#if BS_SEPARATE_POWERLINES == 1
#define CONT_CHARGE_MAIN_PLUS_CONTROL           {0}//IO_PIN_CONTACTOR_3_CONTROL
#define CONT_CHARGE_MAIN_PLUS_FEEDBACK          {0}//IO_PIN_CONTACTOR_3_FEEDBACK

#define CONT_CHARGE_PRECHARGE_PLUS_CONTROL      {0}//IO_PIN_CONTACTOR_4_CONTROL
#define CONT_CHARGE_PRECHARGE_PLUS_FEEDBACK     {0}//IO_PIN_CONTACTOR_4_FEEDBACK

#define CONT_CHARGE_MAIN_MINUS_CONTROL          {0}//IO_PIN_CONTACTOR_5_CONTROL
#define CONT_CHARGE_MAIN_MINUS_FEEDBACK         {0}//IO_PIN_CONTACTOR_5_FEEDBACK
#endif /* BS_SEPARATE_POWERLINES == 1 */
/*
 * additional possible contactors from the io definition
#define CONT_X0_CONTROL                         PIN_CONTACTOR_3_CONTROL
#define CONT_X0_FEEDBACK                        PIN_CONTACTOR_3_FEEDBACK

#define CONT_X1_CONTROL                         PIN_CONTACTOR_4_CONTROL
#define CONT_X1_FEEDBACK                        PIN_CONTACTOR_4_FEEDBACK

#define CONT_X2_CONTROL                         PIN_CONTACTOR_5_CONTROL
#define CONT_X2_FEEDBACK                        PIN_CONTACTOR_5_FEEDBACK
*/


/**
 * This define MUST represent the cycle time of the task in which context the
 * functions run, e.g., if the CONT_Trigger() is running in the 10 ms task
 * then the define must be set to 10.
 *
 * This define also sets the minimum time.
 */

#define CONT_TASK_CYCLE_CONTEXT_MS (10)

/**
 * Counter limit used to prevent contactor oscillation
 */

#define CONT_OSCILLATION_LIMIT 500


/**
 * Number of allowed tries to close contactors
 */
#define CONT_PRECHARGE_TRIES 3

/**
 * Delay between open first and second contactor
 */

#define CONT_DELAY_BETWEEN_OPENING_CONTACTORS_MS        ((1) * (CONT_TASK_CYCLE_CONTEXT_MS))

/**
 * Delay after opening second contactor
 */

#define CONT_DELAY_AFTER_OPENING_SECOND_CONTACTORS_MS   ((50) *  (CONT_TASK_CYCLE_CONTEXT_MS))


/**
 * CONT statemachine short time definition in ms
 */

#define CONT_STATEMACH_SHORTTIME_MS                     (CONT_TASK_CYCLE_CONTEXT_MS)


/**
 * CONT statemachine time to wait after contactors opened because precharge failed in ms
 */
#define CONT_STATEMACH_TIMEAFTERPRECHARGEFAIL_MS        ((100) * (CONT_TASK_CYCLE_CONTEXT_MS))


/*================== Main precharge configuration ====================*/

/**
 * Precharge timeout in ms
 */
#define CONT_PRECHARGE_TIMEOUT_MS ((500) * (CONT_TASK_CYCLE_CONTEXT_MS))

/**
 * Delay after closing main minus in ms
 */
#define CONT_STATEMACH_WAIT_AFTER_CLOSING_MINUS_MS ((50) * (CONT_TASK_CYCLE_CONTEXT_MS))

/**
 * Delay after closing precharge in ms
 */

#define CONT_STATEMACH_WAIT_AFTER_CLOSING_PRECHARGE_MS ((100) * (CONT_TASK_CYCLE_CONTEXT_MS))

/**
 * Delay after closing main plus in ms
 */
#define CONT_STATEMACH_WAIT_AFTER_CLOSING_PLUS_MS ((1) * (CONT_TASK_CYCLE_CONTEXT_MS))

/**
 * Delay after opening precharge in ms
 */
#define CONT_STATEMACH_WAIT_AFTER_OPENING_PRECHARGE_MS ((50) * (CONT_TASK_CYCLE_CONTEXT_MS))

/**
 * @ingroup CONFIG_CONTACTOR
 * \par Type:
 * int
 * \par Default:
 * 1000
 * \par Range:
 * [1000,3000]
 * \par Unit:
 * V
*/
#define CONT_PRECHARGE_VOLTAGE_THRESHOLD_mV     1000  /* mV */

/**
 * @ingroup CONFIG_CONTACTOR
 * \par Type:
 * int
 * \par Default:
 * 10
 * \par Range:
 * [50,500]
 * \par Unit:
 * mA
*/
#define CONT_PRECHARGE_CURRENT_THRESHOLD_mA     50  /* mA */


/*================== Charge precharge configuration ====================*/

/**
 * Charge precharge timeout in ms
 */
#define CONT_CHARGE_PRECHARGE_TIMEOUT_MS ((500) * (CONT_TASK_CYCLE_CONTEXT_MS))

/**
 * Delay after closing charge minus in ms
 */
#define CONT_STATEMACH_CHARGE_WAIT_AFTER_CLOSING_MINUS_MS ((50) * (CONT_TASK_CYCLE_CONTEXT_MS))

/**
 * Delay after closing charge precharge in ms
 */

#define CONT_STATEMACH_CHARGE_WAIT_AFTER_CLOSING_PRECHARGE_MS ((100) * (CONT_TASK_CYCLE_CONTEXT_MS))

/**
 * Delay after closing charge plus in ms
 */
#define CONT_STATEMACH_CHARGE_WAIT_AFTER_CLOSING_PLUS_MS ((1) * (CONT_TASK_CYCLE_CONTEXT_MS))

/**
 * @ingroup CONFIG_CONTACTOR
 * \par Type:
 * int
 * \par Default:
 * 1000
 * \par Range:
 * [1000,3000]
 * \par Unit:
 * V
*/
#define CONT_CHARGE_PRECHARGE_VOLTAGE_THRESHOLD_mV   1000  /* mV */

/**
 * @ingroup CONFIG_CONTACTOR
 * \par Type:
 * int
 * \par Default:
 * 10
 * \par Range:
 * [50,500]
 * \par Unit:
 * mA
*/
#define CONT_CHARGE_PRECHARGE_CURRENT_THRESHOLD_mA  50  /* mA */


/*================== Constant and Variable Definitions ====================*/

/**
 * Symbolic names for contactors' possible states
 */
typedef enum {
    CONT_SWITCH_OFF     = 0,    /*!< Contactor off         --> Contactor is open           */
    CONT_SWITCH_ON      = 1,    /*!< Contactor on          --> Contactor is closed         */
    CONT_SWITCH_UNDEF   = 2,    /*!< Contactor undefined   --> Contactor state not known   */
} CONT_ELECTRICAL_STATE_TYPE_s;

/**
 * Symbolic names for the contactors, which are used in
 * the contactor_config[] array
 */
typedef enum {
    CONT_MAIN_PLUS =0     		,	 /*!< Main contactor in the positive path of the powerline      */
	CONT_CHARGE_MAIN_PLUS     ,    /*!< Main contactor in the positive charge path of the powerline      */
    CONT_PRECHARGE_PLUS         ,    /*!< Precharge contactor in the positive path of the powerline */
    CONT_MAIN_MINUS             ,    /*!< Main contactor in the negative path of the powerline      */
#if BS_SEPARATE_POWERLINES == 1
    //CONT_CHARGE_MAIN_PLUS       ,    /*!< Main contactor in the positive charge path of the powerline      */
    CONT_CHARGE_PRECHARGE_PLUS  ,    /*!< Precharge contactor in the positive charge path of the powerline */
    CONT_CHARGE_MAIN_MINUS      ,    /*!< Main contactor in the negative charge path of the powerline      */
#endif /* BS_SEPARATE_POWERLINES == 1 */
} CONT_NAMES_e;

/**
 * Symbolic names defining the electric behavior of the contactor
 */
typedef enum {
    CONT_FEEDBACK_NORMALLY_OPEN     = 0,    /*!< Feedback line of a contactor is normally open      */
    CONT_FEEDBACK_NORMALLY_CLOSED   = 1,    /*!< Feedback line of a contactor is normally closed    */
    CONT_HAS_NO_FEEDBACK    = 0xFF  /*!< Feedback line of the contactor is not used         */
} CONT_FEEDBACK_TYPE_e;

typedef struct {
    CONT_ELECTRICAL_STATE_TYPE_s set;
    CONT_ELECTRICAL_STATE_TYPE_s feedback;
} CONT_ELECTRICAL_STATE_s;

//TODO:pavi:need to remove
typedef enum {
    IO_PA_0     =   0
}IO_PORTS_e;

//TODO:pavi:need to remove
/**
 * symbolic names for pin state, where reset means low, and set means high
 */
typedef enum {
  IO_PIN_RESET  = 0,   /*!< Pin is set to low/0                */
  IO_PIN_SET    = 1,     /*!< Pin is set to high/1               */
  IO_PIN_DC     = 0,   /*!< default value for Pin (don't care) */
} IO_PIN_STATE_e;

typedef struct {
    IO_PORTS_e control_pin;
    IO_PORTS_e feedback_pin;
    CONT_FEEDBACK_TYPE_e feedback_pin_type;
} CONT_CONFIG_s;


typedef enum {
    CONT_POWERLINE_NORMAL,
    CONT_POWERLINE_CHARGE
} CONT_WHICH_POWERLINE_e;

extern const CONT_CONFIG_s cont_contactors_config[BS_NR_OF_CONTACTORS];
extern CONT_ELECTRICAL_STATE_s cont_contactor_states[BS_NR_OF_CONTACTORS];

/*================== Function Prototypes ==================================*/

/**
 * @brief   Checks if the current limitations are violated
 *
 * @return  E_OK if the current limitations are NOT violated, else E_NOT_OK (type: STD_RETURN_TYPE_e)
 */
extern e_STD_RETURN_TYPE CONT_CheckPrecharge(CONT_WHICH_POWERLINE_e caller);


/**
 * Function to check fuse state. Fuse state can only be checked if at least
 * plus contactors are closed. Furthermore fuse needs to be placed in plus
 * path and monitored by Isabellenhuette HV measurement
 *
 * @return  Returns E_OK if fuse is intact, and E_NOT_OK if fuse is tripped.
 *
 */
extern e_STD_RETURN_TYPE CONT_CheckFuse(CONT_WHICH_POWERLINE_e caller);

/*================== Function Implementations =============================*/


#endif /* CONTACTOR_CFG_H_ */
