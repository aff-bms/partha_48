/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name: watch dog                                                                                                        */
/* Author:Pavithran                                                                                                                     */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file watch_dog.c
*   @brief This file contains API's which are required to test watch dog peripheral functionality.
*
*   @details watch dog .
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Pavithran
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 15-07-2021
*/

/** @defgroup event log
*
* This file contains API's which are required to test watch dog peripheral functionality.
*
*/

/**@{*/
/********************************************************************************************************************************/
/* Include Section                                                                                                              */
/********************************************************************************************************************************/
#include "mcc_generated_files/pin_manager.h"
#include "mcc_generated_files/watchdog.h"
#include "common.h"

/** @fn       void Watcg_Dog_Init(void)
*   @brief    function to initialize watch dog peripheral 
*
*   @details  function to initialize watch dog peripheral 
*
*   @pre      system init
*   @post     
*   @param    void
*   @return   void
*/
void Watch_Dog_Init(void)
{
    //FWDT
    WDTCONLbits.RUNDIV = 12;    //Run Mode Watchdog Timer Post Scaler select bits->1:4096
    WDTCONLbits.CLKSEL = 3;    //Watchdog Timer Clock Select bits->Always use LPRC
    WDTCONLbits.WDTWINEN = 0;    //Watchdog Timer Window Enable bit->Watchdog Timer in Non-Window mode
    //WDTCONLbits.WDTWIN = 25;  //Watchdog Timer Window Select bits->WDT Window is 25% of WDT period
    WDTCONLbits.SLPDIV = 1;    //Sleep Mode Watchdog Timer Post Scaler select bits->1:1
    WDTCONLbits.ON = 0;    //Watchdog Timer Enable bit->WDT controlled via SW, use WDTCON.ON bit
}
/** @fn       void Watch_Dog_Test_Code(void)
*   @brief    function to test watch dog peripheral functionality
*
*   @details  function to test watch dog peripheral functionality
*
*   @pre      watch dog initialization
*   @post     
*   @param    void
*   @return   void
*/
void Watch_Dog_Test_Code(void)
{
    //Watch_Dog_Init();
    
    WATCHDOG_TimerSoftwareEnable();    
    
    /*printf("\nbefore while 1\n");
    MCU_WaitMs(1000);
    
   while(1)
   {      
        IO_RE0_Toggle();
        printf("in while 1\n");
        MCU_WaitMs(1000);
        
        //WATCHDOG_TimerClear();
    }*/
}

/********************************************************************************************************************************/
/* End of file                                                                                                                  */
/********************************************************************************************************************************/
/**@}*/