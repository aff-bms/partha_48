/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name:flash                                                                                                          */
/* Author:Pavithran                                                                                                                     */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file CMU.c
*   @brief This file contains API's which are required to communicate with flash memory
*
*   @details Flash Application Driver.
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Pavithran
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 20-07-2021
*/

/** @defgroup flash
*
* This file contains API's which are required to communicate with flash memory
*
*/

/**@{*/
/********************************************************************************************************************************/
/* Include Section                                                                                                              */
/********************************************************************************************************************************/
#include "mcc_generated_files/memory/flash.h"
#include "flash_app.h"

/** @fn       void Flash_Write(u32 start_address,u8 *pData,u32 len,u8 write_type)
*   @brief    function to log an event in flash memory
*
*   @details  function to write data in flash memory
*
*   @pre      flash initialization
*   @post     
*   @param    start_address start address to write data
*   @param    pData pointer to source buffer
*   @param    len no of bytes to write
*   @param    write_type double word 16 or 24 bit write format
*   @return   void
*/
u8 Flash_Write(u32 u32_start_address,u8 *pData,u32 u32_len,u8 u8_write_type)
{
    u8 u8_status=0;
    
    u32 u32_counter=0,u32_word1=0,u32_word2=0,u32_loop=0;
      
    if((pData==NULL) || (u32_len==0))
    {
        return 1;
    }
    
    /*word allignment to avoid accessing invalid locations*/
    u8 u8_buf[u32_len+5],*pData_temp=NULL;
    pData_temp=u8_buf;
    memset(pData_temp,0xFF,sizeof(u8_buf));
    memcpy(pData_temp,pData,u32_len);
    
    /*unlocking flash memory*/
    FLASH_Unlock(FLASH_UNLOCK_KEY);
    
    /*calculating no of loops required to write data based on length of the data to write*/
    if(u8_write_type==0)
    {
        u32_loop=(u32_len/4);
        if(u32_len%4)
        {
            u32_loop=u32_loop+1;
        }
    }
    else
    {
        u32_loop=(u32_len/6);
        if(u32_len%6)
        {
            u32_loop=u32_loop+1;
        }
    }
           
    for(u32_counter=0;u32_counter<u32_loop;u32_counter++)
    {
        /*erasing flash memory page if it is starting address of the page */
        if(u32_start_address%2048==0)
        {
            FLASH_ErasePage(u32_start_address);
        }
        
        /*if it is write double word 16 bit format*/
        if(u8_write_type==0)
        {   
            /*merging buffer u8 data to u16 byte format*/
            u32_word1=0;
            u32_word1=pData_temp[0];
            u32_word1|=(u16)pData_temp[1]<<8;
            
            /*merging buffer u8 data to u16 byte format*/
            u32_word2=0;
            u32_word2 =pData_temp[2];
            u32_word2|=(u16)pData_temp[3]<<8;
            
            /*writing double word 16 bit data in flash memory*/
            FLASH_WriteDoubleWord16(u32_start_address,u32_word1,u32_word2);
            
             /*incrementing offset address*/
            u32_start_address=u32_start_address+4;
            
             /*incrementing pointer address*/
            pData_temp=pData_temp+4;
        }
        else/*if it is write double word 24 bit format*/
        {
            /*merging buffer u8 data to u32 byte format*/
            u32_word1=0;
            u32_word1=pData_temp[0];
            u32_word1|=(u16)pData_temp[1]<<8;
            u32_word1|=(u32)pData_temp[2]<<16;
            
            /*merging buffer u8 data to u32 byte format*/
            u32_word2=0;
            u32_word2 =pData_temp[3];
            u32_word2|=(u16)pData_temp[4]<<8;
            u32_word2|=(u32)pData_temp[5]<<16;
            
            /*writing double word 24 bit data in flash memory*/
            FLASH_WriteDoubleWord24(u32_start_address,u32_word1,u32_word2);
            
            /*incrementing offset address*/
            u32_start_address=u32_start_address+4;
            
            /*incrementing pointer address*/
            pData_temp=pData_temp+6;
        }
    }
    
     /*locking flash memory*/
     FLASH_Lock();
     
     return u8_status;
}

/** @fn       u8 Flash_Read(u32 u32_start_address,u8 *pData,u32 u32_len,const u8 u8_read_type)
*   @brief    function to read data from flash memory
*
*   @details  function to read data from flash memory
*
*   @pre      flash initialization
*   @post     
*   @param    start_address start address to write data
*   @param    pData pointer to source buffer
*   @param    len no of bytes to read
*   @param    write_type double word 16 or 24 bit
*   @return   void
*/
u8 Flash_Read(u32 u32_start_address,u8 *pData,u32 u32_len,const u8 u8_read_type)
{
    u8 u8_status=0;
    
    u32 u32_counter=0,u32_word=0,u32_loop=0;
    
    if((pData==NULL) || (u32_len==0))
    {
        return 1;
    }
    
    /*word allignment to avoid accessing invalid locations*/
    u8 u8_buf[u32_len+5],*pData_temp=NULL;
    pData_temp=u8_buf;
      
    /*calculating no of loops required to read data from flash based on length of the data*/
    if(u8_read_type==0)
    {      
        u32_loop=(u32_len/2);
        if(u32_len%2)
        {
            u32_loop=u32_loop+1;
        }
    }
    else
    {
        u32_loop=(u32_len/3);
        if(u32_len%3)
        {
            u32_loop=u32_loop+1;
        }
    }
    
    for(u32_counter=0;u32_counter<u32_loop;u32_counter++)
    {
        /*if it is read word 16 bit format*/
        if(u8_read_type==0)
        {
          /*reading 16 bit single word data from flash memory*/
          u32_word=FLASH_ReadWord16(u32_start_address); 
          
          /*masking 16 bit word data to 8 byte data to buffer*/
          pData_temp[0]=((u32_word>>0) & 0x000000FF);
          pData_temp[1]=((u32_word>>8) & 0x000000FF);
          
          /*incrementing flash offset address*/
          u32_start_address=u32_start_address+2;
          
           /*incrementing pointer offset*/
          pData_temp=pData_temp+2;
        }
        else/*if it is read word 24 bit format*/
        {
          /*reading 24 bit single word data from flash memory*/
          u32_word=FLASH_ReadWord24(u32_start_address); 
          
          /*masking 24 bit word data to 8 byte data to buffer*/
          pData_temp[0]=((u32_word>>0) & 0x000000FF);
          pData_temp[1]=((u32_word>>8) & 0x000000FF);
          pData_temp[2]=((u32_word>>16) & 0x000000FF);
          
          /*incrementing flash offset address*/
          u32_start_address=u32_start_address+2;
          
          /*incrementing pointer offset*/
          pData_temp=pData_temp+3;
        }
    }
    
    /*copying data from temporary buffer*/
    memcpy(pData,u8_buf,u32_len);
    
    return u8_status;
}

/********************************************************************************************************************************/
/* End of file                                                                                                                  */
/********************************************************************************************************************************/
/**@}*/