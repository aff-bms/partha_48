/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name:database                                                                                                          */
/* Author:Pavithran                                                                                                                     */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file database.c
*   @brief This file contains API's which are required to set and get the data.
*
*   @details database
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Pavithran
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 22-06-2021
*/

/** @defgroup database
*
* database for maintaining all blocks of data
*
*/
/**@{*/
/********************************************************************************************************************************/
/* Include Section                                                                                                              */
/********************************************************************************************************************************/
#include "database.h"
#include "cmu_reg.h"

/********************************************************************************************************************************/
/* Define Section - GLOBAL                                                                                                      */
/********************************************************************************************************************************/
/*structure for storing battery info*/
static DATA_BLOCK_CELLVOLTAGE_s bms_tab_cellvolt;
static DATA_BLOCK_MINMAX_s bms_tab_minmax;
static DATA_BLOCK_BALANCING_CONTROL_s bal_balancing;
static DATA_BLOCK_CURRENT_SENSOR_s bms_tab_cur_sensor;
static DATA_BLOCK_SYSTEMSTATE_s systemstate = {0};
static DATA_BLOCK_ERRORSTATE_s error_flags;
static DATA_BLOCK_MSL_FLAG_s msl_flags;
static DATA_BLOCK_STATEREQUEST_s state_request;
static DATA_BLOCK_ERRORSTATE_s error_flags = { 0 };
static DATA_BLOCK_MOL_FLAG_s mol_flags = { 0 };
static DATA_BLOCK_RSL_FLAG_s rsl_flags = { 0 };
static DATA_BLOCK_MSL_FLAG_s msl_flags = { 0 };

/********************************************************************************************************************************/
/* Function Implementations Section - GLOBAL                                                                                    */
/********************************************************************************************************************************/
/** @fn       void CMU_Process_ADC_Data(void)
*   @brief    for storing all blocks of data in database
*
*
*   @details  for storing all blocks of data in database 
*
*   @pre      CMU Initiaiization
*   @post     
*   @param    dataptrfromSender Source block data
*   @param    blockID block id of source block data
*   @return   void
*/
void DB_WriteBlock(void *dataptrfromSender, DATA_BLOCK_ID_TYPE_e  blockID)
{
	switch(blockID)
	{
        case DATA_BLOCK_ID_CELLVOLTAGE:
            memcpy(&bms_tab_cellvolt,(DATA_BLOCK_CELLVOLTAGE_s *)dataptrfromSender,sizeof(bms_tab_cellvolt));
            break;
            
		case DATA_BLOCK_ID_BALANCING_CONTROL_VALUES:
			memcpy(&bal_balancing,(DATA_BLOCK_BALANCING_CONTROL_s *)dataptrfromSender,sizeof(bal_balancing));
			break;

		case DATA_BLOCK_ID_SYSTEMSTATE:
			memcpy(&systemstate,(DATA_BLOCK_SYSTEMSTATE_s *)dataptrfromSender,sizeof(systemstate));
			break;

		case DATA_BLOCK_ID_STATEREQUEST:
			memcpy(&state_request,(DATA_BLOCK_STATEREQUEST_s *)dataptrfromSender,sizeof(state_request));
			break;

		case DATA_BLOCK_ID_ERRORSTATE:
			memcpy(&error_flags,(DATA_BLOCK_STATEREQUEST_s *)dataptrfromSender,sizeof(error_flags));
			break;

		case DATA_BLOCK_ID_MOL:
			memcpy(&mol_flags,(DATA_BLOCK_STATEREQUEST_s *)dataptrfromSender,sizeof(mol_flags));
			break;

		case DATA_BLOCK_ID_RSL:
			memcpy(&rsl_flags,(DATA_BLOCK_STATEREQUEST_s *)dataptrfromSender,sizeof(rsl_flags));
			break;

		case DATA_BLOCK_ID_MSL:
			memcpy(&msl_flags,(DATA_BLOCK_STATEREQUEST_s *)dataptrfromSender,sizeof(msl_flags));
			break;
            
        default: 
            break;
	}
}

/** @fn       void CMU_Process_ADC_Data(void)
*   @brief    for reading all blocks of data from database
*
*
*   @details  for processing adc data 
*
*   @pre      for storing all blocks of data in database
*   @post     
*   @param    dataptrfromSender destination block of data
*   @param    blockID block id of block data
*   @return   void
*/
void DB_ReadBlock(void *dataptrtoReceiver, DATA_BLOCK_ID_TYPE_e  blockID)
{
	switch(blockID)
	{
		case DATA_BLOCK_ID_CELLVOLTAGE:
			memcpy((DATA_BLOCK_CELLVOLTAGE_s *)dataptrtoReceiver,&bms_tab_cellvolt,sizeof(bms_tab_cellvolt));
			break;

		case DATA_BLOCK_ID_MINMAX:
			memcpy((DATA_BLOCK_MINMAX_s *)dataptrtoReceiver,&bms_tab_minmax,sizeof(bms_tab_minmax));
			break;

		case DATA_BLOCK_ID_BALANCING_CONTROL_VALUES:
			memcpy((DATA_BLOCK_BALANCING_CONTROL_s *)dataptrtoReceiver,&bal_balancing,sizeof(bal_balancing));
			break;

		case DATA_BLOCK_ID_CURRENT_SENSOR:
			memcpy((DATA_BLOCK_CURRENT_SENSOR_s *)dataptrtoReceiver,&bms_tab_cur_sensor,sizeof(bms_tab_cur_sensor));
			break;

		case DATA_BLOCK_ID_SYSTEMSTATE:
			memcpy((DATA_BLOCK_SYSTEMSTATE_s *)dataptrtoReceiver,&systemstate,sizeof(systemstate));
			break;

		case DATA_BLOCK_ID_STATEREQUEST:
			memcpy((DATA_BLOCK_STATEREQUEST_s *)dataptrtoReceiver,&state_request,sizeof(state_request));
			break;

		case DATA_BLOCK_ID_ERRORSTATE:
			memcpy((DATA_BLOCK_ERRORSTATE_s *)dataptrtoReceiver,&error_flags,sizeof(error_flags));
			break;

		case DATA_BLOCK_ID_MOL:
			memcpy((DATA_BLOCK_MOL_FLAG_s *)dataptrtoReceiver,&mol_flags,sizeof(mol_flags));
			break;

		case DATA_BLOCK_ID_RSL:
			memcpy((DATA_BLOCK_RSL_FLAG_s *)dataptrtoReceiver,&rsl_flags,sizeof(rsl_flags));
			break;

		case DATA_BLOCK_ID_MSL:
			memcpy((DATA_BLOCK_MSL_FLAG_s *)dataptrtoReceiver,&msl_flags,sizeof(msl_flags));
			break;
           
        default: 
            break;
	}
}
/********************************************************************************************************************************/
/* End of file                                                                                                                  */
/********************************************************************************************************************************/
/**@}*/
