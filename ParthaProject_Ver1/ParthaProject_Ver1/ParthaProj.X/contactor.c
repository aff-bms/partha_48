/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name:Driver for the contactors                                                                                                       */
/* Author:Pavithran                                                                                                                     */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file contactor.c
*   @brief This file contains Driver for the contactors
*
*   @details Driver for the contactors
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Pavithran
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 22-06-2021
*/

/** @defgroup CMU
*
* This file contains bms driver header
*
*/

/**@{*/
/********************************************************************************************************************************/
/* Include Section                                                                                                              */
/********************************************************************************************************************************/
#include "contactor.h"
#include "bms.h"
#include "database.h"
#include "FreeRTOS.h"
#include "task.h"
#include "common.h"
#include "event_log.h"


#if BUILD_MODULE_ENABLE_CONTACTOR == 1
/*================== Macros and Definitions ===============================*/

/**
 * used to locally copy the current-sensor value from the global database
 * current table
 */
static DATA_BLOCK_CURRENT_SENSOR_s cont_current_tab = {0};

/**
 * Saves the last state and the last substate
 */
#define CONT_SAVELASTSTATES()   cont_state.laststate = cont_state.state; \
                                cont_state.lastsubstate = cont_state.substate;

#define CONT_OPENALLCONTACTORS()   CONT_SwitchAllContactorsOff();

#define CONT_OPENPLUS()       CONT_SetContactorState(CONT_MAIN_PLUS, CONT_SWITCH_OFF);
#define CONT_CLOSEPLUS()       CONT_SetContactorState(CONT_MAIN_PLUS, CONT_SWITCH_ON);

#define CONT_OPENMINUS()       CONT_SetContactorState(CONT_MAIN_MINUS, CONT_SWITCH_OFF);
#define CONT_CLOSEMINUS()       CONT_SetContactorState(CONT_MAIN_MINUS, CONT_SWITCH_ON);

#define CONT_OPENPRECHARGE()       CONT_SetContactorState(CONT_PRECHARGE_PLUS, CONT_SWITCH_OFF);
#define CONT_CLOSEPRECHARGE()       CONT_SetContactorState(CONT_PRECHARGE_PLUS, CONT_SWITCH_ON);

#if BS_SEPARATE_POWERLINES == 1
#define CONT_OPENCHARGEMINUS()       CONT_SetContactorState(CONT_CHARGE_MAIN_MINUS, CONT_SWITCH_OFF);
#define CONT_CLOSECHARGEMINUS()       CONT_SetContactorState(CONT_CHARGE_MAIN_MINUS, CONT_SWITCH_ON);

#define CONT_OPENCHARGEPLUS()       CONT_SetContactorState(CONT_MAIN_PLUS, CONT_SWITCH_OFF);
#define CONT_CLOSECHARGEPLUS()       CONT_SetContactorState(CONT_MAIN_PLUS, CONT_SWITCH_ON);

#define CONT_OPENCHARGEPRECHARGE()       CONT_SetContactorState(CONT_CHARGE_PRECHARGE_PLUS, CONT_SWITCH_OFF);
#define CONT_CLOSECHARGEPRECHARGE()       CONT_SetContactorState(CONT_CHARGE_PRECHARGE_PLUS, CONT_SWITCH_ON);
#endif /* BS_SEPARATE_POWERLINES == 1 */

/*================== Constant and Variable Definitions ====================*/

/**
 * contains the state of the contactor state machine
 *
 */
static CONT_STATE_s cont_state = {
    .timer                  = 0,
    .statereq               = CONT_STATE_INIT_REQUEST/*CONT_STATE_NO_REQUEST*/,
    .state                  = CONT_STATEMACH_UNINITIALIZED,
    .substate               = CONT_ENTRY,
    .laststate              = CONT_STATEMACH_UNINITIALIZED,
    .lastsubstate           = 0,
    .triggerentry           = 0,
    .ErrRequestCounter      = 0,
    .initFinished           = E_NOT_OK,
    .OscillationCounter     = 0,
    .PrechargeTryCounter    = 0,
    .PrechargeTimeOut       = 0,
    .counter                = 0,
    .activePowerLine        = CONT_POWER_LINE_NONE,
};

static DATA_BLOCK_CONTFEEDBACK_s contfeedback_tab = {
        .contactor_feedback = 0,
        .timestamp = 0,
        .previous_timestamp = 0,
};


/*================== Function Prototypes ==================================*/

static CONT_RETURN_TYPE_e CONT_CheckStateRequest(CONT_STATE_REQUEST_e statereq);
static CONT_STATE_REQUEST_e CONT_GetStateRequest(void);
static CONT_STATE_REQUEST_e CONT_TransferStateRequest(void);
static uint8_t CONT_CheckReEntrance(void);
static void CONT_CheckFeedback(void);

/*================== Function Implementations =============================*/

CONT_ELECTRICAL_STATE_TYPE_s CONT_GetContactorSetValue(CONT_NAMES_e contactor) 
{
    CONT_ELECTRICAL_STATE_TYPE_s contactorSetInformation = false;
    taskENTER_CRITICAL();
    contactorSetInformation = cont_contactor_states[contactor].set;
    taskEXIT_CRITICAL();
    return contactorSetInformation;
}


CONT_ELECTRICAL_STATE_TYPE_s CONT_GetContactorFeedback(CONT_NAMES_e contactor) 
{
    CONT_ELECTRICAL_STATE_TYPE_s measuredContactorState = CONT_SWITCH_UNDEF;
    if (CONT_HAS_NO_FEEDBACK == cont_contactors_config[contactor].feedback_pin_type) 
    {
            measuredContactorState = cont_contactor_states[contactor].set;
    }
#if 0
    else 
    {
        /* the contactor has a feedback pin, but it has to be differenced if the feedback pin is normally open or normally closed */
        if (CONT_FEEDBACK_NORMALLY_OPEN == cont_contactors_config[contactor].feedback_pin_type) 
        {
            IO_PIN_STATE_e pinstate = IO_PIN_RESET;
            taskENTER_CRITICAL();
            pinstate = IO_ReadPin(cont_contactors_config[contactor].feedback_pin);
            taskEXIT_CRITICAL();
            if (IO_PIN_RESET == pinstate) 
            {
                measuredContactorState = CONT_SWITCH_ON;
            } else if (IO_PIN_SET == pinstate) 
            {
                measuredContactorState = CONT_SWITCH_OFF;
            } else 
            {
                measuredContactorState = CONT_SWITCH_UNDEF;
            }
        }
        if (CONT_FEEDBACK_NORMALLY_CLOSED == cont_contactors_config[contactor].feedback_pin_type) 
        {
            IO_PIN_STATE_e pinstate = IO_PIN_SET;
            taskENTER_CRITICAL();
            pinstate = IO_ReadPin(cont_contactors_config[contactor].feedback_pin);
            taskEXIT_CRITICAL();
            if (IO_PIN_SET == pinstate) 
            {
                measuredContactorState = CONT_SWITCH_ON;
            } else if (IO_PIN_RESET == pinstate) 
            {
                measuredContactorState = CONT_SWITCH_OFF;
            } else 
            {
                measuredContactorState = CONT_SWITCH_UNDEF;
            }
        }
    }
#endif
    cont_contactor_states[contactor].feedback = measuredContactorState;
    return measuredContactorState;
}


e_STD_RETURN_TYPE CONT_AcquireContactorFeedbacks(void) 
{
    e_STD_RETURN_TYPE retVal = E_NOT_OK;
    uint8_t i = 0;
    taskENTER_CRITICAL();
    for (i = 0; i < BS_NR_OF_CONTACTORS; i++) 
    {
        cont_contactor_states[i].feedback = CONT_GetContactorFeedback(i);
    }
    retVal = E_OK;
    taskEXIT_CRITICAL();
    return retVal;
}

e_STD_RETURN_TYPE CONT_SetContactorState(CONT_NAMES_e contactor, CONT_ELECTRICAL_STATE_TYPE_s requestedContactorState) {
    e_STD_RETURN_TYPE retVal = E_OK;

    if (requestedContactorState  ==  CONT_SWITCH_ON) 
    {
        cont_contactor_states[contactor].set = CONT_SWITCH_ON;
        //IO_WritePin(cont_contactors_config[contactor].control_pin, IO_PIN_SET);
        //PDC_HsdGpioOn(contactor);
        //if (DIAG_HANDLER_RETURN_OK != DIAG_ContHandler(EVENT_NOK, (uint8_t) contactor, NULL)) {
            /* TODO: explain why empty if */
        //}
    } 
    else if (requestedContactorState  ==  CONT_SWITCH_OFF) 
    {
        DB_ReadBlock(&cont_current_tab, DATA_BLOCK_ID_CURRENT_SENSOR);
        float currentAtSwitchOff = cont_current_tab.current;
        if (((BAD_SWITCHOFF_CURRENT_POS < currentAtSwitchOff) && (0 < currentAtSwitchOff)) ||
             ((BAD_SWITCHOFF_CURRENT_NEG > currentAtSwitchOff) && (0 > currentAtSwitchOff))) 
        {
            //if (DIAG_HANDLER_RETURN_OK != DIAG_ContHandler(EVENT_NOK, (uint8_t) contactor, &currentAtSwitchOff)) {
                /* currently no error handling, just logging */
            //}
        } 
        else 
        {
            /*if (DIAG_HANDLER_RETURN_OK != DIAG_ContHandler(EVENT_NOK, (uint8_t) contactor, NULL)) 
            {
                // TODO: explain why empty if 
            }*/
        }
        cont_contactor_states[contactor].set = CONT_SWITCH_OFF;
        //IO_WritePin(cont_contactors_config[contactor].control_pin, IO_PIN_RESET);
        //PDC_HsdGpioOff(contactor);
    } 
    else 
    {
        retVal = E_NOT_OK;
    }

    return retVal;
}


e_STD_RETURN_TYPE CONT_SwitchAllContactorsOff(void) 
{
    e_STD_RETURN_TYPE retVal = E_NOT_OK;
    uint8_t i=0;
    uint8_t offCounter = 0;
    e_STD_RETURN_TYPE successfullSet = E_NOT_OK;

    for (i = 0; i < BS_NR_OF_CONTACTORS; i++) 
    {
        successfullSet = CONT_SetContactorState(i, CONT_SWITCH_OFF);
        if (E_OK == successfullSet) 
        {
            offCounter = offCounter + 1;
        }
        successfullSet = E_NOT_OK;
    }

    if (BS_NR_OF_CONTACTORS == offCounter) 
    {
        retVal = E_OK;
    } 
    else 
    {
        retVal = E_NOT_OK;
    }
    return retVal;
}



/**
 * @brief   re-entrance check of CONT state machine trigger function
 *
 * @details This function is not re-entrant and should only be called time- or event-triggered. It
 *          increments the triggerentry counter from the state variable ltc_state.
 *          It should never be called by two different processes, so if it is the case,
 *          triggerentry should never be higher than 0 when this function is called.
 *
 *
 * @return  0 if no further instance of the function is active, 0xff else
 *
 */
static uint8_t CONT_CheckReEntrance(void) 
{
    uint8_t retval = 0;

    taskENTER_CRITICAL();
    if (!cont_state.triggerentry) 
    {
        cont_state.triggerentry++;
    } else 
    {
        retval = 0xFF;
    }
    taskEXIT_CRITICAL();
    return retval;
}




/**
 * @brief   gets the current state request.
 *
 * @details This function is used in the functioning of the CONT state machine.
 *
 * @return  return the current pending state request
 */
static CONT_STATE_REQUEST_e CONT_GetStateRequest(void) 
{
    CONT_STATE_REQUEST_e retval = CONT_STATE_NO_REQUEST;

    taskENTER_CRITICAL();
    retval = cont_state.statereq;
    taskEXIT_CRITICAL();

    return retval;
}


CONT_STATEMACH_e CONT_GetState(void) 
{
    return (cont_state.state);
}

e_STD_RETURN_TYPE CONT_GetInitializationState(void) 
{
    return (cont_state.initFinished);
}

CONT_POWER_LINE_e CONT_GetActivePowerLine() 
{
    return (cont_state.activePowerLine);
}


/**
 * @brief   transfers the current state request to the state machine.
 *
 * @details This function takes the current state request from cont_state and transfers it to the
 *          state machine. It resets the value from cont_state to CONT_STATE_NO_REQUEST
 *
 * @return  current state request, taken from CONT_STATE_REQUEST_e
 *
 */
static CONT_STATE_REQUEST_e CONT_TransferStateRequest(void) 
{
    CONT_STATE_REQUEST_e retval = CONT_STATE_NO_REQUEST;

    taskENTER_CRITICAL();
    retval    = cont_state.statereq;
    cont_state.statereq = CONT_STATE_NO_REQUEST;
    taskEXIT_CRITICAL();

    return (retval);
}


CONT_RETURN_TYPE_e CONT_SetStateRequest(CONT_STATE_REQUEST_e statereq) 
{
    CONT_RETURN_TYPE_e retVal = CONT_STATE_NO_REQUEST;

    taskENTER_CRITICAL();
    retVal = CONT_CheckStateRequest(statereq);

    if (retVal == CONT_OK) 
    {
            cont_state.statereq   = statereq;
    }
    taskEXIT_CRITICAL();

    return retVal;
}



/**
 * @brief   checks the state requests that are made.
 *
 * @details This function checks the validity of the state requests. The results of the checked is
 *          returned immediately.
 *
 * @param   statereq    state request to be checked
 *
 * @return  result of the state request that was made, taken from (type: CONT_RETURN_TYPE_e)
 */
static CONT_RETURN_TYPE_e CONT_CheckStateRequest(CONT_STATE_REQUEST_e statereq) 
{
    if (statereq == CONT_STATE_ERROR_REQUEST) 
    {
        return CONT_OK;
    }

    if (cont_state.statereq == CONT_STATE_NO_REQUEST) 
    {
        /* init only allowed from the uninitialized state */
        if (statereq == CONT_STATE_INIT_REQUEST) 
        {
            if (cont_state.state == CONT_STATEMACH_UNINITIALIZED) 
            {
                return CONT_OK;
            } else 
            {
                return CONT_ALREADY_INITIALIZED;
            }
        }

        if ((statereq == CONT_STATE_STANDBY_REQUEST) || (statereq == CONT_STATE_NORMAL_REQUEST) || (statereq == CONT_STATE_CHARGE_REQUEST)) 
        {
            return CONT_OK;
        } else if (statereq == CONT_STATE_NORMAL_REQUEST) 
        {
            if (cont_state.state == CONT_STATEMACH_CHARGE_PRECHARGE || cont_state.state == CONT_STATEMACH_CHARGE) 
            {
                return CONT_REQUEST_IMPOSSIBLE;
            } else 
            {
                return CONT_OK;
            }
        } else if (statereq == CONT_STATE_CHARGE_REQUEST) 
        {
            if (cont_state.state == CONT_STATEMACH_PRECHARGE || cont_state.state == CONT_STATEMACH_NORMAL) 
            {
                return CONT_REQUEST_IMPOSSIBLE;
            } else 
            {
                return CONT_OK;
            }
        } else 
        {
            return CONT_ILLEGAL_REQUEST;
        }
    } else 
    {
        return CONT_REQUEST_PENDING;
    }
}

void CONT_Trigger(void) 
{
    e_STD_RETURN_TYPE retVal = E_OK;
    CONT_STATE_REQUEST_e statereq = CONT_STATE_NO_REQUEST;

   /*if (CONT_CheckReEntrance()) {
        return;
    }*/

    //DIAG_SysMonNotify(DIAG_SYSMON_CONT_ID, 0);  /* task is running, state = ok */

    if (cont_state.state != CONT_STATEMACH_UNINITIALIZED) {
        CONT_CheckFeedback();
    }

    if (cont_state.OscillationCounter > 0)
    {
		cont_state.OscillationCounter--;
    }

    if (cont_state.PrechargeTimeOut > 0)
    {
			if (cont_state.PrechargeTimeOut > CONT_TASK_CYCLE_CONTEXT_MS) {
				cont_state.PrechargeTimeOut -= CONT_TASK_CYCLE_CONTEXT_MS;
			} else {
				cont_state.PrechargeTimeOut = 0;
		}
    }

    if (cont_state.timer)
    {
			if (cont_state.timer > CONT_TASK_CYCLE_CONTEXT_MS) {
				cont_state.timer -= CONT_TASK_CYCLE_CONTEXT_MS;
			} else {
				cont_state.timer = 0;
			}
			if (cont_state.timer) {
				cont_state.triggerentry--;
				return;    // handle state machine only if timer has elapsed
			}
    }

    switch (cont_state.state) {
        /****************************UNINITIALIZED***********************************/
        case CONT_STATEMACH_UNINITIALIZED:
            /* waiting for Initialization Request */
            statereq = CONT_TransferStateRequest();
            if (statereq == CONT_STATE_INIT_REQUEST) {
                CONT_SAVELASTSTATES();
                cont_state.timer = CONT_STATEMACH_SHORTTIME_MS;
                cont_state.state = CONT_STATEMACH_INITIALIZATION;
                cont_state.substate = CONT_ENTRY;
            } else if (statereq == CONT_STATE_NO_REQUEST) {
                /* no actual request pending */
            } else {
                cont_state.ErrRequestCounter++;   /* illegal request pending */
            }
            break;


        /****************************INITIALIZATION**********************************/
        case CONT_STATEMACH_INITIALIZATION:

            CONT_SAVELASTSTATES();
            CONT_OPENALLCONTACTORS();

            cont_state.timer = CONT_STATEMACH_SHORTTIME_MS;
            cont_state.state = CONT_STATEMACH_INITIALIZED;
            cont_state.substate = CONT_ENTRY;

            break;

        /****************************INITIALIZED*************************************/
        case CONT_STATEMACH_INITIALIZED:
            CONT_SAVELASTSTATES();
            cont_state.timer = CONT_STATEMACH_SHORTTIME_MS;
            cont_state.state = CONT_STATEMACH_IDLE;
            cont_state.substate = CONT_ENTRY;
            break;

        /****************************IDLE*************************************/
        case CONT_STATEMACH_IDLE:
            CONT_SAVELASTSTATES();
            cont_state.initFinished = E_OK;
            cont_state.timer = CONT_STATEMACH_SHORTTIME_MS;
            cont_state.state = CONT_STATEMACH_STANDBY;
            //cont_state.substate = CONT_ENTRY;//pavi
            cont_state.substate = CONT_STANDBY;//pavi
            break;

        /****************************STANDBY*************************************/
        case CONT_STATEMACH_STANDBY:
            CONT_SAVELASTSTATES();

            /* first precharge process */
            if (cont_state.substate == CONT_ENTRY) 
            {
                cont_state.OscillationCounter = CONT_OSCILLATION_LIMIT;
                CONT_OPENPRECHARGE();
                #if BS_SEPARATE_POWERLINES == 1
                    CONT_OPENCHARGEPRECHARGE();
                #endif
                cont_state.activePowerLine = CONT_POWER_LINE_NONE;
                cont_state.timer = CONT_STATEMACH_SHORTTIME_MS;
                cont_state.substate = CONT_OPEN_SECOND_CONTACTOR_PLUS;
                break;
            } 
            else if (cont_state.substate == CONT_OPEN_FIRST_CONTACTOR) 
            {
                if (BMS_GetBatterySystemState() == BMS_DISCHARGING) 
                {
                    CONT_OPENPLUS();
                    #if BS_SEPARATE_POWERLINES == 1
                        CONT_OPENCHARGEPLUS();
                    #endif
                    cont_state.timer = CONT_DELAY_BETWEEN_OPENING_CONTACTORS_MS;
                    cont_state.substate = CONT_OPEN_SECOND_CONTACTOR_MINUS;
                }
                else
				{
                    CONT_OPENMINUS();
                    #if BS_SEPARATE_POWERLINES == 1
                        CONT_OPENCHARGEMINUS();
                    #endif
                    cont_state.timer = CONT_DELAY_BETWEEN_OPENING_CONTACTORS_MS;
                    cont_state.substate = CONT_OPEN_SECOND_CONTACTOR_PLUS;//pavi
				}
                break;
            } 
            else if (cont_state.substate == CONT_OPEN_SECOND_CONTACTOR_MINUS) 
            {
                CONT_OPENMINUS();
                #if BS_SEPARATE_POWERLINES == 1
                    CONT_OPENCHARGEMINUS();
                #endif
                cont_state.timer = CONT_DELAY_AFTER_OPENING_SECOND_CONTACTORS_MS;
                cont_state.substate = CONT_STANDBY;
                break;
            } 
            else if (cont_state.substate == CONT_OPEN_SECOND_CONTACTOR_PLUS) 
            {
                CONT_OPENPLUS();
                #if BS_SEPARATE_POWERLINES == 1
                    CONT_OPENCHARGEPLUS();
                #endif
                cont_state.timer = CONT_DELAY_AFTER_OPENING_SECOND_CONTACTORS_MS;
                cont_state.substate = CONT_STANDBY;
                break;
            } 
            else if (cont_state.substate == CONT_STANDBY) 
            {
                /* when process done, look for requests */
                statereq = CONT_TransferStateRequest();
                if (statereq == CONT_STATE_STANDBY_REQUEST) 
                {
                    /* we stay already in requested state, nothing to do */
                } 
                else if (statereq == CONT_STATE_NORMAL_REQUEST) 
                {
                    CONT_SAVELASTSTATES();
                    cont_state.timer = CONT_STATEMACH_SHORTTIME_MS;
                    cont_state.state = CONT_STATEMACH_PRECHARGE;
                    cont_state.substate = CONT_ENTRY;
                }
#if BS_SEPARATE_POWERLINES == 1
                else if (statereq == CONT_STATE_CHARGE_REQUEST) 
                { /* NOLINT(readability/braces) */
                    CONT_SAVELASTSTATES();
                    cont_state.timer = CONT_STATEMACH_SHORTTIME_MS;
                    cont_state.state = CONT_STATEMACH_CHARGE_PRECHARGE;
                    cont_state.substate = CONT_ENTRY;
                }
#endif /* BS_SEPARATE_POWERLINES == 1 */
                else if (statereq == CONT_STATE_ERROR_REQUEST) 
                { /* NOLINT(readability/braces) */
                    CONT_SAVELASTSTATES();
                    cont_state.timer = CONT_STATEMACH_SHORTTIME_MS;
                    cont_state.state = CONT_STATEMACH_ERROR;
                    cont_state.substate = CONT_ENTRY;
                } else if (statereq == CONT_STATE_NO_REQUEST) 
                {
                    /* no actual request pending */
                } 
                else 
                {
                    cont_state.ErrRequestCounter++;  /* illegal request pending */
                }

                /* check fuse state */
                CONT_CheckFuse(CONT_POWERLINE_NORMAL);
                break;
            }
            break;

        /****************************PRECHARGE*************************************/
        case CONT_STATEMACH_PRECHARGE:
            CONT_SAVELASTSTATES();
            /*  check state requests */
            statereq = CONT_TransferStateRequest();
            if (statereq == CONT_STATE_ERROR_REQUEST) 
            {
                CONT_SAVELASTSTATES();
                cont_state.timer = CONT_STATEMACH_SHORTTIME_MS;
                cont_state.state = CONT_STATEMACH_ERROR;
                cont_state.substate = CONT_ENTRY;
                break;
            }
            if (statereq == CONT_STATE_STANDBY_REQUEST) 
            {
                CONT_SAVELASTSTATES();
                cont_state.timer = CONT_STATEMACH_SHORTTIME_MS;
                cont_state.state = CONT_STATEMACH_STANDBY;
                cont_state.substate = CONT_ENTRY;
                break;
            }

            /* precharge process, can be interrupted anytime by the requests above */
            if (cont_state.substate == CONT_ENTRY) 
            {
                if (cont_state.OscillationCounter > 0) 
                {
                    cont_state.timer = CONT_STATEMACH_SHORTTIME_MS;
                    break;
                } else 
                {
                    cont_state.PrechargeTryCounter = 0;
                    cont_state.timer = CONT_STATEMACH_SHORTTIME_MS;
                    //cont_state.substate = CONT_PRECHARGE_CLOSE_MINUS;//pavi
                    cont_state.substate = CONT_PRECHARGE_CHECK_VOLTAGES;//pavi
                    break;
                }
            } 
            else if (cont_state.substate == CONT_PRECHARGE_CLOSE_MINUS) 
            {
                cont_state.PrechargeTryCounter++;
                cont_state.PrechargeTimeOut = CONT_PRECHARGE_TIMEOUT_MS;
                CONT_CLOSEMINUS();
                cont_state.timer = CONT_STATEMACH_WAIT_AFTER_CLOSING_MINUS_MS;
                cont_state.substate = CONT_PRECHARGE_CLOSE_PRECHARGE;
                break;
            } else if (cont_state.substate == CONT_PRECHARGE_CLOSE_PRECHARGE) 
            {
                CONT_CLOSEPRECHARGE();
                cont_state.timer = CONT_STATEMACH_WAIT_AFTER_CLOSING_PRECHARGE_MS;
                cont_state.substate = CONT_PRECHARGE_CHECK_VOLTAGES;
                break;
            } else if (cont_state.substate == CONT_PRECHARGE_CHECK_VOLTAGES) 
            {
                retVal = CONT_CheckPrecharge(CONT_POWERLINE_NORMAL);
                retVal=E_OK;//pavi
                if (retVal == E_OK) {
                    CONT_CLOSEPLUS();
                    cont_state.timer = CONT_STATEMACH_WAIT_AFTER_CLOSING_PLUS_MS;
                    //cont_state.substate = CONT_PRECHARGE_OPEN_PRECHARGE;//pavi
                    cont_state.state = CONT_STATEMACH_NORMAL;//pavi
                    cont_state.substate = CONT_ENTRY;//pavi
                    break;
                } 
                else if (cont_state.PrechargeTimeOut > 0) 
                {
                    break;
                } else 
                {
                    if (cont_state.PrechargeTryCounter < CONT_PRECHARGE_TRIES) 
                    {
                        CONT_OPENALLCONTACTORS();
                        cont_state.timer = CONT_STATEMACH_TIMEAFTERPRECHARGEFAIL_MS;
                        cont_state.substate = CONT_PRECHARGE_CLOSE_MINUS;
                        break;
                    } else 
                    {
                        cont_state.timer = CONT_STATEMACH_SHORTTIME_MS;
                        cont_state.state = CONT_STATEMACH_ERROR;
                        cont_state.substate = CONT_ENTRY;
                        break;
                    }
                }
            } 
            else if (cont_state.substate == CONT_PRECHARGE_OPEN_PRECHARGE) 
            {
                CONT_OPENPRECHARGE();
                cont_state.timer = CONT_STATEMACH_WAIT_AFTER_OPENING_PRECHARGE_MS;
                cont_state.state = CONT_STATEMACH_NORMAL;
                cont_state.substate = CONT_ENTRY;
                cont_state.activePowerLine = CONT_POWER_LINE_0;
                break;
            }

            break;

        /****************************NORMAL*************************************/
        case CONT_STATEMACH_NORMAL:
            CONT_SAVELASTSTATES();
            statereq = CONT_TransferStateRequest();
            if (statereq == CONT_STATE_ERROR_REQUEST) 
            {
                CONT_SAVELASTSTATES();
                cont_state.timer = CONT_STATEMACH_SHORTTIME_MS;
                cont_state.state = CONT_STATEMACH_ERROR;
                cont_state.substate = CONT_ENTRY;
                break;
            }
            if (statereq == CONT_STATE_STANDBY_REQUEST) 
            {
                CONT_SAVELASTSTATES();
                cont_state.timer = CONT_STATEMACH_SHORTTIME_MS;
                cont_state.state = CONT_STATEMACH_STANDBY;
                cont_state.substate = CONT_ENTRY;
                break;
            }

            /* check fuse state */
            CONT_CheckFuse(CONT_POWERLINE_NORMAL);
            break;

#if BS_SEPARATE_POWERLINES == 1
        /****************************CHARGE_PRECHARGE*************************************/
        case CONT_STATEMACH_CHARGE_PRECHARGE:
            CONT_SAVELASTSTATES();

            /* check state requests */
            statereq = CONT_TransferStateRequest();
            if (statereq == CONT_STATE_ERROR_REQUEST) 
            {
                CONT_SAVELASTSTATES();
                cont_state.timer = CONT_STATEMACH_SHORTTIME_MS;
                cont_state.state = CONT_STATEMACH_ERROR;
                cont_state.substate = CONT_ENTRY;
                break;
            }
            if (statereq == CONT_STATE_STANDBY_REQUEST) 
            {
                CONT_SAVELASTSTATES();
                cont_state.timer = CONT_STATEMACH_SHORTTIME_MS;
                cont_state.state = CONT_STATEMACH_STANDBY;
                cont_state.substate = CONT_ENTRY;
                break;
            }

            /* precharge process, can be interrupted anytime by the requests above */
            if (cont_state.substate == CONT_ENTRY) 
            {
                if (cont_state.OscillationCounter > 0) 
                {
                    cont_state.timer = CONT_STATEMACH_SHORTTIME_MS;
                    break;
                } 
                else 
                {
                    cont_state.PrechargeTryCounter = 0;
                    cont_state.timer = CONT_STATEMACH_SHORTTIME_MS;
                    cont_state.substate=CONT_PRECHARGE_CHECK_VOLTAGES;
                    break;
                }
            } 
            else if (cont_state.substate == CONT_PRECHARGE_CLOSE_MINUS) 
            {
                cont_state.PrechargeTryCounter++;
                cont_state.PrechargeTimeOut = CONT_CHARGE_PRECHARGE_TIMEOUT_MS;
                CONT_CLOSECHARGEMINUS();
                cont_state.timer = CONT_STATEMACH_CHARGE_WAIT_AFTER_CLOSING_MINUS_MS;
                cont_state.substate = CONT_PRECHARGE_CLOSE_PRECHARGE;
                break;
            } 
            else if (cont_state.substate == CONT_PRECHARGE_CLOSE_PRECHARGE) 
            {
                CONT_CLOSECHARGEPRECHARGE();
                cont_state.timer = CONT_STATEMACH_CHARGE_WAIT_AFTER_CLOSING_PRECHARGE_MS;
                cont_state.substate = CONT_PRECHARGE_CHECK_VOLTAGES;
                break;
            } 
            else if (cont_state.substate == CONT_PRECHARGE_CHECK_VOLTAGES) 
            {
                retVal = CONT_CheckPrecharge(CONT_POWERLINE_CHARGE);
                retVal=E_OK;//pavi
                if (retVal == E_OK) {
                    CONT_CLOSECHARGEPLUS();
                    cont_state.timer = CONT_STATEMACH_CHARGE_WAIT_AFTER_CLOSING_PLUS_MS;
                    //cont_state.substate = CONT_PRECHARGE_OPEN_PRECHARGE;//pavi
                    cont_state.state = CONT_STATEMACH_CHARGE;//pavi
                    cont_state.substate = CONT_ENTRY;//pavi
                    break;
                } 
                else if (cont_state.PrechargeTimeOut > 0)
                {
                    break;
                } 
                else 
                {
                    if (cont_state.PrechargeTryCounter < CONT_PRECHARGE_TRIES) 
                    {
                        CONT_OPENALLCONTACTORS();
                        cont_state.timer = CONT_STATEMACH_TIMEAFTERPRECHARGEFAIL_MS;
                        cont_state.substate = CONT_PRECHARGE_CLOSE_MINUS;
                        break;
                    } 
                    else 
                    {
                        cont_state.timer = CONT_STATEMACH_SHORTTIME_MS;
                        cont_state.state = CONT_STATEMACH_ERROR;
                        cont_state.substate = CONT_ENTRY;
                        break;
                    }
                }
            } 
            else if (cont_state.substate == CONT_PRECHARGE_OPEN_PRECHARGE) 
            {
                CONT_OPENCHARGEPRECHARGE();
                cont_state.timer = CONT_STATEMACH_WAIT_AFTER_OPENING_PRECHARGE_MS;
                cont_state.state = CONT_STATEMACH_CHARGE;
                cont_state.substate = CONT_ENTRY;
                cont_state.activePowerLine = CONT_POWER_LINE_1;
                break;
            }
            break;

        /****************************CHARGE*************************************/
        case CONT_STATEMACH_CHARGE:
            CONT_SAVELASTSTATES();

            statereq = CONT_TransferStateRequest();
            if (statereq == CONT_STATE_ERROR_REQUEST) 
            {
                CONT_SAVELASTSTATES();
                cont_state.timer = CONT_STATEMACH_SHORTTIME_MS;
                cont_state.state = CONT_STATEMACH_ERROR;
                cont_state.substate = CONT_ENTRY;
                break;
            }
            if (statereq == CONT_STATE_STANDBY_REQUEST) 
            {
                CONT_SAVELASTSTATES();
                cont_state.timer = CONT_STATEMACH_SHORTTIME_MS;
                cont_state.state = CONT_STATEMACH_STANDBY;
                cont_state.substate = CONT_ENTRY;
                break;
            }

#endif /* BS_SEPARATE_POWERLINES == 1 */
            break;

        /****************************ERROR*************************************/
        case CONT_STATEMACH_ERROR:
            CONT_SAVELASTSTATES();

            /* first error process */
            if (cont_state.substate == CONT_ENTRY) 
            {
                cont_state.OscillationCounter = CONT_OSCILLATION_LIMIT;
                CONT_OPENPRECHARGE();
                #if BS_SEPARATE_POWERLINES == 1
                    CONT_OPENCHARGEPRECHARGE();
                #endif
                cont_state.timer = CONT_DELAY_BETWEEN_OPENING_CONTACTORS_MS;
                cont_state.substate = CONT_OPEN_FIRST_CONTACTOR;
                break;

            } 
            else if (cont_state.substate == CONT_OPEN_FIRST_CONTACTOR) 
            {
                if (BMS_GetBatterySystemState() == BMS_DISCHARGING)
                {
                    CONT_OPENPLUS();
                    #if BS_SEPARATE_POWERLINES == 1
                        CONT_OPENCHARGEPLUS();
                    #endif
                    cont_state.timer = CONT_DELAY_BETWEEN_OPENING_CONTACTORS_MS;
                    cont_state.substate = CONT_OPEN_SECOND_CONTACTOR_MINUS;
                } 
                else 
                {
                    CONT_OPENMINUS();
                    #if BS_SEPARATE_POWERLINES == 1
                        CONT_OPENCHARGEMINUS();
                    #endif
                    cont_state.timer = CONT_DELAY_BETWEEN_OPENING_CONTACTORS_MS;
                    cont_state.substate = CONT_OPEN_SECOND_CONTACTOR_PLUS;
                }
                /* mark no powerline as connected */
                cont_state.activePowerLine = CONT_POWER_LINE_NONE;
                break;

            } 
            else if (cont_state.substate == CONT_OPEN_SECOND_CONTACTOR_MINUS) 
            {
                CONT_OPENMINUS();
                #if BS_SEPARATE_POWERLINES == 1
                    CONT_OPENCHARGEMINUS();
                #endif
                cont_state.timer = CONT_DELAY_AFTER_OPENING_SECOND_CONTACTORS_MS;
                cont_state.substate = CONT_ERROR;
                break;

            } 
            else if (cont_state.substate == CONT_OPEN_SECOND_CONTACTOR_PLUS) 
            {
                CONT_OPENPLUS();
                #if BS_SEPARATE_POWERLINES == 1
                    CONT_OPENCHARGEPLUS();
                #endif
                cont_state.timer = CONT_DELAY_AFTER_OPENING_SECOND_CONTACTORS_MS;
                cont_state.substate = CONT_ERROR;
                break;

            } 
            else if (cont_state.substate == CONT_ERROR) 
            {
                /* Check if fuse is tripped */
                CONT_CheckFuse(CONT_POWERLINE_NORMAL);
                /* when process done, look for requests */
                statereq = CONT_TransferStateRequest();
                if (statereq == CONT_STATE_ERROR_REQUEST) 
                {
                    /* we stay already in requested state, nothing to do */
                } 
                else if (statereq == CONT_STATE_STANDBY_REQUEST) 
                {
                    CONT_SAVELASTSTATES();
                    cont_state.timer = CONT_STATEMACH_SHORTTIME_MS;
                    cont_state.state = CONT_STATEMACH_STANDBY;
                    cont_state.substate = CONT_ENTRY;
                } 
                else if (statereq == CONT_STATE_NO_REQUEST) 
                {
                    /* no actual request pending */
                } 
                else 
                {
                    cont_state.ErrRequestCounter++;   /* illegal request pending */
                }
            }
            break;

        default:
            break;
    }  /* end switch (cont_state.state) */

    cont_state.triggerentry--;
    cont_state.counter++;
}

/**
 * @brief   checks the feedback of the contactors
 *
 * @details makes a DIAG entry for each contactor when the feedback does not match the set value
 */
void CONT_CheckFeedback(void) 
{
    CONT_ELECTRICAL_STATE_TYPE_s feedback;
    uint8_t i=0;
    uint16_t contactor_feedback_state = 0;

    for (i = 0; i < BS_NR_OF_CONTACTORS; i++) 
    {
        feedback = CONT_GetContactorFeedback(i);

        switch (i) 
        {
            case CONT_MAIN_PLUS:
                contactor_feedback_state |= feedback << CONT_MAIN_PLUS;
                break;
            case CONT_MAIN_MINUS:
                contactor_feedback_state |= feedback << CONT_MAIN_MINUS;
                break;
            case CONT_PRECHARGE_PLUS:
                contactor_feedback_state |= feedback << CONT_PRECHARGE_PLUS;
                break;
#if BS_SEPARATE_POWERLINES == 1
            case CONT_CHARGE_MAIN_PLUS:
                contactor_feedback_state |= feedback << CONT_CHARGE_MAIN_PLUS;
                break;
            case CONT_CHARGE_MAIN_MINUS:
                contactor_feedback_state |= feedback << CONT_CHARGE_MAIN_MINUS;
                break;
            case CONT_CHARGE_PRECHARGE_PLUS:
                contactor_feedback_state |= feedback << CONT_CHARGE_PRECHARGE_PLUS;
                break;
#endif /* BS_SEPARATE_POWERLINES == 1 */
            default:
                break;
        }

        contfeedback_tab.contactor_feedback &= (~0x3F);
        contfeedback_tab.contactor_feedback |= contactor_feedback_state;

        if (feedback != CONT_GetContactorSetValue(i)) 
        {
            switch (i) {
                case CONT_MAIN_PLUS:
                    Event_Logging(EVENT_LOG_CONTACTOR_MAIN_PLUS_FEEDBACK, EVENT_NOK, 0);
                    break;
                case CONT_MAIN_MINUS:
                    Event_Logging(EVENT_LOG_CONTACTOR_MAIN_MINUS_FEEDBACK, EVENT_NOK, 0);
                    break;
                case CONT_PRECHARGE_PLUS:
                    Event_Logging(EVENT_LOG_CONTACTOR_PRECHARGE_FEEDBACK, EVENT_NOK, 0);
                    break;
#if BS_SEPARATE_POWERLINES == 1
                case CONT_CHARGE_MAIN_PLUS:
                    Event_Logging(EVENT_LOG_CONTACTOR_CHARGE_MAIN_PLUS_FEEDBACK, EVENT_NOK, 0);
                    break;
                case CONT_CHARGE_MAIN_MINUS:
                    Event_Logging(EVENT_LOG_CONTACTOR_CHARGE_MAIN_MINUS_FEEDBACK, EVENT_NOK, 0);
                    break;
                case CONT_CHARGE_PRECHARGE_PLUS:
                    Event_Logging(EVENT_LOG_CONTACTOR_CHARGE_PRECHARGE_FEEDBACK, EVENT_NOK, 0);
                    break;
#endif /* BS_SEPARATE_POWERLINES == 1 */
                default:
                    break;
            }
        } 
        else 
        {
            switch (i) 
            {
                case CONT_MAIN_PLUS:
                    Event_Logging(EVENT_LOG_CONTACTOR_MAIN_PLUS_FEEDBACK, EVENT_NOK, 0);
                    break;
                case CONT_MAIN_MINUS:
                    Event_Logging(EVENT_LOG_CONTACTOR_MAIN_MINUS_FEEDBACK, EVENT_NOK, 0);
                    break;
                case CONT_PRECHARGE_PLUS:
                    Event_Logging(EVENT_LOG_CONTACTOR_PRECHARGE_FEEDBACK, EVENT_NOK, 0);
                    break;
#if BS_SEPARATE_POWERLINES == 1
                case CONT_CHARGE_MAIN_PLUS:
                    Event_Logging(EVENT_LOG_CONTACTOR_CHARGE_MAIN_PLUS_FEEDBACK, EVENT_NOK, 0);
                    break;
                case CONT_CHARGE_MAIN_MINUS:
                    Event_Logging(EVENT_LOG_CONTACTOR_CHARGE_MAIN_MINUS_FEEDBACK, EVENT_NOK, 0);
                    break;
                case CONT_CHARGE_PRECHARGE_PLUS:
                    Event_Logging(EVENT_LOG_CONTACTOR_CHARGE_PRECHARGE_FEEDBACK, EVENT_NOK, 0);
                    break;
#endif /* BS_SEPARATE_POWERLINES == 1 */
                default:
                    break;
            }
        }
    }

    DB_WriteBlock(&contfeedback_tab, DATA_BLOCK_ID_CONTFEEDBACK);
}

#endif /* BUILD_MODULE_ENABLE_CONTACTOR */
