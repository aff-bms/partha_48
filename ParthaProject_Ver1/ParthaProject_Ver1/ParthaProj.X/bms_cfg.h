/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name:bms driver configuration header                                                                                                       */
/* Author:Pavithran                                                                                                                     */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file bms_cfg.h
*   @brief This file contains bms driver configuration header
*
*   @details bms driver configuration header
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Pavithran
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 22-06-2021
*/

/** @defgroup bms
*
* This file contains bms driver configuration header
*
*/

/**@{*/
/********************************************************************************************************************************/
/* Include Section                                                                                                              */
/********************************************************************************************************************************/
#ifndef BMS_CFG_H_
#define BMS_CFG_H_

/*================== Macros and Definitions ===============================*/

#define BMS_REQ_ID_NOREQ 0

/**
 * @ingroup CONFIG_BMS
 * this is the ID that should be requested via CAN signal to go to STANDBY state (ready, but no contactors closed)
 * \par Type:
 * int
 * \par Default:
 * 8
 * \par Range:
 * 0<=x
*/
#define BMS_REQ_ID_STANDBY 8

/**
 * @ingroup CONFIG_BMS
 * this is the ID that should be requested via CAN signal to go to NORMAL state (contactors closing procedure)
 * \par Type:
 * int
 * \par Default:
 * 3
 * \par Range:
 * 0<=x
*/
#define BMS_REQ_ID_NORMAL 3

/**
 * @ingroup CONFIG_BMS
 * this is the ID that should be requested via CAN signal to go to CHARGE state (contactors closing procedure)
 * \par Type:
 * int
 * \par Default:
 * 4
 * \par Range:
 * 0<=x
*/
#define BMS_REQ_ID_CHARGE 4



#define BMS_BAL_NO_REQUEST 0

/**
 * @ingroup CONFIG_BMS
 * this is the ID that should be requested via CAN to deactivate balancing with override
 * \par Type:
 * int
 * \par Default:
 * 8
 * \par Range:
 * 0<=x
*/
#define BMS_BAL_INACTIVE_OVERRIDE 1

/**
 * @ingroup CONFIG_BMS
 * this is the ID that should be requested via CAN to go out of override mode
 * \par Type:
 * int
 * \par Default:
 * 8
 * \par Range:
 * 0<=x
*/
#define BMS_BAL_OUT_OF_OVERRIDE 2

/**
 * @ingroup CONFIG_BMS
 * this is the ID that should be requested via CAN to activate balancing with override
 * \par Type:
 * int
 * \par Default:
 * 8
 * \par Range:
 * 0<=x
*/
#define BMS_BAL_ACTIVE_OVERRIDE 3


/**
 * BMS statemachine short time definition in ms
 */
#define BMS_STATEMACH_SHORTTIME_MS 1

/**
 * BMS statemachine medium time definition in ms
 */
#define BMS_STATEMACH_MEDIUMTIME_MS 5

/**
 * BMS statemachine long time definition in ms
 */
#define BMS_STATEMACH_LONGTIME_MS 100

/**
 * BMS statemachine very long time definition in ms
 */
#define BMS_STATEMACH_VERYLONGTIME_MS 2000


/**
 * @ingroup CONFIG_BMS
 * \par Type:
 * int
 * \par Default:
 * 10
 * \par Range:
 * [5,15]
 * \par Unit:
 * 10*ms
*/
#define BMS_SELFPOWERONCHECK_TIMEOUT    10  /* 100ms */

/**
 * @ingroup CONFIG_BMS
 * \par Type:
 * int
 * \par Default:
 * 10
 * \par Range:
 * [5,15]
 * \par Unit:
 * 10*ms
*/
#define BMS_SELFAWAKECHECK_TIMEOUT      10  /* 100ms */


/**
 * @ingroup CONFIG_BMS
 * \par Type:
 * int
 * \par Default:
 * 50
 * \par Range:
 * [40,60]
 * \par Unit:
 * 10*ms
*/
#define BMS_IDLE_TIMEOUT                500  /* 5s timeout to go to sleep or power off in idle state */

#define BMS_GETSELFCHECK_STATE()            BMS_CHECK_OK            /* function could return: BMS_CHECK_NOT_OK or OK BMS_CHECK_BUSY */
#define BMS_GETPOWERONSELFCHECK_STATE()     BMS_CHECK_OK            /* function could return: BMS_CHECK_NOT_OK or OK BMS_CHECK_BUSY */
#define BMS_CHECKPRECHARGE()                BMS_CheckPrecharge()    /* DIAG_CheckPrecharge() */

/*
 * Mapping the marcos from the contactor-module to
 * bms-macros.
 */
#define BMS_ALL_CONTACTORS_OFF()        CONT_SwitchAllContactorsOff()

#define BMS_CONT_MAINMINUS_ON()         CONT_SetContactorState(CONT_MAIN_MINUS, CONT_SWITCH_ON)
#define BMS_CONT_MAINMINUS_OFF()        CONT_SetContactorState(CONT_MAIN_MINUS, CONT_SWITCH_OFF)

#define BMS_CONT_MAINPRECHARGE_ON()     CONT_SetContactorState(CONT_PRECHARGE_PLUS, CONT_SWITCH_ON)
#define BMS_CONT_MAINPRECHARGE_OFF()    CONT_SetContactorState(CONT_PRECHARGE_PLUS, CONT_SWITCH_OFF)

#define BMS_CONT_MAINPLUS_ON()          CONT_SetContactorState(CONT_MAIN_PLUS, CONT_SWITCH_ON)
#define BMS_CONT_MAINPLUS_OFF()         CONT_SetContactorState(CONT_MAIN_PLUS, CONT_SWITCH_OFF)

#if BS_SEPARATE_POWERLINES == 1
#define BMS_CONT_CHARGE_MAINMINUS_ON()         CONT_SetContactorState(CONT_CHARGE_MAIN_MINUS, CONT_SWITCH_ON)
#define BMS_CONT_CHARGE_MAINMINUS_OFF()        CONT_SetContactorState(CONT_CHARGE_MAIN_MINUS, CONT_SWITCH_OFF)

#define BMS_CONT_CHARGE_MAINPRECHARGE_ON()     CONT_SetContactorState(CONT_CHARGE_PRECHARGE_PLUS, CONT_SWITCH_ON)
#define BMS_CONT_CHARGE_MAINPRECHARGE_OFF()    CONT_SetContactorState(CONT_CHARGE_PRECHARGE_PLUS, CONT_SWITCH_OFF)

#define BMS_CONT_CHARGE_MAINPLUS_ON()          CONT_SetContactorState(CONT_CHARGE_MAIN_PLUS, CONT_SWITCH_ON)
#define BMS_CONT_CHARGE_MAINPLUS_OFF()         CONT_SetContactorState(CONT_CHARGE_MAIN_PLUS, CONT_SWITCH_OFF)
#endif  /* BS_SEPARATE_POWERLINES == 1 */

/*================== Function Prototypes ==================================*/

/*================== Function Implementations =============================*/

#endif /* BMS_CFG_H_ */
