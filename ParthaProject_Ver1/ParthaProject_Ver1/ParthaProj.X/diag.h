/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name: diagnostics                                                                                                        */
/* Author:Pavithran                                                                                                                     */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file diag.h
*   @brief This file contains API's which are required for diagnostics.
*
*   @details diagnostics .
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Pavithran
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 27-07-2021
*/

/** @defgroup diagnostics
*
* This file contains API's which are required for diagnostics
*
*/

/**@{*/
/********************************************************************************************************************************/
/* Include Section                                                                                                              */
/********************************************************************************************************************************/
#ifndef DIAG_H
#define	DIAG_H

#include "event_log.h"

/********************************************************************************************************************************/
/* Defines Section - GLOBAL                                                                                                     */
/********************************************************************************************************************************/

/********************************************************************************************************************************/
/* Function Prototype Section - GLOBAL                                                                                          */
/********************************************************************************************************************************/
void DIAG_updateFlags(void);

void DIAG_Call_Back_Function(e_event_id ch_id, e_event_log event);

#endif	/* DIAG_Hs */
/********************************************************************************************************************************/
/* End of file                                                                                                                  */
/********************************************************************************************************************************/
/**@}*/