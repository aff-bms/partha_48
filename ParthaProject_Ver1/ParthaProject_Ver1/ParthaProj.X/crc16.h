/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name:CRC16                                                                                                         */
/* Author:Pavithran                                                                                                                     */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file crc16.c
*   @brief This file contains API's which are required to calculate 16 bit crc.
*
*   @details crc16.
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Pavithran
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 22-06-2021
*/

/** @defgroup CMU
*
* This file contains API's which are required to calculate 16 bit crc.
*
*/

/**@{*/
/********************************************************************************************************************************/
/* Include Section                                                                                                              */
/********************************************************************************************************************************/
#ifndef CRC16_H
#define	CRC16_H
#include "common.h"

/********************************************************************************************************************************/
/* Function Declaration Section - GLOBAL                                                                                    */
/********************************************************************************************************************************/
u16 CRC16(u8 *pBuf, s32 nLen);

#endif	/* CRC16_H */
/********************************************************************************************************************************/
/* End of file                                                                                                                  */
/********************************************************************************************************************************/
/**@}*/
