/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name:UART                                                                                                          */
/* Author:Pavithran                                                                                                                     */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file uart_app.c
*   @brief This file contains API's which are required to communicate with CMU via UART.
*
*   @details Uart application fucntions.
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Pavithran
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 22-06-2021
*/

/** @defgroup CMU
*
* UART Application functions
*
*/

/**@{*/
/********************************************************************************************************************************/
/* Include Section                                                                                                              */
/********************************************************************************************************************************/
#include "common.h"
#include "uart1.h"

/** @fn       void UART_Send_Buffer(u8 base,u8 *txData,u8 len)
*   @brief    transmit data over uart tx line 
*
*
*   @details  transmit data over uart tx line with requested size
*
*   @pre      call uart initialization
*   @post     check uart transfer status before initiating new transfer
*   @param    txData pointer to source buffer
*   @param    len required length to send data over uart tx line
*   @return   void
*/
void UART_Send_Buffer(u8 Base,u8 *txData,u8 len)
{
    u16 i_l=0;
    
    for(i_l=0;i_l<len;i_l++)
    {
        UART1_Write(*txData);
        txData++;
    }
}

/** @fn       void UART_Receive_Buffer(u8 base,u8 *rxData,u8 len)
*   @brief    transmit data over uart tx line 
*
*
*   @details  transmit data over uart tx line with requested size
*
*   @pre      call uart initialization
*   @post     check uart transfer status before initiating new transfer
*   @param    rxData pointer to source buffer
*   @param    len required length to receive data over uart rx line
*   @return   void
*/
void UART_Receive_Buffer(u8 base,u8 *rxData,u8 len)
{
    u16 i_l=0;
    
    for(i_l=0;i_l<len;i_l++)
    {
        *rxData=UART1_Read();
        rxData++;
    }
}

/********************************************************************************************************************************/
/* End of file                                                                                                                  */
/********************************************************************************************************************************/
/**@}*/