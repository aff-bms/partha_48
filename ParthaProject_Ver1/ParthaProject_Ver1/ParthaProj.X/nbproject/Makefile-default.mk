#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/ParthaProj.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/ParthaProj.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS
SUB_IMAGE_ADDRESS_COMMAND=--image-address $(SUB_IMAGE_ADDRESS)
else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=main.c common.c uart_app.c database.c crc16.c event_log.c flash_app.c can_stack.c watch_dog.c soc.c soh.c contactor.c contactor_cfg.c bms.c bms_cfg.c cell_balancing.c cell_balancing_cfg.c diag.c power_save.c system_config.c ../Source/croutine.c ../Source/list.c ../Source/queue.c ../Source/tasks.c ../Source/portable/MemMang/heap_1.c ../Source/portable/MPLAB/PIC24_dsPIC/port.c ../Source/portable/MPLAB/PIC24_dsPIC/portasm_dsPIC.S ../Source/os.c cmu.c mcc_generated_files/memory/flash_demo.c mcc_generated_files/memory/flash.s mcc_generated_files/reset.c mcc_generated_files/traps.c mcc_generated_files/clock.c mcc_generated_files/system.c mcc_generated_files/mcc.c mcc_generated_files/interrupt_manager.c mcc_generated_files/pin_manager.c mcc_generated_files/uart1.c mcc_generated_files/can1.c mcc_generated_files/crc.c mcc_generated_files/tmr1.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/main.o ${OBJECTDIR}/common.o ${OBJECTDIR}/uart_app.o ${OBJECTDIR}/database.o ${OBJECTDIR}/crc16.o ${OBJECTDIR}/event_log.o ${OBJECTDIR}/flash_app.o ${OBJECTDIR}/can_stack.o ${OBJECTDIR}/watch_dog.o ${OBJECTDIR}/soc.o ${OBJECTDIR}/soh.o ${OBJECTDIR}/contactor.o ${OBJECTDIR}/contactor_cfg.o ${OBJECTDIR}/bms.o ${OBJECTDIR}/bms_cfg.o ${OBJECTDIR}/cell_balancing.o ${OBJECTDIR}/cell_balancing_cfg.o ${OBJECTDIR}/diag.o ${OBJECTDIR}/power_save.o ${OBJECTDIR}/system_config.o ${OBJECTDIR}/_ext/1728301206/croutine.o ${OBJECTDIR}/_ext/1728301206/list.o ${OBJECTDIR}/_ext/1728301206/queue.o ${OBJECTDIR}/_ext/1728301206/tasks.o ${OBJECTDIR}/_ext/38565645/heap_1.o ${OBJECTDIR}/_ext/509314540/port.o ${OBJECTDIR}/_ext/509314540/portasm_dsPIC.o ${OBJECTDIR}/_ext/1728301206/os.o ${OBJECTDIR}/cmu.o ${OBJECTDIR}/mcc_generated_files/memory/flash_demo.o ${OBJECTDIR}/mcc_generated_files/memory/flash.o ${OBJECTDIR}/mcc_generated_files/reset.o ${OBJECTDIR}/mcc_generated_files/traps.o ${OBJECTDIR}/mcc_generated_files/clock.o ${OBJECTDIR}/mcc_generated_files/system.o ${OBJECTDIR}/mcc_generated_files/mcc.o ${OBJECTDIR}/mcc_generated_files/interrupt_manager.o ${OBJECTDIR}/mcc_generated_files/pin_manager.o ${OBJECTDIR}/mcc_generated_files/uart1.o ${OBJECTDIR}/mcc_generated_files/can1.o ${OBJECTDIR}/mcc_generated_files/crc.o ${OBJECTDIR}/mcc_generated_files/tmr1.o
POSSIBLE_DEPFILES=${OBJECTDIR}/main.o.d ${OBJECTDIR}/common.o.d ${OBJECTDIR}/uart_app.o.d ${OBJECTDIR}/database.o.d ${OBJECTDIR}/crc16.o.d ${OBJECTDIR}/event_log.o.d ${OBJECTDIR}/flash_app.o.d ${OBJECTDIR}/can_stack.o.d ${OBJECTDIR}/watch_dog.o.d ${OBJECTDIR}/soc.o.d ${OBJECTDIR}/soh.o.d ${OBJECTDIR}/contactor.o.d ${OBJECTDIR}/contactor_cfg.o.d ${OBJECTDIR}/bms.o.d ${OBJECTDIR}/bms_cfg.o.d ${OBJECTDIR}/cell_balancing.o.d ${OBJECTDIR}/cell_balancing_cfg.o.d ${OBJECTDIR}/diag.o.d ${OBJECTDIR}/power_save.o.d ${OBJECTDIR}/system_config.o.d ${OBJECTDIR}/_ext/1728301206/croutine.o.d ${OBJECTDIR}/_ext/1728301206/list.o.d ${OBJECTDIR}/_ext/1728301206/queue.o.d ${OBJECTDIR}/_ext/1728301206/tasks.o.d ${OBJECTDIR}/_ext/38565645/heap_1.o.d ${OBJECTDIR}/_ext/509314540/port.o.d ${OBJECTDIR}/_ext/509314540/portasm_dsPIC.o.d ${OBJECTDIR}/_ext/1728301206/os.o.d ${OBJECTDIR}/cmu.o.d ${OBJECTDIR}/mcc_generated_files/memory/flash_demo.o.d ${OBJECTDIR}/mcc_generated_files/memory/flash.o.d ${OBJECTDIR}/mcc_generated_files/reset.o.d ${OBJECTDIR}/mcc_generated_files/traps.o.d ${OBJECTDIR}/mcc_generated_files/clock.o.d ${OBJECTDIR}/mcc_generated_files/system.o.d ${OBJECTDIR}/mcc_generated_files/mcc.o.d ${OBJECTDIR}/mcc_generated_files/interrupt_manager.o.d ${OBJECTDIR}/mcc_generated_files/pin_manager.o.d ${OBJECTDIR}/mcc_generated_files/uart1.o.d ${OBJECTDIR}/mcc_generated_files/can1.o.d ${OBJECTDIR}/mcc_generated_files/crc.o.d ${OBJECTDIR}/mcc_generated_files/tmr1.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/main.o ${OBJECTDIR}/common.o ${OBJECTDIR}/uart_app.o ${OBJECTDIR}/database.o ${OBJECTDIR}/crc16.o ${OBJECTDIR}/event_log.o ${OBJECTDIR}/flash_app.o ${OBJECTDIR}/can_stack.o ${OBJECTDIR}/watch_dog.o ${OBJECTDIR}/soc.o ${OBJECTDIR}/soh.o ${OBJECTDIR}/contactor.o ${OBJECTDIR}/contactor_cfg.o ${OBJECTDIR}/bms.o ${OBJECTDIR}/bms_cfg.o ${OBJECTDIR}/cell_balancing.o ${OBJECTDIR}/cell_balancing_cfg.o ${OBJECTDIR}/diag.o ${OBJECTDIR}/power_save.o ${OBJECTDIR}/system_config.o ${OBJECTDIR}/_ext/1728301206/croutine.o ${OBJECTDIR}/_ext/1728301206/list.o ${OBJECTDIR}/_ext/1728301206/queue.o ${OBJECTDIR}/_ext/1728301206/tasks.o ${OBJECTDIR}/_ext/38565645/heap_1.o ${OBJECTDIR}/_ext/509314540/port.o ${OBJECTDIR}/_ext/509314540/portasm_dsPIC.o ${OBJECTDIR}/_ext/1728301206/os.o ${OBJECTDIR}/cmu.o ${OBJECTDIR}/mcc_generated_files/memory/flash_demo.o ${OBJECTDIR}/mcc_generated_files/memory/flash.o ${OBJECTDIR}/mcc_generated_files/reset.o ${OBJECTDIR}/mcc_generated_files/traps.o ${OBJECTDIR}/mcc_generated_files/clock.o ${OBJECTDIR}/mcc_generated_files/system.o ${OBJECTDIR}/mcc_generated_files/mcc.o ${OBJECTDIR}/mcc_generated_files/interrupt_manager.o ${OBJECTDIR}/mcc_generated_files/pin_manager.o ${OBJECTDIR}/mcc_generated_files/uart1.o ${OBJECTDIR}/mcc_generated_files/can1.o ${OBJECTDIR}/mcc_generated_files/crc.o ${OBJECTDIR}/mcc_generated_files/tmr1.o

# Source Files
SOURCEFILES=main.c common.c uart_app.c database.c crc16.c event_log.c flash_app.c can_stack.c watch_dog.c soc.c soh.c contactor.c contactor_cfg.c bms.c bms_cfg.c cell_balancing.c cell_balancing_cfg.c diag.c power_save.c system_config.c ../Source/croutine.c ../Source/list.c ../Source/queue.c ../Source/tasks.c ../Source/portable/MemMang/heap_1.c ../Source/portable/MPLAB/PIC24_dsPIC/port.c ../Source/portable/MPLAB/PIC24_dsPIC/portasm_dsPIC.S ../Source/os.c cmu.c mcc_generated_files/memory/flash_demo.c mcc_generated_files/memory/flash.s mcc_generated_files/reset.c mcc_generated_files/traps.c mcc_generated_files/clock.c mcc_generated_files/system.c mcc_generated_files/mcc.c mcc_generated_files/interrupt_manager.c mcc_generated_files/pin_manager.c mcc_generated_files/uart1.c mcc_generated_files/can1.c mcc_generated_files/crc.c mcc_generated_files/tmr1.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/ParthaProj.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=33CH512MP508
MP_LINKER_FILE_OPTION=,--script=p33CH512MP508.gld
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/main.o: main.c  .generated_files/flags/default/481cb1affeb5b90326b635bd93e18e4021b41955 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.o.d 
	@${RM} ${OBJECTDIR}/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  main.c  -o ${OBJECTDIR}/main.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/main.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/common.o: common.c  .generated_files/flags/default/3ebfeedd71c295ebf20d721478f9113038427284 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/common.o.d 
	@${RM} ${OBJECTDIR}/common.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  common.c  -o ${OBJECTDIR}/common.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/common.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/uart_app.o: uart_app.c  .generated_files/flags/default/99da481c57c2595ca71d33a6f2435026899e7af0 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/uart_app.o.d 
	@${RM} ${OBJECTDIR}/uart_app.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  uart_app.c  -o ${OBJECTDIR}/uart_app.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/uart_app.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/database.o: database.c  .generated_files/flags/default/1787cc79789a0e90b3cbd69abbe1f9f1b2fc35a1 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/database.o.d 
	@${RM} ${OBJECTDIR}/database.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  database.c  -o ${OBJECTDIR}/database.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/database.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/crc16.o: crc16.c  .generated_files/flags/default/4598b4c61271e75d0db1e13dda887176348033a1 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/crc16.o.d 
	@${RM} ${OBJECTDIR}/crc16.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  crc16.c  -o ${OBJECTDIR}/crc16.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/crc16.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/event_log.o: event_log.c  .generated_files/flags/default/de5f7dc15c5fec17377b7f0a129800364a8148e9 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/event_log.o.d 
	@${RM} ${OBJECTDIR}/event_log.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  event_log.c  -o ${OBJECTDIR}/event_log.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/event_log.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/flash_app.o: flash_app.c  .generated_files/flags/default/c9786f31d4c946e78b3b7bdfd2e3ce10a52acc90 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/flash_app.o.d 
	@${RM} ${OBJECTDIR}/flash_app.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  flash_app.c  -o ${OBJECTDIR}/flash_app.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/flash_app.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/can_stack.o: can_stack.c  .generated_files/flags/default/228426dd50398313e7d051d966f36b520ec501ae .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/can_stack.o.d 
	@${RM} ${OBJECTDIR}/can_stack.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  can_stack.c  -o ${OBJECTDIR}/can_stack.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/can_stack.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/watch_dog.o: watch_dog.c  .generated_files/flags/default/60e514db30458185c9c97ed8b07a4ec5d3e16b96 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/watch_dog.o.d 
	@${RM} ${OBJECTDIR}/watch_dog.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  watch_dog.c  -o ${OBJECTDIR}/watch_dog.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/watch_dog.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/soc.o: soc.c  .generated_files/flags/default/a7b6984351c0df3fd2d4b8805381bd623d61e451 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/soc.o.d 
	@${RM} ${OBJECTDIR}/soc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  soc.c  -o ${OBJECTDIR}/soc.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/soc.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/soh.o: soh.c  .generated_files/flags/default/9d6b4ea304d18060ecb280bfd7b79b41647b6fc .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/soh.o.d 
	@${RM} ${OBJECTDIR}/soh.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  soh.c  -o ${OBJECTDIR}/soh.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/soh.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/contactor.o: contactor.c  .generated_files/flags/default/3b2f8c888d3e2359ecd21456f8497c602176b4a0 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/contactor.o.d 
	@${RM} ${OBJECTDIR}/contactor.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  contactor.c  -o ${OBJECTDIR}/contactor.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/contactor.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/contactor_cfg.o: contactor_cfg.c  .generated_files/flags/default/3e1597e041a943e7fd67e8cb30d7fc522f456753 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/contactor_cfg.o.d 
	@${RM} ${OBJECTDIR}/contactor_cfg.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  contactor_cfg.c  -o ${OBJECTDIR}/contactor_cfg.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/contactor_cfg.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/bms.o: bms.c  .generated_files/flags/default/de9cb87b7a7b87e26448f98d1ada064e316542e .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/bms.o.d 
	@${RM} ${OBJECTDIR}/bms.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  bms.c  -o ${OBJECTDIR}/bms.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/bms.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/bms_cfg.o: bms_cfg.c  .generated_files/flags/default/b51a913f8674f80d9407f0af9c2e3a4d694290cb .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/bms_cfg.o.d 
	@${RM} ${OBJECTDIR}/bms_cfg.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  bms_cfg.c  -o ${OBJECTDIR}/bms_cfg.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/bms_cfg.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/cell_balancing.o: cell_balancing.c  .generated_files/flags/default/d911a5f7c0d544d000596e9a13dd015072103e7a .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/cell_balancing.o.d 
	@${RM} ${OBJECTDIR}/cell_balancing.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  cell_balancing.c  -o ${OBJECTDIR}/cell_balancing.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/cell_balancing.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/cell_balancing_cfg.o: cell_balancing_cfg.c  .generated_files/flags/default/bf3a610a7f1471bdf4ffc5bd70502a641b479102 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/cell_balancing_cfg.o.d 
	@${RM} ${OBJECTDIR}/cell_balancing_cfg.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  cell_balancing_cfg.c  -o ${OBJECTDIR}/cell_balancing_cfg.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/cell_balancing_cfg.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/diag.o: diag.c  .generated_files/flags/default/2f40d40506ef55b8c1ebeacf421fe15f3f63ae7e .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/diag.o.d 
	@${RM} ${OBJECTDIR}/diag.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  diag.c  -o ${OBJECTDIR}/diag.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/diag.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/power_save.o: power_save.c  .generated_files/flags/default/cfee1cb896a0a225d0711cc62fde9f92ad745d7a .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/power_save.o.d 
	@${RM} ${OBJECTDIR}/power_save.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  power_save.c  -o ${OBJECTDIR}/power_save.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/power_save.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/system_config.o: system_config.c  .generated_files/flags/default/98b59694265cd4f7562bdaf3e7f7ecf0448b6f12 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/system_config.o.d 
	@${RM} ${OBJECTDIR}/system_config.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  system_config.c  -o ${OBJECTDIR}/system_config.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/system_config.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/_ext/1728301206/croutine.o: ../Source/croutine.c  .generated_files/flags/default/d00a171023aecd3b2aebeba88a0f0aed009fce7c .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/_ext/1728301206" 
	@${RM} ${OBJECTDIR}/_ext/1728301206/croutine.o.d 
	@${RM} ${OBJECTDIR}/_ext/1728301206/croutine.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../Source/croutine.c  -o ${OBJECTDIR}/_ext/1728301206/croutine.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/_ext/1728301206/croutine.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/_ext/1728301206/list.o: ../Source/list.c  .generated_files/flags/default/7c0bde162c459d196fdbb2d46eb8b8cb4a7e37c6 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/_ext/1728301206" 
	@${RM} ${OBJECTDIR}/_ext/1728301206/list.o.d 
	@${RM} ${OBJECTDIR}/_ext/1728301206/list.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../Source/list.c  -o ${OBJECTDIR}/_ext/1728301206/list.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/_ext/1728301206/list.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/_ext/1728301206/queue.o: ../Source/queue.c  .generated_files/flags/default/c7cb941c0dd23bdf92f9fee68b417c003c6bf5c0 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/_ext/1728301206" 
	@${RM} ${OBJECTDIR}/_ext/1728301206/queue.o.d 
	@${RM} ${OBJECTDIR}/_ext/1728301206/queue.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../Source/queue.c  -o ${OBJECTDIR}/_ext/1728301206/queue.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/_ext/1728301206/queue.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/_ext/1728301206/tasks.o: ../Source/tasks.c  .generated_files/flags/default/1626dd1867fc3603eca022448006e3bfed1270bd .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/_ext/1728301206" 
	@${RM} ${OBJECTDIR}/_ext/1728301206/tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/1728301206/tasks.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../Source/tasks.c  -o ${OBJECTDIR}/_ext/1728301206/tasks.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/_ext/1728301206/tasks.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/_ext/38565645/heap_1.o: ../Source/portable/MemMang/heap_1.c  .generated_files/flags/default/16229cad358173a7f401377f6767c76dbd9f1483 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/_ext/38565645" 
	@${RM} ${OBJECTDIR}/_ext/38565645/heap_1.o.d 
	@${RM} ${OBJECTDIR}/_ext/38565645/heap_1.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../Source/portable/MemMang/heap_1.c  -o ${OBJECTDIR}/_ext/38565645/heap_1.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/_ext/38565645/heap_1.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/_ext/509314540/port.o: ../Source/portable/MPLAB/PIC24_dsPIC/port.c  .generated_files/flags/default/c223dd115e6fc9f7cbfc7972f009a1e710a67ed4 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/_ext/509314540" 
	@${RM} ${OBJECTDIR}/_ext/509314540/port.o.d 
	@${RM} ${OBJECTDIR}/_ext/509314540/port.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../Source/portable/MPLAB/PIC24_dsPIC/port.c  -o ${OBJECTDIR}/_ext/509314540/port.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/_ext/509314540/port.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/_ext/1728301206/os.o: ../Source/os.c  .generated_files/flags/default/43681da719cd0e5fcf57b184ac58443de58ee4dc .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/_ext/1728301206" 
	@${RM} ${OBJECTDIR}/_ext/1728301206/os.o.d 
	@${RM} ${OBJECTDIR}/_ext/1728301206/os.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../Source/os.c  -o ${OBJECTDIR}/_ext/1728301206/os.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/_ext/1728301206/os.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/cmu.o: cmu.c  .generated_files/flags/default/d97026cc5228bb70312dfc7fb2d48ef85ffe7f4e .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/cmu.o.d 
	@${RM} ${OBJECTDIR}/cmu.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  cmu.c  -o ${OBJECTDIR}/cmu.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/cmu.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/mcc_generated_files/memory/flash_demo.o: mcc_generated_files/memory/flash_demo.c  .generated_files/flags/default/bbb60c45effef062ee53d1d32ea1f9f8cd603763 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files/memory" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/memory/flash_demo.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/memory/flash_demo.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/memory/flash_demo.c  -o ${OBJECTDIR}/mcc_generated_files/memory/flash_demo.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/mcc_generated_files/memory/flash_demo.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/mcc_generated_files/reset.o: mcc_generated_files/reset.c  .generated_files/flags/default/7b8dbf70850726fea31e97133c3707b0326ac9f7 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/reset.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/reset.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/reset.c  -o ${OBJECTDIR}/mcc_generated_files/reset.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/mcc_generated_files/reset.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/mcc_generated_files/traps.o: mcc_generated_files/traps.c  .generated_files/flags/default/beb215aab219e8a7c0441a0e126e1f9db709770f .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/traps.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/traps.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/traps.c  -o ${OBJECTDIR}/mcc_generated_files/traps.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/mcc_generated_files/traps.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/mcc_generated_files/clock.o: mcc_generated_files/clock.c  .generated_files/flags/default/9ef4724b664261465c79bb6978849b227d3cc40 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/clock.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/clock.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/clock.c  -o ${OBJECTDIR}/mcc_generated_files/clock.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/mcc_generated_files/clock.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/mcc_generated_files/system.o: mcc_generated_files/system.c  .generated_files/flags/default/f3cbe4ebc250bcd376e7c807cc9a9c7a0425774c .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/system.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/system.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/system.c  -o ${OBJECTDIR}/mcc_generated_files/system.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/mcc_generated_files/system.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/mcc_generated_files/mcc.o: mcc_generated_files/mcc.c  .generated_files/flags/default/ac86b1b9e58907d0f0cb268d5762416a12150806 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/mcc.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/mcc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/mcc.c  -o ${OBJECTDIR}/mcc_generated_files/mcc.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/mcc_generated_files/mcc.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/mcc_generated_files/interrupt_manager.o: mcc_generated_files/interrupt_manager.c  .generated_files/flags/default/5e1c760478afee6d5b259d174bfe7cfab17e6685 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/interrupt_manager.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/interrupt_manager.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/interrupt_manager.c  -o ${OBJECTDIR}/mcc_generated_files/interrupt_manager.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/mcc_generated_files/interrupt_manager.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/mcc_generated_files/pin_manager.o: mcc_generated_files/pin_manager.c  .generated_files/flags/default/6310ad4bd1b101987eb15213f390b09892006d79 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/pin_manager.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/pin_manager.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/pin_manager.c  -o ${OBJECTDIR}/mcc_generated_files/pin_manager.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/mcc_generated_files/pin_manager.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/mcc_generated_files/uart1.o: mcc_generated_files/uart1.c  .generated_files/flags/default/57d95b3b44cc456c180e1c6819c642f0f42dc082 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/uart1.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/uart1.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/uart1.c  -o ${OBJECTDIR}/mcc_generated_files/uart1.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/mcc_generated_files/uart1.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/mcc_generated_files/can1.o: mcc_generated_files/can1.c  .generated_files/flags/default/d4af0256ae8bef197059c62ef8a4bb3bf69265b6 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/can1.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/can1.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/can1.c  -o ${OBJECTDIR}/mcc_generated_files/can1.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/mcc_generated_files/can1.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/mcc_generated_files/crc.o: mcc_generated_files/crc.c  .generated_files/flags/default/caf7ee37b162a97c2d0857480d4c5897a80fb1b6 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/crc.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/crc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/crc.c  -o ${OBJECTDIR}/mcc_generated_files/crc.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/mcc_generated_files/crc.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/mcc_generated_files/tmr1.o: mcc_generated_files/tmr1.c  .generated_files/flags/default/88b9d12c6a907c68697ec66ab4dffd3b4bdf524c .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/tmr1.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/tmr1.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/tmr1.c  -o ${OBJECTDIR}/mcc_generated_files/tmr1.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/mcc_generated_files/tmr1.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -mno-eds-warn  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
else
${OBJECTDIR}/main.o: main.c  .generated_files/flags/default/18aae4be3b067cbce9e9d78736a73ee933b70232 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.o.d 
	@${RM} ${OBJECTDIR}/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  main.c  -o ${OBJECTDIR}/main.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/main.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/common.o: common.c  .generated_files/flags/default/de3d1d555851bd89e3006c2d7c4746a43355bb18 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/common.o.d 
	@${RM} ${OBJECTDIR}/common.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  common.c  -o ${OBJECTDIR}/common.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/common.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/uart_app.o: uart_app.c  .generated_files/flags/default/a4051e403eef78ca6b155367c025138b8e9e4337 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/uart_app.o.d 
	@${RM} ${OBJECTDIR}/uart_app.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  uart_app.c  -o ${OBJECTDIR}/uart_app.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/uart_app.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/database.o: database.c  .generated_files/flags/default/2e0aaa5ac8c2b79e0cba0b076b4948167d72d88e .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/database.o.d 
	@${RM} ${OBJECTDIR}/database.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  database.c  -o ${OBJECTDIR}/database.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/database.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/crc16.o: crc16.c  .generated_files/flags/default/37ea92eb191fdc66354531c25a27cf68d4bd582b .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/crc16.o.d 
	@${RM} ${OBJECTDIR}/crc16.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  crc16.c  -o ${OBJECTDIR}/crc16.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/crc16.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/event_log.o: event_log.c  .generated_files/flags/default/17ac88db0c8d00fdd320266b11595e0333400a8 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/event_log.o.d 
	@${RM} ${OBJECTDIR}/event_log.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  event_log.c  -o ${OBJECTDIR}/event_log.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/event_log.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/flash_app.o: flash_app.c  .generated_files/flags/default/e1493c8d9b7f64a06ed6b5d4c7ba633aeff7a08f .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/flash_app.o.d 
	@${RM} ${OBJECTDIR}/flash_app.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  flash_app.c  -o ${OBJECTDIR}/flash_app.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/flash_app.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/can_stack.o: can_stack.c  .generated_files/flags/default/69d1f9fa099f8abe313d785ee3d9909e0ea1e147 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/can_stack.o.d 
	@${RM} ${OBJECTDIR}/can_stack.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  can_stack.c  -o ${OBJECTDIR}/can_stack.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/can_stack.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/watch_dog.o: watch_dog.c  .generated_files/flags/default/3ed8e3e5c4f0d5c59dc3580f588770e661470e5a .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/watch_dog.o.d 
	@${RM} ${OBJECTDIR}/watch_dog.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  watch_dog.c  -o ${OBJECTDIR}/watch_dog.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/watch_dog.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/soc.o: soc.c  .generated_files/flags/default/f0a52d85950fd2f9023e8a8e569826e8e02dedce .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/soc.o.d 
	@${RM} ${OBJECTDIR}/soc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  soc.c  -o ${OBJECTDIR}/soc.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/soc.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/soh.o: soh.c  .generated_files/flags/default/6bd063d448f39a2f08c135df4f5fa9f1f1cec65c .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/soh.o.d 
	@${RM} ${OBJECTDIR}/soh.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  soh.c  -o ${OBJECTDIR}/soh.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/soh.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/contactor.o: contactor.c  .generated_files/flags/default/c8a1122001292a9549e00b1e6ae7a03ef9f68e17 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/contactor.o.d 
	@${RM} ${OBJECTDIR}/contactor.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  contactor.c  -o ${OBJECTDIR}/contactor.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/contactor.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/contactor_cfg.o: contactor_cfg.c  .generated_files/flags/default/42affc8991454501443682427366d71615a41c6a .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/contactor_cfg.o.d 
	@${RM} ${OBJECTDIR}/contactor_cfg.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  contactor_cfg.c  -o ${OBJECTDIR}/contactor_cfg.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/contactor_cfg.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/bms.o: bms.c  .generated_files/flags/default/d1703bf1a258522ed10162290da08fb603c93f05 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/bms.o.d 
	@${RM} ${OBJECTDIR}/bms.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  bms.c  -o ${OBJECTDIR}/bms.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/bms.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/bms_cfg.o: bms_cfg.c  .generated_files/flags/default/955f429322977dab8fd96de3c866366116c492ba .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/bms_cfg.o.d 
	@${RM} ${OBJECTDIR}/bms_cfg.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  bms_cfg.c  -o ${OBJECTDIR}/bms_cfg.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/bms_cfg.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/cell_balancing.o: cell_balancing.c  .generated_files/flags/default/88c4b008c65efb9832dac136593dc1e3bbfbe681 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/cell_balancing.o.d 
	@${RM} ${OBJECTDIR}/cell_balancing.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  cell_balancing.c  -o ${OBJECTDIR}/cell_balancing.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/cell_balancing.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/cell_balancing_cfg.o: cell_balancing_cfg.c  .generated_files/flags/default/b7b668128d6abd8c733528c729ddddba1c8a82d3 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/cell_balancing_cfg.o.d 
	@${RM} ${OBJECTDIR}/cell_balancing_cfg.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  cell_balancing_cfg.c  -o ${OBJECTDIR}/cell_balancing_cfg.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/cell_balancing_cfg.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/diag.o: diag.c  .generated_files/flags/default/6969207ef59ba97c77506691853dd2af145aa774 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/diag.o.d 
	@${RM} ${OBJECTDIR}/diag.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  diag.c  -o ${OBJECTDIR}/diag.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/diag.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/power_save.o: power_save.c  .generated_files/flags/default/76714db631a8c5b9d529a6a134fac437bda723e0 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/power_save.o.d 
	@${RM} ${OBJECTDIR}/power_save.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  power_save.c  -o ${OBJECTDIR}/power_save.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/power_save.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/system_config.o: system_config.c  .generated_files/flags/default/1242073cfd0add114d63443e4ec7647422bc9986 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/system_config.o.d 
	@${RM} ${OBJECTDIR}/system_config.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  system_config.c  -o ${OBJECTDIR}/system_config.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/system_config.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/_ext/1728301206/croutine.o: ../Source/croutine.c  .generated_files/flags/default/4571ae77e2e4e2a9ff01c339572cae5945386f4a .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/_ext/1728301206" 
	@${RM} ${OBJECTDIR}/_ext/1728301206/croutine.o.d 
	@${RM} ${OBJECTDIR}/_ext/1728301206/croutine.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../Source/croutine.c  -o ${OBJECTDIR}/_ext/1728301206/croutine.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/_ext/1728301206/croutine.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/_ext/1728301206/list.o: ../Source/list.c  .generated_files/flags/default/ed5ee555803f97c9e712bde1a63294c37752a8d4 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/_ext/1728301206" 
	@${RM} ${OBJECTDIR}/_ext/1728301206/list.o.d 
	@${RM} ${OBJECTDIR}/_ext/1728301206/list.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../Source/list.c  -o ${OBJECTDIR}/_ext/1728301206/list.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/_ext/1728301206/list.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/_ext/1728301206/queue.o: ../Source/queue.c  .generated_files/flags/default/ff2b635ceef6554029c88114348d55a225958b6c .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/_ext/1728301206" 
	@${RM} ${OBJECTDIR}/_ext/1728301206/queue.o.d 
	@${RM} ${OBJECTDIR}/_ext/1728301206/queue.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../Source/queue.c  -o ${OBJECTDIR}/_ext/1728301206/queue.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/_ext/1728301206/queue.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/_ext/1728301206/tasks.o: ../Source/tasks.c  .generated_files/flags/default/4e9d97d92ef523d8be09f1370523f8f15648a806 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/_ext/1728301206" 
	@${RM} ${OBJECTDIR}/_ext/1728301206/tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/1728301206/tasks.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../Source/tasks.c  -o ${OBJECTDIR}/_ext/1728301206/tasks.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/_ext/1728301206/tasks.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/_ext/38565645/heap_1.o: ../Source/portable/MemMang/heap_1.c  .generated_files/flags/default/20aae22148642d0c843203b3304150a245d4df1c .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/_ext/38565645" 
	@${RM} ${OBJECTDIR}/_ext/38565645/heap_1.o.d 
	@${RM} ${OBJECTDIR}/_ext/38565645/heap_1.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../Source/portable/MemMang/heap_1.c  -o ${OBJECTDIR}/_ext/38565645/heap_1.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/_ext/38565645/heap_1.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/_ext/509314540/port.o: ../Source/portable/MPLAB/PIC24_dsPIC/port.c  .generated_files/flags/default/151a509f8a913119fd272837fadeeedf86abadd5 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/_ext/509314540" 
	@${RM} ${OBJECTDIR}/_ext/509314540/port.o.d 
	@${RM} ${OBJECTDIR}/_ext/509314540/port.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../Source/portable/MPLAB/PIC24_dsPIC/port.c  -o ${OBJECTDIR}/_ext/509314540/port.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/_ext/509314540/port.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/_ext/1728301206/os.o: ../Source/os.c  .generated_files/flags/default/719f41e910601e83e73a67eef0dd9fa17667cc16 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/_ext/1728301206" 
	@${RM} ${OBJECTDIR}/_ext/1728301206/os.o.d 
	@${RM} ${OBJECTDIR}/_ext/1728301206/os.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  ../Source/os.c  -o ${OBJECTDIR}/_ext/1728301206/os.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/_ext/1728301206/os.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/cmu.o: cmu.c  .generated_files/flags/default/9d9af1b60f07754f20d5574ede099c26d4b9d873 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/cmu.o.d 
	@${RM} ${OBJECTDIR}/cmu.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  cmu.c  -o ${OBJECTDIR}/cmu.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/cmu.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/mcc_generated_files/memory/flash_demo.o: mcc_generated_files/memory/flash_demo.c  .generated_files/flags/default/ad72b0665e6b4e1212d699a1b9fb456b1bd9aef5 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files/memory" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/memory/flash_demo.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/memory/flash_demo.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/memory/flash_demo.c  -o ${OBJECTDIR}/mcc_generated_files/memory/flash_demo.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/mcc_generated_files/memory/flash_demo.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/mcc_generated_files/reset.o: mcc_generated_files/reset.c  .generated_files/flags/default/f633ee7a17bd76fb77b2af3b295267253d5bfd2d .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/reset.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/reset.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/reset.c  -o ${OBJECTDIR}/mcc_generated_files/reset.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/mcc_generated_files/reset.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/mcc_generated_files/traps.o: mcc_generated_files/traps.c  .generated_files/flags/default/77c3a16015f5dcbbcda02c94e8345b5af6b222a8 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/traps.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/traps.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/traps.c  -o ${OBJECTDIR}/mcc_generated_files/traps.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/mcc_generated_files/traps.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/mcc_generated_files/clock.o: mcc_generated_files/clock.c  .generated_files/flags/default/4a04f5ca25241dd933019d453d31c4c96640bc04 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/clock.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/clock.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/clock.c  -o ${OBJECTDIR}/mcc_generated_files/clock.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/mcc_generated_files/clock.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/mcc_generated_files/system.o: mcc_generated_files/system.c  .generated_files/flags/default/b795d2ea6082e3a1436dafccd17b51e2e6933570 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/system.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/system.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/system.c  -o ${OBJECTDIR}/mcc_generated_files/system.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/mcc_generated_files/system.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/mcc_generated_files/mcc.o: mcc_generated_files/mcc.c  .generated_files/flags/default/cc06ba6cb4a038f3883772f494887f3d41f53b64 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/mcc.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/mcc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/mcc.c  -o ${OBJECTDIR}/mcc_generated_files/mcc.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/mcc_generated_files/mcc.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/mcc_generated_files/interrupt_manager.o: mcc_generated_files/interrupt_manager.c  .generated_files/flags/default/a34049b9516511447a82f49c6860f9f780ad9d7d .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/interrupt_manager.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/interrupt_manager.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/interrupt_manager.c  -o ${OBJECTDIR}/mcc_generated_files/interrupt_manager.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/mcc_generated_files/interrupt_manager.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/mcc_generated_files/pin_manager.o: mcc_generated_files/pin_manager.c  .generated_files/flags/default/94e529983f3342fa5be65a54879887fbe1af10e1 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/pin_manager.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/pin_manager.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/pin_manager.c  -o ${OBJECTDIR}/mcc_generated_files/pin_manager.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/mcc_generated_files/pin_manager.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/mcc_generated_files/uart1.o: mcc_generated_files/uart1.c  .generated_files/flags/default/70116aa392ed0f1372b82eeedf1bcdbd63b8ba0a .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/uart1.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/uart1.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/uart1.c  -o ${OBJECTDIR}/mcc_generated_files/uart1.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/mcc_generated_files/uart1.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/mcc_generated_files/can1.o: mcc_generated_files/can1.c  .generated_files/flags/default/7c0a0f43860e4a2a6f9f9fcdc74e5d7bd73fc7da .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/can1.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/can1.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/can1.c  -o ${OBJECTDIR}/mcc_generated_files/can1.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/mcc_generated_files/can1.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/mcc_generated_files/crc.o: mcc_generated_files/crc.c  .generated_files/flags/default/5c0a2ccbc4147c67640024e681fe3642688b0af1 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/crc.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/crc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/crc.c  -o ${OBJECTDIR}/mcc_generated_files/crc.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/mcc_generated_files/crc.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
${OBJECTDIR}/mcc_generated_files/tmr1.o: mcc_generated_files/tmr1.c  .generated_files/flags/default/efc81ded86d5ca6d3b66646d814a0461e1fb04e6 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/tmr1.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/tmr1.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/tmr1.c  -o ${OBJECTDIR}/mcc_generated_files/tmr1.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/mcc_generated_files/tmr1.o.d"      -mno-eds-warn  -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -mlarge-code -mlarge-data -O0 -I"../Source" -I"../Common/Full" -I"../Common/Minimal" -I"../Source/portable/Common" -I"../Source/portable/MemMang" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"CMU" -DMPLAB_DSPIC_PORT -msmart-io=1 -Wall -msfr-warn=off    -mdfp="${DFP_DIR}/xc16"
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/mcc_generated_files/memory/flash.o: mcc_generated_files/memory/flash.s  .generated_files/flags/default/9d318ea5c615627787db0f3e2bab0b4c2e7ba2b1 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files/memory" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/memory/flash.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/memory/flash.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  mcc_generated_files/memory/flash.s  -o ${OBJECTDIR}/mcc_generated_files/memory/flash.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -Wa,-MD,"${OBJECTDIR}/mcc_generated_files/memory/flash.o.d",--defsym=__MPLAB_BUILD=1,--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1,-g,--no-relax$(MP_EXTRA_AS_POST)  -mdfp="${DFP_DIR}/xc16"
	
else
${OBJECTDIR}/mcc_generated_files/memory/flash.o: mcc_generated_files/memory/flash.s  .generated_files/flags/default/37d443ae4bc1763b1d32fa9accaf4fcc1c80a8f0 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files/memory" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/memory/flash.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/memory/flash.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  mcc_generated_files/memory/flash.s  -o ${OBJECTDIR}/mcc_generated_files/memory/flash.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -Wa,-MD,"${OBJECTDIR}/mcc_generated_files/memory/flash.o.d",--defsym=__MPLAB_BUILD=1,-g,--no-relax$(MP_EXTRA_AS_POST)  -mdfp="${DFP_DIR}/xc16"
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemblePreproc
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/509314540/portasm_dsPIC.o: ../Source/portable/MPLAB/PIC24_dsPIC/portasm_dsPIC.S  .generated_files/flags/default/431a993e707a83390757db6a9c6f08a5922bbe4e .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/_ext/509314540" 
	@${RM} ${OBJECTDIR}/_ext/509314540/portasm_dsPIC.o.d 
	@${RM} ${OBJECTDIR}/_ext/509314540/portasm_dsPIC.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  ../Source/portable/MPLAB/PIC24_dsPIC/portasm_dsPIC.S  -o ${OBJECTDIR}/_ext/509314540/portasm_dsPIC.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/_ext/509314540/portasm_dsPIC.o.d"  -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -Wa,-MD,"${OBJECTDIR}/_ext/509314540/portasm_dsPIC.o.asm.d",--defsym=__MPLAB_BUILD=1,--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1,-g,--no-relax$(MP_EXTRA_AS_POST)  -mdfp="${DFP_DIR}/xc16"
	
else
${OBJECTDIR}/_ext/509314540/portasm_dsPIC.o: ../Source/portable/MPLAB/PIC24_dsPIC/portasm_dsPIC.S  .generated_files/flags/default/d8e36f399bb41248059534701e46e5c48ffb1719 .generated_files/flags/default/c39017f4fda401a6f44cc2e27e55d84559012e1d
	@${MKDIR} "${OBJECTDIR}/_ext/509314540" 
	@${RM} ${OBJECTDIR}/_ext/509314540/portasm_dsPIC.o.d 
	@${RM} ${OBJECTDIR}/_ext/509314540/portasm_dsPIC.o 
	${MP_CC} $(MP_EXTRA_AS_PRE)  ../Source/portable/MPLAB/PIC24_dsPIC/portasm_dsPIC.S  -o ${OBJECTDIR}/_ext/509314540/portasm_dsPIC.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MP -MMD -MF "${OBJECTDIR}/_ext/509314540/portasm_dsPIC.o.d"  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -Wa,-MD,"${OBJECTDIR}/_ext/509314540/portasm_dsPIC.o.asm.d",--defsym=__MPLAB_BUILD=1,-g,--no-relax$(MP_EXTRA_AS_POST)  -mdfp="${DFP_DIR}/xc16"
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/ParthaProj.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/ParthaProj.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -D__DEBUG=__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU"  -mreserve=data@0x1000:0x101B -mreserve=data@0x101C:0x101D -mreserve=data@0x101E:0x101F -mreserve=data@0x1020:0x1021 -mreserve=data@0x1022:0x1023 -mreserve=data@0x1024:0x1027 -mreserve=data@0x1028:0x104F   -Wl,--local-stack,,--defsym=__MPLAB_BUILD=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,-D__DEBUG=__DEBUG,--defsym=__MPLAB_DEBUGGER_PK3=1,$(MP_LINKER_FILE_OPTION),--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml$(MP_EXTRA_LD_POST)  -mdfp="${DFP_DIR}/xc16" 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/ParthaProj.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/ParthaProj.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -I"../Source/include" -I"../Source/portable/Common" -I"../Source/portable/MPLAB/PIC24_dsPIC" -I"mcc_generated_files" -I"../Common" -I"../Common/Full" -I"../Common/include" -I"../Common/Minimal" -I"CMU" -Wl,--local-stack,,--defsym=__MPLAB_BUILD=1,$(MP_LINKER_FILE_OPTION),--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml$(MP_EXTRA_LD_POST)  -mdfp="${DFP_DIR}/xc16" 
	${MP_CC_DIR}\\xc16-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/ParthaProj.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} -a  -omf=elf   -mdfp="${DFP_DIR}/xc16" 
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
