/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name:common                                                                                                         */
/* Author:Pavithran                                                                                                                     */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file common.h
*   @brief This file contains structure declaration,variable.
*
*   @details variable type naming conventions.
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Pavithran
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 30-06-2021
*/

/** @defgroup CMU
*
* This file contains structure declaration,variable.
*
*/

/**@{*/
/********************************************************************************************************************************/
/* Include Section                                                                                                              */
/********************************************************************************************************************************/
#ifndef COMMON_H
#define	COMMON_H

#include <xc.h> // include processor files - each processor file is guarded. 
#include "string.h"
#include "stdlib.h"
#include "stdbool.h"

/********************************************************************************************************************************/
/* Defines Section - GLOBAL                                                                                                      */
/********************************************************************************************************************************/
#define MASK_1ST_BYTE  0X00000000000000FF
#define MASK_2ND_BYTE  0X000000000000FF00
#define MASK_3RD_BYTE  0X0000000000FF0000
#define MASK_4TH_BYTE  0X00000000FF000000
#define MASK_5TH_BYTE  0X000000FF00000000
#define MASK_6TH_BYTE  0X0000FF0000000000
#define MASK_7TH_BYTE  0X00FF000000000000
#define MASK_8TH_BYTE  0XFF00000000000000

#define SHIFT_1BYTE   8
#define SHIFT_2BYTE   16
#define SHIFT_3BYTE   24
#define SHIFT_4BYTE   32
#define SHIFT_5BYTE   40
#define SHIFT_6BYTE   48
#define SHIFT_7BYTE   56

typedef unsigned char tbool;
typedef signed char s8;
typedef signed int s16;
typedef signed long int s32;
typedef signed long long int s64;
typedef unsigned char u8;
typedef unsigned int u16;
typedef unsigned long int u32;
typedef unsigned long long int u64;
typedef float f32;
typedef float f64;
typedef unsigned long int bitfield;

/********************************************************************************************************************************/
/* variable Section - GLOBAL                                                                                                      */
/********************************************************************************************************************************/
typedef enum
{ 
    E_NOT_OK,
    E_OK
}e_STD_RETURN_TYPE;

/********************************************************************************************************************************/
/* Function Prototype Section - GLOBAL                                                                                          */
/********************************************************************************************************************************/
extern volatile u32 Timer_1ms;

extern e_STD_RETURN_TYPE IsTimerExpired(u32 start_timer,u32 MAX_TIMEOUT);

extern void MCU_WaitMs(u32 delay);

#endif	/* COMMON_H */
/********************************************************************************************************************************/
/* End of file                                                                                                                  */
/********************************************************************************************************************************/
/**@}*/