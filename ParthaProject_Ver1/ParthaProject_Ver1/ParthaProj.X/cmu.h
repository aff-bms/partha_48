/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name:CMU(BQ79656-Q1)                                                                                                          */
/* Author:Pavithran                                                                                                                     */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file CMU.h
*   @brief This file contains structure declaration,variable.
*
*   @details CMU Driver.
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Pavithran
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 22-06-2021
*/

/** @defgroup CMU
*
* The  Texas  Instruments  BQ79656  device  is  a highly  integrated,  high-accuracy  
* battery  monitor and  protector  for  16-series  Li-ion battery  packs. The device 
* includes a high-accuracy monitoring system, a highly configurable protection subsystem, 
* and support for autonomous controlled cell balancing and a host communication peripheral 
* supporting UART/SPI interface.
*
*/

/**@{*/
/********************************************************************************************************************************/
/* Include Section                                                                                                              */
/********************************************************************************************************************************/
#ifndef CMU_H
#define	CMU_H

/* include processor files - each processor file is guarded*/  
#include "common.h" 
#include "cmu_reg.h" 

/********************************************************************************************************************************/
/* Defines Section - GLOBAL                                                                                                      */
/********************************************************************************************************************************/
/*! @brief Number of GPIO/temperature sensor inputs connected. */
#define BCC_GPIO_INPUT_CONNECTED  1

/** @brief CMU Error codes
*
* 
*/
typedef enum
{
    CMU_STATUS_SUCCESS        = 0U,   /*!< No error. */
    CMU_STATUS_INIT_FAIL      = 1U,   /*!< SPI initialization failure. */
    CMU_STATUS_UART_FAIL      = 2U,   /*!< Fail in the SPI communication. */
    CMU_STATUS_PARAM_RANGE    = 3U,   /*!< Parameter out of range. */
    CMU_STATUS_CRC            = 4U,   /*!< Wrong CRC. */
    CMU_STATUS_COM_TIMEOUT    = 5U,   /*!< Communication timeout. */
    CMU_STATUS_NULL_RESP      = 6U,   /*!< Response frame of CMU device is equal to zero (except
                                           CRC). This occurs only in SPI communication mode during
                                           the very first message. */
    CMU_STATUS_DIAG_FAIL      = 7U,   /*!< It is not allowed to enter diagnostic mode. */
	CMU_BALANCING_TIMER_NOT_ELAPSED = 8U,
    CMU_STATUS_FAILURE /*!< Failure. */
} e_bcc_status;

/*! @brief Cluster Identification Address.
 *
 * Note: The maximum number of clusters/devices is 63. */
typedef enum
{
    CMU_CID_UNASSIG       = 0U,    /*!< ID of uninitialized CMU devices. */
    CMU_CID_DEV1          = 1U,    /*!< Cluster ID of device 1. It  is the first device
                                        in daisy chain*/
    CMU_CID_DEV2          = 2U,    /*!< Cluster ID of device 2. */
    CMU_CID_DEV3          = 3U,    /*!< Cluster ID of device 3. */
    CMU_CID_DEV4          = 4U,    /*!< Cluster ID of device 4. */
    CMU_CID_DEV5          = 5U,    /*!< Cluster ID of device 5. */
    CMU_CID_DEV6          = 6U,    /*!< Cluster ID of device 6. */
    CMU_CID_DEV7          = 7U,    /*!< Cluster ID of device 7. */
    CMU_CID_DEV8          = 8U,    /*!< Cluster ID of device 8. */
    CMU_CID_DEV9          = 9U,    /*!< Cluster ID of device 9. */
    CMU_CID_DEV10         = 10U,   /*!< Cluster ID of device 10. */
    CMU_CID_DEV11         = 11U,   /*!< Cluster ID of device 11. */
    CMU_CID_DEV12         = 12U,   /*!< Cluster ID of device 12. */
    CMU_CID_DEV13         = 13U,   /*!< Cluster ID of device 13. */
    CMU_CID_DEV14         = 14U,   /*!< Cluster ID of device 14. */
    CMU_CID_DEV15         = 15U,   /*!< Cluster ID of device 15. */
    CMU_CID_DEV16         = 16U,   /*!< Cluster ID of device 16. */
    CMU_CID_DEV17         = 17U,   /*!< Cluster ID of device 17. */
    CMU_CID_DEV18         = 18U,   /*!< Cluster ID of device 18. */
    CMU_CID_DEV19         = 19U,   /*!< Cluster ID of device 19. */
    CMU_CID_DEV20         = 20U,   /*!< Cluster ID of device 20. */
    CMU_CID_DEV21         = 21U,   /*!< Cluster ID of device 21. */
    CMU_CID_DEV22         = 22U,   /*!< Cluster ID of device 22. */
    CMU_CID_DEV23         = 23U,   /*!< Cluster ID of device 23. */
    CMU_CID_DEV24         = 24U,   /*!< Cluster ID of device 24. */
    CMU_CID_DEV25         = 25U,   /*!< Cluster ID of device 25. */
    CMU_CID_DEV26         = 26U,   /*!< Cluster ID of device 26. */
    CMU_CID_DEV27         = 27U,   /*!< Cluster ID of device 27. */
    CMU_CID_DEV28         = 28U,   /*!< Cluster ID of device 28. */
    CMU_CID_DEV29         = 29U,   /*!< Cluster ID of device 29. */
    CMU_CID_DEV30         = 30U,   /*!< Cluster ID of device 30. */
    CMU_CID_DEV31         = 31U,   /*!< Cluster ID of device 31. */
    CMU_CID_DEV32         = 32U,   /*!< Cluster ID of device 32. */
    CMU_CID_DEV33         = 33U,   /*!< Cluster ID of device 33. */
    CMU_CID_DEV34         = 34U,   /*!< Cluster ID of device 34. */
    CMU_CID_DEV35         = 35U,   /*!< Cluster ID of device 35. */
    CMU_CID_DEV36         = 36U,   /*!< Cluster ID of device 36. */
    CMU_CID_DEV37         = 37U,   /*!< Cluster ID of device 37. */
    CMU_CID_DEV38         = 38U,   /*!< Cluster ID of device 38. */
    CMU_CID_DEV39         = 39U,   /*!< Cluster ID of device 39. */
    CMU_CID_DEV40         = 40U,   /*!< Cluster ID of device 40. */
    CMU_CID_DEV41         = 41U,   /*!< Cluster ID of device 41. */
    CMU_CID_DEV42         = 42U,   /*!< Cluster ID of device 42. */
    CMU_CID_DEV43         = 43U,   /*!< Cluster ID of device 43. */
    CMU_CID_DEV44         = 44U,   /*!< Cluster ID of device 44. */
    CMU_CID_DEV45         = 45U,   /*!< Cluster ID of device 45. */
    CMU_CID_DEV46         = 46U,   /*!< Cluster ID of device 46. */
    CMU_CID_DEV47         = 47U,   /*!< Cluster ID of device 47. */
    CMU_CID_DEV48         = 48U,   /*!< Cluster ID of device 48. */
    CMU_CID_DEV49         = 49U,   /*!< Cluster ID of device 49. */
    CMU_CID_DEV50         = 50U,   /*!< Cluster ID of device 50. */
    CMU_CID_DEV51         = 51U,   /*!< Cluster ID of device 51. */
    CMU_CID_DEV52         = 52U,   /*!< Cluster ID of device 52. */
    CMU_CID_DEV53         = 53U,   /*!< Cluster ID of device 53. */
    CMU_CID_DEV54         = 54U,   /*!< Cluster ID of device 54. */
    CMU_CID_DEV55         = 55U,   /*!< Cluster ID of device 55. */
    CMU_CID_DEV56         = 56U,   /*!< Cluster ID of device 56. */
    CMU_CID_DEV57         = 57U,   /*!< Cluster ID of device 57. */
    CMU_CID_DEV58         = 58U,   /*!< Cluster ID of device 58. */
    CMU_CID_DEV59         = 59U,   /*!< Cluster ID of device 59. */
    CMU_CID_DEV60         = 60U,   /*!< Cluster ID of device 60. */
    CMU_CID_DEV61         = 61U,   /*!< Cluster ID of device 61. */
    CMU_CID_DEV62         = 62U,   /*!< Cluster ID of device 62. */
    CMU_CID_DEV63         = 63U    /*!< Cluster ID of device 63. */
} e_bcc_cid_t;

/*! @brief States of the CMU state machine
 *
 */
typedef enum {
    /* Init-Sequence */
    CMU_STATEMACH_UNINITIALIZED             = 0,    /*!<    */
    CMU_STATEMACH_INITIALIZATION            = 1,    /*!<    */
    CMU_STATEMACH_REINIT                    = 2,    /*!<    */
    CMU_STATEMACH_INITIALIZED               = 3,    /*!< CMU is initialized                             */
    /* Voltage Measurement Cycle */
    CMU_STATEMACH_IDLE                      = 4,    /*!<    */
    CMU_STATEMACH_STARTMEAS                 = 5,    /*!<    */
    CMU_STATEMACH_READVOLTAGE               = 6,    /*!<    */
    CMU_STATEMACH_BALANCECONTROL            = 7,   /*!<    */
    CMU_STATEMACH_ERROR_SPIFAILED           = 8, /*!< Error-State: SPI error                         */
    CMU_STATEMACH_ERROR_PECFAILED           = 9, /*!< Error-State: PEC error                         */
    CMU_STATEMACH_ERROR_INITIALIZATION      = 10, /*!< Error-State: initialization error              */
	CMU_STATEMACH_CHECK_MEAS_STATUS,
	CMU_STATEMACH_INIT_REQUEST,
	CMU_STATEMACH_CLEAR_FAULT,
	CMU_STATEMACH_GET_FAULT,
    CMU_STATEMACH_GOTO_SHUTDOWN,
	CMU_STATEMACH_GOTO_SLEEP,
    CMU_STATEMACH_IN_SLEEP_MODE,
	CMU_STATEMACH_EXIT_FROM_SLEEP_SHUTDOWN,
	CMU_STATEMACH_NO_REQUEST,
} e_CMU_STATEMACH;

/*!
 * @brief Driver configuration.
 *
 * This structure contains all information needed for proper functionality of
 * the driver, such as used communication mode, CMU device(s) configuration or
 * internal driver data.
 */
typedef struct 
{
    u8 u8_drvInstance;                     /*!< CMU driver instance. Passed to the external functions
                                                  defined by the user. */
    u8 u8_devicesCnt;                      /*!< Number of CMU devices. */
    u16 u16_cellMap[16];    /*!< Bit map of used cells of each CMU device. */
    u8 u8_rxBuf[128];      /*!< Buffer for receiving data */
}st_bcc_drv_config;

/* Structure containing a register name and its address. */
typedef struct
{
     u16 u16_address;
     u16 u16_value;
}st_cmu_init_reg;

/**
 * This structure contains all the variables relevant for the CMU state machine.
 * The user can get the current state of the CMU state machine with this variable
 */
typedef struct 
{
    u16 u16_timer;                           /*!< time in ms before the state machine processes the next state, e.g. in counts of 1ms    */
    e_CMU_STATEMACH statereq;            /*!< current state request made to the state machine                                        */
    e_CMU_STATEMACH state;               /*!< state of Driver State Machine                                                          */
    uint8_t u8_substate;                    /*!< current substate of the state machine                                                  */
    e_CMU_STATEMACH laststate;           /*!< previous state of the state machine                                                    */
    u8 u8_lastsubstate;                     /*!< previous substate of the state machine                                                 */
    u8 u8_ErrRetryCounter;                  /*!< counts how many times the drivers retried to communicate with LTC in case of a communication error*/
    u8 u8_ErrRequestCounter;
    u8 u8_balance_control_done;   /*!< indicates if balance control was done */
}st_CMU_STATE;

/**
 * This structure contains all the variables relevant for battery pack
 */
typedef struct
{
    s32 s32_current;
    u16 u16_stackVolt;
    u16 u16_cellVolt[14];
    s16 s16_ntcTemp[6];    /* 7 temperature measured values(physical) */
    s16 s16_icTemp;         /* MC33771 IC temperature */
    u8 u8_bccRawMeas[128];  /* Array needed to store all measured values(raw data). */
    u8 u8_bccStatus[64];
}st_pdc_bms_pack;

/********************************************************************************************************************************/
/* Function Prototype Section - GLOBAL                                                                                          */
/********************************************************************************************************************************/
e_bcc_status CMU_Init_Registers(st_bcc_drv_config* const drvConfig);
void CMU_App_Init(void);
e_bcc_status CMU_Init(void);
u16 CMU_CRC16(u8 *pBuf, s32 nLen);
void CMU_WakeUp(void);
void CMU_HW_Rst(void);
s32 CMU_WriteFrame(u8 bID, u16 wAddr, u8 * pData, u8 bLen, u8 bWriteType);
e_bcc_status CMU_Reg_Write(u8 bID, u16 wAddr, u64 dwData, u8 bLen, u8 bWriteType);
s32 CMU_ReadFrameReq(u8 bID, u16 wAddr, u8 bByteToReturn, u8 bWriteType);
e_bcc_status CMU_Reg_Read(u8 bID, u16 wAddr, u8 * pData, u8 bLen,u8 bWriteType);
e_bcc_status CMU_Meas_StartConversion(u8 bID,u8 ADC_Mode_Value);
e_bcc_status CMU_Meas_IsConverting (u8 bID,u8 *completed);
e_bcc_status CMU_Meas_GetRawValues(u8 bID, u8 measurements[]);
e_bcc_status CMU_MaskAllFaults(u8 bID, u8 bWriteType);
e_bcc_status CMU_Get_Fault_Status(u8 bID, u8 bWriteType);
e_bcc_status CMU_Fault_ClearStatus(u8 bID, u8 bWriteType);
e_bcc_status CMU_Cell_Balance_Configuration(void);
e_bcc_status CMU_Enable_Balancing(u8 balancing_state[]);
e_bcc_status CMU_Disable_Cell_Balancing(void);
e_bcc_status CMU_Get_Cell_Balancing_Status(u8 bal_status[]);
e_bcc_status CMU_Get_Cell_Balancing_Remaining_Time(u8 bal_time[]);
e_bcc_status CMU_Soft_Reset(void);
e_bcc_status CMU_Goto_Sleep(void);
e_bcc_status CMU_Goto_Shutdown(void);
e_CMU_STATEMACH CMU_GetStateRequest(void);
void CMU_StateTransition(e_CMU_STATEMACH state, u8 substate,u16 timer_ms,u8 ret_val);
e_bcc_status CMU_ConvertRawData(st_bcc_drv_config *drvConfig, st_pdc_bms_pack *batPack);
void CMU_Process_ADC_Data(void);
u8 CMU_Set_StateRequest(e_CMU_STATEMACH u8_statereq);
u8 CMU_Get_Current_State(void);
extern void CMU_Trigger(void);


#endif	/* CMU_H */
/********************************************************************************************************************************/
/* End of file                                                                                                                  */
/********************************************************************************************************************************/
/**@}*/

