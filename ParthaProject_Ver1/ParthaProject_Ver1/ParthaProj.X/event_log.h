/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name: event logging                                                                                                        */
/* Author:Pavithran                                                                                                                     */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file can_protocol_stack.c
*   @brief This file contains API's which are required to store events in flash memory.
*
*   @details event logging .
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Pavithran
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 15-07-2021
*/

/** @defgroup C
*
* 
*
*/

/**@{*/
/********************************************************************************************************************************/
/* Include Section                                                                                                              */
/********************************************************************************************************************************/
#ifndef EVENT_LOG_H
#define	EVENT_LOG_H
#include "common.h"

/********************************************************************************************************************************/
/* Defines Section - GLOBAL                                                                                                     */
/********************************************************************************************************************************/
#define EVENT_LOG_BUFFER_SIZE 120

/********************************************************************************************************************************/
/* Macro Section - GLOBAL                                                                                                       */
/********************************************************************************************************************************/

/********************************************************************************************************************************/
/* Data Types Section - GLOBAL                                                                                                  */
/********************************************************************************************************************************/
typedef struct
{
    u16 u16_event_id;
    u32 u32_timestamp;
}st_event;

typedef struct
{
    u16 u16_size;
    u8 u8_lock;
    st_event st_event_field[EVENT_LOG_BUFFER_SIZE];
}st_event_log;

/** diagnosis check result (event) */
typedef enum 
{
    EVENT_OK, /*!<event OK */
    EVENT_NOK, /*!<event NOK */
}e_event_log;

/** diagnosis event id*/
typedef enum 
{
	EVENT_LOG_SOC_MAX_LIMIT=0,
	EVENT_LOG_SOC_MIN_LIMIT,
	EVENT_LOG_SOC_DELTA_LIMIT,
	EVENT_LOG_UART_FAILURE_FOR_CMU,
	EVENT_LOG_RTC_WRITE_FAILURE,
	EVENT_LOG_TASK_MON_EXECUTION_TIME_VIOLATION,
	EVENT_LOG_TASK_NOT_EXECUTED_PERIODICALLY,
	EVENT_LOG_ADC_EXT_GENERAL_ERROR ,
	EVENT_LOG_ADC_EXT_INIT ,
	EVENT_LOG_ADC_EXT_START_SHUNT ,
	EVENT_LOG_ADC_EXT_READ_SHUNT,
	EVENT_LOG_ADC_EXT_START_HVPLUS,
	EVENT_LOG_ADC_EXT_READ_HVPLUS,
	EVENT_LOG_CMU_COMPARISON_FAILED,
	EVENT_LOG_BALANCING_OPERATION,
	EVENT_LOG_TEMP_AUX_NO_VALUE_CH01,
	EVENT_LOG_TEMP_AUX_NO_VALUE_CH02,
	EVENT_LOG_TEMP_AUX_NO_VALUE_CH03,
	EVENT_LOG_TEMP_AUX_NO_VALUE_CH04,
	EVENT_LOG_TEMP_AUX_NO_VALUE_CH05,
	EVENT_LOG_TEMP_AUX_NO_VALUE_CH06,
	EVENT_LOG_TEMP_AUX_SHORTED_CH01,
	EVENT_LOG_TEMP_AUX_SHORTED_CH02,
	EVENT_LOG_TEMP_AUX_SHORTED_CH03,
	EVENT_LOG_TEMP_AUX_SHORTED_CH04,
	EVENT_LOG_TEMP_AUX_SHORTED_CH05,
	EVENT_LOG_TEMP_AUX_SHORTED_CH06,
	EVENT_LOG_TEMP_AUX_OPEN_CH01,
	EVENT_LOG_TEMP_AUX_OPEN_CH02,
	EVENT_LOG_TEMP_AUX_OPEN_CH03,
	EVENT_LOG_TEMP_AUX_OPEN_CH04,
	EVENT_LOG_TEMP_AUX_OPEN_CH05,
	EVENT_LOG_TEMP_AUX_OPEN_CH06,
	EVENT_LOG_TEMP_AUX_MIN_CH01,
	EVENT_LOG_TEMP_AUX_MIN_CH02,
	EVENT_LOG_TEMP_AUX_MIN_CH03,
	EVENT_LOG_TEMP_AUX_MIN_CH04,
	EVENT_LOG_TEMP_AUX_MIN_CH05,
	EVENT_LOG_TEMP_AUX_MIN_CH06,
	EVENT_LOG_TEMP_AUX_MAX_CH01,
	EVENT_LOG_TEMP_AUX_MAX_CH02,
	EVENT_LOG_TEMP_AUX_MAX_CH03,
	EVENT_LOG_TEMP_AUX_MAX_CH04,
	EVENT_LOG_TEMP_AUX_MAX_CH05,
	EVENT_LOG_TEMP_AUX_MAX_CH06,
	EVENT_LOG_SYS_LIM_CELL_V_MIN,
	EVENT_LOG_SYS_LIM_CELL_V_MAX,
	EVENT_LOG_SYS_LIM_CELL_T_MIN,
	EVENT_LOG_SYS_LIM_CELL_T_MAX,
	EVENT_LOG_SYS_LIM_PACK_I_IN,
	EVENT_LOG_SYS_LIM_PACK_I_OUT,
	EVENT_LOG_SYS_CELL_V_NO_VALUE,
	EVENT_LOG_SYS_CELL_T_NO_VALUE,
	EVENT_LOG_SYS_CELL_T_SHORTED,
	EVENT_LOG_SYS_CELL_T_OPEN,
	EVENT_LOG_SYS_CMU_PCB_T_NO_VALUE,
	EVENT_LOG_SYS_CMU_PCB_T_SHORTED,
	EVENT_LOG_SYS_CMU_PCB_T_OPEN,
	EVENT_LOG_SYS_PACK_I_MASTER_SELECTION,
	EVENT_LOG_SYS_COMM_CMU,
	EVENT_LOG_SYS_NUM_CMU_MISMATCH,
	EVENT_LOG_SYS_FB_LOAD_NEG_MISSING,
	EVENT_LOG_SYS_FB_LOAD_POS_MISSING,
	EVENT_LOG_SYS_FB_PRECHARGE_MISSING,
	EVENT_LOG_SYS_FB_CHG_NEG_MISSING,
	EVENT_LOG_SYS_FB_LOAD_NEG_WELDED,
	EVENT_LOG_SYS_FB_LOAD_POS_WELDED,
	EVENT_LOG_SYS_FB_PRECHARGE_WELDED,
	EVENT_LOG_SYS_FB_CHG_NEG_WELDED,
	EVENT_LOG_SYS_CONTACTOR_RETRIES,
	EVENT_LOG_SYS_PROCESS_CMU_DATA,
	EVENT_LOG_SYS_PACK_AND_CELL_INIT,
	EVENT_LOG_SYS_PACK_SOC_CALC,
	EVENT_LOG_SYS_EXEC_STATE_LOGIC,
	EVENT_LOG_SYS_PSU_NOT_IN_DIAGNOSTIC_MODE,
	EVENT_LOG_SYS_LIM_CHG_I_OVER_UNDER,
	EVENT_LOG_SYS_LEAK_ADC,
	EVENT_LOG_SYS_LEAK_LOAD,
	EVENT_LOG_SYS_LEAK_CHARGE,
	EVENT_LOG_SYS_LEAK_READY,
	//foxbms
    EVENT_LOG_FLASHCHECKSUM,                          /*  */
    EVENT_LOG_BKPDIAG_FAILURE,                        /*  */
    EVENT_LOG_WATCHDOGRESET_FAILURE,                  /*  */
    EVENT_LOG_POSTOSINIT_FAILURE,                     /*  */
    EVENT_LOG_CALIB_EEPR_FAILURE,                     /*  */
    EVENT_LOG_CAN_INIT_FAILURE,                       /*  */
    EVENT_LOG_VIC_INIT_FAILURE,
    /* HW-/SW-Runtime events: 16-31 */
    EVENT_LOG_DIV_BY_ZERO_FAILURE,                    /*  */
    EVENT_LOG_UNDEF_INSTRUCTION_FAILURE,              /*  */
    EVENT_LOG_DATA_BUS_FAILURE,                       /*  */
    EVENT_LOG_INSTRUCTION_BUS_FAILURE,                /*  */
    EVENT_LOG_HARDFAULT_NOTHANDLED,                   /*  */
    //EVENT_LOG_RUNTIME_ERROR_RESERVED_1,               /*  reserved for future needs */
    //EVENT_LOG_RUNTIME_ERROR_RESERVED_2,               /*  reserved for future needs */
    //EVENT_LOG_RUNTIME_ERROR_RESERVED_3,               /*  reserved for future needs */
    EVENT_LOG_CONFIGASSERT,                           /*  */
    EVENT_LOG_SYSTEMMONITORING_TIMEOUT,               /*  */
    /* Measurement events: 32-47 */
    EVENT_LOG_CANS_MAX_VALUE_VIOLATE,
    EVENT_LOG_CANS_MIN_VALUE_VIOLATE,
    EVENT_LOG_CANS_CAN_MOD_FAILURE,
    EVENT_LOG_ISOMETER_TIM_ERROR,                     /* Measured frequency too low or no new value captured during last cycle */
    EVENT_LOG_ISOMETER_GROUNDERROR,                   /* Ground error detected */
    EVENT_LOG_ISOMETER_ERROR,                         /* Device error, invalid measurement result */
    EVENT_LOG_ISOMETER_MEAS_INVALID,                  /* Measurement trustworthy or not, hysteresis to ground error flag */
    EVENT_LOG_CELLVOLTAGE_OVERVOLTAGE_MSL,            /* Cell voltage limits violated */
    EVENT_LOG_CELLVOLTAGE_OVERVOLTAGE_RSL,            /* Cell voltage limits violated */
    EVENT_LOG_CELLVOLTAGE_OVERVOLTAGE_MOL,            /* Cell voltage limits violated */
    EVENT_LOG_CELLVOLTAGE_UNDERVOLTAGE_MSL,           /* Cell voltage limits violated */
    EVENT_LOG_CELLVOLTAGE_UNDERVOLTAGE_RSL,           /* Cell voltage limits violated */
    EVENT_LOG_CELLVOLTAGE_UNDERVOLTAGE_MOL,           /* Cell voltage limits violated */
    EVENT_LOG_TEMP_OVERTEMPERATURE_CHARGE_MSL,        /* Temperature limits violated */
    EVENT_LOG_TEMP_OVERTEMPERATURE_CHARGE_RSL,        /* Temperature limits violated */
    EVENT_LOG_TEMP_OVERTEMPERATURE_CHARGE_MOL,        /* Temperature limits violated */
    EVENT_LOG_TEMP_OVERTEMPERATURE_DISCHARGE_MSL,     /* Temperature limits violated */
    EVENT_LOG_TEMP_OVERTEMPERATURE_DISCHARGE_RSL,     /* Temperature limits violated */
    EVENT_LOG_TEMP_OVERTEMPERATURE_DISCHARGE_MOL,     /* Temperature limits violated */
    EVENT_LOG_TEMP_UNDERTEMPERATURE_CHARGE_MSL,       /* Temperature limits violated */
    EVENT_LOG_TEMP_UNDERTEMPERATURE_CHARGE_RSL,       /* Temperature limits violated */
    EVENT_LOG_TEMP_UNDERTEMPERATURE_CHARGE_MOL,       /* Temperature limits violated */
    EVENT_LOG_TEMP_UNDERTEMPERATURE_DISCHARGE_MSL,    /* Temperature limits violated */
    EVENT_LOG_TEMP_UNDERTEMPERATURE_DISCHARGE_RSL,    /* Temperature limits violated */
    EVENT_LOG_TEMP_UNDERTEMPERATURE_DISCHARGE_MOL,    /* Temperature limits violated */
    EVENT_LOG_OVERCURRENT_CHARGE_CELL_MSL,            /* Overcurrent */
    EVENT_LOG_OVERCURRENT_CHARGE_CELL_RSL,            /* Overcurrent */
    EVENT_LOG_OVERCURRENT_CHARGE_CELL_MOL,            /* Overcurrent */
    EVENT_LOG_OVERCURRENT_DISCHARGE_CELL_MSL,         /* Overcurrent */
    EVENT_LOG_OVERCURRENT_DISCHARGE_CELL_RSL,         /* Overcurrent */
    EVENT_LOG_OVERCURRENT_DISCHARGE_CELL_MOL,         /* Overcurrent */
    EVENT_LOG_OVERCURRENT_CHARGE_PL0_MSL,             /* Overcurrent */
    EVENT_LOG_OVERCURRENT_CHARGE_PL0_RSL,             /* Overcurrent */
    EVENT_LOG_OVERCURRENT_CHARGE_PL0_MOL,             /* Overcurrent */
    EVENT_LOG_OVERCURRENT_CHARGE_PL1_MSL,             /* Overcurrent */
    EVENT_LOG_OVERCURRENT_CHARGE_PL1_RSL,             /* Overcurrent */
    EVENT_LOG_OVERCURRENT_CHARGE_PL1_MOL,             /* Overcurrent */
    EVENT_LOG_OVERCURRENT_DISCHARGE_PL0_MSL,          /* Overcurrent */
    EVENT_LOG_OVERCURRENT_DISCHARGE_PL0_RSL,          /* Overcurrent */
    EVENT_LOG_OVERCURRENT_DISCHARGE_PL0_MOL,          /* Overcurrent */
    EVENT_LOG_OVERCURRENT_DISCHARGE_PL1_MSL,          /* Overcurrent */
    EVENT_LOG_OVERCURRENT_DISCHARGE_PL1_RSL,          /* Overcurrent */
    EVENT_LOG_OVERCURRENT_DISCHARGE_PL1_MOL,          /* Overcurrent */
    EVENT_LOG_OVERCURRENT_PL_NONE,
    EVENT_LOG_LTC_SPI,                                /* LTC */
    EVENT_LOG_LTC_PEC,                                /* LTC */
    EVENT_LOG_LTC_MUX,                                /* LTC */
    EVENT_LOG_LTC_CONFIG,                             /* LTC */
    /* Communication events: 50-63*/
    EVENT_LOG_CAN_TIMING,  /* error in CAN timing */
    EVENT_LOG_CAN_CC_RESPONDING, /* CAN C-C */
    EVENT_LOG_CURRENT_SENSOR_RESPONDING, /* Current sensor not responding anymore */
    /* Contactor events: 69-77*/
    EVENT_LOG_CONTACTOR_DAMAGED, /* Opening contactor at over current */
    EVENT_LOG_CONTACTOR_OPENING, /* counter for contactor opening */
    EVENT_LOG_CONTACTOR_CLOSING, /* counter for contactor closing */
    EVENT_LOG_CONTACTOR_MAIN_PLUS_FEEDBACK, /* Contactor feedback error */
    EVENT_LOG_CONTACTOR_MAIN_MINUS_FEEDBACK, /* Contactor feedback error */
    EVENT_LOG_CONTACTOR_PRECHARGE_FEEDBACK, /* Contactor feedback error */
    EVENT_LOG_CONTACTOR_CHARGE_MAIN_PLUS_FEEDBACK, /* Contactor feedback error */
    EVENT_LOG_CONTACTOR_CHARGE_MAIN_MINUS_FEEDBACK, /* Contactor feedback error */
    EVENT_LOG_CONTACTOR_CHARGE_PRECHARGE_FEEDBACK, /* Contactor feedback error */
    EVENT_LOG_INTERLOCK_FEEDBACK, /* Interlock feedback error */
    EVENT_LOG_SLAVE_PCB_UNDERTEMPERATURE_MSL,
    EVENT_LOG_SLAVE_PCB_UNDERTEMPERATURE_RSL,
    EVENT_LOG_SLAVE_PCB_UNDERTEMPERATURE_MOL,
    EVENT_LOG_SLAVE_PCB_OVERTEMPERATURE_MSL,
    EVENT_LOG_SLAVE_PCB_OVERTEMPERATURE_RSL,
    EVENT_LOG_SLAVE_PCB_OVERTEMPERATURE_MOL,
    EVENT_LOG_INSULATION_ERROR, /* Insulation error: measured insulation < threshold */
    EVENT_LOG_FUSE_STATE_NORMAL, /* Fuse tripped */
    EVENT_LOG_FUSE_STATE_CHARGE, /* Fuse tripped */
    EVENT_LOG_ERROR_MCU_DIE_TEMPERATURE, /* MCU die temperature */
    EVENT_LOG_LOW_COIN_CELL_VOLTAGE, /* coin cell voltage */
    EVENT_LOG_CRIT_LOW_COIN_CELL_VOLTAGE, /* coin cell voltage */
    EVENT_LOG_OPEN_WIRE, /* open-wire check */
    EVENT_LOG_DEEP_DISCHARGE_DETECTED, /* DoD was detected */
    EVENT_LOG_PLAUSIBILITY_CELL_VOLTAGE, /* plausibility checks */
    EVENT_LOG_PLAUSIBILITY_CELL_TEMP, /* plausibility checks */
    EVENT_LOG_PLAUSIBILITY_PACK_VOLTAGE, /* plausibility checks */
	/*CMU*/
	EVENT_LOG_I_SENSE_OVER_CURRENT_DETECTED,
	EVENT_LOG_I_SENSE_OPEN_LOAD_DETECTED,
	EVENT_LOG_I2C_EEPROM_COM_ERROR_DETECTED,
	EVENT_LOG_COM_LOSS_FAULT_DETECTED,
	EVENT_LOG_VPWR_LOW_VOLTAGE_DETECTED,
	EVENT_LOG_VPWR_HIGH_VOLTAGE_DETECTED,
	EVENT_LOG_COM_ERR_OVR_FLOW,
	EVENT_LOG_RESET_INDICATION,
	EVENT_LOG_LOW_POWER_OSC_ERROR,
	EVENT_LOG_IC_THERMAL_LIMITATION,
	EVENT_LOG_ADC1_A_B_FAULT,
	EVENT_LOG_VANA_UNDER_VOLTAGE_DETECTED,
	EVENT_LOG_VANA_OVER_VOLTAGE_DETECTED,
	EVENT_LOG_VCOM_UNDER_VOLTAGE_DETECTED,
	EVENT_LOG_VCOM_OVER_VOLTAGE_DETECTED,
	EVENT_LOG_OPEN_LOAD_FAULT_DETECTED,
	EVENT_LOG_SHORTED_LOAD_FAULT_DETECTED,
	EVENT_LOG_GPIO_OPEN_FAULT_DETECTED,
	EVENT_LOG_GPIO_SHORTED_FAULT_DETECTED,
	EVENT_LOG_COM_ERROR_DETECTED,
	EVENT_LOG_BCC_INIT_ERROR,
    EVENT_LOG_ID_MAX, /* MAX indicator - do not change */
} e_event_id;

/********************************************************************************************************************************/
/* Function Prototype Section - GLOBAL                                                                                          */
/********************************************************************************************************************************/
void Event_Logging_Init(void);

void Event_Logging(u16 u16_Event_ID, u8 u8_Event_Status, u32 u32_Time_Stamp);

u8 Flash_Write(u32 start_address,u8 *pData,u32 len,u8 write_type);

u8 Flash_Read(u32 start_address,u8 *pData,u32 len,u8 read_type);

#endif	/* EVENT_LOG */
/********************************************************************************************************************************/
/* End of file                                                                                                                  */
/********************************************************************************************************************************/
/**@}*/