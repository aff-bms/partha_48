/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name: state of charge                                                                                                       */
/* Author:Karthik                                                                                                                     */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file soc.c
*   @brief This file contains API's which are required to communicate with CMU via UART.
*
*   @details state of charge.
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Karthik
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 16-07-2021
*/

/** @defgroup C
*
* 
*
*/

#include "common.h"
#include "math.h"
#include "soc.h"

/********************************************************************************************************************************/
/* Variables Section - GLOBAL                                                                                                   */
/********************************************************************************************************************************/
st_soc_kalmanFiltering st_soc_kf;
st_soc_coulombcounting st_soc_cc;
st_soc_open_circuit_voltage st_soc_ocv;

/*Matrix Multiplication Outputs*/
f32 f32_ResultMatrix_2_2[N][N];
f32 f32_ResultMatrix_2_1[2][1];

/*Transpose Matrix output*/
f32 f32_Tranpose_matrix[2][2];

/*Inverse Matrix Output*/
f32 f32_inverse_matrix[2][2];

/*Temperature Value*/
f32 f32_Temperature =0;

u32 u32_time1 =0;

/*Initial voltage, SOC, Current */
f32 f32_initial_voltage,f32_initialSoC,f32_initialCurrent,f32_stateCurrent;

/*Last state Matrixes*/
f32 f32_LastState1[2][1] ;
f32 f32_LastState2[2][1] ;
f32 f32_LastState3[2][1] ;
f32 f32_LastState4[2][1] ;
f32 f32_LastState5[2][1] ;

/*Last Variance Matrixes*/
f32 f32_lastVariance1[2][2] = {{0.5,0},{0,0.5}};
f32 f32_lastVariance2[2][2] = {{0.5,0},{0,0.5}};
f32 f32_lastVariance3[2][2] = {{0.5,0},{0,0.5}};
f32 f32_lastVariance4[2][2] = {{5.0,0},{0,5.0}};
f32 f32_lastVariance5[2][2] = {{5.0,0},{0,5.0}};

/*Observation Matrix*/
f32 f32_observationMatrix[2][2] = {{1,0},{0,1}};

/*Noise Matrixes*/
f32 f32_observationNoise1[2][2] = {{0.25,0},{0,0.25}};
f32 f32_observationNoise2[2][2] = {{0.25,0},{0,0.25}};
f32 f32_observationNoise3[2][2] = {{0.25,0},{0,0.25}};
f32 f32_observationNoise4[2][2] = {{0.20,0},{0,0.20}};
f32 f32_observationNoise5[2][2] = {{0.20,0},{0,0.20}};

/*Estimate state Matrixes*/
f32 f32_stateEstimate1[2][1];
f32 f32_stateEstimate2[2][1];
f32 f32_stateEstimate3[2][1];
f32 f32_stateEstimate4[2][1];
f32 f32_stateEstimate5[2][1];

/*Estimate Variance matrixes*/
f32 f32_varianceEstimate1[2][2];
f32 f32_varianceEstimate2[2][2];
f32 f32_varianceEstimate3[2][2];
f32 f32_varianceEstimate4[2][2];
f32 f32_varianceEstimate5[2][2];

f32 f32_VarianceDummy[2][2]={{0,0},{0,0}};

/*Kalman Gain*/
f32 f32_kalmanGain3[2][2];
f32 f32_kalmanGain4[2][2];
f32 f32_kalmanGain5[2][2];

/*Identity Matrix*/
f32 f32_IdentityMatrix[2][2]={{1,0},{0,1}} ;

/*Measurement Residual*/
f32 f32_measurementResidual3[2][1];
f32 f32_measurementResidual4[2][1];
f32 f32_measurementResidual5[2][1];

/*measurement predicted*/
f32 f32_stateMeasurement[2][1];
f32 f32_measuredVoltage;

/*Residual Variance*/
f32 f32_residualVariance3[2][2];
f32 f32_residualVariance4[2][2];
f32 f32_residualVariance5[2][2];

/*Transformation Matrix*/
f32 f32_TransformationMatrix[2][2];


/** @fn      void SOC_Update_Transformation_Matrix(f32 DeltaTime)
*   @brief    Update the Transformation Matrix
*
*   @details  To get the transformation matrix
*
*   @pre      
*   @post     
*   @param    Delta time
*   @return   return success
*/
void SOC_Update_Transformation_Matrix(f32 DeltaTime)
{
    	f32_TransformationMatrix[0][0] = 1;
		f32_TransformationMatrix[0][1] = -1*(DeltaTime / TOTALCOULOMBS)*100;
		f32_TransformationMatrix[1][0] = 0;
		f32_TransformationMatrix[1][1] = 1;
}


/*
*   @fn      void SOC_Inverse_Matrix(f32 f32_ar[][2])
*   @brief   To get the inverse of a matrix
*
*   @details  To get the inverse matrix
*
*   @pre      
*   @post     
*   @param    Array matrix
*   @return   return success
*/
void SOC_Inverse_Matrix(f32 f32_ar[][2])
{
    f32 f32_determinate = 0;
    f32_determinate = f32_ar[0][0] * f32_ar[1][1] - f32_ar[0][1] * f32_ar[1][0];

    f32_inverse_matrix[0][0] = f32_ar[1][1]/f32_determinate;
    f32_inverse_matrix[0][1] = -f32_ar[0][1]/f32_determinate;
    f32_inverse_matrix[1][0] = -f32_ar[1][0]/f32_determinate;
    f32_inverse_matrix[1][1] = f32_ar[0][0]/f32_determinate;
}


/*
*   @fn      void   SOC_multiply_Matrix_N_N(f32 mat1[][N],f32 mat2[][N])
*   @brief   Multiplication of two matrixes
*
*   @details  To get the multiplied matrix from two matrices
*
*   @pre      
*   @post     
*   @param    Array Matrix1 and Matrix2
*   @return   return success
*/
void   SOC_multiply_Matrix_N_N(f32 mat1[][N],f32 mat2[][N])
{
    u32 u32_i, u32_j, u32_k;
    for (u32_i = 0; u32_i < N; u32_i++) {
        for (u32_j = 0; u32_j < N; u32_j++) {
            f32_ResultMatrix_2_2[u32_i][u32_j] = 0;
            for (u32_k = 0; u32_k < N; u32_k++)
                f32_ResultMatrix_2_2[u32_i][u32_j] += mat1[u32_i][u32_k] * mat2[u32_k][u32_j];
        }
    }
}


/*
*   @fn      void   SOC_multiply_2_1(f32 mat1[][2],f32 mat2[][1])
*   @brief   Multiplication of two matrixes
*
*   @details  To get the multiplied matrix from two matrices
*
*   @pre      
*   @post     
*   @param    Array Matrix1 and Matrix2
*   @return   return success
*/
void   SOC_multiply_2_1(f32 mat1[][2],f32 mat2[][1])
{
    u32 u32_i, u32_j, u32_k;
    for (u32_i = 0; u32_i < 2; u32_i++) {
        for (u32_j = 0; u32_j < 1; u32_j++) {
            f32_ResultMatrix_2_1[u32_i][u32_j] = 0;
            for (u32_k = 0; u32_k < 2; u32_k++)
                f32_ResultMatrix_2_1[u32_i][u32_j] += mat1[u32_i][u32_k] * mat2[u32_k][u32_j];
        }
    }
}


/*
*   @fn      void SOC_Transpose_Matrix(f32 testarr[][2])
*   @brief   Transpose of a matrixes
*
*   @details  To get the Transpose of given matrix 
*
*   @pre      
*   @post     
*   @param    Array Matrix
*   @return   return success
*/
void SOC_Transpose_Matrix(f32 f32_testarr[][2])
{
    f32_Tranpose_matrix[0][0]=f32_testarr[0][0];
    f32_Tranpose_matrix[0][1]=f32_testarr[1][0];
    f32_Tranpose_matrix[1][0]=f32_testarr[0][1];
    f32_Tranpose_matrix[1][1]=f32_testarr[1][1];
}


/*********Coulomb Counting**********/
#ifndef COULOMB_COUNTING

/*
*   @fn      void SOC_Get_SOC_NVRAM()
*   @brief   Get SOC Data from NVRAM
*
*   @details  To get SOC Data from Flash memory
*
*   @pre      
*   @post     
*   @param    Array Matrix
*   @return   return success
*/
void SOC_Get_SOC_NVRAM()
{
    //TODO: Karthik
    //SOC_Struct_st.CalculatedSOC_previous = Read_Data_From_Flash();
}


/*
*   @fn      void SOC_Calculate_CoulumbCounting()
*   @brief   Get SOC Data Using Coulomb Counting
*
*   @details  To get SOC calculated value 
*
*   @pre      
*   @post     
*   @param    
*   @return   return success
*/
void SOC_Calculate_CoulumbCounting()
{
    
    st_soc_cc.f32_CalculatedSOC_new = st_soc_cc.f32_CalculatedSOC_previous + 100.0f*f32_initialCurrent/(3600.0f*(SOC_CELL_CAPACITY/1000.0f));
    st_soc_cc.f32_CalculatedSOC_previous = st_soc_cc.f32_CalculatedSOC_new;
   
}

/*
*   @fn      f32 SOC_CoulombCounting()
*   @brief   State process for Coulomb counting SOC
*
*   @details  State flow for SoC calculation using Coulomb COunting
*
*   @pre      
*   @post     
*   @param    
*   @return   return success
*/
f32 SOC_CoulombCounting()
{
    switch(st_soc_cc.state)
    {
        case SOC_UNINITIALIZED:
            st_soc_cc.state = SOC_INITIALIZED;
            break;
            
        case SOC_INITIALIZED:
             SOC_Get_SOC_NVRAM();
             st_soc_cc.state = SOC_RUNNING;
             break;
             
        case SOC_RUNNING:
             SOC_Calculate_CoulumbCounting();
             st_soc_cc.state = SOC_RUNNING;
             break;
             
        default: break;         
    }    
}
#endif


/******************OCV Method*************************/
#ifndef OCV

/*
*   @fn      f32 SOC_GetSOC_From_OCV_NMC(f32 f32_Voltage, f32 f32_Temperature)
*   @brief   SoC of NMC cells using OCV method
*
*   @details  SoC Calculation Of NMC cells Using OCV Method
*
*   @pre      
*   @post     Voltage and temperature
*   @param    
*   @return   return success
*/
f32 SOC_GetSOC_From_OCV_NMC(f32 f32_Voltage, f32 f32_Temperature)
{
     f32 f32_coefficients_10[7] = {310854.69979222433,310854.69979222433,428064.20716411038,-171357.92358796950,38343.494794014434,-4546.8188106624739,223.23865533552120};
	 f32 f32_coefficients_0[6]  = {-160414.14428496838,227888.71788951423,-128774.67499341925,36178.115460182220,-5053.8107151435215,280.96665315297412};
	 f32 f32_coefficients_25[7] = {330658.90158516075,-592959.36515212653,441050.73945138755,-174127.95740506929,38474.592867814798,-4510.1295288426127,219.11923205574584};
	 f32 f32_coefficients_45[6] = {-158092.98698180442,224217.56494453427,-126528.95791140423,35510.452563935818,-4957.0434547093373,275.48720753914563};
	 f32 f32_coefficients_60[6] = {-205297.93443790337,291493.65296388295,-164763.18993526546,46341.983762158605,-6486.6896472059352,361.63938867619214};

	 f32 f32_OCV_used = f32_Voltage;
	 f32 f32_SOC = 0;

	 u32  u32_i=0;
	 if(f32_OCV_used < 2.9)
	 	f32_OCV_used = 2.9;
	 if(f32_OCV_used  > 4.2)
	 	f32_OCV_used = 4.2;


	 if(f32_Temperature <= -10  )
	 {
		 for( u32_i=0 ; u32_i<7 ; u32_i++)
		 {
		 		f32_SOC += f32_coefficients_10[u32_i] * pow(f32_OCV_used,u32_i);
		 }
	 }

	 else if((f32_Temperature <= 5)  &&  (f32_Temperature > -10)  )
	 {
		 for( u32_i=0 ; u32_i<6 ; u32_i++)
		 {
		 		f32_SOC += f32_coefficients_0[u32_i] * pow(f32_OCV_used,u32_i);
		 }
	 }

	 else if( (f32_Temperature <= 30)  &&  (f32_Temperature > 5)  )
	 {
	 	  for( u32_i=0 ; u32_i<7 ; u32_i++)
	 	  {
	 		 	f32_SOC += f32_coefficients_25[u32_i] * pow(f32_OCV_used,u32_i);
	 	  }
	 }

	 else if( (f32_Temperature <= 50)  &&  (f32_Temperature > 30)  )
	 {
	 	 	for( u32_i=0 ; u32_i<6 ; u32_i++)
	 	 	{
	 	 		 	f32_SOC += f32_coefficients_45[u32_i] * pow(f32_OCV_used,u32_i);
	 	 	}
	 }

	 else
	 {
		 for( u32_i=0 ; u32_i<6 ; u32_i++)
		 {
		 	 	 	f32_SOC += f32_coefficients_60[u32_i] * pow(f32_OCV_used,u32_i);
		 }

	 }
     return f32_SOC;

}

/*
*   @fn      f32 SOC_GetSOC_From_OCV_LFP(f32 f32_Voltage)
*   @brief   SoC of LFP cells using OCV method
*
*   @details  SoC Calculation Of LFP cells Using OCV Method
*
*   @pre      
*   @post     Voltage 
*   @param    
*   @return   return success
*/
f32 SOC_GetSOC_From_OCV_LFP(f32 f32_Voltage)
{
    f32 f32_Coefficients_LFP_Window_1[4] = {  511128.45438905776,-470900.99954073445,144537.03978541063,-14779.632492584342};
    f32 f32_Coefficients_LFP_Window_2[8] = { -5531793.563,6299645.204,18914.07029,104917.549,-656353.7135,33667.42724,81329.47593,-13985.8034172454};
    f32 f32_Coefficients_LFP_Window_3[7] ={  2861435.0628925730,-491919.60976580624,-338276.98785729118,-64783.610831639904,49190.561989417474,2693.3881973953171,-1636.5353269139127};
    f32 f32_Coefficients_LFP_Window_4[2] = {42.536140791960243,15.713387806409491 };
    f32 f32_OCV_used = f32_Voltage;
	f32 f32_SOC = 0;
     
     if(f32_OCV_used < 3.1500)
	 	f32_OCV_used = 3.1500;
	 if(f32_OCV_used  > 3.660)
	 	f32_OCV_used = 3.660;
     u32 u32_incrementor;
     
     if(  (f32_OCV_used >= 3.1500) &&  (f32_OCV_used < 3.2867) )
	 {
		 for( u32_incrementor=0 ; u32_incrementor<4 ; u32_incrementor++)
		 {
		 		f32_SOC += f32_Coefficients_LFP_Window_1[u32_incrementor] * pow(f32_OCV_used,u32_incrementor);
		 }
	 }

     else if(  (f32_OCV_used < 3.3097) &&  (f32_OCV_used >= 3.2867) )
	 {
		 for( u32_incrementor=0 ; u32_incrementor<8 ; u32_incrementor++)
		 {
		 		f32_SOC += f32_Coefficients_LFP_Window_2[u32_incrementor] * pow(f32_OCV_used,u32_incrementor);
		 }
	 }
	 
     else if(  (f32_OCV_used >= 3.3304) &&  (f32_OCV_used <= 3.3388) )
	 {
		 for( u32_incrementor=0 ; u32_incrementor<7 ; u32_incrementor++)
		 {
		 		f32_SOC += f32_Coefficients_LFP_Window_3[u32_incrementor] * pow(f32_OCV_used,u32_incrementor);
		 }
	 }
     else
     {
         for( u32_incrementor=0 ; u32_incrementor<2 ; u32_incrementor++)
		 {
		 		f32_SOC += f32_Coefficients_LFP_Window_4[u32_incrementor] * pow(f32_OCV_used,u32_incrementor);
		 }
     }
     
     return f32_SOC;
}



/*
*   @fn      f32 SOC_Calculate_Using_OCV(f32 f32_f32_measuredVoltage , f32 f32_Temperature)
*   @brief   SoC of  cells using OCV method
*
*   @details  SoC Calculation Of  cells Using OCV Method
*
*   @pre      
*   @post     Voltage and Temperature 
*   @param    
*   @return   return success
*/
f32 SOC_Calculate_Using_OCV(f32 f32_measuredVoltage , f32 f32_Temperature)
{
#ifdef NMC
	SOC_Struct_OCV_st.CalculatedSOC_OCV = GetSOC_From_OCV_NMC(f32_measuredVoltage,f32_Temperature);
#else
    st_soc_ocv.f32_CalculatedSOC_OCV = SOC_GetSOC_From_OCV_LFP(f32_measuredVoltage);
#endif   
    
	return st_soc_ocv.f32_CalculatedSOC_OCV;
}

#endif


/*******************************Kalman Filtering**********************************************/
#ifdef KALMAN_FILTER

/*
*   @fn      void SOC_Initialize_KalmanFilter()
*   @brief   SoC of  cells using Kalman Filtering
*
*   @details  SoC Calculation Of  cells Using Kalman Filtering
*
*   @pre      
*   @post     
*   @param    
*   @return   return success
*/
void SOC_Initialize_KalmanFilter()
{
         f32 f32_voltage ;//= GetVoltagefromCMU();
         f32 f32_Temperature; //GetTemperature();
         f32 f32_Current; //GetCurrent();
         
#ifndef NMC
         f32_initialSoC = SOC_GetSOC_From_OCV_NMC(f32_voltage,f32_Temperature);
#else
         f32_initialSoC = GetSOC_From_OCV_LFP(f32_voltage);
#endif
		
		 f32_initialCurrent =   f32_Current;
		 f32_stateCurrent   =   f32_Current;

		 f32_LastState1[0][0] =  f32_initialSoC ;
		 f32_LastState2[0][0] =  f32_initialSoC ;
		 f32_LastState3[0][0] =  f32_initialSoC ;
		 f32_LastState4[0][0] =  f32_initialSoC ;
		 f32_LastState5[0][0] =  f32_initialSoC ;

		 f32_LastState1[1][0] = f32_initialCurrent ;
		 f32_LastState2[1][0] = f32_initialCurrent ;
		 f32_LastState3[1][0] = f32_initialCurrent ;
		 f32_LastState4[1][0] = f32_initialCurrent ;
		 f32_LastState5[1][0] = f32_initialCurrent ;

		 SOC_Update_Transformation_Matrix(0);
    
}

/**Update estimate5 every 30seconds**/
u32 u32_Time_Interval ;

/*
*   @fn      f32 SOC_Calculate_KalmanFilter()
*   @brief   SoC of  cells using Kalman Filtering
*
*   @details  SoC Calculation Of  cells Using Kalman Filtering
*
*   @pre      
*   @post     
*   @param    
*   @return   return success
*/
f32 SOC_Calculate_KalmanFilter()
{
        /*Run Every one second*/
        SOC_Update_Transformation_Matrix(1);

		/**State estimate 1**/
		SOC_multiply_2_1(f32_TransformationMatrix,f32_LastState1 );
		f32_stateEstimate1[0][0] = f32_ResultMatrix_2_1[0][0];
		f32_stateEstimate1[1][0] = f32_ResultMatrix_2_1[1][0];
		
        
        /**No need of this**/
        /**State estimate 2**/
		f32_stateEstimate2[0][0] = f32_LastState1[0][0];
		f32_stateEstimate2[1][0] = f32_LastState1[1][0];
		
        /**State estimate 3**/
		SOC_multiply_2_1(f32_TransformationMatrix,f32_LastState3);
		f32_stateEstimate3[0][0] = f32_ResultMatrix_2_1[0][0];
		f32_stateEstimate3[1][0] = f32_ResultMatrix_2_1[1][0];
		
        /**State estimate 4**/
		SOC_multiply_2_1(f32_TransformationMatrix,f32_LastState4);
		f32_stateEstimate4[0][0] = f32_ResultMatrix_2_1[0][0];
		f32_stateEstimate4[1][0] = f32_ResultMatrix_2_1[1][0];
 
		if(1)//(IsTimerExpired(Time_Interval,30000))
		{
            //Time_Interval = Timer_1ms;
		    //Update_Transformation_Matrix(newtime- startTime + extratime );
            
			 /**State estimate 5**/
			 SOC_multiply_2_1(f32_TransformationMatrix,f32_LastState5);
		     f32_stateEstimate5[0][0] = f32_ResultMatrix_2_1[0][0];
		     f32_stateEstimate5[1][0] = f32_ResultMatrix_2_1[1][0];

		}
		else
		{
			 f32_stateEstimate5[0][0] = f32_LastState5[0][0];
			 f32_stateEstimate5[1][0] = f32_LastState5[1][0];
		}
  
        
        /*Variance estimate 1*/
        SOC_multiply_Matrix_N_N(f32_TransformationMatrix,f32_lastVariance1);
        f32_VarianceDummy[0][0] = f32_ResultMatrix_2_2[0][0];
		f32_VarianceDummy[0][1] = f32_ResultMatrix_2_2[0][1];
        f32_VarianceDummy[1][0] = f32_ResultMatrix_2_2[1][0];
		f32_VarianceDummy[1][1] = f32_ResultMatrix_2_2[1][1];
		SOC_Inverse_Matrix(f32_TransformationMatrix);
		SOC_multiply_Matrix_N_N(f32_VarianceDummy,f32_inverse_matrix);
		f32_varianceEstimate1[0][0] = f32_ResultMatrix_2_2[0][0];
		f32_varianceEstimate1[0][1] = f32_ResultMatrix_2_2[0][1];
        f32_varianceEstimate1[1][0] = f32_ResultMatrix_2_2[1][0];
		f32_varianceEstimate1[1][1] = f32_ResultMatrix_2_2[1][1];

		/*Variance estimate 3*/
        SOC_multiply_Matrix_N_N(f32_TransformationMatrix,f32_lastVariance3);
        f32_VarianceDummy[0][0] = f32_ResultMatrix_2_2[0][0];
		f32_VarianceDummy[0][1] = f32_ResultMatrix_2_2[0][1];
        f32_VarianceDummy[1][0] = f32_ResultMatrix_2_2[1][0];
		f32_VarianceDummy[1][1] = f32_ResultMatrix_2_2[1][1];
		SOC_multiply_Matrix_N_N(f32_VarianceDummy,f32_inverse_matrix);
		f32_varianceEstimate3[0][0] = f32_ResultMatrix_2_2[0][0];
		f32_varianceEstimate3[0][1] = f32_ResultMatrix_2_2[0][1];
        f32_varianceEstimate3[1][0] = f32_ResultMatrix_2_2[1][0];
		f32_varianceEstimate3[1][1] = f32_ResultMatrix_2_2[1][1];

		/*Variance estimate 4*/
        SOC_multiply_Matrix_N_N(f32_TransformationMatrix,f32_lastVariance4);
        f32_VarianceDummy[0][0] = f32_ResultMatrix_2_2[0][0];
		f32_VarianceDummy[0][1] = f32_ResultMatrix_2_2[0][1];
        f32_VarianceDummy[1][0] = f32_ResultMatrix_2_2[1][0];
		f32_VarianceDummy[1][1] = f32_ResultMatrix_2_2[1][1];
		SOC_multiply_Matrix_N_N(f32_VarianceDummy,f32_inverse_matrix);
		f32_varianceEstimate4[0][0] = f32_ResultMatrix_2_2[0][0];
		f32_varianceEstimate4[0][1] = f32_ResultMatrix_2_2[0][1];
        f32_varianceEstimate4[1][0] = f32_ResultMatrix_2_2[1][0];
		f32_varianceEstimate4[1][1] = f32_ResultMatrix_2_2[1][1];

        /* Only update variance for estimate 5 if it has been 30 seconds*/
        if (1)//(IsTimerExpired(Time_Interval,30000))
		{
		    /*Variance estimate 5*/
             SOC_multiply_Matrix_N_N(f32_TransformationMatrix,f32_lastVariance5);
             f32_VarianceDummy[0][0] = f32_ResultMatrix_2_2[0][0];
	     	 f32_VarianceDummy[0][1] = f32_ResultMatrix_2_2[0][1];
             f32_VarianceDummy[1][0] = f32_ResultMatrix_2_2[1][0];
	       	 f32_VarianceDummy[1][1] = f32_ResultMatrix_2_2[1][1];
	     	 SOC_multiply_Matrix_N_N(f32_VarianceDummy,f32_inverse_matrix);
		     f32_varianceEstimate5[0][0] = f32_ResultMatrix_2_2[0][0];
	     	 f32_varianceEstimate5[0][1] = f32_ResultMatrix_2_2[0][1];
             f32_varianceEstimate5[1][0] = f32_ResultMatrix_2_2[1][0];
		     f32_varianceEstimate5[1][1] = f32_ResultMatrix_2_2[1][1];
		}
        else
		{
		     f32_varianceEstimate5[0][0] = f32_lastVariance5[0][0];
	     	 f32_varianceEstimate5[0][1] = f32_lastVariance5[0][1];
             f32_varianceEstimate5[1][0] = f32_lastVariance5[1][0];
		     f32_varianceEstimate5[1][1] = f32_lastVariance5[1][1];
		}

        //GetCurrent();
        f32_stateEstimate1[1][0] = f32_stateCurrent;
        f32_stateEstimate3[1][0] = f32_stateCurrent;
        f32_stateEstimate4[1][0] = f32_stateCurrent;
        if(1) //Time related Todo:
            f32_stateEstimate5[1][0] = f32_stateCurrent;
        
        
        
        /*Update the last state to current state estimate*/
        f32_LastState1[0][0] = f32_stateEstimate1[0][0];
        f32_LastState3[0][0] = f32_stateEstimate3[0][0];
        f32_LastState4[0][0] = f32_stateEstimate4[0][0];
        f32_LastState5[0][0] = f32_stateEstimate5[0][0];
        f32_LastState1[1][0] = f32_stateEstimate1[1][0];
        f32_LastState3[1][0] = f32_stateEstimate3[1][0];
        f32_LastState4[1][0] = f32_stateEstimate4[1][0];
        f32_LastState5[1][0] = f32_stateEstimate5[1][0];



	    /* Update the last variance to current variance estimate*/
	    memcpy(f32_lastVariance1,f32_varianceEstimate1,sizeof(f32_varianceEstimate1));
        memcpy(f32_lastVariance3,f32_varianceEstimate3,sizeof(f32_varianceEstimate3));
        memcpy(f32_lastVariance4,f32_varianceEstimate4,sizeof(f32_varianceEstimate4));
        memcpy(f32_lastVariance5,f32_varianceEstimate5,sizeof(f32_varianceEstimate5));

        /**Update to Kalman filter structure**/
        st_soc_kf.f32_SOC_Coulomb_Counting = f32_LastState1[0][0];

                
	    u32_time1++;
		if (u32_time1 > 6)//time - ocvStartTime > 600)//ToDO: Time Related
		{
		    u32_time1 = 0;
            f32 f32_voltage ;//= GetVoltagefromCMU();
            f32 f32_Temperature; //GetTemperature();
            f32 f32_Current; //GetCurrent();
            
			f32 measuredSoC; 
            f32 measuredCurrent;
#ifndef NMC
         measuredSoC = SOC_GetSOC_From_OCV_NMC(f32_voltage,f32_Temperature);
#else
         measuredSoC = GetSOC_From_OCV_LFP(f32_voltage);
#endif
			measuredCurrent = measuredCurrent;
			f32_stateMeasurement[0][0] = measuredSoC;
			f32_stateMeasurement[1][0] = measuredCurrent;
			f32_measurementResidual3[0][0] = f32_stateMeasurement[0][0] - f32_stateEstimate3[0][0];
            f32_measurementResidual4[0][0] = f32_stateMeasurement[0][0] - f32_stateEstimate4[0][0];
            f32_measurementResidual5[0][0] = f32_stateMeasurement[0][0] - f32_stateEstimate5[0][0];

            f32_measurementResidual3[1][0] = f32_stateMeasurement[1][0] - f32_stateEstimate3[1][0];
            f32_measurementResidual4[1][0] = f32_stateMeasurement[1][0] - f32_stateEstimate4[1][0];
            f32_measurementResidual5[1][0] = f32_stateMeasurement[1][0] - f32_stateEstimate5[1][0];
            
            /* Calculate the residual variance matrix*/

            f32_residualVariance3[0][0] = f32_varianceEstimate3[0][0] + f32_observationNoise3[0][0];
            f32_residualVariance3[0][1] = f32_varianceEstimate3[0][1] + f32_observationNoise3[0][1];
            f32_residualVariance3[1][0] = f32_varianceEstimate3[1][0] + f32_observationNoise3[1][0];
            f32_residualVariance3[1][1] = f32_varianceEstimate3[1][1] + f32_observationNoise3[1][1];
            f32_residualVariance4[0][0] = f32_varianceEstimate4[0][0] + f32_observationNoise4[0][0];
            f32_residualVariance4[0][1] = f32_varianceEstimate4[0][1] + f32_observationNoise4[0][1];
            f32_residualVariance4[1][0] = f32_varianceEstimate4[1][0] + f32_observationNoise4[1][0];
            f32_residualVariance4[1][1] = f32_varianceEstimate4[1][1] + f32_observationNoise4[1][1];
            f32_residualVariance5[0][0] = f32_varianceEstimate5[0][0] + f32_observationNoise5[0][0];
            f32_residualVariance5[0][1] = f32_varianceEstimate5[0][1] + f32_observationNoise5[0][1];
            f32_residualVariance5[1][0] = f32_varianceEstimate5[1][0] + f32_observationNoise5[1][0];
            f32_residualVariance5[1][1] = f32_varianceEstimate5[1][1] + f32_observationNoise5[1][1];

			/*Calculate inverse of matrix*/
			SOC_Inverse_Matrix(f32_residualVariance3);
			SOC_multiply_Matrix_N_N(f32_varianceEstimate3,f32_inverse_matrix);
			memcpy(f32_kalmanGain3,f32_ResultMatrix_2_2,sizeof(f32_ResultMatrix_2_2));
			SOC_Inverse_Matrix(f32_residualVariance4);
			SOC_multiply_Matrix_N_N(f32_varianceEstimate4,f32_inverse_matrix);
			memcpy(f32_kalmanGain4,f32_ResultMatrix_2_2,sizeof(f32_ResultMatrix_2_2));
			SOC_Inverse_Matrix(f32_residualVariance5);
			SOC_multiply_Matrix_N_N(f32_varianceEstimate5,f32_inverse_matrix);
			memcpy(f32_kalmanGain5,f32_ResultMatrix_2_2,sizeof(f32_ResultMatrix_2_2));

            memcpy(f32_LastState2,f32_stateMeasurement,sizeof(f32_stateMeasurement));
			
            SOC_multiply_2_1(f32_kalmanGain3,(f32_measurementResidual3));
			f32_LastState3[0][0] = f32_stateEstimate3[0][0] + f32_ResultMatrix_2_1[0][0];
			f32_LastState3[1][0] = f32_stateEstimate3[1][0] + f32_ResultMatrix_2_1[1][0];

			SOC_multiply_2_1(f32_kalmanGain4,(f32_measurementResidual4));
			f32_LastState4[0][0] = f32_stateEstimate4[0][0] + f32_ResultMatrix_2_1[0][0];
			f32_LastState4[1][0] = f32_stateEstimate4[1][0] + f32_ResultMatrix_2_1[1][0];

			SOC_multiply_2_1(f32_kalmanGain5,(f32_measurementResidual5));
			f32_LastState5[0][0] = f32_stateEstimate5[0][0] + f32_ResultMatrix_2_1[0][0];
			f32_LastState5[1][0] = f32_stateEstimate5[1][0] + f32_ResultMatrix_2_1[1][0];

            /* Update the variance*/
            f32_VarianceDummy[0][0] = f32_IdentityMatrix[0][0] - f32_kalmanGain3[0][0];
            f32_VarianceDummy[0][1] = f32_IdentityMatrix[0][1] - f32_kalmanGain3[0][1];
            f32_VarianceDummy[1][0] = f32_IdentityMatrix[1][0] - f32_kalmanGain3[1][0];
            f32_VarianceDummy[1][1] = f32_IdentityMatrix[1][1] - f32_kalmanGain3[1][1];
            SOC_multiply_Matrix_N_N(f32_VarianceDummy,(f32_varianceEstimate3));
            memcpy(f32_lastVariance3,f32_ResultMatrix_2_2,sizeof(f32_ResultMatrix_2_2));

            f32_VarianceDummy[0][0] = f32_IdentityMatrix[0][0] - f32_kalmanGain4[0][0];
            f32_VarianceDummy[0][1] = f32_IdentityMatrix[0][1] - f32_kalmanGain4[0][1];
            f32_VarianceDummy[1][0] = f32_IdentityMatrix[1][0] - f32_kalmanGain4[1][0];
            f32_VarianceDummy[1][1] = f32_IdentityMatrix[1][1] - f32_kalmanGain4[1][1];
            SOC_multiply_Matrix_N_N(f32_VarianceDummy,(f32_varianceEstimate4));
            memcpy(f32_lastVariance4,f32_ResultMatrix_2_2,sizeof(f32_ResultMatrix_2_2));

            f32_VarianceDummy[0][0] = f32_IdentityMatrix[0][0] - f32_kalmanGain5[0][0];
            f32_VarianceDummy[0][1] = f32_IdentityMatrix[0][1] - f32_kalmanGain5[0][1];
            f32_VarianceDummy[1][0] = f32_IdentityMatrix[1][0] - f32_kalmanGain5[1][0];
            f32_VarianceDummy[1][1] = f32_IdentityMatrix[1][1] - f32_kalmanGain5[1][1];
            SOC_multiply_Matrix_N_N(f32_VarianceDummy,(f32_varianceEstimate5));
            memcpy(f32_lastVariance5,f32_ResultMatrix_2_2,sizeof(f32_ResultMatrix_2_2));
            
            st_soc_kf.f32_SOC_OCV                  = f32_LastState2[0][0];
            st_soc_kf.f32_SOC_Kalman_Filter_Type_1 = f32_LastState3[0][0];
            st_soc_kf.f32_SOC_Kalman_Filter_Type_2 = f32_LastState4[0][0];
            st_soc_kf.f32_SOC_Kalman_Filter_Type_3 = f32_LastState5[0][0];
  
		}
        
        
}


/*
*   @fn      f32 SOC_CalculateKalmanFilter()
*   @brief   State machine for calculating SoC of  cells using Kalman Filtering
*
*   @details  State machine for calculating  SoC of cells Using Kalman Filtering
*
*   @pre      
*   @post     
*   @param    
*   @return   return success
*/
f32 SOC_CalculateKalmanFilter()
{
    switch(st_soc_kf.state)
    {
        case SOC_KALMAN_FILTER_UNINITIALIZED:
            st_soc_kf.state = SOC_KALMAN_FILTER_INITIALIZED;
            break;
            
        case SOC_KALMAN_FILTER_INITIALIZED:
             SOC_Initialize_KalmanFilter();
             st_soc_kf.state = SOC_KALMAN_FILTER_RUNNING;
             break;
             
        case SOC_KALMAN_FILTER_RUNNING:
             SOC_Calculate_KalmanFilter();
             st_soc_kf.state = SOC_KALMAN_FILTER_RUNNING;
             break;
             
        default: break;
                   
    }
    
}

#endif