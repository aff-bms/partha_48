/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name:can protocol stack                                                                                                         */
/* Author:KARTHIK                                                                                                                     */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file soc.h
*   @brief This file contains structure declaration,variable.
*
*   @details variable type naming conventions.
*
*   @Copyright Copyright 2021 by Affluence Info Systems Ltd.
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Karthik
*   $LastChangedRevision$ | 1.0
*   $LastChangedDate$     | 16-07-2021
*/

/** @defgroup
*
* This file contains structure declaration,variable.
*
*/

/**@{*/
/********************************************************************************************************************************/
/* Include Section                                                                                                              */
/********************************************************************************************************************************/
#ifndef SOC_H
#define	SOC_H

#include "common.h"
#define KALMAN_FILTER

/********************************************************************************************************************************/
/* Global Macros                                                                                                  */
/********************************************************************************************************************************/
#define     N                 2
#define     DeltaT            10
#define     TOTALCOULOMBS     172800
#define     SOC_CELL_CAPACITY 50000

#ifndef COULOMB_COUNTING
typedef enum 
{
    SOC_UNINITIALIZED,
    SOC_INITIALIZED,
    SOC_RUNNING,
}e_SOC_State;

/**
 * This structure contains variables relevant for the SOC module using Coulomb Counting.
 */
typedef struct
{
    e_SOC_State state;
    f32 f32_CalculatedSOC_previous;
    f32 f32_CalculatedSOC_new;
}st_soc_coulombcounting;

#endif

/**
 * This structure contains variables relevant for the SOC module using OCV.
 */
typedef struct
{
    f32 f32_CalculatedSOC_OCV;
}st_soc_open_circuit_voltage;




#ifdef KALMAN_FILTER
typedef enum 
{
    SOC_KALMAN_FILTER_UNINITIALIZED,
    SOC_KALMAN_FILTER_INITIALIZED,
    SOC_KALMAN_FILTER_RUNNING,
}e_SOC_State_Kalman;


/**
 * This structure contains variables relevant for the SOC module using Kalman Filtering
 */
typedef struct
{
    e_SOC_State_Kalman state;
    f32 f32_SOC_Coulomb_Counting;
    f32 f32_SOC_OCV;
    f32 f32_SOC_Kalman_Filter_Type_1; /*Kalman filtering with variance 0.5 and noise 0.25 */
    f32 f32_SOC_Kalman_Filter_Type_2; /*Kalman filtering with variance 5 and noise 0.20 */
    f32 f32_SOC_Kalman_Filter_Type_3; /*Kalman filtering with variance 0.5 and noise 0.25 and time with every 30 seconds */
    
}st_soc_kalmanFiltering;
#endif

/********************************************************************************************************************************/
/* Function prototype section - Global                                                                                                         */
/********************************************************************************************************************************/

void SOC_Update_Transformation_Matrix(f32 DeltaTime);
void SOC_Inverse_Matrix(f32 f32_ar[][2]);
void SOC_multiply_Matrix_N_N(f32 mat1[][N],f32 mat2[][N]);
void SOC_multiply_2_1(f32 mat1[][2],f32 mat2[][1]);
void SOC_Transpose_Matrix(f32 f32_testarr[][2]);
void SOC_Get_SOC_NVRAM();
void SOC_Calculate_CoulumbCounting();
f32 SOC_CoulombCounting();
f32 SOC_GetSOC_From_OCV_NMC(f32 f32_Voltage, f32 f32_Temperature);
f32 SOC_GetSOC_From_OCV_LFP(f32 f32_Voltage);
f32 SOC_Calculate_Using_OCV(f32 f32_measuredVoltage , f32 f32_Temperature);
void SOC_Initialize_KalmanFilter();
f32 SOC_Calculate_KalmanFilter();
f32 SOC_CalculateKalmanFilter();

#endif	/* SOC_H */
/********************************************************************************************************************************/
/* End of file                                                                                                                  */
/********************************************************************************************************************************/
/**@}*/