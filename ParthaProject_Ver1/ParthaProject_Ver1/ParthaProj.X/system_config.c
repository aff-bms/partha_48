/********************************************************************************************************************************/
/*                                     COPYRIGHT (c) 2021                                                                       */
/*                            Affluence Info Systems Pvt Ltd.                                                                   */
/*                                     ALL RIGHTS RESERVED                                                                      */
/*                                                                                                                              */
/* This software is a copyrighted work and/or information protected as a trade secret. Legal rights of                          */
/* Affluence Info Systems Pvt Ltd. in this software is distinct from ownership of any medium in which the software              */
/* is embodied. Copyright or trade secret notices included must be reproduced in any copies authorized by Affluence Info        */
/* Systems Pvt Ltd.                                                                                                             */
/*                                                                                                                              */
/* The information contained herein is confidential property of Company. The user, copying, transfer or disclosure of           */
/* such information is prohibited except by express written agreement with Company.                                             */
/*                                                                                                                              */
/* Module name: System configuration                                                                                                               */
/* Author: Manikanta Pantala                                                                                                             */
/*                                                                                                                              */
/********************************************************************************************************************************/

/** @file system_config.c
*   @brief Description of file
*
*   @details Detail information about file
*
*   @Copyright Copyright 2021 by Systems Pvt Ltd..
*   \n\n
*   SVN                   | Description
*   :-------------------: | :----------------------------------------------:
*   $LastChangedBy$       | Manikanta Pantala.
*   $LastChangedRevision$ | 23-07-2021: 0.1 Initial version 
                          | 28-07-2021: 0.2 Incorporation of review comments by harinath
*   $LastChangedDate$     | 28-07-2021
*/

/** @defgroup SYSTEM CONFIGURATION
*
*  Detail description about module
*
*/

/**@{*/
/********************************************************************************************************************************/
/* Include Section                                                                                                              */
/********************************************************************************************************************************/
#include "system_config.h"

/********************************************************************************************************************************/
/* Defines Section - LOCAL                                                                                                      */
/********************************************************************************************************************************/


/********************************************************************************************************************************/
/* Macro Section - LOCAL                                                                                                        */
/********************************************************************************************************************************/


/********************************************************************************************************************************/
/* Variables/Data Types Section - LOCAL                                                                                         */
/********************************************************************************************************************************/

typedef struct
{
    u32 u32_address;
    u8 u8_tx_buff[length_of_tx_buff];
    u32 u32_number_of_bytes;
}st_sysconfig_read;


typedef struct
{
    u32 u32_address;
    u8 u8_rx_buff[length_of_rx_buff];
    u32 u32_number_of_bytes;
}st_sysconfig_write;


static SYSCONFIG_STATE_s sysconfig_state = 
{
    .statereq                 = SYSCONFIG_STATE_INIT_REQUEST,
    .state                    = SYSCONFIG_STATEMACH_UNINITIALIZED,
    .laststate                = SYSCONFIG_STATEMACH_UNINITIALIZED,
    .lastsubstate             = 0,
    .triggerentry             = 0,
    .ErrRequestCounter        = 0,
    .initFinished             = E_NOT_OK,
    .write                    = E_NOT_OK,
    .read                    = E_NOT_OK,
    
   
};

SYSCONFIG_STATE_REQUEST_e statereq = SYSCONFIG_STATE_INIT_REQUEST;
st_sysconfig_read Read_Data;
st_sysconfig_write Write_Data;
u8 u8_mode;

/********************************************************************************************************************************/
/* Function Implementations Section - GLOBAL                                                                                    */
/********************************************************************************************************************************/

/** @fn       void SysConfig_Init(void)
*   @brief    System configuration will be initialized
*
*   @details  System configuration will be initialized
*
*   @pre      call after system and os init
*   @post     
*   @param    void
*   @return   void
*/

void SysConfig_Init(void)
{   /*State request to be in Initialize*/
    SYSCONFIG_STATE_REQUEST_e statereq = SYSCONFIG_STATE_INIT_REQUEST;        
    Read_Data.u32_address = sysconfig_ReadFlashAddress; /*Flash_address to read data*/
    Read_Data.u32_number_of_bytes = no_of_ReadBytes; /*Number of bytes to read*/
    Write_Data.u32_address = sysconfig_WriteFlashAddress; /*Flash address to write data*/
    Write_Data.u32_number_of_bytes = no_of_WriteBytes; /*Number  of bytes to write*/
}

/** @fn       u8 SysConfig_Write(u8 *u8_buff_source)
*   @brief    This function is used to write the system configuration  data into the flash memory.
*
*   @details  This function is used to write the system configuration  data into the flash memory.
*
*   @pre      
*   @post     
*   @param    Source buffer as a pointer 
*   @return   SUCCESS status
*/

u8 SysConfig_Write(u8 *u8_buff_source)
{
    u32 u32_i;
    u32 u32_buff[2];
    u32 u32_storage_address = Write_Data.u32_address;
    
    FLASH_Unlock(FLASH_UNLOCK_KEY);
    if(FLASH_ErasePage(u32_storage_address))
    {
       for(u32_i=0;u32_i<Write_Data.u32_number_of_bytes;u32_storage_address=u32_storage_address+4)
        {
            u32_buff[0]=0;
            u32_buff[1]=0;
        
            u32_buff[0]|=(u32)(u8_buff_source[u32_i++])<<0;
            u32_buff[0]|=(u32)(u8_buff_source[u32_i++])<<8;
            u32_buff[0]|=(u32)(u8_buff_source[u32_i++])<<16;
        
            u32_buff[1]|=(u32)(u8_buff_source[u32_i++])<<0;
            u32_buff[1]|=(u32)(u8_buff_source[u32_i++])<<8;
            u32_buff[1]|=(u32)(u8_buff_source[u32_i++])<<16;
        
            FLASH_WriteDoubleWord24(u32_storage_address,u32_buff[0],u32_buff[1]);
        }
    }
    FLASH_Lock();
    return SUCCESS;
}

/** @fn       u8 SysConfig_Read(u8 *u8_buff_dest)
*   @brief    This function is used to read the system configuration data from the flash memory.
*
*   @details  This function is used to read the system configuration data from the flash memory.
*
*   @pre      
*   @post     
*   @param    Destination buffer as a pointer 
*   @return   SUCCESS status
*/
u8 SysConfig_Read(u8 *u8_buff_dest)
{
    u32 u32_i=0,u32_value=0;
    u32 u32_storage_address = Read_Data.u32_address;
    
    for(u32_i=0;u32_i<Read_Data.u32_number_of_bytes;u32_storage_address=u32_storage_address+2)
    {
        u32_value=FLASH_ReadWord24(u32_storage_address);
        u8_buff_dest[u32_i++]=(u32_value&0xFF); //extract first byte
        u8_buff_dest[u32_i++]=((u32_value>>8)&0xFF); //extract second byte
        u8_buff_dest[u32_i++]=((u32_value>>16)&0xFF); //extract third byte
    }
    return SUCCESS;
}

#if 0

***(FOR TESTING PURPOSE ONLY)***

/*FUNCTION**********************************************************************
 *
 * Function Name : SysConfig_Main
 * Description   : This function used to initiate system configuration module in state machines
 *
 *END**************************************************************************/


void SysConfig_Main(void)
{
    switch (sysconfig_state.state) 
    {
        /****************************UNINITIALIZED***********************************/
        case SYSCONFIG_STATEMACH_UNINITIALIZED:
            /* waiting for Initialization Request */
            if (statereq == SYSCONFIG_STATE_INIT_REQUEST) 
            {
                sysconfig_state.state = SYSCONFIG_STATEMACH_INITIALIZATION;
            } 
            else if (statereq == SYSCONFIG_STATE_NO_REQUEST) 
            {
                /* no actual request pending */
            } 
            else 
            {
                sysconfig_state.ErrRequestCounter++;   /* illegal request pending */
            }
            break;


        /****************************INITIALIZATION**********************************/
        case SYSCONFIG_STATEMACH_INITIALIZATION:
            /*Initialization of System Configuration*/   
            SysConfig_Init();
            sysconfig_state.state = SYSCONFIG_STATEMACH_INITIALIZED;
            printf("PRESS 'c' to enter config mode or 'e' to exit config mode\n");
            break;

        /****************************INITIALIZED*************************************/
        case SYSCONFIG_STATEMACH_INITIALIZED:
            /*Initialized System Configuration*/
            sysconfig_state.initFinished = E_OK;           
            while(1)
            {
                /*Waiting for command from PC tool*/
                if(UART1_IsRxReady())
                {
                    u8_mode = UART1_Read();                    
                    /*If received command is 'e', it will go to exit mode*/
                    if(u8_mode == 'e')
                    {
                        sysconfig_state.state = SYSCONFIG_STATEMACH_EXIT_CONFIGMODE;
                        break;
                    }                    
                    /*If received command is 'c', it will go to configuration mode*/
                    else if(u8_mode == 'c')      
                    {
                        sysconfig_state.state = SYSCONFIG_STATEMACH_CHECK_CONFIGMODE;
                        printf("PRESS 'w' to enter write mode or 'r' to read mode\n");
                        break;
                    }
                    else
                    {
                        /* no actual request pending */
                    }
                }           
            }
            break;
        
        case SYSCONFIG_STATEMACH_CHECK_CONFIGMODE:
            /*Waiting for command from PC tool*/
            while(1)
            {
             if(UART1_IsRxReady())
             {
                u8_mode = UART1_Read();
                /*If received command is 'w', it will go to write mode*/
                if(u8_mode == 'w')
                {
                    sysconfig_state.state = SYSCONFIG_STATEMACH_WRITE;
                    printf("Write mode\n");
                    break;
                }
                /*If received command is 'r', it will go to read mode*/
                else if(u8_mode == 'r')
                {
                    sysconfig_state.state = SYSCONFIG_STATEMACH_READ;
                    printf("Read mode\n");
                    break;
                }
                else
                {
                    /* no actual request pending */
                }
             }
            }
            break;
            
        case SYSCONFIG_STATEMACH_WRITE:
            
            printf("Enter data\n");
            /*receives data from PC tool and write into rx_buff */
            UART_Receive_Buffer(1,Write_Data.u8_rx_buff,10);
                        /*Writes the rx_buff data into flash memory*/
            if(SysConfig_Write(0xc000,Write_Data.u8_rx_buff,10)==1)
            {
                sysconfig_state.state = SYSCONFIG_STATEMACH_EXIT_CONFIGMODE;
                printf("Write Successfully\n");
            }
            break;
            
        case SYSCONFIG_STATEMACH_READ:
            /*Read data from flash memory and write into tx_buff*/
            if(SysConfig_Read(0xc000,Read_Data.u8_tx_buff,10))
            {
                sysconfig_state.state = SYSCONFIG_STATEMACH_EXIT_CONFIGMODE;
                
                /*read data from tx_buff and sends to PC */
                UART_Send_Buffer(1,Read_Data.u8_tx_buff,10);
                printf("\nRead successfully");
            }
            else
            {
             /* no actual request pending */  
            }
            break;
            
        case SYSCONFIG_STATEMACH_EXIT_CONFIGMODE:
            {   
                /*Sending back to checking config mode*/
                sysconfig_state.state = SYSCONFIG_STATEMACH_CHECK_CONFIGMODE;  
                printf("Exit");
            }
 
        default:
            break;
    } /*  end switch (SYSCONFIG_state.state) */

    
}
#endif


/********************************************************************************************************************************/
/* End of file                                                                                                                  */
/********************************************************************************************************************************/
/**@}*/
